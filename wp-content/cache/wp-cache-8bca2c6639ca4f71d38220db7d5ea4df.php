<?php die(); ?>



<!DOCTYPE html>

<!--[if lt IE 9]>             <html class="no-js ie lt-ie9" lang="en-US" prefix="og: http://ogp.me/ns#""><![endif]-->

<!--[if IE 9]>                <html class="no-js ie ie9" lang="en-US" prefix="og: http://ogp.me/ns#">   <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js no-ie" lang="en-US" prefix="og: http://ogp.me/ns#">    <!--<![endif]-->

	<head>

		<meta charset="UTF-8" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />

		<!--[if lt IE 9]>

			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

			<script src="http://switchandshift.com/wp-content/themes/time/data/js/selectivizr.min.js"></script>

		<![endif]-->

		<title>Are You Just a Leader or a Just Leader?</title>
		<title>Are You Just a Leader or a Just Leader?</title>
<meta property="og:site_name" content="Switch &amp; Shift" /><meta property="og:title" content="Switch &amp; Shift" /><meta property="og:locale" content="en_US" /><meta property="og:url" content="http://switchandshift.com/are-you-just-a-leader-or-a-just-leader" /><meta property="og:description" content="Being a leader requires taking the right road, not the easy road. Treating our people fairly requires judgment, subjectivity, and clear communication of expectations and goals on an ongoing basis since the world around us changes all the time. [...]" /><meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2014/09/just-a-leader-just-leader.png" />
<!-- This site is optimized with the Yoast SEO plugin v2.3.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="robots" content="noindex,follow"/>
<meta name="description" content="Being a leader requires taking the right road, not the easy road. Treating our people fairly requires judgment, subjectivity, and clear communication of expectations and goals on an ongoing basis since the world around us changes all the time. When we treat our people equally but not fairly, we tell people it’s ok to underperform and under contribute undermining the morale of our dedicated and passionate people and are then surprised when we get mediocre output and outcomes."/>
<link rel="canonical" href="http://switchandshift.com/are-you-just-a-leader-or-a-just-leader" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Are You Just a Leader or a Just Leader?" />
<meta property="og:description" content="Being a leader requires taking the right road, not the easy road. Treating our people fairly requires judgment, subjectivity, and clear communication of expectations and goals on an ongoing basis since the world around us changes all the time. When we treat our people equally but not fairly, we tell people it’s ok to underperform and under contribute undermining the morale of our dedicated and passionate people and are then surprised when we get mediocre output and outcomes." />
<meta property="og:url" content="http://switchandshift.com/are-you-just-a-leader-or-a-just-leader" />
<meta property="og:site_name" content="Switch &amp; Shift" />
<meta property="article:tag" content="Business Ethics" />
<meta property="article:tag" content="Ethics" />
<meta property="article:tag" content="Leadership" />
<meta property="article:section" content="Featured" />
<meta property="article:published_time" content="2014-03-19T07:00:12+00:00" />
<meta property="article:modified_time" content="2014-09-12T15:16:02+00:00" />
<meta property="og:updated_time" content="2014-09-12T15:16:02+00:00" />
<meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2014/09/just-a-leader-just-leader.png" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="Being a leader requires taking the right road, not the easy road. Treating our people fairly requires judgment, subjectivity, and clear communication of expectations and goals on an ongoing basis since the world around us changes all the time. When we treat our people equally but not fairly, we tell people it’s ok to underperform and under contribute undermining the morale of our dedicated and passionate people and are then surprised when we get mediocre output and outcomes."/>
<meta name="twitter:title" content="Are You Just a Leader or a Just Leader?"/>
<meta name="twitter:domain" content="Switch &amp; Shift"/>
<meta name="twitter:image" content="http://switchandshift.com/wp-content/uploads/2014/09/just-a-leader-just-leader.png"/>
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Feed" href="http://switchandshift.com/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Comments Feed" href="http://switchandshift.com/comments/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Are You Just a Leader or a Just Leader? Comments Feed" href="http://switchandshift.com/are-you-just-a-leader-or-a-just-leader/feed" />
<!-- This site is powered by Shareaholic - https://shareaholic.com -->
<script type='text/javascript' data-cfasync='false'>
  //<![CDATA[
    _SHR_SETTINGS = {"endpoints":{"local_recs_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_permalink_related","share_counts_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_share_counts_api"}};
  //]]>
</script>
<script type='text/javascript'
        src='//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js'
        data-shr-siteid='22c416cd99bd31d22f03d82d3a8395e6'
        data-cfasync='false'
        async='async' >
</script>

<!-- Shareaholic Content Tags -->
<meta name='shareaholic:site_name' content='Switch &amp; Shift' />
<meta name='shareaholic:language' content='en-US' />
<meta name='shareaholic:url' content='http://switchandshift.com/are-you-just-a-leader-or-a-just-leader' />
<meta name='shareaholic:keywords' content='business ethics, ethics, leadership, leadership blog, human business, 21st century leadership, deborah mills scofield, featured' />
<meta name='shareaholic:article_published_time' content='2014-03-19T14:00:12+00:00' />
<meta name='shareaholic:article_modified_time' content='2015-11-29T11:00:38+00:00' />
<meta name='shareaholic:shareable_page' content='true' />
<meta name='shareaholic:article_author_name' content='Deborah Mills-Scofield' />
<meta name='shareaholic:site_id' content='22c416cd99bd31d22f03d82d3a8395e6' />
<meta name='shareaholic:wp_version' content='7.6.2.3' />
<meta name='shareaholic:image' content='http://switchandshift.com/wp-content/uploads/2014/09/just-a-leader-just-leader.png' />
<!-- Shareaholic Content Tags End -->
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/switchandshift.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=1d05468803804e8153cb17eab60a20d0"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='flick-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp//css/flick/flick.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimpSF_main_css-css'  href='http://switchandshift.com/?mcsf_action=main_css&#038;ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!--[if IE]>
<link rel='stylesheet' id='mailchimpSF_ie_css-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp/css/ie.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='wild-googlemap-frontend-css'  href='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-css-frontend.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='cptch_stylesheet-css'  href='http://switchandshift.com/wp-content/plugins/captcha/css/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='tm_clicktotweet-css'  href='http://switchandshift.com/wp-content/plugins/click-to-tweet-by-todaymade/assets/css/styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='se-link-styles-css'  href='http://switchandshift.com/wp-content/plugins/search-everything/static/css/se-styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://switchandshift.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-3rd-party-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/3rd-party.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-style-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/style.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-scheme-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/bright.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-stylesheet-css'  href='http://switchandshift.com/wp-content/themes/time_child/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-mobile-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/mobile.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='only screen and (max-width: 767px)' />
<link rel='stylesheet' id='author-avatars-widget-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/widget.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='author-avatars-shortcode-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/shortcode.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!-- This site uses the Google Analytics by Yoast plugin v5.4.6 - Universal disabled - https://yoast.com/wordpress/plugins/google-analytics/ -->
<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-27884223-1']);
	_gaq.push(['_gat._forceSSL']);
	_gaq.push(['_trackPageview']);

	(function () {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();

</script>
<!-- / Google Analytics by Yoast -->
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/scrollTo.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.form.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mailchimpSF = {"ajax_url":"http:\/\/switchandshift.com\/"};
/* ]]> */
</script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/mailchimp.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/ui/core.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp//js/datepicker.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?sensor=false&#038;ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js-frontend.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://switchandshift.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://switchandshift.com/wp-includes/wlwmanifest.xml" /> 

<link rel='shortlink' href='http://switchandshift.com/?p=15399' />
<meta name='robots' content='noindex,follow' />
<!-- Custom Code Start-->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '811956965544989']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=811956965544989&ev=PixelInitialized" /></noscript>
<!-- Custom Code Start-->
	<script type="text/javascript">
		jQuery(function($) {
			$('.date-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd/yyyy';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: true,
					beforeShow: function(input, inst) { $('#ui-datepicker-div').addClass('show'); },
					dateFormat: format.toLowerCase(),
				});
			});
			d = new Date();
			$('.birthdate-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: false,
					minDate: new Date(d.getFullYear(), 1-1, 1),
					maxDate: new Date(d.getFullYear(), 12-1, 31),
					beforeShow: function(input, inst) { $('#ui-datepicker-div').removeClass('show'); },
					dateFormat: format.toLowerCase(),
				});

			});

		});
	</script>
	<script type="text/javascript">
	window._se_plugin_version = '8.1.3';
</script>
<style>a, a.alt:hover, .alt a:hover, #bottom a.alt:hover, #bottom .alt a:hover, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, input[type="button"].active, button.active, .button.active, .color, .super-tabs > div > .nav h2 span, .toggles > div > h3:hover > i, .logo, nav a:hover, #bottom nav a:hover, nav .current > a, nav .current>a:hover{color:#0292b7}mark, .slider .control-nav li a:hover, .slider .control-nav li a.active, #top:before, #top > .before, .background-color, nav.mobile a:hover, nav.mobile .current > a, .mejs-controls .mejs-time-rail .mejs-time-loaded, .mejs-controls .mejs-time-rail .mejs-time-current{background-color:#0292b7}.zoom-hover>.zoom-hover-overlay{background-color:rgba(2, 146, 183, 0.75)}blockquote.bar, .sticky:before, #bottom .outer-container{border-color:#0292b7}body,input,select,textarea,button,.button{font-family:Raleway;font-size:13px;line-height:23px}nav.primary ul, nav.primary a:not(:hover), nav.mobile ul, nav.mobile a:not(:hover){font-family:Raleway;font-size:15px;font-weight:bold;font-style:normal;text-decoration:none}nav.secondary ul, nav.secondary a:not(:hover){font-family:Raleway;font-size:12px;font-weight:bold;font-style:normal;text-decoration:none}.headline h1{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}.headline .breadcrumbs{font-family:Raleway;font-size:15px;font-weight:normal;font-style:normal;text-decoration:none}#top .widget>.title{font:normal 18px/128% Raleway;text-decoration:none}.post h1.title{font:bold 22px/128% Raleway;text-decoration:none}h1{font:bold 22px/128% Raleway;text-decoration:none}h2{font:bold 18px/128% Raleway;text-decoration:none}h3{font:bold 14px/128% Raleway;text-decoration:none}h4{font:bold 14px/128% Raleway;text-decoration:none}input[type="submit"]:not(.big):not(.huge),input[type="reset"]:not(.big):not(.huge),input[type="button"]:not(.big):not(.huge),button:not(.big):not(.huge),.button:not(.big):not(.huge){font-family:Raleway;font-size:14px;font-weight:normal;font-style:normal;text-decoration:none}input[type="submit"].big,input[type="reset"].big,input[type="button"].big,button.big,.button.big{font-family:Raleway;font-size:18px;font-weight:bold;font-style:normal;text-decoration:none}input[type="submit"].huge,input[type="reset"].huge,input[type="button"].huge,button.huge,.button.huge{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}</style>
<script>if(typeof WebFont!='undefined'){WebFont.load({google:{families:["Raleway:400,700:latin"]},active:function(){if(document.createEvent){var e=document.createEvent('HTMLEvents');e.initEvent('webfontactive',true,false);document.dispatchEvent(e);}else{document.documentElement['webfontactive']++;}}});}timeConfig={templatePath:'http://switchandshift.com/wp-content/themes/time',zoomHoverIcons:{"default":"icon-plus-circled","image":"icon-search","mail":"icon-mail","title":"icon-right"},flexsliderOptions:{"animation":"slide","direction":"horizontal","animationSpeed":600,"slideshowSpeed":7000,"slideshow":false},layersliderOptions:{}};(function($){$(document).ready(function($){$('.widget_pages, .widget_archive, .widget_categories, .widget_recent_entries, .widget_recent_comments, .widget_display_forums, .widget_display_replies, .widget_display_topics, .widget_display_views').each(function(){$('ul',this).addClass('fancy alt');$('li',this).prepend($('<i />',{'class':'icon-right-open'}));if($(this).closest('#top').length>0){$('i',this).addClass('color');}});$('#disqus_thread').addClass('section');});})(jQuery);</script>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//switchandshift.com/?wordfence_logHuman=1&hid=AC467FCDA496310CAB68649629E30183');
</script><link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250-48x48.png" sizes="32x32" />
<link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">
<meta name="msapplication-TileImage" content="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">


<!-- Google Code for Remarketing Tag -->
<!-
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 954219818;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/954219818/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


	</head>



	<body class="time-child-3-6-1-child-1-0 single single-post postid-15399 single-format-standard layout-boxed scheme-bright">



		<div id="top" >



			
	<div class="backgrounds"><div style="background: url(&quot;http://switchandshift.com/wp-content/uploads/2014/09/just-a-leader-just-leader.png&quot;) #ffffff no-repeat center top fixed; background-size: cover;" class="stripes"></div></div>



			<div class="upper-container ">



				<div class="outer-container">



					
						<nav id="menu" class="mobile">

							<ul id="menu-main-menu" class=""><li id="menu-item-3819" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-4305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li id="menu-item-4304" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li id="menu-item-4303" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li id="menu-item-19075" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li id="menu-item-19071" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li id="menu-item-4306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li id="menu-item-22648" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li id="menu-item-4177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li id="menu-item-21828" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li id="menu-item-23151" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
</ul>
						</nav>

					


					
						<nav id="search" class="mobile">

							<form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form>
						</nav>

					


				</div>



				<div class="outer-container ">



					<header class="header">



						<div class="container">



							<div class="mobile-helper vertical-align">



								
									<a href="#menu" class="button" title="Menu"><i class="icon-menu"></i></a>

								






								
									<a href="#search" class="button" title="Search"><i class="icon-search"></i></a>

								


							</div>



							


							<h1 class="logo vertical-align"><a href="http://switchandshift.com/" title="Switch &amp; Shift" rel="home"><img src="http://switchandshift.com/wp-content/uploads/2014/07/logo.jpg" width="244" height="50" data-2x="http://switchandshift.com/wp-content/uploads/2014/07/logo-2x.jpg" alt="Switch &amp; Shift" /></a></h1>


							
								<nav class="primary vertical-align">

									<ul id="menu-main-menu-1" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
<li><form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form></li></ul>
								</nav>

							


						</div>



					</header>



					


				</div>



			</div>



			

			

<div class="outer-container">
	
	<nav class="secondary">
		<div class="container">
			<div class="menu-secondary-menu-container"><ul id="menu-secondary-menu" class="menu"><li id="menu-item-19073" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19073"><a href="http://switchandshift.com/work-that-matters-podcast">Work That Matters Podcast</a></li>
<li id="menu-item-21566" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21566"><a href="http://switchandshift.com/speaking-under-construction">Speaking</a></li>
<li id="menu-item-4334" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4334"><a href="http://switchandshift.com/consulting">Consulting</a></li>
<li id="menu-item-4331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4331"><a href="http://switchandshift.com/a-world-gone-social">A World Gone Social</a></li>
<li id="menu-item-20424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20424"><a href="http://switchandshift.com/writing-guidelines">Writing Guidelines</a></li>
<li id="menu-item-19074" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19074"><a href="http://switchandshift.com/contact">Contact Us</a></li>
</ul></div>    				</div>
	</nav>

	<div class="content"><div class="container"><div class="main alpha" style="padding: 0 240px 0 0px; margin: 0 -240px 0 -0px;">


		


			<section class="section">

				<article id="post-15399" class="post post-15399 type-post status-publish format-standard has-post-thumbnail hentry category-featured tag-business-ethics tag-ethics tag-leadership-2">
<img width="700" height="300" src="http://switchandshift.com/wp-content/uploads/2014/09/just-a-leader-just-leader.png" class="attachment-700 wp-post-image" alt="just a leader just leader" 0="" data-2x="http://switchandshift.com/wp-content/uploads/2014/09/just-a-leader-just-leader.png" />					

	<h1 class="title entry-title">Are You Just a Leader or a Just Leader?</h1>

                    

					<div class='shareaholic-canvas' data-app-id='21423060' data-app='share_buttons' data-title='Are You Just a Leader or a Just Leader?' data-link='http://switchandshift.com/are-you-just-a-leader-or-a-just-leader' data-summary='Being a leader requires taking the right road, not the easy road. Treating our people fairly requires judgment, subjectivity, and clear communication of expectations and goals on an ongoing basis since the world around us changes all the time. When we treat our people equally but not fairly, we tell people it’s ok to underperform and under contribute undermining the morale of our dedicated and passionate people and are then surprised when we get mediocre output and outcomes.'></div><p>There are so many important traits in making a great leader – character, integrity, honesty, authenticity, vulnerability, trustworthiness, conviction, vision, communication and others I’m sure you can name.  Let’s talk about communication.  It’s not just the right words in the right tone; grammar plays a role.  Where you place certain words has a big implication on what is important which impacts the culture.  So let me ask you – are you Just a Leader or a Just Leader?</p>
<p>It’s just a trivial part of speech, just an ‘a’, no big deal.  But it is! How often do we combine the words justice and leadership, especially in the for-profit sector?  Obviously it’s a big deal in social enterprises; they focus on ‘social justice’. But justice has a huge impact on any organization’s ethos and culture.</p>
<p>Justice comes from the old French <i>justitia</i> meaning righteousness and equity as well as the Latin <i>justus</i> meaning upright.   So how can we apply this <a href="http://smartblogs.com/leadership/2013/02/07/why-leadership-requires-prudence-and-temperance/" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://smartblogs.com/leadership/2013/02/07/why-leadership-requires-prudence-and-temperance/', 'virtue']);" >virtue</a> in a practical, applicable way as leaders? There are three ways I can think of, and I bet if you try, you can think of more.  I’ll address two: <b>Fair versus Equal</b> and <b>I versus You</b>.  The third, Triple Bottom Line/Corporate Social Responsibility, is better known and discussed, so we will leave that for later.</p>
<h3><span style="color: #0091ba;"><b>Fair versus Equal</b><br />
</span></h3>
<p>Many of us have just been through end of the year or are preparing for mid-year performance management.  This is usually not a fun time to be a leader &#8211; not all the news is good, requiring honest, forthright discussions that rarely happen.  For many of our people, it’s all about the raise or bonus, not ways to professionally grow.  That’s why many companies treat their people equally – it’s easier! We don’t need those hard, open, straightforward discussions about real performance and contribution.  We just pay everyone at this level this much and move on.  It’s more objective and clear – just like everyone getting a medal for showing up.</p>
<p>Being a leader requires taking the right road, not the easy road.  Treating our people fairly requires judgment, subjectivity, and clear communication of expectations and goals on an ongoing basis since the world around us changes all the time. When we treat our people equally but not fairly, we tell people it’s ok to underperform and under contribute undermining the morale of our dedicated and passionate people and are then surprised when we get mediocre <a href="http://blogs.hbr.org/cs/2012/11/its_not_just_semantics_managing_outcomes.html" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://blogs.hbr.org/cs/2012/11/its_not_just_semantics_managing_outcomes.html', 'output and outcomes']);" >output and outcomes</a>.</p>
<p>What if we modify the culture to recognize people fairly, based on their work, effort, passion, and results – as individuals and teams?  We will be surprised to see the positive difference it will make.</p>
<h3><span style="color: #0091ba;"><b>I versus You</b></span></h3>
<p>The current economic crisis may have exacerbated an extant corporate behavior, climbing the corporate ladder and competing for promotions.  But what have we really accomplished? We may have the wonderful corner office, but at whose expense and with what impact on results?   I often ask my corporate colleagues if focusing on ‘I’, on themselves, has really gotten them the career satisfaction they sought.  As leaders, we need to help our people focus on the “You” – the customer, the recipient of our services and products and you the employee.  If we honestly ask ourselves who matters more, ‘I’, ourselves or ‘You’ our customers and people, what is our answer?</p>
<p>A true leader is a servant who leads.  So, is the business about our needs or the needs of ‘others’?  Are we really focused on delighting our customers (to quote my friend <a href="http://www.forbes.com/sites/stevedenning/" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.forbes.com/sites/stevedenning/', 'Steve Denning']);" >Steve Denning</a>), which means we will delight our people because they are working on meaningful, purposeful solutions to real needs (outcomes) that result revenues and profit (outputs) that can be reinvested in the delighting our customers? Or, are we doing this for the next perk, the accolades from our peers, the prestige from our position?   I’m not suggesting total altruism (though that’s not a bad idea!), but I am suggesting we ponder why we’re leading and whom we’re leading – is it about ‘I’ or about ‘You’?  Can we really lead if it’s about us? Would we want to be led by someone who was all about himself? Does our leadership truly reflect our why and who? If someone asked one of our people who mattered to us, ‘I’ or ‘You’, what would they answer?</p>
<p>As we approach the middle of 2013, ask yourself two questions: do you treat people equally or fairly  (or both) and does your leadership, hence your culture, value ‘You’ over ‘I’?</p>
<p>Image credit: <a href="http://www.123rf.com/photo_8172992_statue-to-education-justice-and-equality-in-budapest.html" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.123rf.com/photo_8172992_statue-to-education-justice-and-equality-in-budapest.html', 'griangraf / 123RF Stock Photo']);" >griangraf / 123RF Stock Photo</a></p>
<div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/deb-scofield" title="All posts by Deborah Mills-Scofield" rel="author">Deborah Mills-Scofield</a></h3><div class="bio-gravatar"><img src="http://1.gravatar.com/avatar/d5ceb1b1edf8c8615bd50ca0622982df?s=70&#038;d=http%3A%2F%2Fswitchandshift.com%2Fwp-content%2Fuploads%2F2014%2F10%2Fs-circle-logo-250x250-96x96.png&#038;r=g" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://www.mills-scofield.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="http://www.linkedin.com/groups/MillsScofields-Main-e-Points-2589106?gid=2589106&#038;trk=hb_side_g" class="bio-icon bio-icon-linkedin"></a><p class="bio-description">Deborah Mills-Scofield is a partner at Glengary LLC, an early stage venture capital firm in Cleveland, OH, and an innovation and strategy consultant. Her patent from AT&amp;T Bell Labs was one of the highest-revenue generating patents ever for AT&amp;T &amp; Lucent.</p></div>
					<div class="clear"></div>

				</article>

			</section>



			
			

			
	<section class="section">		<ul class="meta alt">
			<li class="published updated"><a href="http://switchandshift.com/2014/03" title="View all posts from March"><i class="icon-clock"></i>March 19, 2014</a></li><li><i class="icon-comment"></i><a href="http://switchandshift.com/are-you-just-a-leader-or-a-just-leader#comments"><span class="dsq-postid" data-dsqidentifier="15399 http://switchandshift.com/?p=10399">18 Comments</span></a></li><li><i class="icon-list"></i><a href="http://switchandshift.com/category/featured" rel="category tag">Featured</a></li>		</ul>
	</section>
			
<div id="disqus_thread">
            <div id="dsq-content">


            <ul id="dsq-comments">
                    <li class="comment even thread-even depth-1" id="dsq-comment-13704">
        <div id="dsq-comment-header-13704" class="dsq-comment-header">
            <cite id="dsq-cite-13704">
http://www.thindifference.com/                <span id="dsq-author-user-13704">Jon M</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13704" class="dsq-comment-body">
            <div id="dsq-comment-message-13704" class="dsq-comment-message"><p>Well said, Deborah! And, great questions to think through in how we work with others and serve as leaders. The servant leadership way is a key one and it is one that I am excited to see really take hold in Millennial leaders. Thanks for your great insights and challenge! Jon</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="post pingback">
        <p>Pingback: <a href='http://transformationalstrategist.com/the-friday-five-blogs-that-matter-march-21-2014/' rel='external nofollow' class='url'>The Friday Five, Blogs That Matter - March 21, 2014 | The Transformational Leadership Strategist</a>()</p>
    </li>
    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-13732">
        <div id="dsq-comment-header-13732" class="dsq-comment-header">
            <cite id="dsq-cite-13732">
                <span id="dsq-author-user-13732">Derek Harris</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13732" class="dsq-comment-body">
            <div id="dsq-comment-message-13732" class="dsq-comment-message"><p>Great article, but you have mixed a couple of different concepts that really need to be addressed separately. Fairness among employees has nothing to do with providing good customer service. At a previous employer, management focused on treating everyone equally, which meant that it really wasn&#8217;t fair. The medical assistants in patient care areas couldn&#8217;t have snacks at their desks, so people working in the back office weren&#8217;t allowed to either. But providing the most value possible to the customer is the way for a business to thrive. If providing the customer with the most value is not the goal of every employee, the business will struggle.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-13742">
        <div id="dsq-comment-header-13742" class="dsq-comment-header">
            <cite id="dsq-cite-13742">
http://batman-news.com                <span id="dsq-author-user-13742">ed fletcher-Wells</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13742" class="dsq-comment-body">
            <div id="dsq-comment-message-13742" class="dsq-comment-message"><p>Thank you for sharing this Deborah. As I start the climb into senior management, articles like yours are really inspiring and reinforce my own belief in style and approach. The obstructions I face do involve the fairness/equality debate. I have experienced a decade long, frustrating journey in middle management where everybody receives the same incentives regardless of contribution. This has lead to insubordination and unfortunate contract terminations of key hard working individuals over the years. They felt neglected as their value was ignored as their lesser deserving colleagues received the same perks and bonuses. To the company&#8217;s defense the incentives were good and frequent, but this miss-management and lack of effort of proper processing makes it extremely hard to motivate people. Rather than raising an employee production standard, it demoralized the good worker to reduce their output. This in turn, has led to underperformance investigations, and more feelings of unfairness. I still haven&#8217;t achieved the step up into senior managment to correct these blind decisions, but the company has supplied management training for me to proceed to the next level. However, do you have any tips or suggestions on how to manage up as well as down when it comes to changes of the way we think towards staff. I feel my next step maybe a hard one when it comes to attempting to install new ways of thinking to the board rather than implementing to the workforce. Once again, Thank you for sharing. Ed</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="post pingback">
        <p>Pingback: <a href='http://www.benschersten.com/blog/2014/03/are-you-just-a-teacher-or-a-just-teacher/' rel='external nofollow' class='url'>Are You Just a Teacher or a Just Teacher? | Another Way</a>()</p>
    </li>
    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-13796">
        <div id="dsq-comment-header-13796" class="dsq-comment-header">
            <cite id="dsq-cite-13796">
http://www.bensimonton.com/                <span id="dsq-author-user-13796">Ben Simonton</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13796" class="dsq-comment-body">
            <div id="dsq-comment-message-13796" class="dsq-comment-message"><p>Great article Deborah, great!</p>
<p>Fair v equal is all about values. Fairness is a value we all value and the higher the standard the better we like it because we all believe in the same good values and that their opposites are bad.</p>
<p>Equal is not a value. Anything that turns out equal as concerns people is most likely quite unfair and leads to bad outcomes.</p>
<p>Leadership is simply the transmission of value standards to employees who then use those standards as how to do their work and treat their customers, each other, and their bosses. Treating people equally implies that they are all equal and everyone knows that is not true. It may be great for lawyers, but it will most certainly lead people to be demotivated, demoralized, and disengaged.</p>
<p>Great article.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="post pingback">
        <p>Pingback: <a href='http://momandpoptoday.com/leadership-news-can-use-04112014/' rel='external nofollow' class='url'>Leadership News You Can Use 04/11/2014 | Mom and Pop Today</a>()</p>
    </li>
    </li><!-- #comment-## -->
    <li class="post pingback">
        <p>Pingback: <a href='http://xiaobreynold.wordpress.com/2014/04/14/just-leader/' rel='external nofollow' class='url'>Just Leader | xiaobreynold</a>()</p>
    </li>
    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-15257">
        <div id="dsq-comment-header-15257" class="dsq-comment-header">
            <cite id="dsq-cite-15257">
                <span id="dsq-author-user-15257">Shannon Steene</span>
            </cite>
        </div>
        <div id="dsq-comment-body-15257" class="dsq-comment-body">
            <div id="dsq-comment-message-15257" class="dsq-comment-message"><p>This sends my mind whirling.  I work in the nonprofit sector with a wide array of leaders.  CEOs really wrestle with the Fair Vs Equal you reference.  I like your framing on it.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-13731">
        <div id="dsq-comment-header-13731" class="dsq-comment-header">
            <cite id="dsq-cite-13731">
http://www.mills-scofield.com/                <span id="dsq-author-user-13731">Deb Mills-Scofield</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13731" class="dsq-comment-body">
            <div id="dsq-comment-message-13731" class="dsq-comment-message"><p>Thank you, Jon &#8211; it is inspiring to see millennials take hold of servant leadership and hopefully be role models for their parents!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-13733">
        <div id="dsq-comment-header-13733" class="dsq-comment-header">
            <cite id="dsq-cite-13733">
http://www.mills-scofield.com/                <span id="dsq-author-user-13733">Deb Mills-Scofield</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13733" class="dsq-comment-body">
            <div id="dsq-comment-message-13733" class="dsq-comment-message"><p>Derek &#8211; Fairness among employees and customer service are different things.  Help me understand how the article led you to that conclusion so I can address it. The only way for a business to survive and thrive is to delight their customers &#8211; and delighted customers are one of the many ways to make it a goal of employees &#8211; not the only, but one.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-13793">
        <div id="dsq-comment-header-13793" class="dsq-comment-header">
            <cite id="dsq-cite-13793">
http://www.bensimonton.com/                <span id="dsq-author-user-13793">Ben Simonton</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13793" class="dsq-comment-body">
            <div id="dsq-comment-message-13793" class="dsq-comment-message"><p>Derek, Fairness among employees has everything to do with customer service. Treating employees equally and not fairly demotivates, demoralizes, and disengages them because they know they are not valued or respected. They will as a result treat their customers with the same level of disrespect. They have been &#8220;led&#8221; by management to do so.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-13743">
        <div id="dsq-comment-header-13743" class="dsq-comment-header">
            <cite id="dsq-cite-13743">
http://www.mills-scofield.com/                <span id="dsq-author-user-13743">Deb Mills-Scofield</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13743" class="dsq-comment-body">
            <div id="dsq-comment-message-13743" class="dsq-comment-message"><p>Ed &#8211; thank you so much for sharing your thoughts.  That&#8217;s a key problem of &#8216;equal&#8217; &#8211; it&#8217;s demoralizing (it&#8217;s socialism) and people start wondering why bother.  I will think more about the managing up part which is critical, but for now, put on your anthropologist hat and think of what the key drivers, incentives, issues are for your upper management &#8211; what worries them, what do they need to do to get ahead, meet their objectives from their bosses, have an impact etc. Then, as you think of things that need to be changed in terms of how the organization treats its staff, put them in the context of what is best for your management &#8211; for them in their individual professional careers and for the organization&#8230; how can doing &#8220;X&#8221; help them (e.g., look better to their bosses, reduce conflict, make them more productive, increase revenue, increase profit).  I don&#8217;t mean to be so crass about it but sometimes you have to.  Think of your upper management as the customer, discover their issues and needs and then how best you can help make their jobs easier, better and help them look good AND help how the org treats its people&#8230;. does that help?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-13745">
        <div id="dsq-comment-header-13745" class="dsq-comment-header">
            <cite id="dsq-cite-13745">
http://batman-news.com                <span id="dsq-author-user-13745">ed fletcher-Wells</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13745" class="dsq-comment-body">
            <div id="dsq-comment-message-13745" class="dsq-comment-message"><p>That&#8217;s excellent, really helpful. One thing I have to improve on myself is management &#8216;speak&#8217; as sometimes I feel on occasion my meaning is lost in my words, this is because I don&#8217;t have a managerial vocabulary when trying to express different methods. Many Thanks for your reply, it is really appreciated.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-13746">
        <div id="dsq-comment-header-13746" class="dsq-comment-header">
            <cite id="dsq-cite-13746">
http://www.mills-scofield.com/                <span id="dsq-author-user-13746">Deb Mills-Scofield</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13746" class="dsq-comment-body">
            <div id="dsq-comment-message-13746" class="dsq-comment-message"><p>I&#8217;ve got mixed feelings about &#8216;management speak&#8217; because that&#8217;s part of the problem &#8211; no one uses plain old english and says thing like they are &#8211; we use fancy words to obscure meaning &#8212; but learning the language of the culture you&#8217;re working in/with/want to transform is very useful :)</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-13750">
        <div id="dsq-comment-header-13750" class="dsq-comment-header">
            <cite id="dsq-cite-13750">
                <span id="dsq-author-user-13750">S. Bell</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13750" class="dsq-comment-body">
            <div id="dsq-comment-message-13750" class="dsq-comment-message"><p>As an administrator dealing with parent and student issues, I would sometimes be asked if the discipline was &#8220;fair&#8221; or was I treating everyone the same.  I would respond that I am &#8216;consistently fair&#8217; about dealing with issues.  I learned from my own children, that each one was unique and fair is not the same as equal.  Fair is what is right for that student.  Your article hit a definite chord with how we treat all people &#8211; from the youngest to the oldest.  Thanks!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-13751">
        <div id="dsq-comment-header-13751" class="dsq-comment-header">
            <cite id="dsq-cite-13751">
http://www.mills-scofield.com/                <span id="dsq-author-user-13751">Deb Mills-Scofield</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13751" class="dsq-comment-body">
            <div id="dsq-comment-message-13751" class="dsq-comment-message"><p>What a terrific example &#8211; how many of us remember saying to our parents and then hearing from our kids &#8220;but it&#8217;s not fair!!!&#8221;  And we know that what worked yesterday may not work tomorrow with the same child yet alone different children&#8230;. thank you!</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-13794">
        <div id="dsq-comment-header-13794" class="dsq-comment-header">
            <cite id="dsq-cite-13794">
http://www.bensimonton.com/                <span id="dsq-author-user-13794">Ben Simonton</span>
            </cite>
        </div>
        <div id="dsq-comment-body-13794" class="dsq-comment-body">
            <div id="dsq-comment-message-13794" class="dsq-comment-message"><p>Great observation! Children are no less people than adults are people. How you bring up a highly motivated, highly committed, and fully engaged child is the same as creating a workforce of highly motivated, etc employees &#8211; &#8220;helping them do their best&#8221; in the words of Admiral Chester W. Nimitz of WWII fame when explaining what leadership is.</p>
</div>
        </div>

    </li><!-- #comment-## -->
            </ul>


        </div>

    </div>

<script type="text/javascript">
var disqus_url = 'http://switchandshift.com/are-you-just-a-leader-or-a-just-leader';
var disqus_identifier = '15399 http://switchandshift.com/?p=10399';
var disqus_container_id = 'disqus_thread';
var disqus_shortname = 'switchandshift2';
var disqus_title = "Are You Just a Leader or a Just Leader?";
var disqus_config_custom = window.disqus_config;
var disqus_config = function () {
    /*
    All currently supported events:
    onReady: fires when everything is ready,
    onNewComment: fires when a new comment is posted,
    onIdentify: fires when user is authenticated
    */
    
    
    this.language = '';
        this.callbacks.onReady.push(function () {

        // sync comments in the background so we don't block the page
        var script = document.createElement('script');
        script.async = true;
        script.src = '?cf_action=sync_comments&post_id=15399';

        var firstScript = document.getElementsByTagName('script')[0];
        firstScript.parentNode.insertBefore(script, firstScript);
    });
    
    if (disqus_config_custom) {
        disqus_config_custom.call(this);
    }
};

(function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript';
    dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>




		


	</div><aside class="aside beta" style="width: 240px;"><section id="wpb_widget-2" class="section widget widget_wpb_widget"><h2 class="title">About the Author</h2><div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/deb-scofield" title="All posts by Deborah Mills-Scofield" rel="author">Deborah Mills-Scofield</a></h3><div class="bio-gravatar"><img src="http://1.gravatar.com/avatar/d5ceb1b1edf8c8615bd50ca0622982df?s=70&#038;d=http%3A%2F%2Fswitchandshift.com%2Fwp-content%2Fuploads%2F2014%2F10%2Fs-circle-logo-250x250-96x96.png&#038;r=g" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://www.mills-scofield.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="http://www.linkedin.com/groups/MillsScofields-Main-e-Points-2589106?gid=2589106&#038;trk=hb_side_g" class="bio-icon bio-icon-linkedin"></a><p class="bio-description">Deborah Mills-Scofield is a partner at Glengary LLC, an early stage venture capital firm in Cleveland, OH, and an innovation and strategy consultant. Her patent from AT&amp;T Bell Labs was one of the highest-revenue generating patents ever for AT&amp;T &amp; Lucent.</p></div></section><section id="time-child-unwrapped-text-7" class="section widget widget-unwrapped-text"><h2 class="title">Subscribe</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</section><section id="adwidget_imagewidget-5" class="section widget AdWidget_ImageWidget"><a target="_blank" href='http://www.inc.com/author/shawn-murphy' alt='Ad'><img  src='http://switchandshift.com/wp-content/uploads/2015/10/Inc-Ad.png' alt='Ad' /></a></section>		<section id="recent-posts-3" class="section widget widget_recent_entries">		<h2 class="title">Recent Articles</h2>		<ul>
					<li>
				<a href="http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community">You Can&#8217;t Do This Alone: Time to Build a Community</a>
						</li>
					<li>
				<a href="http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce">6 Ways to Awaken the Potential in Your Workforce</a>
						</li>
					<li>
				<a href="http://switchandshift.com/our-most-cherished-thanksgiving-traditions">Our Most Cherished Thanksgiving Traditions</a>
						</li>
					<li>
				<a href="http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world">How Gratitude Changes the Way You See the World</a>
						</li>
					<li>
				<a href="http://switchandshift.com/are-you-ready-for-the-digital-workplace">Are You Ready for the Digital Workplace?</a>
						</li>
					<li>
				<a href="http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership">A Necessary Redefinition of Responsibility and Leadership</a>
						</li>
				</ul>
		</section><section id="categories-3" class="section widget widget_categories"><h2 class="title">Articles by Category</h2><label class="screen-reader-text" for="cat">Articles by Category</label><select name='cat' id='cat' class='postform' >
	<option value='-1'>Select Category</option>
	<option class="level-0" value="2196">#SocialLeader</option>
	<option class="level-0" value="588">21st Century Customer Service</option>
	<option class="level-0" value="2454">Being Present</option>
	<option class="level-0" value="32">Blog</option>
	<option class="level-0" value="1126">Bold</option>
	<option class="level-0" value="495">Books</option>
	<option class="level-0" value="1211">Brand Engage</option>
	<option class="level-0" value="104">Business</option>
	<option class="level-0" value="305">Business Heretic&#8217;s Bookstore</option>
	<option class="level-0" value="2508">Change Management</option>
	<option class="level-0" value="1268">Communication</option>
	<option class="level-0" value="654">Company Culture</option>
	<option class="level-0" value="496">Culture</option>
	<option class="level-0" value="2270">Culture Change</option>
	<option class="level-0" value="336">Customer Service</option>
	<option class="level-0" value="1787">Editor&#8217;s Choice</option>
	<option class="level-0" value="2433">Employee Engagement</option>
	<option class="level-0" value="432">Engagement</option>
	<option class="level-0" value="497">Featured</option>
	<option class="level-0" value="1427">Feed Your Brain</option>
	<option class="level-0" value="482">Future Of Leadership</option>
	<option class="level-0" value="1732">Home Page</option>
	<option class="level-0" value="490">Hr</option>
	<option class="level-0" value="2519">Human workplace</option>
	<option class="level-0" value="1385">Humanbiz Community Carnival</option>
	<option class="level-0" value="105">Inspirational</option>
	<option class="level-0" value="103">Leadership</option>
	<option class="level-0" value="1269">Leadership Blog</option>
	<option class="level-0" value="1806">Leadership Presence</option>
	<option class="level-0" value="792">Mentoring</option>
	<option class="level-0" value="1795">Millennials</option>
	<option class="level-0" value="1465">Mini Vlogs</option>
	<option class="level-0" value="1831">My Story Campaign</option>
	<option class="level-0" value="694">New Leadership In The New Economy: Diversity Matters</option>
	<option class="level-0" value="913">Pivot Point</option>
	<option class="level-0" value="2430">Purpose</option>
	<option class="level-0" value="1428">Rebel Heretics Community Feed</option>
	<option class="level-0" value="2395">Rebellious Leaders</option>
	<option class="level-0" value="2391">Rebellious Leadership</option>
	<option class="level-0" value="609">Recognition</option>
	<option class="level-0" value="261">Return On Morale</option>
	<option class="level-0" value="1680">Return On Trust</option>
	<option class="level-0" value="262">Social Era</option>
	<option class="level-0" value="2197">Social Leadership</option>
	<option class="level-0" value="152">Social Media</option>
	<option class="level-0" value="208">Social You</option>
	<option class="level-0" value="841">Strategy</option>
	<option class="level-0" value="493">Strengths</option>
	<option class="level-0" value="796">Switch &Amp; Shift Tv</option>
	<option class="level-0" value="344">Talent</option>
	<option class="level-0" value="1132">The Human Side Tv</option>
	<option class="level-0" value="1061">The Values Revolution</option>
	<option class="level-0" value="363">Transparency</option>
	<option class="level-0" value="101">Uncategorized</option>
	<option class="level-0" value="1125">Vision Cast</option>
	<option class="level-0" value="153">Weekend Post</option>
	<option class="level-0" value="483">Winning Through Engagement</option>
	<option class="level-0" value="1015">Work That Matters Podcast</option>
	<option class="level-0" value="1520">Workplace Morale</option>
	<option class="level-0" value="494">Workplace Optimism</option>
	<option class="level-0" value="180">You: Reinvented</option>
</select>

<script type='text/javascript'>
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "cat" );
	function onCatChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
			location.href = "http://switchandshift.com/?cat=" + dropdown.options[ dropdown.selectedIndex ].value;
		}
	}
	dropdown.onchange = onCatChange;
})();
/* ]]> */
</script>

</section><section id="archives-3" class="section widget widget_archive"><h2 class="title">Archives</h2>		<label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
		<select id="archives-dropdown-3" name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
			
			<option value="">Select Month</option>
				<option value='http://switchandshift.com/2015/11'> November 2015 </option>
	<option value='http://switchandshift.com/2015/10'> October 2015 </option>
	<option value='http://switchandshift.com/2015/09'> September 2015 </option>
	<option value='http://switchandshift.com/2015/08'> August 2015 </option>
	<option value='http://switchandshift.com/2015/07'> July 2015 </option>
	<option value='http://switchandshift.com/2015/06'> June 2015 </option>
	<option value='http://switchandshift.com/2015/05'> May 2015 </option>
	<option value='http://switchandshift.com/2015/04'> April 2015 </option>
	<option value='http://switchandshift.com/2015/03'> March 2015 </option>
	<option value='http://switchandshift.com/2015/02'> February 2015 </option>
	<option value='http://switchandshift.com/2015/01'> January 2015 </option>
	<option value='http://switchandshift.com/2014/12'> December 2014 </option>
	<option value='http://switchandshift.com/2014/11'> November 2014 </option>
	<option value='http://switchandshift.com/2014/10'> October 2014 </option>
	<option value='http://switchandshift.com/2014/09'> September 2014 </option>
	<option value='http://switchandshift.com/2014/08'> August 2014 </option>
	<option value='http://switchandshift.com/2014/07'> July 2014 </option>
	<option value='http://switchandshift.com/2014/06'> June 2014 </option>
	<option value='http://switchandshift.com/2014/05'> May 2014 </option>
	<option value='http://switchandshift.com/2014/04'> April 2014 </option>
	<option value='http://switchandshift.com/2014/03'> March 2014 </option>
	<option value='http://switchandshift.com/2014/02'> February 2014 </option>
	<option value='http://switchandshift.com/2014/01'> January 2014 </option>
	<option value='http://switchandshift.com/2013/12'> December 2013 </option>
	<option value='http://switchandshift.com/2013/11'> November 2013 </option>
	<option value='http://switchandshift.com/2013/10'> October 2013 </option>
	<option value='http://switchandshift.com/2013/09'> September 2013 </option>
	<option value='http://switchandshift.com/2013/08'> August 2013 </option>
	<option value='http://switchandshift.com/2013/07'> July 2013 </option>
	<option value='http://switchandshift.com/2013/06'> June 2013 </option>
	<option value='http://switchandshift.com/2013/05'> May 2013 </option>
	<option value='http://switchandshift.com/2013/04'> April 2013 </option>
	<option value='http://switchandshift.com/2013/03'> March 2013 </option>
	<option value='http://switchandshift.com/2013/02'> February 2013 </option>
	<option value='http://switchandshift.com/2013/01'> January 2013 </option>
	<option value='http://switchandshift.com/2012/12'> December 2012 </option>
	<option value='http://switchandshift.com/2012/11'> November 2012 </option>
	<option value='http://switchandshift.com/2012/10'> October 2012 </option>
	<option value='http://switchandshift.com/2012/09'> September 2012 </option>
	<option value='http://switchandshift.com/2012/08'> August 2012 </option>
	<option value='http://switchandshift.com/2012/07'> July 2012 </option>
	<option value='http://switchandshift.com/2012/06'> June 2012 </option>
	<option value='http://switchandshift.com/2012/05'> May 2012 </option>
	<option value='http://switchandshift.com/2012/04'> April 2012 </option>
	<option value='http://switchandshift.com/2012/03'> March 2012 </option>
	<option value='http://switchandshift.com/2012/02'> February 2012 </option>
	<option value='http://switchandshift.com/2012/01'> January 2012 </option>

		</select>
</section><section id="text-10" class="section widget widget_text">			<div class="textwidget"><iframe src="http://widgets.itunes.apple.com/widget.html?c=us&brc=FFFFFF&blc=FFFFFF&trc=FFFFFF&tlc=FFFFFF&d=Podcast shows we recommend.&t=Get more leadership content&m=podcast&e=podcast&w=220&h=400&ids=741665948,351616584,435836905,647826736,794030859,596047499&wt=playlist&partnerId=&affiliate_id=&at=&ct=" frameborder=0 style="overflow-x:hidden;overflow-y:hidden;width:220px;height: 400px;border:0px; margin-left: -8px;"></iframe></div>
		</section><section id="text-11" class="section widget widget_text"><h2 class="title">Recognition</h2>			<div class="textwidget"><a href="http://servetolead.org/best-21st-century-leadership-blogs-new-media/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://servetolead.org/best-21st-century-leadership-blogs-new-media/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/s2l-badge-21st-century-126x126pr.jpg" style="margin-bottom: 10px;">
</a>

<a href="http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Career-Rocketeer-Career-Blogs-Official-Badge-2014.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.nevermindthemanager.com" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.nevermindthemanager.com', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Leblin-Badge.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.cmoe.com/top-shared-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.cmoe.com/top-shared-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/CMOE-BADGE-2.001.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.skipprichard.com/top-lists/top-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.skipprichard.com/top-lists/top-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/07/top-leadership-blog-award-300x300.png" style="margin-bottom: 10px;">
</a></div>
		</section></aside></div></div>

</div>




		</div>

		<div id="bottom">

			
<div class="outer-container">

	
		<div class="container">

			<section class="section">

				<div class="columns alt-mobile">
					<ul>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-2" class="widget widget-unwrapped-text"><span><img class="alignnone size-full wp-image-3817" src="http://switchandshift.com/wp-content/uploads/2014/08/SS_white_logo500.png" alt="footer-logo" width="250" height="63" />

<p>There’s a more human way to do business.</p>

<p>In the Social Age, it’s how we engage with customers, collaborators and strategic partners that matters; it’s how we create workplace optimism that sets us apart; it’s how we recruit, retain (and repel) employees that becomes our differentiator. This isn’t a “people first, profits second” movement, but a “profits as a direct result of putting people first” movement.</p>
</span></div>							</li>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-4" class="widget widget-unwrapped-text"><h2 class="title">Connect</h2><hr></div><div id="time-child-social-media-3" class="widget widget-social-media"><div class="social-icons native-colors"><ul class="alt"><li><a href="https://twitter.com/switchandshift" target="_blank"><i class="icon-twitter"></i></a></li><li><a href="https://plus.google.com/+Switchandshift1/posts" target="_blank"><i class="icon-gplus"></i></a></li><li><a href="https://www.facebook.com/switchandshift" target="_blank"><i class="icon-facebook"></i></a></li></ul></div></div><div id="time-child-unwrapped-text-3" class="widget widget-unwrapped-text"><hr>
<p>email:<a href="mailto:charmian@switchandshift.com"> connect@switch&shift.com</a> <br/>
1133 Ferreto Parkway<br/>Dayton, NV 89403<br/>
</p>
<hr>
<p>
<a href="http://switchandshift.com/terms-and-conditions/">Terms & Conditions</a> &nbsp;|&nbsp; <a href="http://switchandshift.com/privacy-policy/">Privacy Policy</a>
</p></div><div id="time-child-unwrapped-text-5" class="widget widget-unwrapped-text"><h2 class="title">Newsletter Subscription</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</div>							</li>
													<li class="col-1-3">
								<div id="time-child-contact-2" class="widget widget-contact"><h2 class="title">Contact Us</h2><form class="contact-form" action="http://switchandshift.com/wp-admin/admin-ajax.php" method="post"><input name="action" id="action" type="hidden" value="time-child_contact_form" /><p><input type="text" name="name" placeholder="name*" /></p><p><input type="text" name="email" placeholder="e-mail*" /></p><p><input type="text" name="subject" placeholder="subject" /></p><p><textarea class="full-width" name="message"></textarea></p><p><input name="cntctfrm_contact_action" id="cntctfrm-contact-action" type="hidden" value="true" /><input type="hidden" name="cptch_result" value="4Gk=" />
				<input type="hidden" name="cptch_time" value="1448822427" />
				<input type="hidden" value="Version: 4.1.5" /><label class="cptch_label" for="cptch_input_50"><input id="cptch_input_50" class="cptch_input" type="text" autocomplete="off" name="cptch_number" value="" maxlength="1" size="1" /> &#43; 9 =  &#101;&#105;g&#104;tee&#110;</label></p><p><input type="submit" value="Send&nbsp;&rsaquo;" /><i class="icon-arrows-ccw load"></i><span class="msg small"></span></p></form></div>							</li>
											</ul>
				</div><!-- // .columns -->

			</section>

		</div><!-- // .container -->

	
</div><!-- // .outer-container -->
							<footer class="footer">

					<div class="container">

						<div class="section">
							<p class="small alpha">&copy; Copyright 2015 Switch &amp; Shift</p>
							<p class="small beta">Website Developed by <a href="http:/solutionsred.com" target="blank">Solutions Red</a></p>
						</div>

					</div>

				</footer>
			
		</div>

		        <script type="text/javascript">
        // <![CDATA[
        var disqus_shortname = 'switchandshift2';
        (function () {
            var nodes = document.getElementsByTagName('span');
            for (var i = 0, url; i < nodes.length; i++) {
                if (nodes[i].className.indexOf('dsq-postid') != -1) {
                    nodes[i].parentNode.setAttribute('data-disqus-identifier', nodes[i].getAttribute('data-dsqidentifier'));
                    url = nodes[i].parentNode.href.split('#', 1);
                    if (url.length == 1) { url = url[0]; }
                    else { url = url[1]; }
                    nodes[i].parentNode.href = url + '#disqus_thread';
                }
            }
            var s = document.createElement('script'); 
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
        // ]]>
        </script>
        <link rel='stylesheet' id='author-bio-box-styles-css'  href='http://switchandshift.com/wp-content/plugins/author-bio-box/assets/css/author-bio-box.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/comment-reply.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/3rd-party.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/time.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>

	</body>

</html>
<!-- Dynamic page generated in 1.134 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:40:29 -->
