<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	
	>
<channel>
	<title>Comments for Switch &amp; Shift</title>
	<atom:link href="http://switchandshift.com/comments/feed" rel="self" type="application/rss+xml" />
	<link>http://switchandshift.com</link>
	<description>Human Side of Business</description>
	<lastBuildDate>Sun, 29 Nov 2015 15:25:00 +0000</lastBuildDate>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	
	<item>
		<title>Comment on How Gratitude Changes the Way You See the World by Skip Prichard</title>
		<link>http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world#comment-17400</link>
		<dc:creator><![CDATA[Skip Prichard]]></dc:creator>
		<pubDate>Sun, 29 Nov 2015 15:25:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25783#comment-17400</guid>
		<description><![CDATA[Love this infographic and all it conveys. Happy Thanksgiving Charmian and Switch &#038; Shift!]]></description>
		<content:encoded><![CDATA[<p>Love this infographic and all it conveys. Happy Thanksgiving Charmian and Switch &amp; Shift!</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on 6 Ways to Awaken the Potential in Your Workforce by Ben Simonton</title>
		<link>http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce#comment-17398</link>
		<dc:creator><![CDATA[Ben Simonton]]></dc:creator>
		<pubDate>Sat, 28 Nov 2015 12:22:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25786#comment-17398</guid>
		<description><![CDATA[Is it worthwhile to unleash the full potential of your workforce? The last time I did it, as the executive of a 1300 person unionized workforce, productivity per person rose over 300% in about 3 years. I did it then as I had before by listening to what employees had to say more than enough to satisfy their need to be heard and responding to what I heard to the satisfaction of those employees.]]></description>
		<content:encoded><![CDATA[<p>Is it worthwhile to unleash the full potential of your workforce? The last time I did it, as the executive of a 1300 person unionized workforce, productivity per person rose over 300% in about 3 years. I did it then as I had before by listening to what employees had to say more than enough to satisfy their need to be heard and responding to what I heard to the satisfaction of those employees.</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on Switch and Shift&#8217;s Top 75 List of Human Business Champions by Matt Monge</title>
		<link>http://switchandshift.com/switch-and-shifts-top-75-list-of-human-business-champions#comment-17397</link>
		<dc:creator><![CDATA[Matt Monge]]></dc:creator>
		<pubDate>Thu, 26 Nov 2015 18:10:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=10881#comment-17397</guid>
		<description><![CDATA[I&#039;d be honored to be considered for inclusion on this list in the future! I&#039;m such a fan of everyone on this list!]]></description>
		<content:encoded><![CDATA[<p>I&#8217;d be honored to be considered for inclusion on this list in the future! I&#8217;m such a fan of everyone on this list!</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on The Importance of Leadership on Employee Engagement by Charmian Solter</title>
		<link>http://switchandshift.com/the-importance-of-leadership-on-employee-engagement#comment-17396</link>
		<dc:creator><![CDATA[Charmian Solter]]></dc:creator>
		<pubDate>Mon, 23 Nov 2015 18:06:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25672#comment-17396</guid>
		<description><![CDATA[I think it does when it outlines how to engage employees on an executive, managerial and individual level. So much of it is communication and building a relationship with the employee, where genuine communication is valued and occurs.]]></description>
		<content:encoded><![CDATA[<p>I think it does when it outlines how to engage employees on an executive, managerial and individual level. So much of it is communication and building a relationship with the employee, where genuine communication is valued and occurs.</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on The Importance of Leadership on Employee Engagement by Ben Simonton</title>
		<link>http://switchandshift.com/the-importance-of-leadership-on-employee-engagement#comment-17395</link>
		<dc:creator><![CDATA[Ben Simonton]]></dc:creator>
		<pubDate>Sat, 21 Nov 2015 22:37:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25672#comment-17395</guid>
		<description><![CDATA[Employees cannot be directed to be engaged, but they can be led to be engaged. This article does not tell us how that is done.]]></description>
		<content:encoded><![CDATA[<p>Employees cannot be directed to be engaged, but they can be led to be engaged. This article does not tell us how that is done.</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on 9 Ways to Challenge Your Assumptions by Joan van den Brink</title>
		<link>http://switchandshift.com/9-ways-to-challenge-your-assumptions#comment-17394</link>
		<dc:creator><![CDATA[Joan van den Brink]]></dc:creator>
		<pubDate>Sat, 21 Nov 2015 09:30:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25596#comment-17394</guid>
		<description><![CDATA[This is also true in group discussions when we assume we all attach the same meaning to commonly used words without testing and clarifying those assumptions.]]></description>
		<content:encoded><![CDATA[<p>This is also true in group discussions when we assume we all attach the same meaning to commonly used words without testing and clarifying those assumptions.</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on #SocialLeader Recap: This is How Social Leaders Amplify Their Personal Brand by Ralph Mayhew</title>
		<link>http://switchandshift.com/socialleader-recap-this-is-how-social-leaders-amplify-their-personal-brand#comment-17393</link>
		<dc:creator><![CDATA[Ralph Mayhew]]></dc:creator>
		<pubDate>Fri, 20 Nov 2015 21:54:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25812#comment-17393</guid>
		<description><![CDATA[This is my first visit to this site but it&#039;s fantastic. It feels cutting edge with the drop down to twitter and all that. (gosh this almost sounds like  spam comment - yikes, sorry). I loved the quote: &quot;Share quality content with original commentary to stand out&quot;. It&#039;s such a good reminder! thanks!]]></description>
		<content:encoded><![CDATA[<p>This is my first visit to this site but it&#8217;s fantastic. It feels cutting edge with the drop down to twitter and all that. (gosh this almost sounds like  spam comment &#8211; yikes, sorry). I loved the quote: &#8220;Share quality content with original commentary to stand out&#8221;. It&#8217;s such a good reminder! thanks!</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on The Untapped Potential of Imperfection by Joeri Van den Bergh</title>
		<link>http://switchandshift.com/the-untapped-potential-of-imperfection#comment-17391</link>
		<dc:creator><![CDATA[Joeri Van den Bergh]]></dc:creator>
		<pubDate>Tue, 17 Nov 2015 13:16:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25593#comment-17391</guid>
		<description><![CDATA[Good to see the ideas, trends and examples on &quot;failosophy&quot; and &quot;perfectly imperfect&quot; I already wrote about back in 2014 on my blog http://www.howcoolbrandsstayhot.com/2014/12/10/trends-for-2015-perfectly-imperfect-14/ reflected in kristof&#039;s post]]></description>
		<content:encoded><![CDATA[<p>Good to see the ideas, trends and examples on &#8220;failosophy&#8221; and &#8220;perfectly imperfect&#8221; I already wrote about back in 2014 on my blog <a href="http://www.howcoolbrandsstayhot.com/2014/12/10/trends-for-2015-perfectly-imperfect-14/" rel="nofollow">http://www.howcoolbrandsstayhot.com/2014/12/10/trends-for-2015-perfectly-imperfect-14/</a> reflected in kristof&#8217;s post</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on 10 Mistakes All Great Leaders Avoid in Building Work Culture by Fred Akele</title>
		<link>http://switchandshift.com/10-mistakes-all-great-leaders-avoid-in-building-work-culture#comment-17389</link>
		<dc:creator><![CDATA[Fred Akele]]></dc:creator>
		<pubDate>Mon, 16 Nov 2015 20:45:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25451#comment-17389</guid>
		<description><![CDATA[A lot of value in this post. Thanks for sharing this information. How can you convince people in your company that building a culture should come first?]]></description>
		<content:encoded><![CDATA[<p>A lot of value in this post. Thanks for sharing this information. How can you convince people in your company that building a culture should come first?</p>
]]></content:encoded>
	</item>
	<item>
		<title>Comment on 5 High-Leverage Shifts to Evolve Management by Lowell Nerenberg</title>
		<link>http://switchandshift.com/5-high-leverage-shifts-to-evolve-management#comment-17386</link>
		<dc:creator><![CDATA[Lowell Nerenberg]]></dc:creator>
		<pubDate>Sun, 15 Nov 2015 14:48:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=25635#comment-17386</guid>
		<description><![CDATA[Shawn, I am in total alignment with your objective of shifting the descriptor of what we now call “management” into something more in tune with today’s enlightened understanding and need. However, the word stewardship does not sit so well with me. To me it connotes control and responsibility for things or stuff more than people. 

For my own purpose in redefining what’s needed in today’s world of work, the word guidance fits best.  Admittedly, as an executive leadership and management coach, I may be biased towards promoting the idea that managers would benefit greatly by simply imagining themselves as coaches, appropriately guiding teams to their intended outcomes. (Phil Jackson of NBA coaching fame comes to my mind here. I think of myself as a lot like him - only much shorter.)

My take-away from your wise analysis is to appreciate you for bringing to light the notion that “management” is no longer sufficient, and suggesting these explicit shifts are important aspects of whatever we call this role. Thank you.]]></description>
		<content:encoded><![CDATA[<p>Shawn, I am in total alignment with your objective of shifting the descriptor of what we now call “management” into something more in tune with today’s enlightened understanding and need. However, the word stewardship does not sit so well with me. To me it connotes control and responsibility for things or stuff more than people. </p>
<p>For my own purpose in redefining what’s needed in today’s world of work, the word guidance fits best.  Admittedly, as an executive leadership and management coach, I may be biased towards promoting the idea that managers would benefit greatly by simply imagining themselves as coaches, appropriately guiding teams to their intended outcomes. (Phil Jackson of NBA coaching fame comes to my mind here. I think of myself as a lot like him &#8211; only much shorter.)</p>
<p>My take-away from your wise analysis is to appreciate you for bringing to light the notion that “management” is no longer sufficient, and suggesting these explicit shifts are important aspects of whatever we call this role. Thank you.</p>
]]></content:encoded>
	</item>
</channel>
</rss>

<!-- Dynamic page generated in 1.238 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:39:40 -->
