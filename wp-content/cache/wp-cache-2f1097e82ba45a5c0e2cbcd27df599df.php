<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Switch &#38; Shift</title>
	<atom:link href="http://switchandshift.com/feed" rel="self" type="application/rss+xml" />
	<link>http://switchandshift.com</link>
	<description>Human Side of Business</description>
	<lastBuildDate>Sun, 29 Nov 2015 11:00:38 +0000</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	

<image>
	<url>http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250-48x48.png</url>
	<title>Switch &amp; Shift</title>
	<link>http://switchandshift.com</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>You Can&#8217;t Do This Alone: Time to Build a Community</title>
		<link>http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community</link>
		<comments>http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community#comments</comments>
		<pubDate>Sun, 29 Nov 2015 11:00:38 +0000</pubDate>
		<dc:creator><![CDATA[Lauren Kirkpatrick]]></dc:creator>
				<category><![CDATA[#SocialLeader]]></category>
		<category><![CDATA[#socialleader]]></category>
		<category><![CDATA[Community]]></category>
		<category><![CDATA[Social Leader Chat]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25869</guid>
		<description><![CDATA[<p>A few weeks ago the Social Leader community discussed how to be the best example of your brand, but social media isn't just one person's responsibility. It takes a community to build a brand. ... <a class="readmore" href="http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community">You Can&#8217;t Do This Alone: Time to Build a Community</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community/feed</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>6 Ways to Awaken the Potential in Your Workforce</title>
		<link>http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce</link>
		<comments>http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce#comments</comments>
		<pubDate>Fri, 27 Nov 2015 11:00:40 +0000</pubDate>
		<dc:creator><![CDATA[Pete Wheelan]]></dc:creator>
				<category><![CDATA[Human workplace]]></category>
		<category><![CDATA[Employee Compensation]]></category>
		<category><![CDATA[Employee Recognition]]></category>
		<category><![CDATA[Growth Opportunity]]></category>
		<category><![CDATA[Vibrant Workplace]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25786</guid>
		<description><![CDATA[<p>The great thing about investing in human potential is it’s both morally and fiscally responsible. When you help people become their best possible selves, your company benefits as much as they do. The effects can also spread beyond the company’s walls, unlocking potential within families, communities, and society at large.... <a class="readmore" href="http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce">6 Ways to Awaken the Potential in Your Workforce</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce/feed</wfw:commentRss>
		<slash:comments>1</slash:comments>
		</item>
		<item>
		<title>Our Most Cherished Thanksgiving Traditions</title>
		<link>http://switchandshift.com/our-most-cherished-thanksgiving-traditions</link>
		<comments>http://switchandshift.com/our-most-cherished-thanksgiving-traditions#comments</comments>
		<pubDate>Thu, 26 Nov 2015 13:00:03 +0000</pubDate>
		<dc:creator><![CDATA[The Switch and Shift Team]]></dc:creator>
				<category><![CDATA[Editor's Choice]]></category>
		<category><![CDATA[family values]]></category>
		<category><![CDATA[Gratitude]]></category>
		<category><![CDATA[Thanksgiving]]></category>
		<category><![CDATA[thanksgiving traditions]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25852</guid>
		<description><![CDATA[<p>Despite the commercialization of Thanksgiving, there are traditions that families and friends honor to bring meaning to sharing a meal together. It’s these traditions that our Switch &#038; Shift family are reflecting on. Below are our cherished Thanksgiving traditions.... <a class="readmore" href="http://switchandshift.com/our-most-cherished-thanksgiving-traditions">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/our-most-cherished-thanksgiving-traditions">Our Most Cherished Thanksgiving Traditions</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/our-most-cherished-thanksgiving-traditions/feed</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>How Gratitude Changes the Way You See the World</title>
		<link>http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world</link>
		<comments>http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world#comments</comments>
		<pubDate>Thu, 26 Nov 2015 11:00:05 +0000</pubDate>
		<dc:creator><![CDATA[Charmian Solter]]></dc:creator>
				<category><![CDATA[Editor's Choice]]></category>
		<category><![CDATA[Power of Gratitude]]></category>
		<category><![CDATA[Thankfulness]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25783</guid>
		<description><![CDATA[<p>As we sit down this Thanksgiving with family and friends, before you put that first bite of food in your mouth, we hope that you will take a moment to think about all the things in life that you are thankful for.<br />
... <a class="readmore" href="http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world">How Gratitude Changes the Way You See the World</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world/feed</wfw:commentRss>
		<slash:comments>1</slash:comments>
		</item>
		<item>
		<title>Are You Ready for the Digital Workplace?</title>
		<link>http://switchandshift.com/are-you-ready-for-the-digital-workplace</link>
		<comments>http://switchandshift.com/are-you-ready-for-the-digital-workplace#comments</comments>
		<pubDate>Wed, 25 Nov 2015 11:00:27 +0000</pubDate>
		<dc:creator><![CDATA[Kathryn Everest]]></dc:creator>
				<category><![CDATA[Social Era]]></category>
		<category><![CDATA[creating work culture]]></category>
		<category><![CDATA[digital workplace]]></category>
		<category><![CDATA[remote workforce]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25781</guid>
		<description><![CDATA[<p>For many people, the workday doesn’t have a set beginning or end time and the workplace isn’t always a physical destination. You may be one of those people. Do you drink your morning coffee while electronically catching up on your laptop or other device? Did you do the same before you went to bed last night (sans coffee)? ... <a class="readmore" href="http://switchandshift.com/are-you-ready-for-the-digital-workplace">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/are-you-ready-for-the-digital-workplace">Are You Ready for the Digital Workplace?</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/are-you-ready-for-the-digital-workplace/feed</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>A Necessary Redefinition of Responsibility and Leadership</title>
		<link>http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership</link>
		<comments>http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership#comments</comments>
		<pubDate>Tue, 24 Nov 2015 13:00:28 +0000</pubDate>
		<dc:creator><![CDATA[Karen Kimsey-House]]></dc:creator>
				<category><![CDATA[Leadership Blog]]></category>
		<category><![CDATA[Angles of Leadership]]></category>
		<category><![CDATA[Co-Active Leadership]]></category>
		<category><![CDATA[Redefining Responsibility]]></category>
		<category><![CDATA[Responsible Leadership]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25779</guid>
		<description><![CDATA[<p>This post will highlight the Co-Active Leadership Model, based on responsible leadership and how responsibility is defined.... <a class="readmore" href="http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership">A Necessary Redefinition of Responsibility and Leadership</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership/feed</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>4 Leadership Lessons from Nine-Year-Olds</title>
		<link>http://switchandshift.com/4-leadership-lessons-from-nine-year-olds</link>
		<comments>http://switchandshift.com/4-leadership-lessons-from-nine-year-olds#comments</comments>
		<pubDate>Tue, 24 Nov 2015 11:00:51 +0000</pubDate>
		<dc:creator><![CDATA[John Bell]]></dc:creator>
				<category><![CDATA[Leadership Blog]]></category>
		<category><![CDATA[childhood wisdom]]></category>
		<category><![CDATA[Diverse work culture]]></category>
		<category><![CDATA[Leadership Lessons]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25776</guid>
		<description><![CDATA[<p>Five years ago, I became a blogger. With 45 years of business experience as a foundation, I began sharing personal reflections and contemporary insights on leadership – fully aware my content would not matter a damn had I not kept pace with the pulse of the new economy, and the changing dynamics of customers and the workforce.... <a class="readmore" href="http://switchandshift.com/4-leadership-lessons-from-nine-year-olds">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/4-leadership-lessons-from-nine-year-olds">4 Leadership Lessons from Nine-Year-Olds</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/4-leadership-lessons-from-nine-year-olds/feed</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>5 Ways to Increase Your Visibility</title>
		<link>http://switchandshift.com/5-ways-to-increase-your-visibility</link>
		<comments>http://switchandshift.com/5-ways-to-increase-your-visibility#comments</comments>
		<pubDate>Mon, 23 Nov 2015 11:00:55 +0000</pubDate>
		<dc:creator><![CDATA[Bob Pritchett]]></dc:creator>
				<category><![CDATA[Recognition]]></category>
		<category><![CDATA[developing your personal brand]]></category>
		<category><![CDATA[raising visibility]]></category>
		<category><![CDATA[ways to increase your visibility]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25820</guid>
		<description><![CDATA[<p>It’s quite simple: being visible helps you achieve your goals.</p>
<p>People prefer to work with people they’re familiar with. That familiarity doesn’t have to be based on impressive and daring feats; it can come from simple exposure. You, your name, and your work need to be visible to your peers, to your boss, to your boss’s boss, to casual observers, to industry experts, to anyone who goes looking for you or your area of expertise on the Internet, etc.<br />
... <a class="readmore" href="http://switchandshift.com/5-ways-to-increase-your-visibility">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/5-ways-to-increase-your-visibility">5 Ways to Increase Your Visibility</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/5-ways-to-increase-your-visibility/feed</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>The Importance of Leadership on Employee Engagement</title>
		<link>http://switchandshift.com/the-importance-of-leadership-on-employee-engagement</link>
		<comments>http://switchandshift.com/the-importance-of-leadership-on-employee-engagement#comments</comments>
		<pubDate>Fri, 20 Nov 2015 11:00:36 +0000</pubDate>
		<dc:creator><![CDATA[Charmian Solter]]></dc:creator>
				<category><![CDATA[Employee Engagement]]></category>
		<category><![CDATA[Employee Motivation]]></category>
		<category><![CDATA[trust in leadership]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25672</guid>
		<description><![CDATA[<p>Here at Switch &#038; Shift we strive to illuminate effective leadership practices. We pride ourselves on creating cutting edge solutions for employee engagement, communication and creating company culture to name a few.<br />
... <a class="readmore" href="http://switchandshift.com/the-importance-of-leadership-on-employee-engagement">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/the-importance-of-leadership-on-employee-engagement">The Importance of Leadership on Employee Engagement</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/the-importance-of-leadership-on-employee-engagement/feed</wfw:commentRss>
		<slash:comments>2</slash:comments>
		</item>
		<item>
		<title>Avoid the Water Cooler &#8211; Negative Thinking Stays with You</title>
		<link>http://switchandshift.com/avoid-the-water-cooler-negative-thinking-stays-with-you</link>
		<comments>http://switchandshift.com/avoid-the-water-cooler-negative-thinking-stays-with-you#comments</comments>
		<pubDate>Thu, 19 Nov 2015 11:00:11 +0000</pubDate>
		<dc:creator><![CDATA[Susan Mazza]]></dc:creator>
				<category><![CDATA[Workplace Optimism]]></category>
		<category><![CDATA[avoid negative thinking]]></category>
		<category><![CDATA[optimistic workplace]]></category>
		<category><![CDATA[positive work culture]]></category>
		<category><![CDATA[power of positive thinking]]></category>

		<guid isPermaLink="false">http://switchandshift.com/?p=25666</guid>
		<description><![CDATA[<p>We are programmed to be emotional, and prone to negative thoughts. This post explains why we need to step away from the water cooler and train ourselves to think positively.... <a class="readmore" href="http://switchandshift.com/avoid-the-water-cooler-negative-thinking-stays-with-you">Read More&#187;</a></p>
<p>The post <a rel="nofollow" href="http://switchandshift.com/avoid-the-water-cooler-negative-thinking-stays-with-you">Avoid the Water Cooler &#8211; Negative Thinking Stays with You</a> appeared first on <a rel="nofollow" href="http://switchandshift.com">Switch &amp; Shift</a>.</p>
]]></description>
		<wfw:commentRss>http://switchandshift.com/avoid-the-water-cooler-negative-thinking-stays-with-you/feed</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
	</channel>
</rss>

<!-- Dynamic page generated in 1.339 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:39:52 -->
