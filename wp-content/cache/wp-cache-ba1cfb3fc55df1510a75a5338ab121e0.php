<?php die(); ?>



<!DOCTYPE html>

<!--[if lt IE 9]>             <html class="no-js ie lt-ie9" lang="en-US" prefix="og: http://ogp.me/ns#""><![endif]-->

<!--[if IE 9]>                <html class="no-js ie ie9" lang="en-US" prefix="og: http://ogp.me/ns#">   <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js no-ie" lang="en-US" prefix="og: http://ogp.me/ns#">    <!--<![endif]-->

	<head>

		<meta charset="UTF-8" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />

		<!--[if lt IE 9]>

			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

			<script src="http://switchandshift.com/wp-content/themes/time/data/js/selectivizr.min.js"></script>

		<![endif]-->

		<title>In Defense of Entitlement</title>
		<title>In Defense of Entitlement</title>
<meta property="og:site_name" content="Switch &amp; Shift" /><meta property="og:title" content="Switch &amp; Shift" /><meta property="og:locale" content="en_US" /><meta property="og:url" content="http://switchandshift.com/in-defense-of-entitlement" /><meta property="og:description" content="A recent conversation with my son, left me feeling quite unsettled. The topic was work ethic. As we discussed my short stint in retail work, I commented that my generation had a much better work ethic than his generation. I might even have been [...]" /><meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2014/09/defense-of-entitlement.jpeg" />
<!-- This site is optimized with the Yoast SEO plugin v2.3.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="robots" content="noindex,follow"/>
<meta name="description" content="A recent conversation with my son, left me feeling quite unsettled. The topic was work ethic. As we discussed my short stint in retail work, I commented that my generation had a much better work ethic than his generation. I might even have been a bit boastful about it. I expected him to defend his generation, or at least try to excuse the actions of “some” as not being indicative of “all.” That is NOT what happened next."/>
<link rel="canonical" href="http://switchandshift.com/in-defense-of-entitlement" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="In Defense of Entitlement" />
<meta property="og:description" content="A recent conversation with my son, left me feeling quite unsettled. The topic was work ethic. As we discussed my short stint in retail work, I commented that my generation had a much better work ethic than his generation. I might even have been a bit boastful about it. I expected him to defend his generation, or at least try to excuse the actions of “some” as not being indicative of “all.” That is NOT what happened next." />
<meta property="og:url" content="http://switchandshift.com/in-defense-of-entitlement" />
<meta property="og:site_name" content="Switch &amp; Shift" />
<meta property="article:author" content="https://www.facebook.com/anita.stout.77" />
<meta property="article:tag" content="Anita Stout" />
<meta property="article:tag" content="Business" />
<meta property="article:tag" content="Entitlement" />
<meta property="article:tag" content="Gen y" />
<meta property="article:tag" content="Leadership" />
<meta property="article:tag" content="Leadership Blog" />
<meta property="article:tag" content="Millennials" />
<meta property="article:section" content="Business" />
<meta property="article:published_time" content="2014-06-03T09:00:44+00:00" />
<meta property="article:modified_time" content="2014-09-10T19:39:35+00:00" />
<meta property="og:updated_time" content="2014-09-10T19:39:35+00:00" />
<meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2014/09/defense-of-entitlement.jpeg" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="A recent conversation with my son, left me feeling quite unsettled. The topic was work ethic. As we discussed my short stint in retail work, I commented that my generation had a much better work ethic than his generation. I might even have been a bit boastful about it. I expected him to defend his generation, or at least try to excuse the actions of “some” as not being indicative of “all.” That is NOT what happened next."/>
<meta name="twitter:title" content="In Defense of Entitlement"/>
<meta name="twitter:domain" content="Switch &amp; Shift"/>
<meta name="twitter:image" content="http://switchandshift.com/wp-content/uploads/2014/09/defense-of-entitlement.jpeg"/>
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Feed" href="http://switchandshift.com/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Comments Feed" href="http://switchandshift.com/comments/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; In Defense of Entitlement Comments Feed" href="http://switchandshift.com/in-defense-of-entitlement/feed" />
<!-- This site is powered by Shareaholic - https://shareaholic.com -->
<script type='text/javascript' data-cfasync='false'>
  //<![CDATA[
    _SHR_SETTINGS = {"endpoints":{"local_recs_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_permalink_related","share_counts_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_share_counts_api"}};
  //]]>
</script>
<script type='text/javascript'
        src='//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js'
        data-shr-siteid='22c416cd99bd31d22f03d82d3a8395e6'
        data-cfasync='false'
        async='async' >
</script>

<!-- Shareaholic Content Tags -->
<meta name='shareaholic:site_name' content='Switch &amp; Shift' />
<meta name='shareaholic:language' content='en-US' />
<meta name='shareaholic:url' content='http://switchandshift.com/in-defense-of-entitlement' />
<meta name='shareaholic:keywords' content='anita stout, business, entitlement, gen y, leadership, leadership blog, millennials, entitlement , culture, engagement, featured' />
<meta name='shareaholic:article_published_time' content='2014-06-03T16:00:44+00:00' />
<meta name='shareaholic:article_modified_time' content='2015-11-29T11:00:38+00:00' />
<meta name='shareaholic:shareable_page' content='true' />
<meta name='shareaholic:article_author_name' content='Anita Stout' />
<meta name='shareaholic:site_id' content='22c416cd99bd31d22f03d82d3a8395e6' />
<meta name='shareaholic:wp_version' content='7.6.2.3' />
<meta name='shareaholic:image' content='http://switchandshift.com/wp-content/uploads/2014/09/defense-of-entitlement.jpeg' />
<!-- Shareaholic Content Tags End -->
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/switchandshift.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=1d05468803804e8153cb17eab60a20d0"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='flick-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp//css/flick/flick.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimpSF_main_css-css'  href='http://switchandshift.com/?mcsf_action=main_css&#038;ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!--[if IE]>
<link rel='stylesheet' id='mailchimpSF_ie_css-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp/css/ie.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='wild-googlemap-frontend-css'  href='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-css-frontend.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='cptch_stylesheet-css'  href='http://switchandshift.com/wp-content/plugins/captcha/css/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='tm_clicktotweet-css'  href='http://switchandshift.com/wp-content/plugins/click-to-tweet-by-todaymade/assets/css/styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='se-link-styles-css'  href='http://switchandshift.com/wp-content/plugins/search-everything/static/css/se-styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://switchandshift.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-3rd-party-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/3rd-party.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-style-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/style.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-scheme-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/bright.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-stylesheet-css'  href='http://switchandshift.com/wp-content/themes/time_child/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-mobile-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/mobile.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='only screen and (max-width: 767px)' />
<link rel='stylesheet' id='author-avatars-widget-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/widget.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='author-avatars-shortcode-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/shortcode.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!-- This site uses the Google Analytics by Yoast plugin v5.4.6 - Universal disabled - https://yoast.com/wordpress/plugins/google-analytics/ -->
<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-27884223-1']);
	_gaq.push(['_gat._forceSSL']);
	_gaq.push(['_trackPageview']);

	(function () {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();

</script>
<!-- / Google Analytics by Yoast -->
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/scrollTo.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.form.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mailchimpSF = {"ajax_url":"http:\/\/switchandshift.com\/"};
/* ]]> */
</script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/mailchimp.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/ui/core.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp//js/datepicker.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?sensor=false&#038;ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js-frontend.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://switchandshift.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://switchandshift.com/wp-includes/wlwmanifest.xml" /> 

<link rel='shortlink' href='http://switchandshift.com/?p=16794' />
<meta name='robots' content='noindex,follow' />
<!-- Custom Code Start-->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '811956965544989']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=811956965544989&ev=PixelInitialized" /></noscript>
<!-- Custom Code Start-->
	<script type="text/javascript">
		jQuery(function($) {
			$('.date-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd/yyyy';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: true,
					beforeShow: function(input, inst) { $('#ui-datepicker-div').addClass('show'); },
					dateFormat: format.toLowerCase(),
				});
			});
			d = new Date();
			$('.birthdate-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: false,
					minDate: new Date(d.getFullYear(), 1-1, 1),
					maxDate: new Date(d.getFullYear(), 12-1, 31),
					beforeShow: function(input, inst) { $('#ui-datepicker-div').removeClass('show'); },
					dateFormat: format.toLowerCase(),
				});

			});

		});
	</script>
	<script type="text/javascript">
	window._se_plugin_version = '8.1.3';
</script>
<style>a, a.alt:hover, .alt a:hover, #bottom a.alt:hover, #bottom .alt a:hover, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, input[type="button"].active, button.active, .button.active, .color, .super-tabs > div > .nav h2 span, .toggles > div > h3:hover > i, .logo, nav a:hover, #bottom nav a:hover, nav .current > a, nav .current>a:hover{color:#0292b7}mark, .slider .control-nav li a:hover, .slider .control-nav li a.active, #top:before, #top > .before, .background-color, nav.mobile a:hover, nav.mobile .current > a, .mejs-controls .mejs-time-rail .mejs-time-loaded, .mejs-controls .mejs-time-rail .mejs-time-current{background-color:#0292b7}.zoom-hover>.zoom-hover-overlay{background-color:rgba(2, 146, 183, 0.75)}blockquote.bar, .sticky:before, #bottom .outer-container{border-color:#0292b7}body,input,select,textarea,button,.button{font-family:Raleway;font-size:13px;line-height:23px}nav.primary ul, nav.primary a:not(:hover), nav.mobile ul, nav.mobile a:not(:hover){font-family:Raleway;font-size:15px;font-weight:bold;font-style:normal;text-decoration:none}nav.secondary ul, nav.secondary a:not(:hover){font-family:Raleway;font-size:12px;font-weight:bold;font-style:normal;text-decoration:none}.headline h1{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}.headline .breadcrumbs{font-family:Raleway;font-size:15px;font-weight:normal;font-style:normal;text-decoration:none}#top .widget>.title{font:normal 18px/128% Raleway;text-decoration:none}.post h1.title{font:bold 22px/128% Raleway;text-decoration:none}h1{font:bold 22px/128% Raleway;text-decoration:none}h2{font:bold 18px/128% Raleway;text-decoration:none}h3{font:bold 14px/128% Raleway;text-decoration:none}h4{font:bold 14px/128% Raleway;text-decoration:none}input[type="submit"]:not(.big):not(.huge),input[type="reset"]:not(.big):not(.huge),input[type="button"]:not(.big):not(.huge),button:not(.big):not(.huge),.button:not(.big):not(.huge){font-family:Raleway;font-size:14px;font-weight:normal;font-style:normal;text-decoration:none}input[type="submit"].big,input[type="reset"].big,input[type="button"].big,button.big,.button.big{font-family:Raleway;font-size:18px;font-weight:bold;font-style:normal;text-decoration:none}input[type="submit"].huge,input[type="reset"].huge,input[type="button"].huge,button.huge,.button.huge{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}</style>
<script>if(typeof WebFont!='undefined'){WebFont.load({google:{families:["Raleway:400,700:latin"]},active:function(){if(document.createEvent){var e=document.createEvent('HTMLEvents');e.initEvent('webfontactive',true,false);document.dispatchEvent(e);}else{document.documentElement['webfontactive']++;}}});}timeConfig={templatePath:'http://switchandshift.com/wp-content/themes/time',zoomHoverIcons:{"default":"icon-plus-circled","image":"icon-search","mail":"icon-mail","title":"icon-right"},flexsliderOptions:{"animation":"slide","direction":"horizontal","animationSpeed":600,"slideshowSpeed":7000,"slideshow":false},layersliderOptions:{}};(function($){$(document).ready(function($){$('.widget_pages, .widget_archive, .widget_categories, .widget_recent_entries, .widget_recent_comments, .widget_display_forums, .widget_display_replies, .widget_display_topics, .widget_display_views').each(function(){$('ul',this).addClass('fancy alt');$('li',this).prepend($('<i />',{'class':'icon-right-open'}));if($(this).closest('#top').length>0){$('i',this).addClass('color');}});$('#disqus_thread').addClass('section');});})(jQuery);</script>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//switchandshift.com/?wordfence_logHuman=1&hid=96D1CEC65548091EF79DCCA759A697F2');
</script><link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250-48x48.png" sizes="32x32" />
<link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">
<meta name="msapplication-TileImage" content="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">


<!-- Google Code for Remarketing Tag -->
<!-
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 954219818;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/954219818/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


	</head>



	<body class="time-child-3-6-1-child-1-0 single single-post postid-16794 single-format-standard layout-boxed scheme-bright">



		<div id="top" >



			
	<div class="backgrounds"><div style="background: url(&quot;http://switchandshift.com/wp-content/uploads/2014/09/defense-of-entitlement.jpeg&quot;) #ffffff no-repeat center top fixed; background-size: cover;" class="stripes"></div></div>



			<div class="upper-container ">



				<div class="outer-container">



					
						<nav id="menu" class="mobile">

							<ul id="menu-main-menu" class=""><li id="menu-item-3819" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-4305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li id="menu-item-4304" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li id="menu-item-4303" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li id="menu-item-19075" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li id="menu-item-19071" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li id="menu-item-4306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li id="menu-item-22648" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li id="menu-item-4177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li id="menu-item-21828" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li id="menu-item-23151" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
</ul>
						</nav>

					


					
						<nav id="search" class="mobile">

							<form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form>
						</nav>

					


				</div>



				<div class="outer-container ">



					<header class="header">



						<div class="container">



							<div class="mobile-helper vertical-align">



								
									<a href="#menu" class="button" title="Menu"><i class="icon-menu"></i></a>

								






								
									<a href="#search" class="button" title="Search"><i class="icon-search"></i></a>

								


							</div>



							


							<h1 class="logo vertical-align"><a href="http://switchandshift.com/" title="Switch &amp; Shift" rel="home"><img src="http://switchandshift.com/wp-content/uploads/2014/07/logo.jpg" width="244" height="50" data-2x="http://switchandshift.com/wp-content/uploads/2014/07/logo-2x.jpg" alt="Switch &amp; Shift" /></a></h1>


							
								<nav class="primary vertical-align">

									<ul id="menu-main-menu-1" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
<li><form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form></li></ul>
								</nav>

							


						</div>



					</header>



					


				</div>



			</div>



			

			

<div class="outer-container">
	
	<nav class="secondary">
		<div class="container">
			<div class="menu-secondary-menu-container"><ul id="menu-secondary-menu" class="menu"><li id="menu-item-19073" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19073"><a href="http://switchandshift.com/work-that-matters-podcast">Work That Matters Podcast</a></li>
<li id="menu-item-21566" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21566"><a href="http://switchandshift.com/speaking-under-construction">Speaking</a></li>
<li id="menu-item-4334" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4334"><a href="http://switchandshift.com/consulting">Consulting</a></li>
<li id="menu-item-4331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4331"><a href="http://switchandshift.com/a-world-gone-social">A World Gone Social</a></li>
<li id="menu-item-20424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20424"><a href="http://switchandshift.com/writing-guidelines">Writing Guidelines</a></li>
<li id="menu-item-19074" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19074"><a href="http://switchandshift.com/contact">Contact Us</a></li>
</ul></div>    				</div>
	</nav>

	<div class="content"><div class="container"><div class="main alpha" style="padding: 0 240px 0 0px; margin: 0 -240px 0 -0px;">


		


			<section class="section">

				<article id="post-16794" class="post post-16794 type-post status-publish format-standard has-post-thumbnail hentry category-business category-culture-topics category-engagement-2 category-featured category-leadership tag-anita-stout tag-business-2 tag-entitlement tag-gen-y tag-leadership-2 tag-leadership-blog tag-millennials">
<img width="700" height="300" src="http://switchandshift.com/wp-content/uploads/2014/09/defense-of-entitlement.jpeg" class="attachment-700 wp-post-image" alt="defense of entitlement" 0="" data-2x="http://switchandshift.com/wp-content/uploads/2014/09/defense-of-entitlement.jpeg" />					

	<h1 class="title entry-title">In Defense of Entitlement</h1>

                    

					<div class='shareaholic-canvas' data-app-id='21423060' data-app='share_buttons' data-title='In Defense of Entitlement' data-link='http://switchandshift.com/in-defense-of-entitlement' data-summary='A recent conversation with my son, left me feeling quite unsettled. The topic was work ethic. As we discussed my short stint in retail work, I commented that my generation had a much better work ethic than his generation. I might even have been a bit boastful about it. I expected him to defend his generation, or at least try to excuse the actions of “some” as not being indicative of “all.” That is NOT what happened next.'></div><p>A recent conversation with my son, left me feeling quite unsettled. The topic was work ethic.  As we discussed my short stint in retail work, I commented that my generation had a much better work ethic than his generation. I <i>might </i>even have been a bit boastful about it.</p>
<p>I expected him to defend his generation, or at least try to excuse the actions of “some” as not being indicative of “all.”  That is NOT what happened next. What happened instead is he attacked my generation!</p>
<p>As a baby-boomer I was taught to “work hard, always do my best.”  After all “your work is a reflection of your character.” Baby boomers were taught to “go the extra mile, keep our noses to the grindstone” and show deference to those who signed our paychecks. Then, if we were lucky, we could hope to advance to a better position.</p>
<blockquote><p>Say all you want against this “entitled” generation, but I believe they are the ones driving the changes that are happening in business.</p></blockquote>
<p>My son pointed out that it was those very platitudes that allowed “soulless corporations” to employ people at poverty level wages and keep them from bettering themselves. “Why”, he asked “should someone put their heart and soul into a situation like that.”</p>
<p>I countered with “pride in their work?” That <i>really</i> fell flat.</p>
<p>Lunch ended and with it our conversation, but I left the restaurant feeling like I had somehow failed him. What had I missed? What more could I have taught him?</p>
<p>That was two weeks ago. Since then, I’ve done a lot of thinking on that conversation. I’m starting to believe that maybe teaching my son his inherent worth was the best thing I could have done.</p>
<p>Say all you want against this “entitled” generation, but I believe they are the ones driving the changes that are happening in business. This generation is not willing to make a living at the expense of making a life. (How absurd! Right?)</p>
<blockquote><p>This generation is not willing to make a living at the expense of making a life.</p></blockquote>
<p>To find and keep good people, corporations are needing to switch from business as usual to business as “you-sual.” This “entitled” generation of workers are looking for meaning and engagement in what they do. They require that they be taken seriously and that their contributions be appreciated.</p>
<p>Even as consumers they are no longer satisfied with being handed what companies decide to offer. They’re driving innovation by demanding what they want to satisfy their own needs and desires.</p>
<p>This entitled generation could just be the best hope the world has had for a better tomorrow in quite some time. This old dog is grateful for a son who understands his own value enough not to just go along to get along. I’m grateful that I taught him that his point of view is as valid as mine and that he feels entitled to express it. I’m looking forward to seeing what more he and his generation have in store for us.</p>
<p>Did you like today’s post? If so you’ll love our frequent newsletter! Sign up <a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc', 'HERE']);" >HERE</a> and receive <i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</p>
<p>Copyright: <a href="http://www.123rf.com/profile_iosphere" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.123rf.com/profile_iosphere', 'iosphere / 123RF Stock Photo']);" >iosphere / 123RF Stock Photo</a></p>
<div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/anita-stout" title="All posts by Anita Stout" rel="author">Anita Stout</a></h3><div class="bio-gravatar"><img src="http://1.gravatar.com/avatar/4b67c48a57351aa83d8ee2df73f9d6ea?s=70&#038;d=http%3A%2F%2Fswitchandshift.com%2Fwp-content%2Fuploads%2F2014%2F10%2Fs-circle-logo-250x250-96x96.png&#038;r=g" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://blog.lifeisntbroken.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="https://www.facebook.com/anita.stout.77" class="bio-icon bio-icon-facebook"></a><a target="_blank" href="https://plus.google.com/u/0/110617096357381981063/posts" class="bio-icon bio-icon-googleplus"></a><p class="bio-description">Anita Stout is an author, and the CEO of Wellness Pays Inc. which focuses on helping people live better lives through better choices physically and financially. She believes her greatest contribution, raising six children, will prove to be her life’s masterpiece.  She recently completed a new book titled Life Isn’t Broken (in editing) and publishes a blog by the same name. Her love of diversity, brilliance, and lively conversation, led her to her husband David, who pastors a different church than she attends and has opposing political views.</p></div>
					<div class="clear"></div>

				</article>

			</section>



			
			

			
	<section class="section">		<ul class="meta alt">
			<li class="published updated"><a href="http://switchandshift.com/2014/06" title="View all posts from June"><i class="icon-clock"></i>June 3, 2014</a></li><li><i class="icon-comment"></i><a href="http://switchandshift.com/in-defense-of-entitlement#comments"><span class="dsq-postid" data-dsqidentifier="16794 http://switchandshift.com/?p=11794">9 Comments</span></a></li><li><i class="icon-list"></i><a href="http://switchandshift.com/category/topics/business" rel="category tag">Business</a>, <a href="http://switchandshift.com/category/topics/culture-topics" rel="category tag">Culture</a>, <a href="http://switchandshift.com/category/series/engagement-2" rel="category tag">Engagement</a>, <a href="http://switchandshift.com/category/featured" rel="category tag">Featured</a>, <a href="http://switchandshift.com/category/topics/leadership" rel="category tag">Leadership</a></li>		</ul>
	</section>
			
<div id="disqus_thread">
            <div id="dsq-content">


            <ul id="dsq-comments">
                    <li class="comment even thread-even depth-1" id="dsq-comment-14573">
        <div id="dsq-comment-header-14573" class="dsq-comment-header">
            <cite id="dsq-cite-14573">
http://www.bensimonton.com/                <span id="dsq-author-user-14573">Ben Simonton</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14573" class="dsq-comment-body">
            <div id="dsq-comment-message-14573" class="dsq-comment-message"><p>Soulless corporations who employ people at poverty level wages? Wow! Kept from bettering themselves? How was that done?</p>
<p>Why should someone put<br />
their heart and soul into a situation like that? Duh &#8211; to be able to support self and a family while bettering oneself by studying at night to learn valuable things!</p>
<p>Millennials merely grew up in a land of plenty so they are accustomed to having everything they want and for that reason they expect to be given it no matter where they are. Can&#8217;t blame them for that. Sadly for them, the land of plenty is disappearing slowly but surely in the US &#8211; wages have not improved for almost 40 years so our average standard of living is on the way down. The middle class is shrinking steadily as more and more people become poor and dependent on government programs, over 70 of them. The capitalism that fueled our economic growth has been replaced by government created unlimited credit and the free markets that also fueled our economic growth are being replaced by government control. The nation that was the most prosperous by far and was the greatest creditor country by far has morphed into being the greatest debtor nation and less prosperous than quite a few, dropping steadily.</p>
<p>I feel sorry for millennials since we that came before them have killed the goose that laid the golden eggs.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-14577">
        <div id="dsq-comment-header-14577" class="dsq-comment-header">
            <cite id="dsq-cite-14577">
                <span id="dsq-author-user-14577">Cecil Whorton</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14577" class="dsq-comment-body">
            <div id="dsq-comment-message-14577" class="dsq-comment-message"><p>Living in the land of plenty by &#8220;the grace of God&#8221; does not equate to entitlement. My father live with passion and purpose to avoid being shot at and to survive the hardships of the great depression. He got a fifth grade education and rose to be successful.<br />
I was born in 1936,  lived off the grid and learned through the school of hard knocks and in schools where the golden rule was taught along with prayer and songs like &#8220;row, row, row your boat and onward christen soldiers&#8221;.<br />
Living with a tight knit family and working together we have had our ups and downs. but have stood together and raised our families with work ethic.<br />
Times are changing and opportunities are available to those who have the gumption to put forth the effort to learn how the new change will effect their decisions or vice versa.<br />
Change, as I have said before is constant, we have to expect change and embrace it.<br />
A corrupt nation with insurmountable debt and too many rules and regulations, taxes, and disgruntled voters cannot stand.  I too, feel for the young and restless that have been given too much and are not willing to earn a living. They were born with too much time on their hands, Too much TV, fast food, soda pop and available drugs.<br />
The warning signs are in your face and being taught through all forms of media. We are going to hell in a hand-basket if we don&#8217;t wake up, stand up and heed the warnings.  It is written, &#8220;The poor ye shall have with you always&#8221; &#8211;Bible</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-14591">
        <div id="dsq-comment-header-14591" class="dsq-comment-header">
            <cite id="dsq-cite-14591">
http://authormonicanelson.com/                <span id="dsq-author-user-14591">Monica Nelson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14591" class="dsq-comment-body">
            <div id="dsq-comment-message-14591" class="dsq-comment-message"><p>I find this article insightful and informative. I, too, am a baby boomer who grew up during a different time. And, it is enticing to want to apply yesterday&#8217;s standards to today&#8217;s generation. But, I see where they are coming from.</p>
<p>As I recall, we did not accept the values the &#8220;older generation&#8221; placed on us. We were a different group of people with our own ideas and philosophy formed from the times we were born into. We set out to change the world too.</p>
<p>And now that we have (the results are in the younger peoples attitudes), are we then going to deny them the same process? This is how change is effected . . . how we grow and advance as a society.</p>
<p>Thanks, Anita, for your courage in pointing this out to us.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-14574">
        <div id="dsq-comment-header-14574" class="dsq-comment-header">
            <cite id="dsq-cite-14574">
http://www.lifeisntbroken.com                <span id="dsq-author-user-14574">lifeisntbroken</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14574" class="dsq-comment-body">
            <div id="dsq-comment-message-14574" class="dsq-comment-message"><p>Thanks for your comments Ben. I also feel sorry for the millennials (and responsible as well) My son did the &#8220;go to school and learn valuable things&#8221; route. He was fortunate enough to be able to. That doesn&#8217;t always seem to offer better opportunity these days for the reasons you stated- sadly. I agree that betterment is crucial &#8211; even for the sake of betterment.</p>
<p>To his point, while in retail I saw firsthand what he meant by &#8220;soulless corporations employing people at poverty level wages.&#8221; I saw babyboomers who had run businesses of their own &#8211; highly competent people &#8211; being paid below minimum wage and treated like complete idiots. Sadly many of them were buying their necessities on the company credit card at DEEP employee discounts and 29% interest rates. (which at one time would have been considered loan sharking and illegal)  In effect some of them had become indentured servants, feeling unable to leave because of what they owed on their company card. I feel sorry for them as well.<br />
Yes, going to school at night to better themselves could still be an option for them as well I suppose. (however, retail hours aren&#8217;t consistent and are hard to schedule around.)</p>
<p>My intended point in this was to acknowledge that some good has also come from what my generation not only calls &#8220;entitlement&#8221; but also helped create.  I wish my generation felt a little more of it. After all aren&#8217;t we all &#8220;entitled&#8221; to be treated fairly and appreciated?</p>
<p>My son, even with his point of view has incredible work ethic and skills as do so many of his generation. I believe they will bring about many good changes in the way that company&#8217;s treat their employee&#8217;s &#8211; and that&#8217;s long overdue.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-14583">
        <div id="dsq-comment-header-14583" class="dsq-comment-header">
            <cite id="dsq-cite-14583">
http://www.lifeisntbroken.com                <span id="dsq-author-user-14583">lifeisntbroken</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14583" class="dsq-comment-body">
            <div id="dsq-comment-message-14583" class="dsq-comment-message"><p>Thanks for commenting Cecil</p>
<p>&#8220;Times are changing and opportunities are available to those who have the<br />
 gumption to put forth the effort to learn how the new change will<br />
effect their decisions or vice versa.&#8221;   </p>
<p>How are you defining gumption? Is it opportunity? Innate ability? Access to education? Motivation? Just trying to put it into context.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-14602">
        <div id="dsq-comment-header-14602" class="dsq-comment-header">
            <cite id="dsq-cite-14602">
                <span id="dsq-author-user-14602">Greg Ferguson</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14602" class="dsq-comment-body">
            <div id="dsq-comment-message-14602" class="dsq-comment-message"><p>Who gave us the TV, fast food, and soda pop? I won&#8217;t include drugs because they do not relate to the other three. Fast food is the result of this work ethic your father had and that you want everyone to have. Because everyone is so busy trying to make a buck they have no time to put any care or effort into anything else in their lives, food being a great example. So we have quick, nutritionally void, salty/sugar foods because they are easy and cheap to make.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-14584">
        <div id="dsq-comment-header-14584" class="dsq-comment-header">
            <cite id="dsq-cite-14584">
                <span id="dsq-author-user-14584">Cecil Whorton</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14584" class="dsq-comment-body">
            <div id="dsq-comment-message-14584" class="dsq-comment-message"><p>Gumption = Gitty-up-go  / Get-&#8216;r-done.. (Southern Redneck) among the many of my mom&#8217;s Southern vocabulary terms.<br />
 try &#8220;Push it and shove it.&#8221;  &#8220;Parental guidance&#8221; &#8220;Leader-ship&#8221; &#8220;Purpose&#8221;  &#8220;Christian training and knowledge&#8221;   O.K. ?</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment odd alt thread-odd thread-alt depth-1" id="dsq-comment-14585">
        <div id="dsq-comment-header-14585" class="dsq-comment-header">
            <cite id="dsq-cite-14585">
http://www.lifeisntbroken.com                <span id="dsq-author-user-14585">lifeisntbroken</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14585" class="dsq-comment-body">
            <div id="dsq-comment-message-14585" class="dsq-comment-message"><p>Got it. Thanks.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-14592">
        <div id="dsq-comment-header-14592" class="dsq-comment-header">
            <cite id="dsq-cite-14592">
http://www.lifeisntbroken.com                <span id="dsq-author-user-14592">lifeisntbroken</span>
            </cite>
        </div>
        <div id="dsq-comment-body-14592" class="dsq-comment-body">
            <div id="dsq-comment-message-14592" class="dsq-comment-message"><p>Thanks Monica<br />
I find the more willing I am to not only  listen, but also to reflect on opinions that are different from my own, the more insight I receive. I might not always change my opinion but what I gain from understanding another point of view is always valuable to me. In this case a whole new perspective that I&#8217;d never allowed for opened up. We miss so much by closing ourselves off with judgement and assumptions.</p>
</div>
        </div>

    </li><!-- #comment-## -->
            </ul>


        </div>

    </div>

<script type="text/javascript">
var disqus_url = 'http://switchandshift.com/in-defense-of-entitlement';
var disqus_identifier = '16794 http://switchandshift.com/?p=11794';
var disqus_container_id = 'disqus_thread';
var disqus_shortname = 'switchandshift2';
var disqus_title = "In Defense of Entitlement";
var disqus_config_custom = window.disqus_config;
var disqus_config = function () {
    /*
    All currently supported events:
    onReady: fires when everything is ready,
    onNewComment: fires when a new comment is posted,
    onIdentify: fires when user is authenticated
    */
    
    
    this.language = '';
        this.callbacks.onReady.push(function () {

        // sync comments in the background so we don't block the page
        var script = document.createElement('script');
        script.async = true;
        script.src = '?cf_action=sync_comments&post_id=16794';

        var firstScript = document.getElementsByTagName('script')[0];
        firstScript.parentNode.insertBefore(script, firstScript);
    });
    
    if (disqus_config_custom) {
        disqus_config_custom.call(this);
    }
};

(function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript';
    dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>




		


	</div><aside class="aside beta" style="width: 240px;"><section id="wpb_widget-2" class="section widget widget_wpb_widget"><h2 class="title">About the Author</h2><div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/anita-stout" title="All posts by Anita Stout" rel="author">Anita Stout</a></h3><div class="bio-gravatar"><img src="http://1.gravatar.com/avatar/4b67c48a57351aa83d8ee2df73f9d6ea?s=70&#038;d=http%3A%2F%2Fswitchandshift.com%2Fwp-content%2Fuploads%2F2014%2F10%2Fs-circle-logo-250x250-96x96.png&#038;r=g" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://blog.lifeisntbroken.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="https://www.facebook.com/anita.stout.77" class="bio-icon bio-icon-facebook"></a><a target="_blank" href="https://plus.google.com/u/0/110617096357381981063/posts" class="bio-icon bio-icon-googleplus"></a><p class="bio-description">Anita Stout is an author, and the CEO of Wellness Pays Inc. which focuses on helping people live better lives through better choices physically and financially. She believes her greatest contribution, raising six children, will prove to be her life’s masterpiece.  She recently completed a new book titled Life Isn’t Broken (in editing) and publishes a blog by the same name. Her love of diversity, brilliance, and lively conversation, led her to her husband David, who pastors a different church than she attends and has opposing political views.</p></div></section><section id="time-child-unwrapped-text-7" class="section widget widget-unwrapped-text"><h2 class="title">Subscribe</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</section><section id="adwidget_imagewidget-5" class="section widget AdWidget_ImageWidget"><a target="_blank" href='http://www.inc.com/author/shawn-murphy' alt='Ad'><img  src='http://switchandshift.com/wp-content/uploads/2015/10/Inc-Ad.png' alt='Ad' /></a></section>		<section id="recent-posts-3" class="section widget widget_recent_entries">		<h2 class="title">Recent Articles</h2>		<ul>
					<li>
				<a href="http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community">You Can&#8217;t Do This Alone: Time to Build a Community</a>
						</li>
					<li>
				<a href="http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce">6 Ways to Awaken the Potential in Your Workforce</a>
						</li>
					<li>
				<a href="http://switchandshift.com/our-most-cherished-thanksgiving-traditions">Our Most Cherished Thanksgiving Traditions</a>
						</li>
					<li>
				<a href="http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world">How Gratitude Changes the Way You See the World</a>
						</li>
					<li>
				<a href="http://switchandshift.com/are-you-ready-for-the-digital-workplace">Are You Ready for the Digital Workplace?</a>
						</li>
					<li>
				<a href="http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership">A Necessary Redefinition of Responsibility and Leadership</a>
						</li>
				</ul>
		</section><section id="categories-3" class="section widget widget_categories"><h2 class="title">Articles by Category</h2><label class="screen-reader-text" for="cat">Articles by Category</label><select name='cat' id='cat' class='postform' >
	<option value='-1'>Select Category</option>
	<option class="level-0" value="2196">#SocialLeader</option>
	<option class="level-0" value="588">21st Century Customer Service</option>
	<option class="level-0" value="2454">Being Present</option>
	<option class="level-0" value="32">Blog</option>
	<option class="level-0" value="1126">Bold</option>
	<option class="level-0" value="495">Books</option>
	<option class="level-0" value="1211">Brand Engage</option>
	<option class="level-0" value="104">Business</option>
	<option class="level-0" value="305">Business Heretic&#8217;s Bookstore</option>
	<option class="level-0" value="2508">Change Management</option>
	<option class="level-0" value="1268">Communication</option>
	<option class="level-0" value="654">Company Culture</option>
	<option class="level-0" value="496">Culture</option>
	<option class="level-0" value="2270">Culture Change</option>
	<option class="level-0" value="336">Customer Service</option>
	<option class="level-0" value="1787">Editor&#8217;s Choice</option>
	<option class="level-0" value="2433">Employee Engagement</option>
	<option class="level-0" value="432">Engagement</option>
	<option class="level-0" value="497">Featured</option>
	<option class="level-0" value="1427">Feed Your Brain</option>
	<option class="level-0" value="482">Future Of Leadership</option>
	<option class="level-0" value="1732">Home Page</option>
	<option class="level-0" value="490">Hr</option>
	<option class="level-0" value="2519">Human workplace</option>
	<option class="level-0" value="1385">Humanbiz Community Carnival</option>
	<option class="level-0" value="105">Inspirational</option>
	<option class="level-0" value="103">Leadership</option>
	<option class="level-0" value="1269">Leadership Blog</option>
	<option class="level-0" value="1806">Leadership Presence</option>
	<option class="level-0" value="792">Mentoring</option>
	<option class="level-0" value="1795">Millennials</option>
	<option class="level-0" value="1465">Mini Vlogs</option>
	<option class="level-0" value="1831">My Story Campaign</option>
	<option class="level-0" value="694">New Leadership In The New Economy: Diversity Matters</option>
	<option class="level-0" value="913">Pivot Point</option>
	<option class="level-0" value="2430">Purpose</option>
	<option class="level-0" value="1428">Rebel Heretics Community Feed</option>
	<option class="level-0" value="2395">Rebellious Leaders</option>
	<option class="level-0" value="2391">Rebellious Leadership</option>
	<option class="level-0" value="609">Recognition</option>
	<option class="level-0" value="261">Return On Morale</option>
	<option class="level-0" value="1680">Return On Trust</option>
	<option class="level-0" value="262">Social Era</option>
	<option class="level-0" value="2197">Social Leadership</option>
	<option class="level-0" value="152">Social Media</option>
	<option class="level-0" value="208">Social You</option>
	<option class="level-0" value="841">Strategy</option>
	<option class="level-0" value="493">Strengths</option>
	<option class="level-0" value="796">Switch &Amp; Shift Tv</option>
	<option class="level-0" value="344">Talent</option>
	<option class="level-0" value="1132">The Human Side Tv</option>
	<option class="level-0" value="1061">The Values Revolution</option>
	<option class="level-0" value="363">Transparency</option>
	<option class="level-0" value="101">Uncategorized</option>
	<option class="level-0" value="1125">Vision Cast</option>
	<option class="level-0" value="153">Weekend Post</option>
	<option class="level-0" value="483">Winning Through Engagement</option>
	<option class="level-0" value="1015">Work That Matters Podcast</option>
	<option class="level-0" value="1520">Workplace Morale</option>
	<option class="level-0" value="494">Workplace Optimism</option>
	<option class="level-0" value="180">You: Reinvented</option>
</select>

<script type='text/javascript'>
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "cat" );
	function onCatChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
			location.href = "http://switchandshift.com/?cat=" + dropdown.options[ dropdown.selectedIndex ].value;
		}
	}
	dropdown.onchange = onCatChange;
})();
/* ]]> */
</script>

</section><section id="archives-3" class="section widget widget_archive"><h2 class="title">Archives</h2>		<label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
		<select id="archives-dropdown-3" name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
			
			<option value="">Select Month</option>
				<option value='http://switchandshift.com/2015/11'> November 2015 </option>
	<option value='http://switchandshift.com/2015/10'> October 2015 </option>
	<option value='http://switchandshift.com/2015/09'> September 2015 </option>
	<option value='http://switchandshift.com/2015/08'> August 2015 </option>
	<option value='http://switchandshift.com/2015/07'> July 2015 </option>
	<option value='http://switchandshift.com/2015/06'> June 2015 </option>
	<option value='http://switchandshift.com/2015/05'> May 2015 </option>
	<option value='http://switchandshift.com/2015/04'> April 2015 </option>
	<option value='http://switchandshift.com/2015/03'> March 2015 </option>
	<option value='http://switchandshift.com/2015/02'> February 2015 </option>
	<option value='http://switchandshift.com/2015/01'> January 2015 </option>
	<option value='http://switchandshift.com/2014/12'> December 2014 </option>
	<option value='http://switchandshift.com/2014/11'> November 2014 </option>
	<option value='http://switchandshift.com/2014/10'> October 2014 </option>
	<option value='http://switchandshift.com/2014/09'> September 2014 </option>
	<option value='http://switchandshift.com/2014/08'> August 2014 </option>
	<option value='http://switchandshift.com/2014/07'> July 2014 </option>
	<option value='http://switchandshift.com/2014/06'> June 2014 </option>
	<option value='http://switchandshift.com/2014/05'> May 2014 </option>
	<option value='http://switchandshift.com/2014/04'> April 2014 </option>
	<option value='http://switchandshift.com/2014/03'> March 2014 </option>
	<option value='http://switchandshift.com/2014/02'> February 2014 </option>
	<option value='http://switchandshift.com/2014/01'> January 2014 </option>
	<option value='http://switchandshift.com/2013/12'> December 2013 </option>
	<option value='http://switchandshift.com/2013/11'> November 2013 </option>
	<option value='http://switchandshift.com/2013/10'> October 2013 </option>
	<option value='http://switchandshift.com/2013/09'> September 2013 </option>
	<option value='http://switchandshift.com/2013/08'> August 2013 </option>
	<option value='http://switchandshift.com/2013/07'> July 2013 </option>
	<option value='http://switchandshift.com/2013/06'> June 2013 </option>
	<option value='http://switchandshift.com/2013/05'> May 2013 </option>
	<option value='http://switchandshift.com/2013/04'> April 2013 </option>
	<option value='http://switchandshift.com/2013/03'> March 2013 </option>
	<option value='http://switchandshift.com/2013/02'> February 2013 </option>
	<option value='http://switchandshift.com/2013/01'> January 2013 </option>
	<option value='http://switchandshift.com/2012/12'> December 2012 </option>
	<option value='http://switchandshift.com/2012/11'> November 2012 </option>
	<option value='http://switchandshift.com/2012/10'> October 2012 </option>
	<option value='http://switchandshift.com/2012/09'> September 2012 </option>
	<option value='http://switchandshift.com/2012/08'> August 2012 </option>
	<option value='http://switchandshift.com/2012/07'> July 2012 </option>
	<option value='http://switchandshift.com/2012/06'> June 2012 </option>
	<option value='http://switchandshift.com/2012/05'> May 2012 </option>
	<option value='http://switchandshift.com/2012/04'> April 2012 </option>
	<option value='http://switchandshift.com/2012/03'> March 2012 </option>
	<option value='http://switchandshift.com/2012/02'> February 2012 </option>
	<option value='http://switchandshift.com/2012/01'> January 2012 </option>

		</select>
</section><section id="text-10" class="section widget widget_text">			<div class="textwidget"><iframe src="http://widgets.itunes.apple.com/widget.html?c=us&brc=FFFFFF&blc=FFFFFF&trc=FFFFFF&tlc=FFFFFF&d=Podcast shows we recommend.&t=Get more leadership content&m=podcast&e=podcast&w=220&h=400&ids=741665948,351616584,435836905,647826736,794030859,596047499&wt=playlist&partnerId=&affiliate_id=&at=&ct=" frameborder=0 style="overflow-x:hidden;overflow-y:hidden;width:220px;height: 400px;border:0px; margin-left: -8px;"></iframe></div>
		</section><section id="text-11" class="section widget widget_text"><h2 class="title">Recognition</h2>			<div class="textwidget"><a href="http://servetolead.org/best-21st-century-leadership-blogs-new-media/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://servetolead.org/best-21st-century-leadership-blogs-new-media/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/s2l-badge-21st-century-126x126pr.jpg" style="margin-bottom: 10px;">
</a>

<a href="http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Career-Rocketeer-Career-Blogs-Official-Badge-2014.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.nevermindthemanager.com" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.nevermindthemanager.com', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Leblin-Badge.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.cmoe.com/top-shared-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.cmoe.com/top-shared-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/CMOE-BADGE-2.001.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.skipprichard.com/top-lists/top-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.skipprichard.com/top-lists/top-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/07/top-leadership-blog-award-300x300.png" style="margin-bottom: 10px;">
</a></div>
		</section></aside></div></div>

</div>




		</div>

		<div id="bottom">

			
<div class="outer-container">

	
		<div class="container">

			<section class="section">

				<div class="columns alt-mobile">
					<ul>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-2" class="widget widget-unwrapped-text"><span><img class="alignnone size-full wp-image-3817" src="http://switchandshift.com/wp-content/uploads/2014/08/SS_white_logo500.png" alt="footer-logo" width="250" height="63" />

<p>There’s a more human way to do business.</p>

<p>In the Social Age, it’s how we engage with customers, collaborators and strategic partners that matters; it’s how we create workplace optimism that sets us apart; it’s how we recruit, retain (and repel) employees that becomes our differentiator. This isn’t a “people first, profits second” movement, but a “profits as a direct result of putting people first” movement.</p>
</span></div>							</li>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-4" class="widget widget-unwrapped-text"><h2 class="title">Connect</h2><hr></div><div id="time-child-social-media-3" class="widget widget-social-media"><div class="social-icons native-colors"><ul class="alt"><li><a href="https://twitter.com/switchandshift" target="_blank"><i class="icon-twitter"></i></a></li><li><a href="https://plus.google.com/+Switchandshift1/posts" target="_blank"><i class="icon-gplus"></i></a></li><li><a href="https://www.facebook.com/switchandshift" target="_blank"><i class="icon-facebook"></i></a></li></ul></div></div><div id="time-child-unwrapped-text-3" class="widget widget-unwrapped-text"><hr>
<p>email:<a href="mailto:charmian@switchandshift.com"> connect@switch&shift.com</a> <br/>
1133 Ferreto Parkway<br/>Dayton, NV 89403<br/>
</p>
<hr>
<p>
<a href="http://switchandshift.com/terms-and-conditions/">Terms & Conditions</a> &nbsp;|&nbsp; <a href="http://switchandshift.com/privacy-policy/">Privacy Policy</a>
</p></div><div id="time-child-unwrapped-text-5" class="widget widget-unwrapped-text"><h2 class="title">Newsletter Subscription</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</div>							</li>
													<li class="col-1-3">
								<div id="time-child-contact-2" class="widget widget-contact"><h2 class="title">Contact Us</h2><form class="contact-form" action="http://switchandshift.com/wp-admin/admin-ajax.php" method="post"><input name="action" id="action" type="hidden" value="time-child_contact_form" /><p><input type="text" name="name" placeholder="name*" /></p><p><input type="text" name="email" placeholder="e-mail*" /></p><p><input type="text" name="subject" placeholder="subject" /></p><p><textarea class="full-width" name="message"></textarea></p><p><input name="cntctfrm_contact_action" id="cntctfrm-contact-action" type="hidden" value="true" /><input type="hidden" name="cptch_result" value="0Ws=" />
				<input type="hidden" name="cptch_time" value="1448822388" />
				<input type="hidden" value="Version: 4.1.5" /><label class="cptch_label" for="cptch_input_6"><input id="cptch_input_6" class="cptch_input" type="text" autocomplete="off" name="cptch_number" value="" maxlength="1" size="1" /> &minus; s&#101;ven = 0</label></p><p><input type="submit" value="Send&nbsp;&rsaquo;" /><i class="icon-arrows-ccw load"></i><span class="msg small"></span></p></form></div>							</li>
											</ul>
				</div><!-- // .columns -->

			</section>

		</div><!-- // .container -->

	
</div><!-- // .outer-container -->
							<footer class="footer">

					<div class="container">

						<div class="section">
							<p class="small alpha">&copy; Copyright 2015 Switch &amp; Shift</p>
							<p class="small beta">Website Developed by <a href="http:/solutionsred.com" target="blank">Solutions Red</a></p>
						</div>

					</div>

				</footer>
			
		</div>

		        <script type="text/javascript">
        // <![CDATA[
        var disqus_shortname = 'switchandshift2';
        (function () {
            var nodes = document.getElementsByTagName('span');
            for (var i = 0, url; i < nodes.length; i++) {
                if (nodes[i].className.indexOf('dsq-postid') != -1) {
                    nodes[i].parentNode.setAttribute('data-disqus-identifier', nodes[i].getAttribute('data-dsqidentifier'));
                    url = nodes[i].parentNode.href.split('#', 1);
                    if (url.length == 1) { url = url[0]; }
                    else { url = url[1]; }
                    nodes[i].parentNode.href = url + '#disqus_thread';
                }
            }
            var s = document.createElement('script'); 
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
        // ]]>
        </script>
        <link rel='stylesheet' id='author-bio-box-styles-css'  href='http://switchandshift.com/wp-content/plugins/author-bio-box/assets/css/author-bio-box.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/comment-reply.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/3rd-party.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/time.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>

	</body>

</html>
<!-- Dynamic page generated in 1.055 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:39:49 -->
