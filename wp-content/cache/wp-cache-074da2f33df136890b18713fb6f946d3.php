<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	
	>
<channel>
	<title>Comments on: Creating Personal Connection in an Electronically Connected World</title>
	<atom:link href="http://switchandshift.com/creating-personal-connection-in-an-electronically-connected-world/feed" rel="self" type="application/rss+xml" />
	<link>http://switchandshift.com/creating-personal-connection-in-an-electronically-connected-world</link>
	<description>Human Side of Business</description>
	<lastBuildDate>Sun, 29 Nov 2015 15:25:00 +0000</lastBuildDate>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	
	<item>
		<title>By: Monthly Mash and Self Service</title>
		<link>http://switchandshift.com/creating-personal-connection-in-an-electronically-connected-world#comment-15441</link>
		<dc:creator><![CDATA[Monthly Mash and Self Service]]></dc:creator>
		<pubDate>Wed, 06 Aug 2014 17:44:40 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=5574#comment-15441</guid>
		<description><![CDATA[[&#8230;] Creating Personal Connection in an Electronically Connected World &#8211; A great guide for companies or brands struggling to give their digital communication a personal touch. [&#8230;]]]></description>
		<content:encoded><![CDATA[<p>[&#8230;] Creating Personal Connection in an Electronically Connected World &#8211; A great guide for companies or brands struggling to give their digital communication a personal touch. [&#8230;]</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: How Great Companies Invest in True Leadership</title>
		<link>http://switchandshift.com/creating-personal-connection-in-an-electronically-connected-world#comment-10838</link>
		<dc:creator><![CDATA[How Great Companies Invest in True Leadership]]></dc:creator>
		<pubDate>Sat, 15 Jun 2013 16:00:52 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=5574#comment-10838</guid>
		<description><![CDATA[[...] my take on creating an authentic and compelling approach to your career, with a focus on the value of your personal [...]]]></description>
		<content:encoded><![CDATA[<p>[&#8230;] my take on creating an authentic and compelling approach to your career, with a focus on the value of your personal [&#8230;]</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: TedCoine</title>
		<link>http://switchandshift.com/creating-personal-connection-in-an-electronically-connected-world#comment-10711</link>
		<dc:creator><![CDATA[TedCoine]]></dc:creator>
		<pubDate>Thu, 30 May 2013 12:36:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=5574#comment-10711</guid>
		<description><![CDATA[Marilyn,


This post is just awesome - as I knew it would be! We&#039;re honored to host this post here at Switch and Shift. 


You make several compelling points here. In the interests of brevity, let me focus on one: bringing an irate customer from social media (or email) to the phone as quickly as possible in order to give them a more human touch - so they can hear the empathy in your voice, and so they can feel how important their complaint is to this previously nameless, faceless company. As Tony Hsei of Zappos says, &quot;The phone is the ultimate social medium.&quot; Even though his company is a true pioneer of company-wide use of social (especially Twitter), they recognize the superior value of the phone. Food for thought for the rest of us, I hope.


***
As you know, a few years ago, I read your and Lori Jo&#039;s book, &quot;Who&#039;s Your Gladys?&quot; which I continue to recommend to clients all the time. Congratulations on this month&#039;s publication of the paperback version! 



READERS: you will love their book. You will learn some invaluable insights from it that you can apply at work immediately for improved results. Two thumbs way up. Buy it. Read it. Underline its best passages, dog-ear its pages. Buy another copy for a friend.]]></description>
		<content:encoded><![CDATA[<p>Marilyn,</p>
<p>This post is just awesome &#8211; as I knew it would be! We&#8217;re honored to host this post here at Switch and Shift. </p>
<p>You make several compelling points here. In the interests of brevity, let me focus on one: bringing an irate customer from social media (or email) to the phone as quickly as possible in order to give them a more human touch &#8211; so they can hear the empathy in your voice, and so they can feel how important their complaint is to this previously nameless, faceless company. As Tony Hsei of Zappos says, &#8220;The phone is the ultimate social medium.&#8221; Even though his company is a true pioneer of company-wide use of social (especially Twitter), they recognize the superior value of the phone. Food for thought for the rest of us, I hope.</p>
<p>***<br />
As you know, a few years ago, I read your and Lori Jo&#8217;s book, &#8220;Who&#8217;s Your Gladys?&#8221; which I continue to recommend to clients all the time. Congratulations on this month&#8217;s publication of the paperback version! </p>
<p>READERS: you will love their book. You will learn some invaluable insights from it that you can apply at work immediately for improved results. Two thumbs way up. Buy it. Read it. Underline its best passages, dog-ear its pages. Buy another copy for a friend.</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: TedCoine</title>
		<link>http://switchandshift.com/creating-personal-connection-in-an-electronically-connected-world#comment-10710</link>
		<dc:creator><![CDATA[TedCoine]]></dc:creator>
		<pubDate>Thu, 30 May 2013 12:17:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=5574#comment-10710</guid>
		<description><![CDATA[Michelle, you couldn&#039;t be more right about that! What&#039;s the difference, really? If we treated EVERYONE with whom we worked as a customer, someone who deserves five-star service from us - respect, honor, our full attention and our devotion to exceeding their expectations and needs - then I think a whole lot of the ills of our working life would slip away, cured.]]></description>
		<content:encoded><![CDATA[<p>Michelle, you couldn&#8217;t be more right about that! What&#8217;s the difference, really? If we treated EVERYONE with whom we worked as a customer, someone who deserves five-star service from us &#8211; respect, honor, our full attention and our devotion to exceeding their expectations and needs &#8211; then I think a whole lot of the ills of our working life would slip away, cured.</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: Michelle Pokorny</title>
		<link>http://switchandshift.com/creating-personal-connection-in-an-electronically-connected-world#comment-10686</link>
		<dc:creator><![CDATA[Michelle Pokorny]]></dc:creator>
		<pubDate>Tue, 28 May 2013 21:18:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=5574#comment-10686</guid>
		<description><![CDATA[Thanks for the post.  The interesting thing as I read this - employess, IMHO, want all the same things.]]></description>
		<content:encoded><![CDATA[<p>Thanks for the post.  The interesting thing as I read this &#8211; employess, IMHO, want all the same things.</p>
]]></content:encoded>
	</item>
</channel>
</rss>

<!-- Dynamic page generated in 1.081 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:10:29 -->
