<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	
	>
<channel>
	<title>Comments on: We Succeed When We Give Recognition</title>
	<atom:link href="http://switchandshift.com/we-succeed-when-we-give-recognition/feed" rel="self" type="application/rss+xml" />
	<link>http://switchandshift.com/we-succeed-when-we-give-recognition</link>
	<description>Human Side of Business</description>
	<lastBuildDate>Sun, 29 Nov 2015 15:25:00 +0000</lastBuildDate>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	
	<item>
		<title>By: It’s A Culture, Not A Cult</title>
		<link>http://switchandshift.com/we-succeed-when-we-give-recognition#comment-13667</link>
		<dc:creator><![CDATA[It’s A Culture, Not A Cult]]></dc:creator>
		<pubDate>Mon, 17 Mar 2014 18:00:45 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=7408#comment-13667</guid>
		<description><![CDATA[[&#8230;] couldn’t think of anymore U’s so lets go to V. People need to feel validated and valued for what they do.  Also, bring value to your company and share value with others.  Both employees [&#8230;]]]></description>
		<content:encoded><![CDATA[<p>[&#8230;] couldn’t think of anymore U’s so lets go to V. People need to feel validated and valued for what they do.  Also, bring value to your company and share value with others.  Both employees [&#8230;]</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: It's a Culture, Not a Cult &#124; Switch and Shift</title>
		<link>http://switchandshift.com/we-succeed-when-we-give-recognition#comment-13429</link>
		<dc:creator><![CDATA[It's a Culture, Not a Cult &#124; Switch and Shift]]></dc:creator>
		<pubDate>Fri, 28 Feb 2014 15:01:34 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=7408#comment-13429</guid>
		<description><![CDATA[[&#8230;] couldn’t think of anymore U’s so lets go to V. People need to feel validated and valued for what they do.  Also, bring value to your company and share value with others.  Both employees [&#8230;]]]></description>
		<content:encoded><![CDATA[<p>[&#8230;] couldn’t think of anymore U’s so lets go to V. People need to feel validated and valued for what they do.  Also, bring value to your company and share value with others.  Both employees [&#8230;]</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: tikoo</title>
		<link>http://switchandshift.com/we-succeed-when-we-give-recognition#comment-11832</link>
		<dc:creator><![CDATA[tikoo]]></dc:creator>
		<pubDate>Tue, 15 Oct 2013 10:08:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=7408#comment-11832</guid>
		<description><![CDATA[A great article and a valuable guidance to be a team player, to lead and be led!]]></description>
		<content:encoded><![CDATA[<p>A great article and a valuable guidance to be a team player, to lead and be led!</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: Roy Saunderson</title>
		<link>http://switchandshift.com/we-succeed-when-we-give-recognition#comment-11755</link>
		<dc:creator><![CDATA[Roy Saunderson]]></dc:creator>
		<pubDate>Mon, 07 Oct 2013 17:55:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=7408#comment-11755</guid>
		<description><![CDATA[Love this connection, William, through Switch and Shift!
Recognition is clearly a felt phenomenon. I agree that recognition stems first from mindfulness and gratitude and is given root and freedom through the culture of people.
Keep engaging people!
Roy]]></description>
		<content:encoded><![CDATA[<p>Love this connection, William, through Switch and Shift!<br />
Recognition is clearly a felt phenomenon. I agree that recognition stems first from mindfulness and gratitude and is given root and freedom through the culture of people.<br />
Keep engaging people!<br />
Roy</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: Roy Saunderson</title>
		<link>http://switchandshift.com/we-succeed-when-we-give-recognition#comment-11754</link>
		<dc:creator><![CDATA[Roy Saunderson]]></dc:creator>
		<pubDate>Mon, 07 Oct 2013 17:52:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=7408#comment-11754</guid>
		<description><![CDATA[Ted: Sure appreciate the life sharing from your first job and your kind feedback on this post.
Let&#039;s all commit to giving heartfelt recognition to each person we feel to give to.
Great connecting through Switch and Shift!
Roy]]></description>
		<content:encoded><![CDATA[<p>Ted: Sure appreciate the life sharing from your first job and your kind feedback on this post.<br />
Let&#8217;s all commit to giving heartfelt recognition to each person we feel to give to.<br />
Great connecting through Switch and Shift!<br />
Roy</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: William Powell</title>
		<link>http://switchandshift.com/we-succeed-when-we-give-recognition#comment-11721</link>
		<dc:creator><![CDATA[William Powell]]></dc:creator>
		<pubDate>Wed, 02 Oct 2013 17:54:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=7408#comment-11721</guid>
		<description><![CDATA[Recognition is definitely a precursor for engagement Roy. It&#039;s great that you asked that question. Of course engaged people will offer more recognition, but the cycle has to begin somewhere. 



Engagement comes from an emotional response to culture. Recognition is born in the emotion of gratitude and exemplified by leadership. Such an important key to having a healthy and profitable organization. 



Thanks for the awesome reminder!!]]></description>
		<content:encoded><![CDATA[<p>Recognition is definitely a precursor for engagement Roy. It&#8217;s great that you asked that question. Of course engaged people will offer more recognition, but the cycle has to begin somewhere. </p>
<p>Engagement comes from an emotional response to culture. Recognition is born in the emotion of gratitude and exemplified by leadership. Such an important key to having a healthy and profitable organization. </p>
<p>Thanks for the awesome reminder!!</p>
]]></content:encoded>
	</item>
	<item>
		<title>By: TedCoine</title>
		<link>http://switchandshift.com/we-succeed-when-we-give-recognition#comment-11719</link>
		<dc:creator><![CDATA[TedCoine]]></dc:creator>
		<pubDate>Wed, 02 Oct 2013 16:06:00 +0000</pubDate>
		<guid isPermaLink="false">http://switchandshift.com/?p=7408#comment-11719</guid>
		<description><![CDATA[Roy, 

When I think &quot;who is my go-to guy for advice on recognition?&quot; your name comes to me 10 out of the first 10 times*  - so when I tell you that you nailed it yet again with an outstanding post... Of course you did! I learn something new and important from you every time we speak and every time you write. 


With this post in particular, your final point, to learn what your peers like and dislike first - that&#039;s really important and often overlooked! You may think, &quot;I like this, so of course she will, too. Who wouldn&#039;t?&quot; But just as giving your spouse a bowling ball with your name on it isn&#039;t all that romantic, praising your peer in front of a large group when they are painfully shy is unlikely to be received as well as you had hoped.

I was really lucky at my first job out of college, when our leaders modeled recognition and actually taught it, as in, literally, &quot;This is how you do it. Like this...&quot; So few of us have experience with giving heartfelt recognition, and if we don&#039;t, we may feel that type of thing is too &quot;mushy,&quot; makes us look weak, or some other nonsense we picked up perhaps through osmosis as we were growing up in what can still sometimes be a dour culture. The Puritans may have died long ago, but their cultural baggage lives on.

My favorite line will always be from Dale Carnegie: &quot;Be hearty in your approbation and lavish in your praise.” That Dale guy wasn&#039;t all bad, was he?

*My go-to gal? Margy Bresslour, without a doubt! http://switchandshift.com/author/margy-bresslour]]></description>
		<content:encoded><![CDATA[<p>Roy, </p>
<p>When I think &#8220;who is my go-to guy for advice on recognition?&#8221; your name comes to me 10 out of the first 10 times*  &#8211; so when I tell you that you nailed it yet again with an outstanding post&#8230; Of course you did! I learn something new and important from you every time we speak and every time you write. </p>
<p>With this post in particular, your final point, to learn what your peers like and dislike first &#8211; that&#8217;s really important and often overlooked! You may think, &#8220;I like this, so of course she will, too. Who wouldn&#8217;t?&#8221; But just as giving your spouse a bowling ball with your name on it isn&#8217;t all that romantic, praising your peer in front of a large group when they are painfully shy is unlikely to be received as well as you had hoped.</p>
<p>I was really lucky at my first job out of college, when our leaders modeled recognition and actually taught it, as in, literally, &#8220;This is how you do it. Like this&#8230;&#8221; So few of us have experience with giving heartfelt recognition, and if we don&#8217;t, we may feel that type of thing is too &#8220;mushy,&#8221; makes us look weak, or some other nonsense we picked up perhaps through osmosis as we were growing up in what can still sometimes be a dour culture. The Puritans may have died long ago, but their cultural baggage lives on.</p>
<p>My favorite line will always be from Dale Carnegie: &#8220;Be hearty in your approbation and lavish in your praise.” That Dale guy wasn&#8217;t all bad, was he?</p>
<p>*My go-to gal? Margy Bresslour, without a doubt! <a href="http://switchandshift.com/author/margy-bresslour" rel="nofollow">http://switchandshift.com/author/margy-bresslour</a></p>
]]></content:encoded>
	</item>
</channel>
</rss>

<!-- Dynamic page generated in 0.549 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:15:53 -->
