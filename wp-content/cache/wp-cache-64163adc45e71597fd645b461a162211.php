<?php die(); ?>



<!DOCTYPE html>

<!--[if lt IE 9]>             <html class="no-js ie lt-ie9" lang="en-US" prefix="og: http://ogp.me/ns#""><![endif]-->

<!--[if IE 9]>                <html class="no-js ie ie9" lang="en-US" prefix="og: http://ogp.me/ns#">   <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js no-ie" lang="en-US" prefix="og: http://ogp.me/ns#">    <!--<![endif]-->

	<head>

		<meta charset="UTF-8" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />

		<!--[if lt IE 9]>

			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

			<script src="http://switchandshift.com/wp-content/themes/time/data/js/selectivizr.min.js"></script>

		<![endif]-->

		<title>Top 11 SwitchandShift TV Episodes Ever - Switch &amp; Shift</title>
		<title>Top 11 SwitchandShift TV Episodes Ever - Switch &amp; Shift</title>
<meta property="og:site_name" content="Switch &amp; Shift" /><meta property="og:title" content="Switch &amp; Shift" /><meta property="og:locale" content="en_US" /><meta property="og:url" content="http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever" /><meta property="og:description" content="So far on Switch and Shift TV, we’ve interviewed bestselling authors, CEOs, a presentation coach to the C-Suite with Broadway directing credits, a venture capitalist and inventor, a massively successful Fortune 250 CIO, a teacher whose TED Talk [...]" /><meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2013/09/tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x3001.jpg" />
<!-- This site is optimized with the Yoast SEO plugin v2.3.5 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Top 11 SwitchandShift TV Episodes Ever - Switch &amp; Shift" />
<meta property="og:description" content="So far on Switch and Shift TV, we’ve interviewed bestselling authors, CEOs, a presentation coach to the C-Suite with Broadway directing credits, a venture capitalist and inventor, a massively successful Fortune 250 CIO, a teacher whose TED Talk went viral… and we’re just getting started! What do all of these fascinating people have in common? &hellip; Read more... Read More&raquo;" />
<meta property="og:url" content="http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever" />
<meta property="og:site_name" content="Switch &amp; Shift" />
<meta property="article:tag" content="Leaders" />
<meta property="article:tag" content="Switch And Shift Tv" />
<meta property="article:section" content="Business" />
<meta property="article:published_time" content="2013-09-29T05:00:29+00:00" />
<meta property="article:modified_time" content="2014-09-10T03:52:03+00:00" />
<meta property="og:updated_time" content="2014-09-10T03:52:03+00:00" />
<meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2013/09/tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x3001.jpg" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="So far on Switch and Shift TV, we’ve interviewed bestselling authors, CEOs, a presentation coach to the C-Suite with Broadway directing credits, a venture capitalist and inventor, a massively successful Fortune 250 CIO, a teacher whose TED Talk went viral… and we’re just getting started! What do all of these fascinating people have in common? [&hellip;] Read more... Read More&raquo;"/>
<meta name="twitter:title" content="Top 11 SwitchandShift TV Episodes Ever - Switch &amp; Shift"/>
<meta name="twitter:domain" content="Switch &amp; Shift"/>
<meta name="twitter:image" content="http://switchandshift.com/wp-content/uploads/2013/09/tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x3001.jpg"/>
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Feed" href="http://switchandshift.com/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Comments Feed" href="http://switchandshift.com/comments/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Top 11 SwitchandShift TV Episodes Ever Comments Feed" href="http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever/feed" />
<!-- This site is powered by Shareaholic - https://shareaholic.com -->
<script type='text/javascript' data-cfasync='false'>
  //<![CDATA[
    _SHR_SETTINGS = {"endpoints":{"local_recs_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_permalink_related","share_counts_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_share_counts_api"}};
  //]]>
</script>
<script type='text/javascript'
        src='//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js'
        data-shr-siteid='22c416cd99bd31d22f03d82d3a8395e6'
        data-cfasync='false'
        async='async' >
</script>

<!-- Shareaholic Content Tags -->
<meta name='shareaholic:site_name' content='Switch &amp; Shift' />
<meta name='shareaholic:language' content='en-US' />
<meta name='shareaholic:url' content='http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever' />
<meta name='shareaholic:keywords' content='leaders, switch and shift tv, switch and shift, joy, meaningful work, business, featured, leadership, switch &amp;amp; shift tv, the human side tv' />
<meta name='shareaholic:article_published_time' content='2013-09-29T12:00:29+00:00' />
<meta name='shareaholic:article_modified_time' content='2015-11-29T11:00:38+00:00' />
<meta name='shareaholic:shareable_page' content='true' />
<meta name='shareaholic:article_author_name' content='Ted Coiné' />
<meta name='shareaholic:site_id' content='22c416cd99bd31d22f03d82d3a8395e6' />
<meta name='shareaholic:wp_version' content='7.6.2.3' />
<meta name='shareaholic:image' content='http://switchandshift.com/wp-content/uploads/2013/09/tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x3001.jpg' />
<!-- Shareaholic Content Tags End -->
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/switchandshift.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=1d05468803804e8153cb17eab60a20d0"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='flick-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp//css/flick/flick.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimpSF_main_css-css'  href='http://switchandshift.com/?mcsf_action=main_css&#038;ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!--[if IE]>
<link rel='stylesheet' id='mailchimpSF_ie_css-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp/css/ie.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='wild-googlemap-frontend-css'  href='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-css-frontend.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='cptch_stylesheet-css'  href='http://switchandshift.com/wp-content/plugins/captcha/css/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='tm_clicktotweet-css'  href='http://switchandshift.com/wp-content/plugins/click-to-tweet-by-todaymade/assets/css/styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='se-link-styles-css'  href='http://switchandshift.com/wp-content/plugins/search-everything/static/css/se-styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://switchandshift.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-3rd-party-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/3rd-party.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-style-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/style.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-scheme-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/bright.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-stylesheet-css'  href='http://switchandshift.com/wp-content/themes/time_child/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-mobile-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/mobile.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='only screen and (max-width: 767px)' />
<link rel='stylesheet' id='author-avatars-widget-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/widget.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='author-avatars-shortcode-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/shortcode.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='author-bio-box-styles-css'  href='http://switchandshift.com/wp-content/plugins/author-bio-box/assets/css/author-bio-box.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!-- This site uses the Google Analytics by Yoast plugin v5.4.6 - Universal disabled - https://yoast.com/wordpress/plugins/google-analytics/ -->
<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-27884223-1']);
	_gaq.push(['_gat._forceSSL']);
	_gaq.push(['_trackPageview']);

	(function () {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();

</script>
<!-- / Google Analytics by Yoast -->
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/scrollTo.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.form.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mailchimpSF = {"ajax_url":"http:\/\/switchandshift.com\/"};
/* ]]> */
</script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/mailchimp.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/ui/core.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp//js/datepicker.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?sensor=false&#038;ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js-frontend.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://switchandshift.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://switchandshift.com/wp-includes/wlwmanifest.xml" /> 

<link rel='shortlink' href='http://switchandshift.com/?p=12374' />
<!-- Custom Code Start-->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '811956965544989']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=811956965544989&ev=PixelInitialized" /></noscript>
<!-- Custom Code Start-->
	<script type="text/javascript">
		jQuery(function($) {
			$('.date-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd/yyyy';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: true,
					beforeShow: function(input, inst) { $('#ui-datepicker-div').addClass('show'); },
					dateFormat: format.toLowerCase(),
				});
			});
			d = new Date();
			$('.birthdate-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: false,
					minDate: new Date(d.getFullYear(), 1-1, 1),
					maxDate: new Date(d.getFullYear(), 12-1, 31),
					beforeShow: function(input, inst) { $('#ui-datepicker-div').removeClass('show'); },
					dateFormat: format.toLowerCase(),
				});

			});

		});
	</script>
	<script type="text/javascript">
	window._se_plugin_version = '8.1.3';
</script>
<style>a, a.alt:hover, .alt a:hover, #bottom a.alt:hover, #bottom .alt a:hover, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, input[type="button"].active, button.active, .button.active, .color, .super-tabs > div > .nav h2 span, .toggles > div > h3:hover > i, .logo, nav a:hover, #bottom nav a:hover, nav .current > a, nav .current>a:hover{color:#0292b7}mark, .slider .control-nav li a:hover, .slider .control-nav li a.active, #top:before, #top > .before, .background-color, nav.mobile a:hover, nav.mobile .current > a, .mejs-controls .mejs-time-rail .mejs-time-loaded, .mejs-controls .mejs-time-rail .mejs-time-current{background-color:#0292b7}.zoom-hover>.zoom-hover-overlay{background-color:rgba(2, 146, 183, 0.75)}blockquote.bar, .sticky:before, #bottom .outer-container{border-color:#0292b7}body,input,select,textarea,button,.button{font-family:Raleway;font-size:13px;line-height:23px}nav.primary ul, nav.primary a:not(:hover), nav.mobile ul, nav.mobile a:not(:hover){font-family:Raleway;font-size:15px;font-weight:bold;font-style:normal;text-decoration:none}nav.secondary ul, nav.secondary a:not(:hover){font-family:Raleway;font-size:12px;font-weight:bold;font-style:normal;text-decoration:none}.headline h1{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}.headline .breadcrumbs{font-family:Raleway;font-size:15px;font-weight:normal;font-style:normal;text-decoration:none}#top .widget>.title{font:normal 18px/128% Raleway;text-decoration:none}.post h1.title{font:bold 22px/128% Raleway;text-decoration:none}h1{font:bold 22px/128% Raleway;text-decoration:none}h2{font:bold 18px/128% Raleway;text-decoration:none}h3{font:bold 14px/128% Raleway;text-decoration:none}h4{font:bold 14px/128% Raleway;text-decoration:none}input[type="submit"]:not(.big):not(.huge),input[type="reset"]:not(.big):not(.huge),input[type="button"]:not(.big):not(.huge),button:not(.big):not(.huge),.button:not(.big):not(.huge){font-family:Raleway;font-size:14px;font-weight:normal;font-style:normal;text-decoration:none}input[type="submit"].big,input[type="reset"].big,input[type="button"].big,button.big,.button.big{font-family:Raleway;font-size:18px;font-weight:bold;font-style:normal;text-decoration:none}input[type="submit"].huge,input[type="reset"].huge,input[type="button"].huge,button.huge,.button.huge{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}</style>
<script>if(typeof WebFont!='undefined'){WebFont.load({google:{families:["Raleway:400,700:latin"]},active:function(){if(document.createEvent){var e=document.createEvent('HTMLEvents');e.initEvent('webfontactive',true,false);document.dispatchEvent(e);}else{document.documentElement['webfontactive']++;}}});}timeConfig={templatePath:'http://switchandshift.com/wp-content/themes/time',zoomHoverIcons:{"default":"icon-plus-circled","image":"icon-search","mail":"icon-mail","title":"icon-right"},flexsliderOptions:{"animation":"slide","direction":"horizontal","animationSpeed":600,"slideshowSpeed":7000,"slideshow":false},layersliderOptions:{}};(function($){$(document).ready(function($){$('.widget_pages, .widget_archive, .widget_categories, .widget_recent_entries, .widget_recent_comments, .widget_display_forums, .widget_display_replies, .widget_display_topics, .widget_display_views').each(function(){$('ul',this).addClass('fancy alt');$('li',this).prepend($('<i />',{'class':'icon-right-open'}));if($(this).closest('#top').length>0){$('i',this).addClass('color');}});$('#disqus_thread').addClass('section');});})(jQuery);</script>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//switchandshift.com/?wordfence_logHuman=1&hid=33AF8F1815EA0BAFB9BA5B155A89018E');
</script><link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250-48x48.png" sizes="32x32" />
<link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">
<meta name="msapplication-TileImage" content="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">


<!-- Google Code for Remarketing Tag -->
<!-
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 954219818;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/954219818/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


	</head>



	<body class="time-child-3-6-1-child-1-0 single single-post postid-12374 single-format-standard layout-boxed scheme-bright">



		<div id="top" >



			
	<div class="backgrounds"><div style="background: url(&quot;http://switchandshift.com/wp-content/uploads/2013/09/tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x3001.jpg&quot;) #ffffff no-repeat center top fixed; background-size: cover;" class="stripes"></div></div>



			<div class="upper-container ">



				<div class="outer-container">



					
						<nav id="menu" class="mobile">

							<ul id="menu-main-menu" class=""><li id="menu-item-3819" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-4305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li id="menu-item-4304" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li id="menu-item-4303" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li id="menu-item-19075" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li id="menu-item-19071" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li id="menu-item-4306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li id="menu-item-22648" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li id="menu-item-4177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li id="menu-item-21828" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li id="menu-item-23151" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
</ul>
						</nav>

					


					
						<nav id="search" class="mobile">

							<form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form>
						</nav>

					


				</div>



				<div class="outer-container ">



					<header class="header">



						<div class="container">



							<div class="mobile-helper vertical-align">



								
									<a href="#menu" class="button" title="Menu"><i class="icon-menu"></i></a>

								






								
									<a href="#search" class="button" title="Search"><i class="icon-search"></i></a>

								


							</div>



							


							<h1 class="logo vertical-align"><a href="http://switchandshift.com/" title="Switch &amp; Shift" rel="home"><img src="http://switchandshift.com/wp-content/uploads/2014/07/logo.jpg" width="244" height="50" data-2x="http://switchandshift.com/wp-content/uploads/2014/07/logo-2x.jpg" alt="Switch &amp; Shift" /></a></h1>


							
								<nav class="primary vertical-align">

									<ul id="menu-main-menu-1" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
<li><form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form></li></ul>
								</nav>

							


						</div>



					</header>



					
<nav class="secondary">
	<div class="container">
		<ul id="menu-secondary-menu" class=""><li id="menu-item-19073" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19073"><a href="http://switchandshift.com/work-that-matters-podcast">Work That Matters Podcast</a></li>
<li id="menu-item-21566" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21566"><a href="http://switchandshift.com/speaking-under-construction">Speaking</a></li>
<li id="menu-item-4334" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4334"><a href="http://switchandshift.com/consulting">Consulting</a></li>
<li id="menu-item-4331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4331"><a href="http://switchandshift.com/a-world-gone-social">A World Gone Social</a></li>
<li id="menu-item-20424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20424"><a href="http://switchandshift.com/writing-guidelines">Writing Guidelines</a></li>
<li id="menu-item-19074" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19074"><a href="http://switchandshift.com/contact">Contact Us</a></li>
</ul>	</div>
</nav>


				</div>



			</div>



			

			

<div class="outer-container">
	
	<nav class="secondary">
		<div class="container">
			<div class="menu-secondary-menu-container"><ul id="menu-secondary-menu-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19073"><a href="http://switchandshift.com/work-that-matters-podcast">Work That Matters Podcast</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21566"><a href="http://switchandshift.com/speaking-under-construction">Speaking</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4334"><a href="http://switchandshift.com/consulting">Consulting</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4331"><a href="http://switchandshift.com/a-world-gone-social">A World Gone Social</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20424"><a href="http://switchandshift.com/writing-guidelines">Writing Guidelines</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19074"><a href="http://switchandshift.com/contact">Contact Us</a></li>
</ul></div>    		
<nav class="secondary">
	<div class="container">
		<ul id="menu-secondary-menu-2" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19073"><a href="http://switchandshift.com/work-that-matters-podcast">Work That Matters Podcast</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21566"><a href="http://switchandshift.com/speaking-under-construction">Speaking</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4334"><a href="http://switchandshift.com/consulting">Consulting</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4331"><a href="http://switchandshift.com/a-world-gone-social">A World Gone Social</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20424"><a href="http://switchandshift.com/writing-guidelines">Writing Guidelines</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19074"><a href="http://switchandshift.com/contact">Contact Us</a></li>
</ul>	</div>
</nav>		</div>
	</nav>

	<div class="content"><div class="container"><div class="main alpha" style="padding: 0 240px 0 0px; margin: 0 -240px 0 -0px;">


		


			<section class="section">

				<article id="post-12374" class="post post-12374 type-post status-publish format-standard has-post-thumbnail hentry category-business category-featured category-leadership category-switch-shift-tv category-the-human-side-tv-2 category-uncategorized tag-leaders tag-switch-and-shift-tv">
<img width="700" height="300" src="http://switchandshift.com/wp-content/uploads/2013/09/tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x3001.jpg" class="attachment-700 wp-post-image" alt="tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x300" 0="" data-2x="http://switchandshift.com/wp-content/uploads/2013/09/tv2_Image-credit-a-hrefhttp-__www.123rf.com_photo_10423711_retro-red-tv.htmlfilip323-_-123RF-Stock-Photo_a700x3001.jpg" />					

	<h1 class="title entry-title">Top 11 SwitchandShift TV Episodes Ever</h1>

                    

					<div class='shareaholic-canvas' data-app-id='21423060' data-app='share_buttons' data-title='Top 11 SwitchandShift TV Episodes Ever' data-link='http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever' data-summary=''></div><p>So far on Switch and Shift TV, we’ve interviewed bestselling authors, CEOs, a presentation coach to the C-Suite with Broadway directing credits, a venture capitalist and inventor, a massively successful Fortune 250 CIO, a teacher whose TED Talk went viral… and we’re just getting started!</p>
<p>What do all of these fascinating people have in common? Each one, in his or her own way, is an Extraordinary Thinker (whether they’re officially in the League or not), and each is an active proponent of The Human Side of Business.</p>
<p>Switch and Shift TV is just gaining steam – indeed, we’re working with a TV producer to see if we can’t marry old media with new to expand the reach of our message (stay tuned for more on that as it develops). And, as I’m one who must learn by doing, I think you’ll find my interviewing skills continue to grow over time. This will help me better showcase the thought leadership of our guests.</p>
<p>Have you missed an episode of Switch and Shift TV?</p>
<p>They’re all listed below, from the latest on down to the first. We recommend you bookmark this page so you can return to it as often as you need until you catch up. This also makes a really easy way to share the list with your colleagues, so they can catch up, too.</p>
<p>Or, better yet: schedule a brown-bag lunch and watch it with your whole team. We actually got this idea from a viewer who does just that. She says it’s a great way to spark conversation for the rest of the hour, to discuss how their company can adopt some of the ideas our guests bring to light.</p>
<h3><span style="color: #0091ba;">Switch and Shift TV with Ori Brafman, author of <a href="http://www.amazon.com/The-Chaos-Imperative-Disruption-Effectiveness/dp/0307886670/ref=sr_1_1?ie=UTF8&amp;qid=1380428628&amp;sr=8-1&amp;keywords=ori+brafman" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.amazon.com/The-Chaos-Imperative-Disruption-Effectiveness/dp/0307886670/ref=sr_1_1?ie=UTF8&amp;qid=1380428628&amp;sr=8-1&amp;keywords=ori+brafman', 'The Chaos Imperative: How Chance and Disruption Increase Innovation, Effectiveness, and Success']);" ><em>The Chaos Imperative: How Chance and Disruption Increase Innovation, Effectiveness, and Success</em></a></span><br />
<div class="embed"><iframe src="//www.youtube.com/embed/9k8yvyQaAY0" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Johann Gauthier</span> <div class="embed"><iframe src="//www.youtube.com/embed/VPNsqe2uwJ0" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Frank Wander </span><div class="embed"><iframe src="//www.youtube.com/embed/3f8rtC79Ok0" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Achim Nowak</span> <div class="embed"><iframe src="//www.youtube.com/embed/y0TIQRO18Q4" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Deb Mills-Scofield</span> <div class="embed"><iframe src="//www.youtube.com/embed/4p1duipyZnk" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Angela Maiers</span> <div class="embed"><iframe src="//www.youtube.com/embed/YbroQoX0yEI" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with CJ Alvarado </span><div class="embed"><iframe src="//www.youtube.com/embed/CkqwOWBwcM4" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with William Powell</span> <div class="embed"><iframe src="//www.youtube.com/embed/8APoYKgljK4" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Stan Phelps, author of </span><a href="http://www.amazon.com/Whats-Your-Green-Goldfish-Engagement/dp/0984983813/ref=sr_1_2?ie=UTF8&amp;qid=1380428656&amp;sr=8-2&amp;keywords=stan+phelps" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.amazon.com/Whats-Your-Green-Goldfish-Engagement/dp/0984983813/ref=sr_1_2?ie=UTF8&amp;qid=1380428656&amp;sr=8-2&amp;keywords=stan+phelps', 'What&#8217;s Your Green Goldfish? Beyond Dollars: 15 Ways to Drive Employee Engagement and Reinforce Culture ']);" ><em><span style="color: #0091ba;">What&#8217;s Your Green Goldfish? Beyond Dollars: 15 Ways to Drive Employee</span> <span style="color: #0091ba;">Engagement and Reinforce Culture </span></em></a><div class="embed"><iframe src="//www.youtube.com/embed/vG7QZbdQj28" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Josh Allan Dykstra, author of<a href="http://www.amazon.com/Igniting-Invisible-Tribe-Designing-Organization/dp/0985832614/ref=sr_1_1?ie=UTF8&amp;qid=1380428688&amp;sr=8-1&amp;keywords=josh+allan+dykstra" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.amazon.com/Igniting-Invisible-Tribe-Designing-Organization/dp/0985832614/ref=sr_1_1?ie=UTF8&amp;qid=1380428688&amp;sr=8-1&amp;keywords=josh+allan+dykstra', ' Igniting the Invisible Tribe: Designing An Organization That Doesn&#8217;t Suck']);" > <em>Igniting the Invisible Tribe: Designing An Organization That Doesn&#8217;t Suck</em></a></span><div class="embed"><iframe src="//www.youtube.com/embed/BkYvBt7qXVk" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Mark Babbitt, CEO of YouTern &#8211; Part 1 </span><div class="embed"><iframe src="//www.youtube.com/embed/KVq0mABiMEg" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<h3><span style="color: #0091ba;">Switch and Shift TV with Mark Babbitt, CEO of YouTern &#8211; Part 2 </span><div class="embed"><iframe src="//www.youtube.com/embed/GNnFsIUvIKw" height="315" width="560" allowfullscreen="" frameborder="0"></iframe></div></h3>
<p>&nbsp;</p>
<p>Bonus Guest Video #1:</p>
<p><a href="http://www.youtube.com/watch?v=yWY1yGpHK4I" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.youtube.com/watch?v=yWY1yGpHK4I', '#LeadWithGiants TV With Ted Coine and Dan Forbes &#8211; &#8220;Return on Morale&#8221;']);" ><b>#LeadWithGiants TV With Ted Coine and Dan Forbes &#8211; &#8220;Return on Morale&#8221;</b></a></p>
<p>Bonus Guest Video #2:</p>
<p><a href="http://www.youtube.com/watch?v=z9ybb88chUQ" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://www.youtube.com/watch?v=z9ybb88chUQ', '#BBPShow530 featuring the guys from Switch And Shift']);" ><b>#BBPShow530 featuring the guys from Switch And Shift</b></a></p>
<div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/tedcoine" title="All posts by Ted Coiné" rel="author">Ted Coiné</a></h3><div class="bio-gravatar"><img src="http://2.gravatar.com/avatar/b16b77a4517c6d5b0234f7643ce7035b?s=70&#038;d=http%3A%2F%2Fswitchandshift.com%2Fwp-content%2Fuploads%2F2014%2F10%2Fs-circle-logo-250x250-96x96.png&#038;r=g" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://www.switchandshift.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="https://plus.google.com/101108067425017867244/posts" class="bio-icon bio-icon-googleplus"></a><a target="_blank" href="http://www.linkedin.com/pub/ted-coine/1/133/773" class="bio-icon bio-icon-linkedin"></a><p class="bio-description">Ted Coiné is a Forbes Top 10 Social Media Power Influencer and an Inc. Top 

100 Leadership and Management Expert. This stance at the crossroads of social 

and leadership put him in a unique perspective to identify the demise of Industrial 

Age management and the birth of the Social Age. The result, after five years of 

trend watching, interviewing and intensive research, is his latest book, A World 

Gone Social: How Companies Must Adapt to Survive, which he co-authored with 

Mark Babbitt.

An inspirational speaker and popular blogger, Ted is a pioneer of the Human 

Side of Business (#humanbiz) movement. He is also a serial business founder 

and three-time CEO. When not speaking at conferences and corporate functions, 

Ted advises CEOs on how to become Truly Social Leaders, or “Blue Unicorns” 

as they put it in A World Gone Social, in order to bring their companies into the 

Social Age. 

Ted’s advice: “Change is only scary if it’s happening to you. Instead, bring the 

change your competitors dread. That is something only a Social Age business 

leader can accomplish.”</p></div>
					<div class="clear"></div>

				</article>

			</section>



			
			

			
	<section class="section">		<ul class="meta alt">
			<li class="published updated"><a href="http://switchandshift.com/2013/09" title="View all posts from September"><i class="icon-clock"></i>September 29, 2013</a></li><li><i class="icon-comment"></i><a href="http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever#comments"><span class="dsq-postid" data-dsqidentifier="12374 http://switchandshift.com/?p=7374">2 Comments</span></a></li><li><i class="icon-list"></i><a href="http://switchandshift.com/category/topics/business" rel="category tag">Business</a>, <a href="http://switchandshift.com/category/featured" rel="category tag">Featured</a>, <a href="http://switchandshift.com/category/topics/leadership" rel="category tag">Leadership</a>, <a href="http://switchandshift.com/category/switch-shift-tv" rel="category tag">Switch &Amp; Shift Tv</a>, <a href="http://switchandshift.com/category/the-human-side-tv-2" rel="category tag">The Human Side Tv</a>, <a href="http://switchandshift.com/category/uncategorized" rel="category tag">Uncategorized</a></li>		</ul>
	</section>
			
<div id="disqus_thread">
            <div id="dsq-content">


            <ul id="dsq-comments">
                    <li class="comment even thread-even depth-1" id="dsq-comment-11675">
        <div id="dsq-comment-header-11675" class="dsq-comment-header">
            <cite id="dsq-cite-11675">
http://www.savvycapitalist.blogspot.com                <span id="dsq-author-user-11675">TedCoine</span>
            </cite>
        </div>
        <div id="dsq-comment-body-11675" class="dsq-comment-body">
            <div id="dsq-comment-message-11675" class="dsq-comment-message"><p>Newsflash: I&#8217;m really excited to tell you that we now have 10 shows in the bag (TEN!) in addition to what you see above. Thus far, Switch and Shift TV has been in ultra-soft-launch-mode (indeed, it&#8217;s the best-kept secret on this website, not to mention beyond). But that&#8217;s all about to change, first with this post, then with a solid week of new episodes on our show!</p>
<p>I&#8217;ll update everyone more come next Saturday. For now, let me share this exciting development: From Sunday, October 5th to Saturday, October 12th we are running a daily series at noon ET: &#8220;7 Leaders in 7 Days,&#8221; with a new episode each day. This will be in addition to our regular 8 am ET blog posts.</p>
<p>With that series, Switch and Shift will officially go from a blog to a multimedia website!</p>
<p>Can you tell I&#8217;m proud of our little baby as it really starts to grow up? People keep asking me the secret to our breakout success, and my answer never waivers: it&#8217;s the high quality of our contributors, like the guests on the shows above, married to the passionate leaders in our community who read, watch, comment, connect, and share. For that, I must thank you all.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="post pingback">
        <p>Pingback: <a href='http://switchandshift.com/7-leaders-in-7-days-on-switch-and-shift-tv' rel='external nofollow' class='url'>7 Leaders In 7 Days on Switch and Shift TV! | Switch and Shift</a>()</p>
    </li>
    </li><!-- #comment-## -->
            </ul>


        </div>

    </div>

<script type="text/javascript">
var disqus_url = 'http://switchandshift.com/top-11-swtichandshift-tv-episodes-ever';
var disqus_identifier = '12374 http://switchandshift.com/?p=7374';
var disqus_container_id = 'disqus_thread';
var disqus_shortname = 'switchandshift2';
var disqus_title = "Top 11 SwitchandShift TV Episodes Ever";
var disqus_config_custom = window.disqus_config;
var disqus_config = function () {
    /*
    All currently supported events:
    onReady: fires when everything is ready,
    onNewComment: fires when a new comment is posted,
    onIdentify: fires when user is authenticated
    */
    
    
    this.language = '';
        this.callbacks.onReady.push(function () {

        // sync comments in the background so we don't block the page
        var script = document.createElement('script');
        script.async = true;
        script.src = '?cf_action=sync_comments&post_id=12374';

        var firstScript = document.getElementsByTagName('script')[0];
        firstScript.parentNode.insertBefore(script, firstScript);
    });
    
    if (disqus_config_custom) {
        disqus_config_custom.call(this);
    }
};

(function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript';
    dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>




		


	</div><aside class="aside beta" style="width: 240px;"><section id="wpb_widget-2" class="section widget widget_wpb_widget"><h2 class="title">About the Author</h2><div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/tedcoine" title="All posts by Ted Coiné" rel="author">Ted Coiné</a></h3><div class="bio-gravatar"><img src="http://2.gravatar.com/avatar/b16b77a4517c6d5b0234f7643ce7035b?s=70&#038;d=http%3A%2F%2Fswitchandshift.com%2Fwp-content%2Fuploads%2F2014%2F10%2Fs-circle-logo-250x250-96x96.png&#038;r=g" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://www.switchandshift.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="https://plus.google.com/101108067425017867244/posts" class="bio-icon bio-icon-googleplus"></a><a target="_blank" href="http://www.linkedin.com/pub/ted-coine/1/133/773" class="bio-icon bio-icon-linkedin"></a><p class="bio-description">Ted Coiné is a Forbes Top 10 Social Media Power Influencer and an Inc. Top 

100 Leadership and Management Expert. This stance at the crossroads of social 

and leadership put him in a unique perspective to identify the demise of Industrial 

Age management and the birth of the Social Age. The result, after five years of 

trend watching, interviewing and intensive research, is his latest book, A World 

Gone Social: How Companies Must Adapt to Survive, which he co-authored with 

Mark Babbitt.

An inspirational speaker and popular blogger, Ted is a pioneer of the Human 

Side of Business (#humanbiz) movement. He is also a serial business founder 

and three-time CEO. When not speaking at conferences and corporate functions, 

Ted advises CEOs on how to become Truly Social Leaders, or “Blue Unicorns” 

as they put it in A World Gone Social, in order to bring their companies into the 

Social Age. 

Ted’s advice: “Change is only scary if it’s happening to you. Instead, bring the 

change your competitors dread. That is something only a Social Age business 

leader can accomplish.”</p></div></section><section id="time-child-unwrapped-text-7" class="section widget widget-unwrapped-text"><h2 class="title">Subscribe</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</section><section id="adwidget_imagewidget-5" class="section widget AdWidget_ImageWidget"><a target="_blank" href='http://www.inc.com/author/shawn-murphy' alt='Ad'><img  src='http://switchandshift.com/wp-content/uploads/2015/10/Inc-Ad.png' alt='Ad' /></a></section>		<section id="recent-posts-3" class="section widget widget_recent_entries">		<h2 class="title">Recent Articles</h2>		<ul>
					<li>
				<a href="http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community">You Can&#8217;t Do This Alone: Time to Build a Community</a>
						</li>
					<li>
				<a href="http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce">6 Ways to Awaken the Potential in Your Workforce</a>
						</li>
					<li>
				<a href="http://switchandshift.com/our-most-cherished-thanksgiving-traditions">Our Most Cherished Thanksgiving Traditions</a>
						</li>
					<li>
				<a href="http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world">How Gratitude Changes the Way You See the World</a>
						</li>
					<li>
				<a href="http://switchandshift.com/are-you-ready-for-the-digital-workplace">Are You Ready for the Digital Workplace?</a>
						</li>
					<li>
				<a href="http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership">A Necessary Redefinition of Responsibility and Leadership</a>
						</li>
				</ul>
		</section><section id="categories-3" class="section widget widget_categories"><h2 class="title">Articles by Category</h2><label class="screen-reader-text" for="cat">Articles by Category</label><select name='cat' id='cat' class='postform' >
	<option value='-1'>Select Category</option>
	<option class="level-0" value="2196">#SocialLeader</option>
	<option class="level-0" value="588">21st Century Customer Service</option>
	<option class="level-0" value="2454">Being Present</option>
	<option class="level-0" value="32">Blog</option>
	<option class="level-0" value="1126">Bold</option>
	<option class="level-0" value="495">Books</option>
	<option class="level-0" value="1211">Brand Engage</option>
	<option class="level-0" value="104">Business</option>
	<option class="level-0" value="305">Business Heretic&#8217;s Bookstore</option>
	<option class="level-0" value="2508">Change Management</option>
	<option class="level-0" value="1268">Communication</option>
	<option class="level-0" value="654">Company Culture</option>
	<option class="level-0" value="496">Culture</option>
	<option class="level-0" value="2270">Culture Change</option>
	<option class="level-0" value="336">Customer Service</option>
	<option class="level-0" value="1787">Editor&#8217;s Choice</option>
	<option class="level-0" value="2433">Employee Engagement</option>
	<option class="level-0" value="432">Engagement</option>
	<option class="level-0" value="497">Featured</option>
	<option class="level-0" value="1427">Feed Your Brain</option>
	<option class="level-0" value="482">Future Of Leadership</option>
	<option class="level-0" value="1732">Home Page</option>
	<option class="level-0" value="490">Hr</option>
	<option class="level-0" value="2519">Human workplace</option>
	<option class="level-0" value="1385">Humanbiz Community Carnival</option>
	<option class="level-0" value="105">Inspirational</option>
	<option class="level-0" value="103">Leadership</option>
	<option class="level-0" value="1269">Leadership Blog</option>
	<option class="level-0" value="1806">Leadership Presence</option>
	<option class="level-0" value="792">Mentoring</option>
	<option class="level-0" value="1795">Millennials</option>
	<option class="level-0" value="1465">Mini Vlogs</option>
	<option class="level-0" value="1831">My Story Campaign</option>
	<option class="level-0" value="694">New Leadership In The New Economy: Diversity Matters</option>
	<option class="level-0" value="913">Pivot Point</option>
	<option class="level-0" value="2430">Purpose</option>
	<option class="level-0" value="1428">Rebel Heretics Community Feed</option>
	<option class="level-0" value="2395">Rebellious Leaders</option>
	<option class="level-0" value="2391">Rebellious Leadership</option>
	<option class="level-0" value="609">Recognition</option>
	<option class="level-0" value="261">Return On Morale</option>
	<option class="level-0" value="1680">Return On Trust</option>
	<option class="level-0" value="262">Social Era</option>
	<option class="level-0" value="2197">Social Leadership</option>
	<option class="level-0" value="152">Social Media</option>
	<option class="level-0" value="208">Social You</option>
	<option class="level-0" value="841">Strategy</option>
	<option class="level-0" value="493">Strengths</option>
	<option class="level-0" value="796">Switch &Amp; Shift Tv</option>
	<option class="level-0" value="344">Talent</option>
	<option class="level-0" value="1132">The Human Side Tv</option>
	<option class="level-0" value="1061">The Values Revolution</option>
	<option class="level-0" value="363">Transparency</option>
	<option class="level-0" value="101">Uncategorized</option>
	<option class="level-0" value="1125">Vision Cast</option>
	<option class="level-0" value="153">Weekend Post</option>
	<option class="level-0" value="483">Winning Through Engagement</option>
	<option class="level-0" value="1015">Work That Matters Podcast</option>
	<option class="level-0" value="1520">Workplace Morale</option>
	<option class="level-0" value="494">Workplace Optimism</option>
	<option class="level-0" value="180">You: Reinvented</option>
</select>

<script type='text/javascript'>
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "cat" );
	function onCatChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
			location.href = "http://switchandshift.com/?cat=" + dropdown.options[ dropdown.selectedIndex ].value;
		}
	}
	dropdown.onchange = onCatChange;
})();
/* ]]> */
</script>

</section><section id="archives-3" class="section widget widget_archive"><h2 class="title">Archives</h2>		<label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
		<select id="archives-dropdown-3" name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
			
			<option value="">Select Month</option>
				<option value='http://switchandshift.com/2015/11'> November 2015 </option>
	<option value='http://switchandshift.com/2015/10'> October 2015 </option>
	<option value='http://switchandshift.com/2015/09'> September 2015 </option>
	<option value='http://switchandshift.com/2015/08'> August 2015 </option>
	<option value='http://switchandshift.com/2015/07'> July 2015 </option>
	<option value='http://switchandshift.com/2015/06'> June 2015 </option>
	<option value='http://switchandshift.com/2015/05'> May 2015 </option>
	<option value='http://switchandshift.com/2015/04'> April 2015 </option>
	<option value='http://switchandshift.com/2015/03'> March 2015 </option>
	<option value='http://switchandshift.com/2015/02'> February 2015 </option>
	<option value='http://switchandshift.com/2015/01'> January 2015 </option>
	<option value='http://switchandshift.com/2014/12'> December 2014 </option>
	<option value='http://switchandshift.com/2014/11'> November 2014 </option>
	<option value='http://switchandshift.com/2014/10'> October 2014 </option>
	<option value='http://switchandshift.com/2014/09'> September 2014 </option>
	<option value='http://switchandshift.com/2014/08'> August 2014 </option>
	<option value='http://switchandshift.com/2014/07'> July 2014 </option>
	<option value='http://switchandshift.com/2014/06'> June 2014 </option>
	<option value='http://switchandshift.com/2014/05'> May 2014 </option>
	<option value='http://switchandshift.com/2014/04'> April 2014 </option>
	<option value='http://switchandshift.com/2014/03'> March 2014 </option>
	<option value='http://switchandshift.com/2014/02'> February 2014 </option>
	<option value='http://switchandshift.com/2014/01'> January 2014 </option>
	<option value='http://switchandshift.com/2013/12'> December 2013 </option>
	<option value='http://switchandshift.com/2013/11'> November 2013 </option>
	<option value='http://switchandshift.com/2013/10'> October 2013 </option>
	<option value='http://switchandshift.com/2013/09'> September 2013 </option>
	<option value='http://switchandshift.com/2013/08'> August 2013 </option>
	<option value='http://switchandshift.com/2013/07'> July 2013 </option>
	<option value='http://switchandshift.com/2013/06'> June 2013 </option>
	<option value='http://switchandshift.com/2013/05'> May 2013 </option>
	<option value='http://switchandshift.com/2013/04'> April 2013 </option>
	<option value='http://switchandshift.com/2013/03'> March 2013 </option>
	<option value='http://switchandshift.com/2013/02'> February 2013 </option>
	<option value='http://switchandshift.com/2013/01'> January 2013 </option>
	<option value='http://switchandshift.com/2012/12'> December 2012 </option>
	<option value='http://switchandshift.com/2012/11'> November 2012 </option>
	<option value='http://switchandshift.com/2012/10'> October 2012 </option>
	<option value='http://switchandshift.com/2012/09'> September 2012 </option>
	<option value='http://switchandshift.com/2012/08'> August 2012 </option>
	<option value='http://switchandshift.com/2012/07'> July 2012 </option>
	<option value='http://switchandshift.com/2012/06'> June 2012 </option>
	<option value='http://switchandshift.com/2012/05'> May 2012 </option>
	<option value='http://switchandshift.com/2012/04'> April 2012 </option>
	<option value='http://switchandshift.com/2012/03'> March 2012 </option>
	<option value='http://switchandshift.com/2012/02'> February 2012 </option>
	<option value='http://switchandshift.com/2012/01'> January 2012 </option>

		</select>
</section><section id="text-10" class="section widget widget_text">			<div class="textwidget"><iframe src="http://widgets.itunes.apple.com/widget.html?c=us&brc=FFFFFF&blc=FFFFFF&trc=FFFFFF&tlc=FFFFFF&d=Podcast shows we recommend.&t=Get more leadership content&m=podcast&e=podcast&w=220&h=400&ids=741665948,351616584,435836905,647826736,794030859,596047499&wt=playlist&partnerId=&affiliate_id=&at=&ct=" frameborder=0 style="overflow-x:hidden;overflow-y:hidden;width:220px;height: 400px;border:0px; margin-left: -8px;"></iframe></div>
		</section><section id="text-11" class="section widget widget_text"><h2 class="title">Recognition</h2>			<div class="textwidget"><a href="http://servetolead.org/best-21st-century-leadership-blogs-new-media/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://servetolead.org/best-21st-century-leadership-blogs-new-media/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/s2l-badge-21st-century-126x126pr.jpg" style="margin-bottom: 10px;">
</a>

<a href="http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Career-Rocketeer-Career-Blogs-Official-Badge-2014.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.nevermindthemanager.com" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.nevermindthemanager.com', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Leblin-Badge.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.cmoe.com/top-shared-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.cmoe.com/top-shared-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/CMOE-BADGE-2.001.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.skipprichard.com/top-lists/top-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.skipprichard.com/top-lists/top-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/07/top-leadership-blog-award-300x300.png" style="margin-bottom: 10px;">
</a></div>
		</section></aside></div></div>

</div>




		</div>

		<div id="bottom">

			
<div class="outer-container">

	
		<div class="container">

			<section class="section">

				<div class="columns alt-mobile">
					<ul>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-2" class="widget widget-unwrapped-text"><span><img class="alignnone size-full wp-image-3817" src="http://switchandshift.com/wp-content/uploads/2014/08/SS_white_logo500.png" alt="footer-logo" width="250" height="63" />

<p>There’s a more human way to do business.</p>

<p>In the Social Age, it’s how we engage with customers, collaborators and strategic partners that matters; it’s how we create workplace optimism that sets us apart; it’s how we recruit, retain (and repel) employees that becomes our differentiator. This isn’t a “people first, profits second” movement, but a “profits as a direct result of putting people first” movement.</p>
</span></div>							</li>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-4" class="widget widget-unwrapped-text"><h2 class="title">Connect</h2><hr></div><div id="time-child-social-media-3" class="widget widget-social-media"><div class="social-icons native-colors"><ul class="alt"><li><a href="https://twitter.com/switchandshift" target="_blank"><i class="icon-twitter"></i></a></li><li><a href="https://plus.google.com/+Switchandshift1/posts" target="_blank"><i class="icon-gplus"></i></a></li><li><a href="https://www.facebook.com/switchandshift" target="_blank"><i class="icon-facebook"></i></a></li></ul></div></div><div id="time-child-unwrapped-text-3" class="widget widget-unwrapped-text"><hr>
<p>email:<a href="mailto:charmian@switchandshift.com"> connect@switch&shift.com</a> <br/>
1133 Ferreto Parkway<br/>Dayton, NV 89403<br/>
</p>
<hr>
<p>
<a href="http://switchandshift.com/terms-and-conditions/">Terms & Conditions</a> &nbsp;|&nbsp; <a href="http://switchandshift.com/privacy-policy/">Privacy Policy</a>
</p></div><div id="time-child-unwrapped-text-5" class="widget widget-unwrapped-text"><h2 class="title">Newsletter Subscription</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</div>							</li>
													<li class="col-1-3">
								<div id="time-child-contact-2" class="widget widget-contact"><h2 class="title">Contact Us</h2><form class="contact-form" action="http://switchandshift.com/wp-admin/admin-ajax.php" method="post"><input name="action" id="action" type="hidden" value="time-child_contact_form" /><p><input type="text" name="name" placeholder="name*" /></p><p><input type="text" name="email" placeholder="e-mail*" /></p><p><input type="text" name="subject" placeholder="subject" /></p><p><textarea class="full-width" name="message"></textarea></p><p><input name="cntctfrm_contact_action" id="cntctfrm-contact-action" type="hidden" value="true" /><input type="hidden" name="cptch_result" value="rP8=" />
				<input type="hidden" name="cptch_time" value="1448822521" />
				<input type="hidden" value="Version: 4.1.5" /><label class="cptch_label" for="cptch_input_55">f&#111;u&#114; &times; <input id="cptch_input_55" class="cptch_input" type="text" autocomplete="off" name="cptch_number" value="" maxlength="1" size="1" /> = 8</label></p><p><input type="submit" value="Send&nbsp;&rsaquo;" /><i class="icon-arrows-ccw load"></i><span class="msg small"></span></p></form></div>							</li>
											</ul>
				</div><!-- // .columns -->

			</section>

		</div><!-- // .container -->

	
</div><!-- // .outer-container -->
							<footer class="footer">

					<div class="container">

						<div class="section">
							<p class="small alpha">&copy; Copyright 2015 Switch &amp; Shift</p>
							<p class="small beta">Website Developed by <a href="http:/solutionsred.com" target="blank">Solutions Red</a></p>
						</div>

					</div>

				</footer>
			
		</div>

		        <script type="text/javascript">
        // <![CDATA[
        var disqus_shortname = 'switchandshift2';
        (function () {
            var nodes = document.getElementsByTagName('span');
            for (var i = 0, url; i < nodes.length; i++) {
                if (nodes[i].className.indexOf('dsq-postid') != -1) {
                    nodes[i].parentNode.setAttribute('data-disqus-identifier', nodes[i].getAttribute('data-dsqidentifier'));
                    url = nodes[i].parentNode.href.split('#', 1);
                    if (url.length == 1) { url = url[0]; }
                    else { url = url[1]; }
                    nodes[i].parentNode.href = url + '#disqus_thread';
                }
            }
            var s = document.createElement('script'); 
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
        // ]]>
        </script>
        <script type='text/javascript' src='http://switchandshift.com/wp-includes/js/comment-reply.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/3rd-party.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/time.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>

	</body>

</html>
<!-- Dynamic page generated in 1.176 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:42:02 -->
