<?php die(); ?>



<!DOCTYPE html>

<!--[if lt IE 9]>             <html class="no-js ie lt-ie9" lang="en-US" prefix="og: http://ogp.me/ns#""><![endif]-->

<!--[if IE 9]>                <html class="no-js ie ie9" lang="en-US" prefix="og: http://ogp.me/ns#">   <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js no-ie" lang="en-US" prefix="og: http://ogp.me/ns#">    <!--<![endif]-->

	<head>

		<meta charset="UTF-8" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />

		<!--[if lt IE 9]>

			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

			<script src="http://switchandshift.com/wp-content/themes/time/data/js/selectivizr.min.js"></script>

		<![endif]-->

		<title>A Gen Y Definition of Leadership - Switch &amp; Shift</title>
		<title>A Gen Y Definition of Leadership - Switch &amp; Shift</title>
<meta property="og:site_name" content="Switch &amp; Shift" /><meta property="og:title" content="Switch &amp; Shift" /><meta property="og:locale" content="en_US" /><meta property="og:url" content="http://switchandshift.com/a-gen-y-definition-of-leadership" /><meta property="og:description" content="I was surprised (and more than somewhat honored) when I was asked to participate in this blog. After all, I’m in fantastic company… what could I possibly contribute that adds value? And then it hit me: I can offer insight into the leader I hope [...]" /><meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2013/05/Gen-Y-Text.jpg" />
<!-- This site is optimized with the Yoast SEO plugin v2.3.5 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="http://switchandshift.com/a-gen-y-definition-of-leadership" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="A Gen Y Definition of Leadership - Switch &amp; Shift" />
<meta property="og:description" content="I was surprised (and more than somewhat honored) when I was asked to participate in this blog. After all, I’m in fantastic company… what could I possibly contribute that adds value? And then it hit me: I can offer insight into the leader I hope to be in the near future. Gen Y is the &hellip; Read more... Read More&raquo;" />
<meta property="og:url" content="http://switchandshift.com/a-gen-y-definition-of-leadership" />
<meta property="og:site_name" content="Switch &amp; Shift" />
<meta property="article:tag" content="Gen y" />
<meta property="article:tag" content="Leadership" />
<meta property="article:tag" content="Young Professionals" />
<meta property="article:section" content="Culture" />
<meta property="article:published_time" content="2013-05-09T06:00:32+00:00" />
<meta property="article:modified_time" content="2013-05-14T13:25:50+00:00" />
<meta property="og:updated_time" content="2013-05-14T13:25:50+00:00" />
<meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2013/05/Gen-Y-Text.jpg" />
<meta property="og:image" content="http://switchandshift.com/wp-content/uploads/2013/05/Jason.jpg" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="I was surprised (and more than somewhat honored) when I was asked to participate in this blog. After all, I’m in fantastic company… what could I possibly contribute that adds value? And then it hit me: I can offer insight into the leader I hope to be in the near future. Gen Y is the [&hellip;] Read more... Read More&raquo;"/>
<meta name="twitter:title" content="A Gen Y Definition of Leadership - Switch &amp; Shift"/>
<meta name="twitter:domain" content="Switch &amp; Shift"/>
<meta name="twitter:image" content="http://switchandshift.com/wp-content/uploads/2013/05/Gen-Y-Text.jpg"/>
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Feed" href="http://switchandshift.com/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; Comments Feed" href="http://switchandshift.com/comments/feed" />
<link rel="alternate" type="application/rss+xml" title="Switch &amp; Shift &raquo; A Gen Y Definition of Leadership Comments Feed" href="http://switchandshift.com/a-gen-y-definition-of-leadership/feed" />
<!-- This site is powered by Shareaholic - https://shareaholic.com -->
<script type='text/javascript' data-cfasync='false'>
  //<![CDATA[
    _SHR_SETTINGS = {"endpoints":{"local_recs_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_permalink_related","share_counts_url":"http:\/\/switchandshift.com\/wp-admin\/admin-ajax.php?action=shareaholic_share_counts_api"}};
  //]]>
</script>
<script type='text/javascript'
        src='//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js'
        data-shr-siteid='22c416cd99bd31d22f03d82d3a8395e6'
        data-cfasync='false'
        async='async' >
</script>

<!-- Shareaholic Content Tags -->
<meta name='shareaholic:site_name' content='Switch &amp; Shift' />
<meta name='shareaholic:language' content='en-US' />
<meta name='shareaholic:url' content='http://switchandshift.com/a-gen-y-definition-of-leadership' />
<meta name='shareaholic:keywords' content='gen y, leadership, young professionals, millennials, business leadership, culture, featured, new leadership in the new economy: diversity matters' />
<meta name='shareaholic:article_published_time' content='2013-05-09T13:00:32+00:00' />
<meta name='shareaholic:article_modified_time' content='2015-11-29T11:00:38+00:00' />
<meta name='shareaholic:shareable_page' content='true' />
<meta name='shareaholic:article_author_name' content='Jason Repovs' />
<meta name='shareaholic:site_id' content='22c416cd99bd31d22f03d82d3a8395e6' />
<meta name='shareaholic:wp_version' content='7.6.2.3' />
<meta name='shareaholic:image' content='http://switchandshift.com/wp-content/uploads/2013/05/Gen-Y-Text.jpg' />
<!-- Shareaholic Content Tags End -->
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/switchandshift.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=1d05468803804e8153cb17eab60a20d0"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='flick-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp//css/flick/flick.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='mailchimpSF_main_css-css'  href='http://switchandshift.com/?mcsf_action=main_css&#038;ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!--[if IE]>
<link rel='stylesheet' id='mailchimpSF_ie_css-css'  href='http://switchandshift.com/wp-content/plugins/mailchimp/css/ie.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='wild-googlemap-frontend-css'  href='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-css-frontend.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='cptch_stylesheet-css'  href='http://switchandshift.com/wp-content/plugins/captcha/css/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='tm_clicktotweet-css'  href='http://switchandshift.com/wp-content/plugins/click-to-tweet-by-todaymade/assets/css/styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='se-link-styles-css'  href='http://switchandshift.com/wp-content/plugins/search-everything/static/css/se-styles.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://switchandshift.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-3rd-party-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/3rd-party.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-style-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/style.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-scheme-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/bright.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-stylesheet-css'  href='http://switchandshift.com/wp-content/themes/time_child/style.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='time-mobile-css'  href='http://switchandshift.com/wp-content/themes/time/data/css/mobile.min.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='only screen and (max-width: 767px)' />
<link rel='stylesheet' id='author-avatars-widget-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/widget.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='author-avatars-shortcode-css'  href='http://switchandshift.com/wp-content/plugins/author-avatars/css/shortcode.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<link rel='stylesheet' id='author-bio-box-styles-css'  href='http://switchandshift.com/wp-content/plugins/author-bio-box/assets/css/author-bio-box.css?ver=1d05468803804e8153cb17eab60a20d0' type='text/css' media='all' />
<!-- This site uses the Google Analytics by Yoast plugin v5.4.6 - Universal disabled - https://yoast.com/wordpress/plugins/google-analytics/ -->
<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-27884223-1']);
	_gaq.push(['_gat._forceSSL']);
	_gaq.push(['_trackPageview']);

	(function () {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();

</script>
<!-- / Google Analytics by Yoast -->
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/scrollTo.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/jquery.form.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mailchimpSF = {"ajax_url":"http:\/\/switchandshift.com\/"};
/* ]]> */
</script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp/js/mailchimp.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-includes/js/jquery/ui/core.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/mailchimp//js/datepicker.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?sensor=false&#038;ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/plugins/wild-googlemap/wild-googlemap-js-frontend.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://switchandshift.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://switchandshift.com/wp-includes/wlwmanifest.xml" /> 

<link rel='shortlink' href='http://switchandshift.com/?p=10217' />
<!-- Custom Code Start-->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '811956965544989']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=811956965544989&ev=PixelInitialized" /></noscript>
<!-- Custom Code Start-->
	<script type="text/javascript">
		jQuery(function($) {
			$('.date-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd/yyyy';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: true,
					beforeShow: function(input, inst) { $('#ui-datepicker-div').addClass('show'); },
					dateFormat: format.toLowerCase(),
				});
			});
			d = new Date();
			$('.birthdate-pick').each(function() {
				var format = $(this).data('format') || 'mm/dd';
				format = format.replace(/yyyy/i, 'yy');
				$(this).datepicker({
					autoFocusNextInput: true,
					constrainInput: false,
					changeMonth: true,
					changeYear: false,
					minDate: new Date(d.getFullYear(), 1-1, 1),
					maxDate: new Date(d.getFullYear(), 12-1, 31),
					beforeShow: function(input, inst) { $('#ui-datepicker-div').removeClass('show'); },
					dateFormat: format.toLowerCase(),
				});

			});

		});
	</script>
	<script type="text/javascript">
	window._se_plugin_version = '8.1.3';
</script>
<style>a, a.alt:hover, .alt a:hover, #bottom a.alt:hover, #bottom .alt a:hover, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, input[type="button"].active, button.active, .button.active, .color, .super-tabs > div > .nav h2 span, .toggles > div > h3:hover > i, .logo, nav a:hover, #bottom nav a:hover, nav .current > a, nav .current>a:hover{color:#0292b7}mark, .slider .control-nav li a:hover, .slider .control-nav li a.active, #top:before, #top > .before, .background-color, nav.mobile a:hover, nav.mobile .current > a, .mejs-controls .mejs-time-rail .mejs-time-loaded, .mejs-controls .mejs-time-rail .mejs-time-current{background-color:#0292b7}.zoom-hover>.zoom-hover-overlay{background-color:rgba(2, 146, 183, 0.75)}blockquote.bar, .sticky:before, #bottom .outer-container{border-color:#0292b7}body,input,select,textarea,button,.button{font-family:Raleway;font-size:13px;line-height:23px}nav.primary ul, nav.primary a:not(:hover), nav.mobile ul, nav.mobile a:not(:hover){font-family:Raleway;font-size:15px;font-weight:bold;font-style:normal;text-decoration:none}nav.secondary ul, nav.secondary a:not(:hover){font-family:Raleway;font-size:12px;font-weight:bold;font-style:normal;text-decoration:none}.headline h1{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}.headline .breadcrumbs{font-family:Raleway;font-size:15px;font-weight:normal;font-style:normal;text-decoration:none}#top .widget>.title{font:normal 18px/128% Raleway;text-decoration:none}.post h1.title{font:bold 22px/128% Raleway;text-decoration:none}h1{font:bold 22px/128% Raleway;text-decoration:none}h2{font:bold 18px/128% Raleway;text-decoration:none}h3{font:bold 14px/128% Raleway;text-decoration:none}h4{font:bold 14px/128% Raleway;text-decoration:none}input[type="submit"]:not(.big):not(.huge),input[type="reset"]:not(.big):not(.huge),input[type="button"]:not(.big):not(.huge),button:not(.big):not(.huge),.button:not(.big):not(.huge){font-family:Raleway;font-size:14px;font-weight:normal;font-style:normal;text-decoration:none}input[type="submit"].big,input[type="reset"].big,input[type="button"].big,button.big,.button.big{font-family:Raleway;font-size:18px;font-weight:bold;font-style:normal;text-decoration:none}input[type="submit"].huge,input[type="reset"].huge,input[type="button"].huge,button.huge,.button.huge{font-family:Raleway;font-size:22px;font-weight:bold;font-style:normal;text-decoration:none}</style>
<script>if(typeof WebFont!='undefined'){WebFont.load({google:{families:["Raleway:400,700:latin"]},active:function(){if(document.createEvent){var e=document.createEvent('HTMLEvents');e.initEvent('webfontactive',true,false);document.dispatchEvent(e);}else{document.documentElement['webfontactive']++;}}});}timeConfig={templatePath:'http://switchandshift.com/wp-content/themes/time',zoomHoverIcons:{"default":"icon-plus-circled","image":"icon-search","mail":"icon-mail","title":"icon-right"},flexsliderOptions:{"animation":"slide","direction":"horizontal","animationSpeed":600,"slideshowSpeed":7000,"slideshow":false},layersliderOptions:{}};(function($){$(document).ready(function($){$('.widget_pages, .widget_archive, .widget_categories, .widget_recent_entries, .widget_recent_comments, .widget_display_forums, .widget_display_replies, .widget_display_topics, .widget_display_views').each(function(){$('ul',this).addClass('fancy alt');$('li',this).prepend($('<i />',{'class':'icon-right-open'}));if($(this).closest('#top').length>0){$('i',this).addClass('color');}});$('#disqus_thread').addClass('section');});})(jQuery);</script>
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//switchandshift.com/?wordfence_logHuman=1&hid=721496CBDF50D0D82CEB741E637D19B9');
</script><link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250-48x48.png" sizes="32x32" />
<link rel="icon" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">
<meta name="msapplication-TileImage" content="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250.png">


<!-- Google Code for Remarketing Tag -->
<!-
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 954219818;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/954219818/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


	</head>



	<body class="time-child-3-6-1-child-1-0 single single-post postid-10217 single-format-standard layout-boxed scheme-bright">



		<div id="top" >



			
	<div class="backgrounds"><div style="background:  #ffffff no-repeat center top fixed; background-size: cover;"></div></div>



			<div class="upper-container ">



				<div class="outer-container">



					
						<nav id="menu" class="mobile">

							<ul id="menu-main-menu" class=""><li id="menu-item-3819" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-4305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li id="menu-item-4304" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li id="menu-item-4303" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li id="menu-item-19075" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li id="menu-item-19071" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li id="menu-item-4306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li id="menu-item-22648" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li id="menu-item-4177" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li id="menu-item-21828" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li id="menu-item-23151" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
</ul>
						</nav>

					


					
						<nav id="search" class="mobile">

							<form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form>
						</nav>

					


				</div>



				<div class="outer-container ">



					<header class="header">



						<div class="container">



							<div class="mobile-helper vertical-align">



								
									<a href="#menu" class="button" title="Menu"><i class="icon-menu"></i></a>

								






								
									<a href="#search" class="button" title="Search"><i class="icon-search"></i></a>

								


							</div>



							


							<h1 class="logo vertical-align"><a href="http://switchandshift.com/" title="Switch &amp; Shift" rel="home"><img src="http://switchandshift.com/wp-content/uploads/2014/07/logo.jpg" width="244" height="50" data-2x="http://switchandshift.com/wp-content/uploads/2014/07/logo-2x.jpg" alt="Switch &amp; Shift" /></a></h1>


							
								<nav class="primary vertical-align">

									<ul id="menu-main-menu-1" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3819"><a href="http://switchandshift.com/about-us">About Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4305"><a href="http://switchandshift.com/about-us/what-we-stand-for">What We Stand For</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4304"><a href="http://switchandshift.com/about-us/the-human-side-of-business">The Human Side of Business</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4303"><a href="http://switchandshift.com/about-us/team">Team</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19075"><a href="http://switchandshift.com/what-is-a-rebel-heretic">What Is a Rebel Heretic?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19071"><a href="http://switchandshift.com/faq">FAQ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4306"><a href="http://switchandshift.com/league-of-extraordinary-thinkers">Writers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22648"><a href="http://switchandshift.com/blog">Blog</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4177"><a href="http://switchandshift.com/gallery/the-business-heretics-bookstore/">Bookstore</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21828"><a href="http://switchandshift.com/video-portfolio">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23151"><a href="http://switchandshift.com/socialleader-chat">#SocialLeader Chat</a></li>
<li><form method="get" action="http://switchandshift.com/" class="search" role="search"><input type="text" name="s" value="" placeholder="Search site" /><button type="submit"><i class="icon-search"></i></button></form></li></ul>
								</nav>

							


						</div>



					</header>



					


				</div>



			</div>



			

			

<div class="outer-container">
	
	<nav class="secondary">
		<div class="container">
			<div class="menu-secondary-menu-container"><ul id="menu-secondary-menu" class="menu"><li id="menu-item-19073" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19073"><a href="http://switchandshift.com/work-that-matters-podcast">Work That Matters Podcast</a></li>
<li id="menu-item-21566" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21566"><a href="http://switchandshift.com/speaking-under-construction">Speaking</a></li>
<li id="menu-item-4334" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4334"><a href="http://switchandshift.com/consulting">Consulting</a></li>
<li id="menu-item-4331" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4331"><a href="http://switchandshift.com/a-world-gone-social">A World Gone Social</a></li>
<li id="menu-item-20424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20424"><a href="http://switchandshift.com/writing-guidelines">Writing Guidelines</a></li>
<li id="menu-item-19074" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19074"><a href="http://switchandshift.com/contact">Contact Us</a></li>
</ul></div>    				</div>
	</nav>

	<div class="content"><div class="container"><div class="main alpha" style="padding: 0 240px 0 0px; margin: 0 -240px 0 -0px;">


		


			<section class="section">

				<article id="post-10217" class="post post-10217 type-post status-publish format-standard has-post-thumbnail hentry category-culture-topics category-featured category-leadership category-new-leadership-in-the-new-economy-diversity-matters tag-gen-y tag-leadership-2 tag-young-professionals">
					

	<h1 class="title entry-title">A Gen Y Definition of Leadership</h1>

                    

					<div class='shareaholic-canvas' data-app-id='21423060' data-app='share_buttons' data-title='A Gen Y Definition of Leadership' data-link='http://switchandshift.com/a-gen-y-definition-of-leadership' data-summary=''></div><figure id="attachment_5224" class="size-full wp-image-5224 aligncenter" style="width: 700px;"><a data-fancybox-title="" class="" href="http://switchandshift.com/wp-content/uploads/2013/05/Gen-Y-Text.jpg"><img  alt="Gen Y" src="http://switchandshift.com/wp-content/uploads/2013/05/Gen-Y-Text.jpg" width="700" height="300" /></a></figure>
<figure id="attachment_5230" class="size-full wp-image-5230 alignleft fixed" style="width: 134px;"><a data-fancybox-title="" class="" href="http://switchandshift.com/wp-content/uploads/2013/05/Jason.jpg"><img  alt="Jason" src="http://switchandshift.com/wp-content/uploads/2013/05/Jason.jpg" width="134" height="200" /></a></figure>
<p>I was surprised (and more than somewhat honored) when I was asked to participate in this blog. After all, I’m in fantastic company… what could I possibly contribute that adds value? And then it hit me: I can offer insight into the leader I hope to be in the near future.</p>
<p>Gen Y is the next generation of leaders, and we bring with us a fresh outlook on leadership. So, I’ll focus on my desired leadership style, the rationale behind it, and the implications to companies big and small – after all, I’m probably not the only one who thinks the way I do!</p>
<p>I’ve had the opportunity to serve under a wide variety of leadership styles, and each of them has provided a valuable opportunity shape my definition of an ideal leader. I’ll summarize the leader I hope to be through the following five traits:</p>
<p><b>Be open and honest with communication and feedback</b></p>
<p>There’s nothing worse than being blindsided by unanticipated constructive feedback during your annual review… except maybe seeing a project derailed because of a simple communication breakdown.</p>
<p>On the flipside, I’ve seen the benefits of real-time feedback and ongoing communication: problems get fixed faster, and annual or semi-annual reviews aren’t nearly as stressful. This is just as true for leaders – I want my team to speak up when problems need fixing, without fear of retribution. After all, the purpose of feedback is to help the team perform more effectively!</p>
<p><b>Work hard, play hard</b></p>
<p>Odds are the people you work with are some of the people you see most often in your life. It’s natural to become friends with those with whom you work most closely. Some of the most cohesive teams I know of consistently get together as a group outside work.</p>
<p>I’m a firm believer that a team that celebrates together not only stays together, but engages in more healthy forms of debate, and is more productive. This is especially true where Gen Y is involved.</p>
<blockquote><p>Gen Y is the next generation of leaders, and we bring with us a fresh outlook on leadership.</p></blockquote>
<p><b>Give and demand excellence</b></p>
<p>If something’s worth doing, it’s worth doing to the very best of your ability. That’s the way I operate, and that’s the way I would expect my team to operate. I aim to create a culture of performance, where sandbagging is unforgivable and going the extra mile is standard. The celebrating that comes from a victory hard-fought and hard-won is infinitely satisfying, and it forms a virtuous cycle of ongoing improvement.</p>
<p><b>Hold myself accountable</b></p>
<p>I believe it’s a leader’s duty to take responsibility for their team, for better or worse. If my team fails, it’s because I’ve failed them in some way. It’s my job to make sure it never happens again. Likewise, if my team is unfairly attacked, it’s my job to defend them.</p>
<p><b>Be flexible in holding my team accountable<br />
</b></p>
<p>I don’t buy into the concept of &#8220;presenteeism&#8221; – that a person’s work ethic should be judged on how many hours they put in. In my experience, it encourages people to slow down their pace of work to fit exactly into the required hours. I’d rather let my employees leave early if their work is done one day, if this meant I could count on them to stay late other days when things are busier.</p>
<p><b>Be open to taking risks</b></p>
<p>Events change so quickly in today’s economy. Any company that requires its employees to be 100% sure before proceeding with an idea will quickly be left in the dust of others who took action earlier. If an idea seems well thought-out and rational, I never want to be the one holding it back.</p>
<p>Now, if you read all of this and scoffed, then I wish you luck as you descend into irrelevance. If, however, you’re reading this, wondering how it will affect your organization, you may want to consider the following:</p>
<ul>
<li>How do my company’s programs support Gen Y in leadership roles?</li>
<li>What is likely to become redundant or obsolete?</li>
<li>What frustrations might Gen Y leaders face, and can anything be done to alleviate them?</li>
<li>What are the implications of changing nothing?</li>
<li>How will newer leadership styles mesh with more traditional styles? What might be the impact on employees transitioning from a traditional-style manager to a Gen Y?</li>
</ul>
<p>I urge you to consider the above. Preparing your organization for the changing of the guard might be the competitive advantage that sets you apart.</p>
<p>&nbsp;</p>
<p>Continue reading our New Leadership series with <a title="Appreciating Diversity and Differences Builds Effective Teams" href="http://switchandshift.com/appreciating-diversity-and-differences-builds-effective-teams" target="_blank">Appreciating Diversity and Differences Builds Effective Teams</a></p>
<p>&nbsp;</p>
<p style="text-align: right;">Art by: <a href="http://azureluck.deviantart.com/art/Young-at-work-read-captions-131353614" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'http://azureluck.deviantart.com/art/Young-at-work-read-captions-131353614', 'Azureluck']);" target="_blank">Azureluck</a></p>
<div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/jason-repovs" title="All posts by Jason Repovs" rel="author">Jason Repovs</a></h3><div class="bio-gravatar"><img src="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250-96x96.png" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://www.jasonrepovs.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="http://in/jasonrepovsmarketing/" class="bio-icon bio-icon-linkedin"></a><p class="bio-description">Jason Repovs is the blogger behind The Personal Professional. He’s a professional marketer who also provides insight into the way Generation Y thinks. Jason’s areas of focus include consumer behavior, Gen Y management, personal branding, and social media marketing.</p></div>
					<div class="clear"></div>

				</article>

			</section>



			
			

			
	<section class="section">		<ul class="meta alt">
			<li class="published updated"><a href="http://switchandshift.com/2013/05" title="View all posts from May"><i class="icon-clock"></i>May 9, 2013</a></li><li><i class="icon-comment"></i><a href="http://switchandshift.com/a-gen-y-definition-of-leadership#comments"><span class="dsq-postid" data-dsqidentifier="10217 http://switchandshift.com/?p=5217">4 Comments</span></a></li><li><i class="icon-list"></i><a href="http://switchandshift.com/category/topics/culture-topics" rel="category tag">Culture</a>, <a href="http://switchandshift.com/category/featured" rel="category tag">Featured</a>, <a href="http://switchandshift.com/category/topics/leadership" rel="category tag">Leadership</a>, <a href="http://switchandshift.com/category/series/new-leadership-in-the-new-economy-diversity-matters" rel="category tag">New Leadership In The New Economy: Diversity Matters</a></li>		</ul>
	</section>
			
<div id="disqus_thread">
            <div id="dsq-content">


            <ul id="dsq-comments">
                    <li class="post pingback">
        <p>Pingback: <a href='http://advancedvectors.com/?p=172' rel='external nofollow' class='url'>A Gen Y Definition of Leadership | Advanced Vectors</a>()</p>
    </li>
    </li><!-- #comment-## -->
    <li class="post pingback">
        <p>Pingback: <a href='http://performancemarks.com/2013/05/10/happiness-at-work-45-some-highlights-from-this-weeks-collection/' rel='external nofollow' class='url'>Happiness At Work #45 ~ some highlights from this week&#8217;s collection | performance~marks</a>()</p>
    </li>
    </li><!-- #comment-## -->
    <li class="comment even thread-even depth-1" id="dsq-comment-10547">
        <div id="dsq-comment-header-10547" class="dsq-comment-header">
            <cite id="dsq-cite-10547">
http://twitter.com/Palayo                <span id="dsq-author-user-10547">Brian MacIver</span>
            </cite>
        </div>
        <div id="dsq-comment-body-10547" class="dsq-comment-body">
            <div id="dsq-comment-message-10547" class="dsq-comment-message"><p>Jason, thanks for the insight.  Having Trained and Coached Leaders and Managers for almost 40 years, it is interesting to see &#8220;what&#8217;s new&#8221;.<br />
I had my first Management role, 40 years ago, and I learned on-the-job.  40 years and an MBA later, I would not change much of my younger self!<br />
I used the Golden Rule:  &#8220;Do unto others as you would have them do unto you!&#8221;</p>
<p>Ken Blanchard, [@kenblanchard] I believe is offering the best structure for GenY Managers and Managers of GenY employees, in his Servant Leadership.<br />
The greatest Problem GenY Managers are having&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;<br />
is managing GenX employees!<br />
I wish you well.</p>
</div>
        </div>

    </li><!-- #comment-## -->
    <li class="post pingback">
        <p>Pingback: <a href='http://millennialceo.com/guest-blog/millennial-leader-guest-post-jasonrepovs/' rel='external nofollow' class='url'>The Next Millennial Leader - Guest Post by @jasonrepovs - Millennial CEO | Millennial CEO</a>()</p>
    </li>
    </li><!-- #comment-## -->
            </ul>


        </div>

    </div>

<script type="text/javascript">
var disqus_url = 'http://switchandshift.com/a-gen-y-definition-of-leadership';
var disqus_identifier = '10217 http://switchandshift.com/?p=5217';
var disqus_container_id = 'disqus_thread';
var disqus_shortname = 'switchandshift2';
var disqus_title = "A Gen Y Definition of Leadership";
var disqus_config_custom = window.disqus_config;
var disqus_config = function () {
    /*
    All currently supported events:
    onReady: fires when everything is ready,
    onNewComment: fires when a new comment is posted,
    onIdentify: fires when user is authenticated
    */
    
    
    this.language = '';
        this.callbacks.onReady.push(function () {

        // sync comments in the background so we don't block the page
        var script = document.createElement('script');
        script.async = true;
        script.src = '?cf_action=sync_comments&post_id=10217';

        var firstScript = document.getElementsByTagName('script')[0];
        firstScript.parentNode.insertBefore(script, firstScript);
    });
    
    if (disqus_config_custom) {
        disqus_config_custom.call(this);
    }
};

(function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript';
    dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>




		


	</div><aside class="aside beta" style="width: 240px;"><section id="wpb_widget-2" class="section widget widget_wpb_widget"><h2 class="title">About the Author</h2><div id="author-bio-box" style="background: #f8f8f8; border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; color: #333333"><h3><a style="color: #555555;" href="http://switchandshift.com/author/jason-repovs" title="All posts by Jason Repovs" rel="author">Jason Repovs</a></h3><div class="bio-gravatar"><img src="http://switchandshift.com/wp-content/uploads/2014/10/s-circle-logo-250x250-96x96.png" width="70" height="70" alt="" class="avatar avatar-70 wp-user-avatar wp-user-avatar-70 photo avatar-default" /></div><a target="_blank" href="http://www.jasonrepovs.com" class="bio-icon bio-icon-website"></a><a target="_blank" href="http://in/jasonrepovsmarketing/" class="bio-icon bio-icon-linkedin"></a><p class="bio-description">Jason Repovs is the blogger behind The Personal Professional. He’s a professional marketer who also provides insight into the way Generation Y thinks. Jason’s areas of focus include consumer behavior, Gen Y management, personal branding, and social media marketing.</p></div></section><section id="time-child-unwrapped-text-7" class="section widget widget-unwrapped-text"><h2 class="title">Subscribe</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</section><section id="adwidget_imagewidget-5" class="section widget AdWidget_ImageWidget"><a target="_blank" href='http://www.inc.com/author/shawn-murphy' alt='Ad'><img  src='http://switchandshift.com/wp-content/uploads/2015/10/Inc-Ad.png' alt='Ad' /></a></section>		<section id="recent-posts-3" class="section widget widget_recent_entries">		<h2 class="title">Recent Articles</h2>		<ul>
					<li>
				<a href="http://switchandshift.com/you-cant-do-this-alone-time-to-build-a-community">You Can&#8217;t Do This Alone: Time to Build a Community</a>
						</li>
					<li>
				<a href="http://switchandshift.com/6-ways-to-awaken-the-potential-in-your-workforce">6 Ways to Awaken the Potential in Your Workforce</a>
						</li>
					<li>
				<a href="http://switchandshift.com/our-most-cherished-thanksgiving-traditions">Our Most Cherished Thanksgiving Traditions</a>
						</li>
					<li>
				<a href="http://switchandshift.com/how-gratitude-changes-the-way-you-see-the-world">How Gratitude Changes the Way You See the World</a>
						</li>
					<li>
				<a href="http://switchandshift.com/are-you-ready-for-the-digital-workplace">Are You Ready for the Digital Workplace?</a>
						</li>
					<li>
				<a href="http://switchandshift.com/a-necessary-redefinition-of-responsibility-and-leadership">A Necessary Redefinition of Responsibility and Leadership</a>
						</li>
				</ul>
		</section><section id="categories-3" class="section widget widget_categories"><h2 class="title">Articles by Category</h2><label class="screen-reader-text" for="cat">Articles by Category</label><select name='cat' id='cat' class='postform' >
	<option value='-1'>Select Category</option>
	<option class="level-0" value="2196">#SocialLeader</option>
	<option class="level-0" value="588">21st Century Customer Service</option>
	<option class="level-0" value="2454">Being Present</option>
	<option class="level-0" value="32">Blog</option>
	<option class="level-0" value="1126">Bold</option>
	<option class="level-0" value="495">Books</option>
	<option class="level-0" value="1211">Brand Engage</option>
	<option class="level-0" value="104">Business</option>
	<option class="level-0" value="305">Business Heretic&#8217;s Bookstore</option>
	<option class="level-0" value="2508">Change Management</option>
	<option class="level-0" value="1268">Communication</option>
	<option class="level-0" value="654">Company Culture</option>
	<option class="level-0" value="496">Culture</option>
	<option class="level-0" value="2270">Culture Change</option>
	<option class="level-0" value="336">Customer Service</option>
	<option class="level-0" value="1787">Editor&#8217;s Choice</option>
	<option class="level-0" value="2433">Employee Engagement</option>
	<option class="level-0" value="432">Engagement</option>
	<option class="level-0" value="497">Featured</option>
	<option class="level-0" value="1427">Feed Your Brain</option>
	<option class="level-0" value="482">Future Of Leadership</option>
	<option class="level-0" value="1732">Home Page</option>
	<option class="level-0" value="490">Hr</option>
	<option class="level-0" value="2519">Human workplace</option>
	<option class="level-0" value="1385">Humanbiz Community Carnival</option>
	<option class="level-0" value="105">Inspirational</option>
	<option class="level-0" value="103">Leadership</option>
	<option class="level-0" value="1269">Leadership Blog</option>
	<option class="level-0" value="1806">Leadership Presence</option>
	<option class="level-0" value="792">Mentoring</option>
	<option class="level-0" value="1795">Millennials</option>
	<option class="level-0" value="1465">Mini Vlogs</option>
	<option class="level-0" value="1831">My Story Campaign</option>
	<option class="level-0" value="694">New Leadership In The New Economy: Diversity Matters</option>
	<option class="level-0" value="913">Pivot Point</option>
	<option class="level-0" value="2430">Purpose</option>
	<option class="level-0" value="1428">Rebel Heretics Community Feed</option>
	<option class="level-0" value="2395">Rebellious Leaders</option>
	<option class="level-0" value="2391">Rebellious Leadership</option>
	<option class="level-0" value="609">Recognition</option>
	<option class="level-0" value="261">Return On Morale</option>
	<option class="level-0" value="1680">Return On Trust</option>
	<option class="level-0" value="262">Social Era</option>
	<option class="level-0" value="2197">Social Leadership</option>
	<option class="level-0" value="152">Social Media</option>
	<option class="level-0" value="208">Social You</option>
	<option class="level-0" value="841">Strategy</option>
	<option class="level-0" value="493">Strengths</option>
	<option class="level-0" value="796">Switch &Amp; Shift Tv</option>
	<option class="level-0" value="344">Talent</option>
	<option class="level-0" value="1132">The Human Side Tv</option>
	<option class="level-0" value="1061">The Values Revolution</option>
	<option class="level-0" value="363">Transparency</option>
	<option class="level-0" value="101">Uncategorized</option>
	<option class="level-0" value="1125">Vision Cast</option>
	<option class="level-0" value="153">Weekend Post</option>
	<option class="level-0" value="483">Winning Through Engagement</option>
	<option class="level-0" value="1015">Work That Matters Podcast</option>
	<option class="level-0" value="1520">Workplace Morale</option>
	<option class="level-0" value="494">Workplace Optimism</option>
	<option class="level-0" value="180">You: Reinvented</option>
</select>

<script type='text/javascript'>
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "cat" );
	function onCatChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
			location.href = "http://switchandshift.com/?cat=" + dropdown.options[ dropdown.selectedIndex ].value;
		}
	}
	dropdown.onchange = onCatChange;
})();
/* ]]> */
</script>

</section><section id="archives-3" class="section widget widget_archive"><h2 class="title">Archives</h2>		<label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
		<select id="archives-dropdown-3" name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
			
			<option value="">Select Month</option>
				<option value='http://switchandshift.com/2015/11'> November 2015 </option>
	<option value='http://switchandshift.com/2015/10'> October 2015 </option>
	<option value='http://switchandshift.com/2015/09'> September 2015 </option>
	<option value='http://switchandshift.com/2015/08'> August 2015 </option>
	<option value='http://switchandshift.com/2015/07'> July 2015 </option>
	<option value='http://switchandshift.com/2015/06'> June 2015 </option>
	<option value='http://switchandshift.com/2015/05'> May 2015 </option>
	<option value='http://switchandshift.com/2015/04'> April 2015 </option>
	<option value='http://switchandshift.com/2015/03'> March 2015 </option>
	<option value='http://switchandshift.com/2015/02'> February 2015 </option>
	<option value='http://switchandshift.com/2015/01'> January 2015 </option>
	<option value='http://switchandshift.com/2014/12'> December 2014 </option>
	<option value='http://switchandshift.com/2014/11'> November 2014 </option>
	<option value='http://switchandshift.com/2014/10'> October 2014 </option>
	<option value='http://switchandshift.com/2014/09'> September 2014 </option>
	<option value='http://switchandshift.com/2014/08'> August 2014 </option>
	<option value='http://switchandshift.com/2014/07'> July 2014 </option>
	<option value='http://switchandshift.com/2014/06'> June 2014 </option>
	<option value='http://switchandshift.com/2014/05'> May 2014 </option>
	<option value='http://switchandshift.com/2014/04'> April 2014 </option>
	<option value='http://switchandshift.com/2014/03'> March 2014 </option>
	<option value='http://switchandshift.com/2014/02'> February 2014 </option>
	<option value='http://switchandshift.com/2014/01'> January 2014 </option>
	<option value='http://switchandshift.com/2013/12'> December 2013 </option>
	<option value='http://switchandshift.com/2013/11'> November 2013 </option>
	<option value='http://switchandshift.com/2013/10'> October 2013 </option>
	<option value='http://switchandshift.com/2013/09'> September 2013 </option>
	<option value='http://switchandshift.com/2013/08'> August 2013 </option>
	<option value='http://switchandshift.com/2013/07'> July 2013 </option>
	<option value='http://switchandshift.com/2013/06'> June 2013 </option>
	<option value='http://switchandshift.com/2013/05'> May 2013 </option>
	<option value='http://switchandshift.com/2013/04'> April 2013 </option>
	<option value='http://switchandshift.com/2013/03'> March 2013 </option>
	<option value='http://switchandshift.com/2013/02'> February 2013 </option>
	<option value='http://switchandshift.com/2013/01'> January 2013 </option>
	<option value='http://switchandshift.com/2012/12'> December 2012 </option>
	<option value='http://switchandshift.com/2012/11'> November 2012 </option>
	<option value='http://switchandshift.com/2012/10'> October 2012 </option>
	<option value='http://switchandshift.com/2012/09'> September 2012 </option>
	<option value='http://switchandshift.com/2012/08'> August 2012 </option>
	<option value='http://switchandshift.com/2012/07'> July 2012 </option>
	<option value='http://switchandshift.com/2012/06'> June 2012 </option>
	<option value='http://switchandshift.com/2012/05'> May 2012 </option>
	<option value='http://switchandshift.com/2012/04'> April 2012 </option>
	<option value='http://switchandshift.com/2012/03'> March 2012 </option>
	<option value='http://switchandshift.com/2012/02'> February 2012 </option>
	<option value='http://switchandshift.com/2012/01'> January 2012 </option>

		</select>
</section><section id="text-10" class="section widget widget_text">			<div class="textwidget"><iframe src="http://widgets.itunes.apple.com/widget.html?c=us&brc=FFFFFF&blc=FFFFFF&trc=FFFFFF&tlc=FFFFFF&d=Podcast shows we recommend.&t=Get more leadership content&m=podcast&e=podcast&w=220&h=400&ids=741665948,351616584,435836905,647826736,794030859,596047499&wt=playlist&partnerId=&affiliate_id=&at=&ct=" frameborder=0 style="overflow-x:hidden;overflow-y:hidden;width:220px;height: 400px;border:0px; margin-left: -8px;"></iframe></div>
		</section><section id="text-11" class="section widget widget_text"><h2 class="title">Recognition</h2>			<div class="textwidget"><a href="http://servetolead.org/best-21st-century-leadership-blogs-new-media/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://servetolead.org/best-21st-century-leadership-blogs-new-media/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/s2l-badge-21st-century-126x126pr.jpg" style="margin-bottom: 10px;">
</a>

<a href="http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://careerrocketeer.com/2014/09/top-150-career-leadership-blogs-2014.html', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Career-Rocketeer-Career-Blogs-Official-Badge-2014.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.nevermindthemanager.com" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.nevermindthemanager.com', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/Leblin-Badge.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.cmoe.com/top-shared-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.cmoe.com/top-shared-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/10/CMOE-BADGE-2.001.png" style="margin-bottom: 10px;">
</a>

<a href="http://www.skipprichard.com/top-lists/top-leadership-blogs/" onclick="_gaq.push(['_trackEvent', 'outbound-widget', 'http://www.skipprichard.com/top-lists/top-leadership-blogs/', '\n\n']);" target="blank">
<img src="../wp-content/uploads/2014/07/top-leadership-blog-award-300x300.png" style="margin-bottom: 10px;">
</a></div>
		</section></aside></div></div>

</div>




		</div>

		<div id="bottom">

			
<div class="outer-container">

	
		<div class="container">

			<section class="section">

				<div class="columns alt-mobile">
					<ul>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-2" class="widget widget-unwrapped-text"><span><img class="alignnone size-full wp-image-3817" src="http://switchandshift.com/wp-content/uploads/2014/08/SS_white_logo500.png" alt="footer-logo" width="250" height="63" />

<p>There’s a more human way to do business.</p>

<p>In the Social Age, it’s how we engage with customers, collaborators and strategic partners that matters; it’s how we create workplace optimism that sets us apart; it’s how we recruit, retain (and repel) employees that becomes our differentiator. This isn’t a “people first, profits second” movement, but a “profits as a direct result of putting people first” movement.</p>
</span></div>							</li>
													<li class="col-1-3">
								<div id="time-child-unwrapped-text-4" class="widget widget-unwrapped-text"><h2 class="title">Connect</h2><hr></div><div id="time-child-social-media-3" class="widget widget-social-media"><div class="social-icons native-colors"><ul class="alt"><li><a href="https://twitter.com/switchandshift" target="_blank"><i class="icon-twitter"></i></a></li><li><a href="https://plus.google.com/+Switchandshift1/posts" target="_blank"><i class="icon-gplus"></i></a></li><li><a href="https://www.facebook.com/switchandshift" target="_blank"><i class="icon-facebook"></i></a></li></ul></div></div><div id="time-child-unwrapped-text-3" class="widget widget-unwrapped-text"><hr>
<p>email:<a href="mailto:charmian@switchandshift.com"> connect@switch&shift.com</a> <br/>
1133 Ferreto Parkway<br/>Dayton, NV 89403<br/>
</p>
<hr>
<p>
<a href="http://switchandshift.com/terms-and-conditions/">Terms & Conditions</a> &nbsp;|&nbsp; <a href="http://switchandshift.com/privacy-policy/">Privacy Policy</a>
</p></div><div id="time-child-unwrapped-text-5" class="widget widget-unwrapped-text"><h2 class="title">Newsletter Subscription</h2>Do you like our posts? If so, you’ll love our frequent newsletter!
<a href="http://switchandshift.us7.list-manage.com/subscribe?u=6ceff63e5798243a254fa4f47&amp;id=2257a3a7fc">Sign up HERE</a> and receive<i><b>The Switch and Shift Change Playbook</b></i>, by Shawn Murphy, as our thanks to you!</div>							</li>
													<li class="col-1-3">
								<div id="time-child-contact-2" class="widget widget-contact"><h2 class="title">Contact Us</h2><form class="contact-form" action="http://switchandshift.com/wp-admin/admin-ajax.php" method="post"><input name="action" id="action" type="hidden" value="time-child_contact_form" /><p><input type="text" name="name" placeholder="name*" /></p><p><input type="text" name="email" placeholder="e-mail*" /></p><p><input type="text" name="subject" placeholder="subject" /></p><p><textarea class="full-width" name="message"></textarea></p><p><input name="cntctfrm_contact_action" id="cntctfrm-contact-action" type="hidden" value="true" /><input type="hidden" name="cptch_result" value="g8g=" />
				<input type="hidden" name="cptch_time" value="1448822439" />
				<input type="hidden" value="Version: 4.1.5" /><label class="cptch_label" for="cptch_input_59">7 &minus; <input id="cptch_input_59" class="cptch_input" type="text" autocomplete="off" name="cptch_number" value="" maxlength="1" size="1" /> =  &#102;ou&#114;</label></p><p><input type="submit" value="Send&nbsp;&rsaquo;" /><i class="icon-arrows-ccw load"></i><span class="msg small"></span></p></form></div>							</li>
											</ul>
				</div><!-- // .columns -->

			</section>

		</div><!-- // .container -->

	
</div><!-- // .outer-container -->
							<footer class="footer">

					<div class="container">

						<div class="section">
							<p class="small alpha">&copy; Copyright 2015 Switch &amp; Shift</p>
							<p class="small beta">Website Developed by <a href="http:/solutionsred.com" target="blank">Solutions Red</a></p>
						</div>

					</div>

				</footer>
			
		</div>

		        <script type="text/javascript">
        // <![CDATA[
        var disqus_shortname = 'switchandshift2';
        (function () {
            var nodes = document.getElementsByTagName('span');
            for (var i = 0, url; i < nodes.length; i++) {
                if (nodes[i].className.indexOf('dsq-postid') != -1) {
                    nodes[i].parentNode.setAttribute('data-disqus-identifier', nodes[i].getAttribute('data-dsqidentifier'));
                    url = nodes[i].parentNode.href.split('#', 1);
                    if (url.length == 1) { url = url[0]; }
                    else { url = url[1]; }
                    nodes[i].parentNode.href = url + '#disqus_thread';
                }
            }
            var s = document.createElement('script'); 
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
        // ]]>
        </script>
        <script type='text/javascript' src='http://switchandshift.com/wp-includes/js/comment-reply.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/3rd-party.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>
<script type='text/javascript' src='http://switchandshift.com/wp-content/themes/time/data/js/time.min.js?ver=1d05468803804e8153cb17eab60a20d0'></script>

	</body>

</html>
<!-- Dynamic page generated in 1.805 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2015-11-29 10:40:40 -->
