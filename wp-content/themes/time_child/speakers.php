<?php
/* 
Template Name: Speakers
*/
//response generation function

  $response = "";

//function to generate response
function my_contact_form_generate_response($type, $message){

	global $response;

	if($type == "success") $response = "<div class='success'>{$message}</div>";
	else $response = "<div class='error'>{$message}</div>";

}


//response messages
$not_human       = "Human verification incorrect.";
$missing_content1 = "name Please supply all information.";
$missing_content = "Please supply all information.";
$email_invalid   = "Email Address Invalid.";
$message_unsent  = "Message was not sent. Try Again.";
$message_sent    = "Thanks! We got your message.";
$bad_captcha     = "Please enter the correct Captcha."; 
//user posted variables
$message_first_name = $_POST['message_first_name'];
$message_last_name = $_POST['message_last_name'];
$message_position = $_POST['message_position'];
$message_organization = $_POST['message_organization'];
$message_telephone = $_POST['message_telephone'];
$message_email = $_POST['message_email'];
$message_address = $_POST['message_address'];
$message_city = $_POST['message_city'];
$message_state = $_POST['message_state'];
$message_zip = $_POST['message_zip'];
$message_country = $_POST['message_country'];
$message_budget = $_POST['message_budget'];
$message_event_dates = $_POST['message_event_dates'];
$message_event_location = $_POST['message_event_location'];
$message_speaker = $_POST['message_speaker'];
$message_previous_speaker = $_POST['message_previous_speaker'];
$message_additional_comments = $_POST['message_additional_comments'];
$message_keynote = $_POST['message_keynote'];
//if($message_keynote){$message_keynote = 'Yes';} else {$message_keynote = 'NO';}
$message_training = $_POST['message_training'];
//if($message_training){$message_training = 'Yes';} else {$message_training = 'NO';}
$message_retreat = $_POST['message_retreat'];
//if($message_retreat){$message_retreat = 'Yes';} else {$message_retreat = 'NO';}
$message_org_dev = $_POST['message_org_dev'];
//if($message_org_dev){$message_org_dev = 'Yes';} else {$message_org_dev = 'NO';}
$message_comment_other = $_POST['message_comment_other'];
$message_other = $_POST['message_other'];
$message_audience_employee = $_POST['message_audience_employee'];
//if($message_audience_employee){$message_audience_employee = 'Yes';} else {$message_audience_employee = 'NO';}
$message_audience_executive = $_POST['message_audience_executive'];
//if($message_audience_executive){$message_audience_executive = 'Yes';}else {$message_audience_executive = 'NO';}
$message_audience_customer = $_POST['message_audience_customer'];
//if($message_audience_customer){$message_audience_customer = 'Yes';} else {$message_audience_customer = 'NO';}
$message_audience_association = $_POST['message_audience_association'];
//if($message_audience_association){$message_audience_association = 'Yes';} else {$message_audience_association = 'NO';}
$message_audience_other = $_POST['message_audience_other'];
$message_audience_other2 = $_POST['message_audience_other2'];

$human = $_POST['submitted'];

$message = "First Name: $message_first_name
Last Name: $message_last_name
Position: $message_position
Organization: $message_organization
Telephone: $message_telephone
Email: $message_email
Address: $message_address
City: $message_city
State: $message_state
Zip: $message_zip
Country: $message_country
Budget: $message_budget
Event Date(s): $message_event_dates
Event Location: $message_event_location
Speaker: $message_speaker
Previous Speakers: $message_previous_speaker
Additional Comments: $message_additional_comments
Event Format:
	Training: $message_training
	Keynote: $message_retreat
	Organization Development: $message_org_dev
	Other: $message_comment_other
Audience:
	Employees: $message_audience_employee
	Executives: $message_audience_executive
	Customers: $message_audience_customer
	Association: $message_audience_association
	Other: $message_audience_other2
"; 

//php mailer variables

//echo get_option('admin_email');
$to = 'speakers@switchandshift.com';
//$to = 'timdhahn@gmail.com';
$subject = "Someone sent a message from ".get_bloginfo('name');
$headers = 'From: '. $email . "rn" .
  'Reply-To: ' . $email . "rn";
 
if(!$human == 0){  
// using Captcha plugin

    if(function_exists( 'cptch_check_custom_form' ) && cptch_check_custom_form() !== true ) my_contact_form_generate_response("error", $bad_captcha);  //not human!
    else {

      //validate email
      if(!filter_var($message_email, FILTER_VALIDATE_EMAIL))
        my_contact_form_generate_response("error", $email_invalid);
      else //email is valid
      {
        //validate presence of name and message
        if(empty($message_first_name) || empty($message_last_name) || empty($message)){
          my_contact_form_generate_response("error", $missing_content1);
        }
        else //ready to go!
        {
          
		  //echo $message;
		  $sent = wp_mail($to, $subject, strip_tags($message), $headers);
          if($sent) { my_contact_form_generate_response("success", $message_sent); //message sent!
		  $message_first_name = '';
		$message_last_name = '';
$message_position = '';
$message_organization = '';
$message_telephone = '';
$message_email = '';
$message_address = '';
$message_city = '';
$message_state = '';
$message_zip = '';
$message_country = '';
$message_budget = '';
$message_event_dates = '';
$message_event_location = '';
$message_speaker = '';
$message_previous_speaker = '';
$message_additional_comments = '';
$message_keynote = '';
$message_training = '';
$message_retreat = '';
$message_org_dev = '';
$message_comment_other = '';
$message_audience_employee = '';
$message_audience_executive = '';
$message_audience_customer = '';
$message_audience_association = '';
$message_audience_other = '';
$message_audience_other2 = '';
		  }
          else { my_contact_form_generate_response("error", $message_unsent);} //message wasn't sent
        }
      }
    }
  }
  else if ($_POST['submitted']) my_contact_form_generate_response("error", $missing_content);
get_header(); ?>

<div class="outer-container">
	<?php get_template_part('parts/nav-secondary', 'lower'); ?>
	<?php Time::openContent(); ?>
    <?php echo $response; ?>
		<?php if (have_posts()): the_post(); ?>

			<section class="section">
				<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
                <? 
				$postid = get_the_ID();
				switch ($postid) {
					case 20050:
					$message_speaker = 'Vala Ashafar';
					break;
					case 20052:
					$message_speaker = 'Kare Anderson';
					break;
					case 20054:
					$message_speaker = 'Mark Babbit';
					break;
					case 20056:
					$message_speaker = 'Irene Becker';
					break;
					case 20058:
					$message_speaker = 'John Bell';
					break;
					case 20060:
					$message_speaker = 'Margy Bresslour';
					break;
					case 20062:
					$message_speaker = 'Rebel Brown';
					break;
					case 20016:
					$message_speaker = 'Ted Coine';
					break;
					case 20067:
					$message_speaker = 'Todd Dewett';
					break;
					case 20069:
					$message_speaker = 'Josh Dykstra';
					break;
					case 20071:
					$message_speaker = 'Matthew Fritz';
					break;
					case 20073:
					$message_speaker = 'David Houle';
					break;
					case 20075:
					$message_speaker = 'Patti Johnson';
					break;
					case 20077:
					$message_speaker = 'Alan Kay';
					break;
					case 20079:
					$message_speaker = 'Carleen MacKay';
					break;
					case 20081:
					$message_speaker = 'Angela Maiers';
					break;
					case 20083:
					$message_speaker = 'Susan Mazza';
					break;
					case 20085:
					$message_speaker = 'Meghan Biro';
					break;
					case 20087:
					$message_speaker = 'Tim Mcdonald';
					break;
					case 20089:
					$message_speaker = 'John Michel';
					break;
					case 20092:
					$message_speaker = 'Debora Mills-Scofield';
					break;
					case 20026:
					$message_speaker = 'Shawn Murphy';
					break;
					case 20094:
					$message_speaker = 'Kate Nasser';
					break;
					case 20096:
					$message_speaker = 'Dan Newman';
					break;
					case 20099:
					$message_speaker = 'Jamie Notter';
					break;
					case 20101:
					$message_speaker = 'Achim Nowak';
					break;
					case 20103:
					$message_speaker = 'Stan Phelps';
					break;
					case 20105:
					$message_speaker = 'William Powell';
					break;
					case 20107:
					$message_speaker = 'Janice Presser';
					break;
					case 20109:
					$message_speaker = 'Ted Rubin';
					break;
					case 20111:
					$message_speaker = 'Roy Saunderson';
					break;
					case 20113:
					$message_speaker = 'Lisa Shelley';
					break;
					case 20115:
					$message_speaker = 'Frank Sonnenberg';
					break;
					case 20117:
					$message_speaker = 'Jesse Lyn Stoner';
					break;
					case 20119:
					$message_speaker = 'Dana Theus';
					break;
					case 20121:
					$message_speaker = 'Ellen Weber';
					break;
					default:
					$message_speaker = '';
				}				
				?>

					<?php get_template_part('parts/post-thumbnail'); ?>

					<?php if (!Time::io('layout/page/hide_title', 'page/hide_title')) get_template_part('parts/title'); ?>
                   

					<?php if ( has_post_thumbnail() ) {
						echo 'hi';
					} the_content(); ?>

					<?php get_template_part('parts/paginate-links', 'page'); ?>
<hr/>
<h1 style="margin-top: 0; text-align: center;">Request a Booking Today! </h1>                    
					<div id="respond">
  
  <form id="speakerForm" action="<?php the_permalink(); ?>" method="post">
    <p><label for="first_name">First Name<span>*</span>:</label> <input type="text" name="message_first_name" value="<?php echo esc_attr($message_first_name); ?>" required></p>
    <p><label for="last_name">Last Name<span>*</span>:</label> <input type="text" name="message_last_name" value="<?php echo esc_attr($message_last_name); ?>" required></p>
    <p><label for="position">Position/Title: </label><input type="text" name="message_position" value="<?php echo esc_attr($message_position); ?>" ></p>
    <p><label for="organization">Organization<span>*</span>:</label> <input type="text" name="message_organization" value="<?php echo esc_attr($message_organization); ?>" required></p>
    <p><label for="telephone">Telephone:</label> <input type="text" name="message_telephone" value="<?php echo esc_attr($message_telephone); ?>" ></p>
    <p><label for="message_email">Email<span>*</span>:</label> <input type="email" name="message_email" value="<?php echo esc_attr($message_email); ?>" required></p>
    <p><label for="address">Address:</label> <input type="text" name="message_address" value="<?php echo esc_attr($message_address); ?>" ></p>
    <p><label for="city">City:</label> <input type="text" name="message_city" value="<?php echo esc_attr($message_city); ?>" ></p>
    <p><label for="state">State:</label> <input type="text" name="message_state" value="<?php echo esc_attr($message_state); ?>" ></p>
    <p><label for="zip">Zip:</label> <input type="text" name="message_zip" value="<?php echo esc_attr($message_zip); ?>" ></p>
    <p><label for="country">Country:</label> <input type="text" name="message_country" value="<?php echo esc_attr($message_country); ?>" ></p>
	
    	<p><label for="budget">Budget Range:</label> 
    	<select name="message_budget" size="3">
			<option value="$2,000 to $5,000" <? if ($message_budget == '$2,000 to $5,000') echo 'selected'; ?>>$2,000 to $5,000</option>
            <option value="$5,000 to $10,000" <? if ($message_budget == '$5,000 to $10,000') echo 'selected'; ?>>$5,000 to $10,000</option>
            <option value="$10,000+" <? if ($message_budget == '$10,000+') echo 'selected'; ?>>$10,000+</option>
      	</select>
        </p>
		
    <p><label for="event_dates">Event Date(s):</label> <input type="text" name="message_event_dates" value="<?php echo esc_attr($message_event_dates); ?>" ></p>
    <p><label for="event_location">Event Location:</label> <input type="text" name="message_event_location" value="<?php echo esc_attr($message_event_location); ?>" ></p>
    <p><label for="speaker">Speaker(s) of Interest:</label> <input type="text" name="message_speaker" value="<?php echo esc_attr($message_speaker); ?>" ></p>
    <p><label for="previous_speaker">Previous Speakers (if any):</label> <input type="text" name="message_previous_speaker" value="<?php echo esc_attr($message_previous_speaker); ?>" ></p>
	
    <p><label for="additional_comments">Additional Comments:</label>
		<textarea rows="7" placeholder="(Please provide us with as much information as you can about your event: themes, desired outcomes, etc.)" name="message_additional_comments" value="<?php echo esc_attr($message_additional_comments); ?>" ><?php echo esc_attr($message_additional_comments); ?></textarea></p>
		
	<p><label for="event_format" class="label_break">Please describe the event's format:</label><br>
    <input type="checkbox" name="message_keynote" value="Keynote" <? if ($message_keynote == 'Keynote') echo 'checked'; ?>>Keynote<br>
	<input type="checkbox" name="message_training" value="Training &amp; Workshops" <? if ($message_training == 'Training & Workshops') echo 'checked'; ?>>Training &amp; Workshops<br>
    <input type="checkbox" name="message_retreat" value="Executive Retreat" <? if ($message_retreat == 'Executive Retreat') echo 'checked'; ?>>Executive Retreat<br>
    <input type="checkbox" name="message_org_dev" value="Organizational Development" <? if ($message_org_dev == 'Organizational Development') echo 'checked'; ?>>Organizational Development<br>
	<input type="checkbox" name="message_other" value="Association Other" <? if ($message_other == 'Association Other') echo 'checked'; ?>>Other<br>
    <input type="text" class="other_textfield" placeholder="If Other, Please Describe." class="speaker_other" name="message_comment_other" value="<?php echo esc_attr($message_comment_other); ?>">
	</p>

     <p><label for="audience" class="label_break">The audience will be:</label><br>
    <input type="checkbox" name="message_audience_employee" value="Employees" <? if ($message_audience_employee == 'Employees') echo 'checked'; ?>>Employees<br>
	<input type="checkbox" name="message_audience_executive" value="Company Executives" <? if ($message_audience_executive == 'Company Executives') echo 'checked'; ?>>Company Executives<br>
    <input type="checkbox" name="message_audience_customer" value="Customers" <? if ($message_audience_customer == 'Customers') echo 'checked'; ?>>Customers<br>
    <input type="checkbox" name="message_audience_association" value="Association Members" <? if ($message_audience_association == 'Association Members') echo 'checked'; ?> >Association Members<br>
	<input type="checkbox" name="message_audience_other" value="Association Members" <? if ($message_audience_other == 'Association Members') echo 'checked'; ?>>Other<br>
    <input type="text" class="other_textfield" placeholder="If Other, Please Describe." class="speaker_other" name="message_audience_other2" value="<?php echo esc_attr($message_audience_other2); ?>">
    <? echo $message_audience_association;?>
    </p>
	<div class="catpcha_wrapper">
    <?php
		// using Captcha plugin
		if ( function_exists( 'cptch_display_captcha_custom' ) ) {
		?><input type="hidden" name="cntctfrm_contact_action" value="true"><?php
			echo cptch_display_captcha_custom();
		}
	?>
   </div>
	
    <!--<p><label for="message_human">Human Verification*: </label> <input type="text" style="width: 60px;" name="message_human"> + 3 = 5</p> -->
    <input type="hidden" name="submitted" value="1">
	<hr/>
    <p style="text-align: center;"><input type="submit" value="Submit">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="formReset()" value="Reset Form"></p>
  </form>
</div>


				</article>

			</section>

			<?php get_template_part('parts/meta'); ?>
		<?php endif; ?>
	<?php Time::closeContent(); ?>
</div>
<?php get_footer(); ?>
