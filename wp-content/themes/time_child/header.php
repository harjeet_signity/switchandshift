<?php
/**

 * @package    WordPress

 * @subpackage Time

 * @since      1.0

 */

?>

<!DOCTYPE html>

<!--[if lt IE 9]>             <html class="no-js ie lt-ie9" <?php language_attributes(); ?>"><![endif]-->

<!--[if IE 9]>                <html class="no-js ie ie9" <?php language_attributes(); ?>>   <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js no-ie" <?php language_attributes(); ?>>    <!--<![endif]-->

	<head>

		<meta charset="<?php bloginfo('charset'); ?>" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">



		<!--[if lt IE 9]>

			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

			<script src="<?php echo Time::getInstance()->template_uri; ?>/data/js/selectivizr.min.js"></script>

		<![endif]-->

		<title><?php wp_title('-', true, 'right'); ?></title>
<?php wp_enqueue_script("jquery"); ?>
		<?php wp_head(); ?>


<!-- Google Code for Remarketing Tag -->
<!-
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 954219818;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/954219818/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


	</head>



	<body <?php body_class(); ?>>



		<div id="top" <?php

			$class = array();

			if (Time::to('header/hide_bar'))         $class[] = 'no-bar';

			if (Time::to('header/style') == 'fixed') $class[] = 'fixed';

			if (!empty($class)) {

				printf('class="%s"', implode(' ', $class));

			}

		?>>



			<?php get_template_part('parts/background', 'boxed'); ?>



			<div class="upper-container <?php if (Time::to('header/style') == 'fixed') echo 'fixed'; ?>">



				<div class="outer-container">



					<?php if (Time::to_('header/primary')->value('mobile')): ?>

						<nav id="menu" class="mobile">

							<?php Time::navMenu('primary-mobile'); ?>

						</nav>

					<?php endif; ?>



					<?php if (Time::to_('header/search')->value('mobile')): ?>

						<nav id="search" class="mobile">

							<?php get_search_form(); ?>

						</nav>

					<?php endif; ?>



				</div>



				<div class="outer-container <?php if (Time::to('header/style') == 'blank') echo 'blank'; ?>">



					<header class="header">



						<div class="container">



							<div class="mobile-helper vertical-align">


								<?php if (Time::to_('header/primary')->value('mobile')): ?>

									<a href="#menu" class="button" title="<?php _e('Menu', 'time'); ?>"><i class="icon-menu"></i></a>

								<?php endif; ?>







								<?php if (Time::to_('header/search')->value('mobile')): ?>

									<a href="#search" class="button" title="<?php _e('Search', 'time'); ?>"><i class="icon-search"></i></a>
										
								<?php endif; ?>



							</div>



							<?php if (Time::to('header/lang') == 'short'): ?>

								<nav class="lang vertical-align">

									<ul>

										<?php foreach (icl_get_languages('skip_missing=0&orderby=code') as $lang): ?>

											<li <?php if ($lang['active']) echo 'class="current"'; ?>>

												<a href="<?php echo $lang['url']; ?>" title="<?php echo $lang['native_name']; ?>">

													<?php echo $lang['language_code']; ?>

												</a>

											</li>

										<?php endforeach; ?>
											
									</ul>
									
								</nav>

							<?php endif; ?>



							<?php get_template_part('parts/logo'); ?>



							<?php if (Time::to_('header/primary')->value('desktop') && !Time::to('header/logo/center')): ?>

								<nav class="primary vertical-align">

									<?php Time::navMenu('primary-desktop'); ?>
									
								</nav>
								
							<?php endif; ?>
							<?php if(!(is_cart() || is_checkout()) ){ ?>
								<div class="cart-new">
                                <i class="fa fa-shopping-cart cart-contents_shop"></i>

									<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></a>
								</div>
								<div class="cart_area"><?php dynamic_sidebar( 'custom-widget-area' ); ?></div>
							<?php } ?>

						</div>						
						
					</header>


				
					<?php get_template_part('parts/nav-secondary', 'upper'); 
					?>



				</div>



			</div>



			<?php get_template_part('parts/banner'); ?>

			<?php //get_template_part('parts/headline'); ?>
			
