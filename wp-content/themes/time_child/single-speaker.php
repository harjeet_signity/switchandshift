<?php
/**

 * @package    WordPress

 * @subpackage Time

 * @since      1.0

 */
get_header();
?>
<div class="outer-container">
	<!------------ Sub Menu Area----------------------------->
	<nav class="secondary">
		<div class="container">
			<? 
			if(is_single()) { 
				wp_nav_menu( array('menu' => 'Secondary Menu' ) ); 
			} 
			get_template_part('parts/nav-secondary', 'upper'); ?>
		</div>
	</nav>
	<?php 
	// Getting Post ID
	$post_id	=	 get_the_ID(); 
				 
	// Getting Feature image url
	$feat_image          = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
			
	// Getting Speaker Photos
	$speaker_shortcode   = get_field( "testimonial_shortocode" );
					
	// Getting ralated video category id(To query posts based on this id)		
	$term_id = get_field( "related_video" );
	// Fetching all videos related to Speaker
	$args = array(
		'post_type'          => 'videos',
		'order'              => 'DESC',
		'posts_per_page'     => -1,
		'tax_query'          => array(
			array(
				'taxonomy'   => 'video_categories',
				'field'      => 'id',
				'terms'      => $term_id
			)
	  )
	);
	$related_videos = query_posts( $args );
	
	// Getting related product category id(To query posts based on this id)		
	$product_term_id = get_field( "choose_product" );	 
			 
	// Fetching all products related to Speaker
	$args = array(
		'post_type'      => 'product',
		'order'          => 'DESC',
		'posts_per_page' => -1,
		'tax_query'      => array(
								array(
									'taxonomy' => 'product_cat',
									'field'    => 'id',
									'terms'    =>  $product_term_id
								)
						)
	);
	$related_products       = query_posts( $args );
	
	// Fetch speaker meta
	$speaker_meta_data      = get_post_meta( $post_id ,'speaker_meta_data');
	
	// Getting ralated Testimonial category id(To query posts based on this id)		
	$testimonial_term_id = get_field( "select_testimonial" );
	
	// Topics args
	$topic_args = array(
					'post_type'      => 'post',
					'order'          => 'DESC',
					'posts_per_page' => 3,
					'author'         => $post->post_author,
					'tax_query'      => array(
						array(
						'taxonomy'   => 'category',
						'field'      => 'slug',
						'terms'      => 'speaker'
						 )
					  )
					);
													
	$speaker_topics = query_posts( $topic_args );
	
	// Speaker first name
	$speaker_first_name = get_user_meta( $post->post_author , 'first_name', true);
		
	if ( !empty($feat_image ) ) { 
		?>
		<div class="row banner_image top_image_speaking"  style="background-image: url('<?php echo $feat_image; ?>');">
			<div class="col-md-12 banner_textsingle text-center">
				<h2><?php echo $post->post_title; ?></h2>
				<span style="font-family: Avenir Book,serif;"><?php echo $speaker_meta_data[0]['banner_text']; ?></span>
			</div>
			<div class="speaker-detail-social-icons">
				<?php 
				if ( isset($speaker_meta_data) && is_array($speaker_meta_data[0])  ) {
					if ( $speaker_meta_data[0]['fb'] != '' || $speaker_meta_data[0]['twitter'] != '' || $speaker_meta_data[0]['linkedin'] != '' ) {
						?>
						<div class="left-social-icons">
							<span>Follow <?php echo $speaker_first_name; ?></span>
						</div>
						<div class="right-social-icons">
							<ul class="social-icons right-social-icons_banner">
								<?php
										
								// Twitter Icon
								if ($speaker_meta_data[0]['twitter'] != '') {
									?>
									<li class="twiiter_li">
										<a href="<?php 
											if (strpos($speaker_meta_data[0]['twitter'],'http://') === false AND strpos($speaker_meta_data[0]['twitter'],'https://') === false) {
												echo 'https://'.$speaker_meta_data[0]['twitter'];
											} else {
												echo $speaker_meta_data[0]['twitter'];
											}
										?>" target="_blank">
										<i class="icon-twitter"></i>
										</a>
									</li>
									<?php 
								}
											
								// Linkedin Icon
								if ($speaker_meta_data[0]['linkedin'] != '') {
									?>
									<li class="gplus_li">
										<a href="<?php 
											if (strpos($speaker_meta_data[0]['linkedin'],'http://') === false AND strpos($speaker_meta_data[0]['linkedin'],'https://') === false) {
												echo 'https://'.$speaker_meta_data[0]['linkedin'];
											} else {
												echo $speaker_meta_data[0]['linkedin'];
											}
										?>" target="_blank">
										<i class="linkded_in"></i>
										</a>
									</li>
									<?php
								}								
								
											
								// Facebook Icon
								if ($speaker_meta_data[0]['fb'] != '') {
									?>
									<li class="facebook_li">
										<a href="<?php 
											if (strpos($speaker_meta_data[0]['fb'],'http://') === false AND strpos($speaker_meta_data[0]['fb'],'https://') === false) {
												echo 'https://'.$speaker_meta_data[0]['fb'];
											} else {
												echo $speaker_meta_data[0]['fb'];
											}
										?>" target="_blank">
										<i class="icon-facebook"></i>
										</a>
									</li>
									<?php
								}
								?>
							</ul>
						</div>
						<?php
					}
				}
				?>
			</div>
		</div>
		<?php 
	}
?>
<div class="speaker_desc">
	
	
	<div class="speaker-bio">
		<?php  // About Speaker container
			if ( is_array( $speaker_meta_data[0] ) && isset($speaker_meta_data[0]['about_speaker']) && $speaker_meta_data[0]['about_speaker'] != '' ) {
				?>
				<div class="col-md-7 bio">
					<h1>About <?php echo $speaker_first_name; ?></h1>
					<p><?php echo $speaker_meta_data[0]['about_speaker']; ?></p>
					<b>Bring <?php echo $speaker_first_name; ?> into our organization or to your next event to speak about:</b>
					<?php 
						if ( !empty( $speaker_topics )) {
							?>
							<div class="speaker-topics-container">
								<!--<h1><span>Speaker Topics</span></h1>-->
								<ul class="speaker-topics-bullets">
									<?php 
									foreach ( $speaker_topics as $speaker_topic) {
										?>
										<li>
											<a href="<?php echo get_permalink($speaker_topic->ID); ?>"><?php echo $speaker_topic->post_title; ?></a>
										</li>
										<?php
									}
									?>
								</ul>
							</div>
							<?php
						}
					?>
				</div>
				<?php
			}
		?>
		<div class="col-md-5 reel">
			<h1>Learn More About <?php echo $speaker_first_name; ?>...</h1>
			<iframe height="230px" src="<?php echo $speaker_meta_data[0]['reel']; ?>" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>	
	
</div>
<!----------------- Speaker Carousel Testimonial ------------------------------------>	      
<?php
	if(!empty($testimonial_term_id)){  // Only call testimonial template if user has assigned some video category to speaker
		?>
		<div class="testimonial-section">
		<?php
			set_query_var( 'testimonial_term_id', $testimonial_term_id);
			get_template_part( "template", "testimonials" );
		?>
		</div>
		<?php
	}		 
?>
<div class="speaker-content-desc">
	<div class="container">
        <div class="authr-dsc">
		<?php the_content(); ?>
        </div>
	</div>
	<div class="container sponsors-container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8 text-center">
				<h3>To make their workplace more human centered, <?php echo $speaker_first_name; ?> has partnered with these fine organizations:</h3>
			</div>
		</div>
		<?php get_template_part( "template", "sponsors" ); ?>
	</div>	
</div> 
<?php
if( !empty($related_videos ) ) { ?>
	<div class="row related_videos speaker_contact" data-ride="carousel">
		<div class="contact_photos_title video-healine">	
			<center><h1>See <?php echo $speaker_first_name; ?> in Action…</h1></center>
		</div>
		<div class="container">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
					<?php
						$inc = 1;
						foreach( $related_videos as $key => $video ) {
							$curr_post_id 	= $video->ID;
							$video_url		= get_field( "video_url", $curr_post_id );
							$share_link		= end(explode('/', $video_url));
							if ( $key == 0 || $key % 3 == 0 ) {
							?>
							<div class="item  <?php echo $key == 0 ? 'active' : ''; ?>">
								<div class="row"> 
									
							<?php } ?>
									 
							<div class="col-md-4">
								<iframe height="250px" src="https://www.youtube.com/embed/<?php echo $share_link; ?>" frameborder="0" allowfullscreen></iframe>
								<div class="info_area">
									<div class="video_title">
										<h3>
											<?php  
												echo $video_title = strlen($video->post_title) > 50 ? substr($video->post_title,0,50)."..." : $video->post_title ;
											?>
										</h3>
									</div>
									<div class="video_excerpt">
										<?php
											echo $video_excerpt = strlen($video->post_excerpt) > 70 ? substr($video->post_excerpt,0,70)."..." : $video->post_excerpt ;
										?>
									</div>
									<div class="video_link">
										<a href="<?php echo get_permalink($curr_post_id); ?>">Read More</a>							
									</div>
								</div>
							</div>
							 <?php if ( $inc % 3 == 0 || $key == count($related_videos)-1) { 
								echo "</div></div>";
							} 
							$inc++;
						}
					?>
				</div>
				<?php 
					if( count( $related_videos ) > 3 ) {
						?>
						<a data-slide="prev" href="#myCarousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
						<a data-slide="next" href="#myCarousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
						<?php
					}
				?>
			</div>
		</div>	
	</div>
	<?php 
} ?>
	<div class="row contact_photos">
		<div class="col-md-5 speaker_contact">
			<div class="contact_photos_title">
				<h1 class="first_title">Contact <?php echo $speaker_first_name; ?> Today!</h1>
				<!--<h1 class="second_title">Clark to Speak</h1>-->
			</div>
			<div class="contact_form">
				<?php echo do_shortcode( '[contact-form-7 id="25975" title="Speaker Contact"]' ); ?>
			</div>
		</div>

		<div class="col-md-7 speaker-products">
			<div class="contact_photos_title">
				<h1 class="first_title">Recent Events</h1>
			</div>
			<div class="events-container">
				<div class="events-content-container">
					<?php 
					// Include EventBrite
					$organizer_id = get_post_meta($post_id, 'brite_event_organizer' , true);
					if ( isset( $speaker_meta_data[0]['brite_event_organizer'] ) &&  $speaker_meta_data[0]['brite_event_organizer'] != '' ) {
						set_query_var( 'organizer_id', $speaker_meta_data[0]['brite_event_organizer'] );
						get_template_part("template","briteevent");
					}
					?>
				</div>
				<?php 
				if (!empty($related_products)) {
					?>
					<div class="products-container">
						<div class="row brite-events-row">
							<center><h1>Products</h1></center>
						</div>
						<div class="products-row">
							<?php 
							foreach ($related_products as $key => $value) {
								$title   = strlen($value->post_title) > 25 ? substr($value->post_title,0,25).' ...' : $value->post_title; 
								$product = wc_get_product( $value->ID );
								?>
								<div class="col-md-4">
									<?php echo $product->get_image(); // accepts 2 arguments ( size, attr ) ?>
									<h2>
										<a href="<?php echo get_permalink($value->ID); ?>"><?php echo $title; ?></a>
									</h2>
								</div>
								<?php 
							}
							?>
						</div>
						<?php 
						if (count($related_products) > 6) {
							?>
							<div class="products-view-all">
								<a href="<?php echo get_term_link( $product_term_id[0], 'product_cat' ); ?>" class="btn btn-primary">View All</a>
							</div>
							<?php 
						}
						?>
					</div>
					<?php 
				}
				?>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function() {
	  //Set the carousel options
	  jQuery('#quote-carousel').carousel({
		pause: true,
		interval: 4000,
	  });
	  jQuery('#quote-carousel').hover(function () { 
		jQuery(this).carousel('pause') 
	  }, function () { 
		jQuery(this).carousel('cycle') 
	  });
	});
</script>
<?php get_footer(); ?>
