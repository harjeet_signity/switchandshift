<?php
/* 
Template Name: Leaguers Temp
*/

?>



<?php get_header(); ?>


<div class="outer-container">

	<?php Time::openContent(); ?>



		<?php if (have_posts()): the_post(); ?>



			<section class="section">

				<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

                    

					<?php the_content(); ?>
					
					<img alt='' src='http://0.gravatar.com/avatar/8e25cd32d844c17e55463a4a1ace7b0e?s=150&amp;d=http%3A%2F%2Fsolutionsred.com%2Fswitch%2Fwp-content%2Fthemes%2Ftime_child%2Fimg%2Fswitch_user_icon.jpg%3Fs%3D150&amp;r=G' class='alignleft size-thumbnail wp-image-1064 avatar-150 photo' height='150' width='150' />
					<a href="http://solutionsred.com/switch/author/carleen-mackay/" title="View all posts by Carleen MacKay">
						<h2 class="link_title1">Carleen MacKay</h2></a><a href="carleenmackay@sbcglobal.net">carleenmackay@sbcglobal.net</a>
					<ul style="list-style: none;margin-top: 0;margin-bottom:0">
						<li style="display: inline-block;padding:0;"><a class="bio-icon bio-icon-website" style="width: 16px;" target="_blank" href="http://www.agelessinamerica.com">Website</a></li>
						<li style="display: inline-block;padding:0;"><a class="bio-icon bio-icon-linkedin" style="width: 16px;" target="_blank" href="http://www.linkedin.com/pub/carleen-mackay/0/19a/222">Linked In</a></li>
					</ul>
					<p class="author-bio">1. Mid and Late Career Workforce Expert 
					2. Emergent Workforce Expert; particular emphasis on alternative career strategies 
					3. Author/Co-author 4 books (Return of the Boomers, Boom or Bust, Live Smart after 50, Plan “B” for Boomers and multiple products. 
					4. National Keynoter 
					5. Product &amp; Service Developer for Organizational Clients 
					6. Coach Career and Talent Management Professionals in all Sectors of the Economy<br>

					Carleen is particularly interested in helping to develop products and services for team members who are still active in building their businesses.

					Major clients/advisory boards include Career Partners International, the talent management company with the largest global footprint (over 200 offices in 26 countries); the San Diego Workforce Partnership, a non-profit that runs multiple One-Stops in San Diego County, The Intergenerational Center at Chaminade University in Honolulu, Hawaii; The Life Planning Network, a national network for professionals in the “over” 50 life space; The San Diego Mature Workforce Coalition. &nbsp;<a class="readmore" href="http://solutionsred.com/switch/author/carleen-mackay/" title="View all posts by Carleen MacKay">View Author's Articles &raquo;</a></p><p class="author-archive"></p>
					<hr/>


<img alt='' src='http://1.gravatar.com/avatar/b1d0cb2ecdc14b3d2a71f8b7e1050c0f?s=150&amp;d=http%3A%2F%2Fsolutionsred.com%2Fswitch%2Fwp-content%2Fthemes%2Ftime_child%2Fimg%2Fswitch_user_icon.jpg%3Fs%3D150&amp;r=G' class='alignleft size-thumbnail wp-image-1064 avatar-150 photo' height='150' width='150' />
			<a href="http://solutionsred.com/switch/author/aaron-bartlett/" title="View all posts by Aaron Bartlett">
				<h2 class="link_title1">Aaron Bartlett</h2></a><a href="waaronbartlett@gmail.com">waaronbartlett@gmail.com</a>
					<ul style="list-style: none;margin-top: 0;margin-bottom:0">
						<li style="display: inline-block;padding:0;"><a class="bio-icon bio-icon-website" style="width: 16px;" target="_blank" href="http://waaronbartlett.blogspot.com/">Website</a></li>
						<li style="display: inline-block;padding:0;"><a class="bio-icon bio-icon-twitter" style="width: 16px;" target="_blank" href="https://www.twitter.com/bearbartlett">Twitter</a></li>
						<li style="display: inline-block;padding:0;"><a class="bio-icon bio-icon-linkedin" style="width: 16px;" target="_blank" href="http://www.linkedin.com/in/aaronbartlett">Linked In</a></li>
					</ul>
	
	<p class="author-bio">Aaron Bartlett has a passion for strategic sales and business development, and has spent his career launching new products and territories. He lives in his right-brain, and will likely develop the most creative strategies to address a problem. He holds a strong belief that learning never stops, and that you can win with integrity. Proud husband, father, son, and brother to the best family around (no bias). &nbsp;<a class="readmore" href="http://solutionsred.com/switch/author/aaron-bartlett/" title="View all posts by Aaron Bartlett">View Author's Articles &raquo;</a></p><p class="author-archive"></p>
	<hr/>








					<?php get_template_part('parts/paginate-links', 'page'); ?>

				</article>

			</section>



			<?php /*get_template_part('parts/author-bio');*/ ?>

			<?php get_template_part('parts/social-buttons'); ?>

			<?php get_template_part('parts/meta'); ?>

			<?php comments_template(); ?>



		<?php endif; ?>



	<?php Time::closeContent(); ?>



</div>



<?php get_footer(); ?>