<?php

/**

 * @package    WordPress

 * @subpackage Time

 * @since      1.0

 */

?>



<?php if (apply_filters('time_author_bio_display', (bool)Time::io('layout/page/author_bio', array(get_post_type().'/author_bio', 'page/author_bio')))): ?>



	<section class="section">

		<figure class="alignleft fixed inset-border featured author_bio_figure">

			<?php echo userphoto_the_author_thumbnail(get_the_author_meta('ID')); ?>

		</figure>

		<h3 class="author_bio_name"><?php the_author(); ?></h3>

		<p class="author_bio_description"><?php echo nl2br(get_the_author_meta('description')); ?></p>

	</section>



<?php endif; ?>
