<?php

/**

 * @package    WordPress

 * @subpackage Time

 * @since      1.0

 */
?>
<div class="outer-container page-topics">
	<!------------ Sub Menu Area----------------->
	<nav class="secondary">
		<div class="container">
			<? 
			if(is_single()) { 
				wp_nav_menu( array('menu' => 'Secondary Menu' ));
			} 
			?>
		</div>
	</nav>
	<?php 
	// Getting Feature image url
	$feat_image        = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
	$topic_banner_text = get_field( "topic_banner_text" );	
	?>
	<!----------------- Feature Image ------------------------->
	<section class="section sliderspesking">
		<article id="post-<?php the_ID(); ?>" <?php post_class(array('post', 'hentry')); ?>>
		<?php 
		if ( !empty( $feat_image ) ) { 
			?>
			<div class="row banner_image top_image_speaking"  style="background-image: url('<?php echo $feat_image; ?>');">
				<div class="col-md-12 banner_text text-center">
					<h1>Speaking Topic</h1>
					<?php echo $topic_banner_text; ?>
				</div>
			</div>
			<?php 
		} 
		?>
		</article>
	</section>
	<!--------------- Topic Decription ---------------------------->	
	<div class="row topic_desc">
		<div class="container">
			<div class="topic_title"><?php the_title(); ?></div>
			<div class="topic_description"><?php the_content(); ?></div>
		</div>
	</div>
	<!-------------- Topic Related Speakers --------------------->
	<?php 
	$topic_speakers_ids = get_post_meta( $post_id,'topic_speakers' );  
	$args = array(	
		'post__in'  => $topic_speakers_ids[0],
		'post_type' => 'speaker'
	);
	
	$speakers    = get_posts($args);
	$counter     = 1;
	if( !empty( $speakers ) ) { 
		?>
		<div class="row speaker_topics text-center">
			<div class="container">
				<div class="col-md-12">
					<h1>Related Speakers</h1>
					<div class="expertise_text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt neque nec nibh vehicula, at rhoncus purus consectetur. Morbi sit amet sem ut libero consequat tincidunt. Maecenas non lorem accumsan, vehicula lectus vel
					</div>
				</div>
				<?php 
				foreach($speakers as $speaker) { 
					if ( $counter > 3 ) {
						break;
					}
					$speaker_image	=	get_field( "profile_image", $speaker->ID );
					?>
					<div class="col-md-4">
						<?php 
						$speaker_image = !empty($speaker_image) ? $speaker_image : get_stylesheet_directory_uri().'/img/demo_user.jpeg';
						?>
						<div class="profile_image">
							<img class="img-responsive" src="<?php echo $speaker_image; ?>" />
						</div>
						<p class="topic_title">
							<?php 
								echo strlen($speaker->post_title) > 70 ? substr($speaker->post_title,0,70)."..." : $speaker->post_title;
							?>
						</p>
						<a class="topic_link" href="<?php echo get_post_permalink($speaker->ID); ?>">Read More</a>
					</div>
					<?php $counter++; 
				} 
				?>
			</div>
		</div>
		<?php  
	} ?>
	<!------------- Showing comments on Topic ------------------>
	<?php Time::openContent(); ?>
	<?php 
	if (have_posts()): the_post(); ?>
		<section class="section">
			<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
				<?php comments_template(); ?>
				<div class="clear"></div>
			</article>
		</section>			
		<?php 
	endif; ?>
	<?php Time::closeContent(); ?>
</div>
<?php get_footer(); ?>
