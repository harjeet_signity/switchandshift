<?php
/**
 * Template Name: Banner & Text
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
get_header(); ?>

<div class="outer-container banner_text_temp">

	<?php get_template_part('parts/nav-secondary', 'lower'); ?>

	<?php //Time::openContent(); ?>

		<?php if (have_posts()): the_post(); 
			// Getting Post ID
			$post_id	=	 get_the_ID(); 
				 
			// Getting Feature image url
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
			
			// Getting Banner Text
			$page_meta_data      = get_post_meta( $post_id ,'page_meta_data');
		?>
		<?php 
		if (!empty($feat_image)) { ?>
			<div class="row banner_image top_image_speaking"  style="background-image: url('<?php echo $feat_image; ?>');">
				<div class="col-md-12 banner_textsingle text-center">
					<h2><?php echo the_title(); ?></h2>
					<span style="font-family: Avenir Book,serif;"><?php echo $page_meta_data[0]['banner_text']; ?></span>
				</div>
			</div>
			<?php 
		} 
		?>
			<section class="section">
				<article id="post-<?php the_ID(); ?>" <?php post_class(array('post', 'hentry')); ?>>
					<div class="">
						<div class="main">
							<?php the_content(); ?>
						</div>			
					</div>
					<?php get_template_part('parts/paginate-links', 'page'); ?>					
				</article>				
			</section>

			<?php get_template_part('parts/author-bio'); ?>
			<?php get_template_part('parts/social-buttons'); ?>
			<?php get_template_part('parts/meta'); ?>
			<?php comments_template(); ?>

		<?php endif; ?>

	<?php //Time::closeContent(); ?>

</div>
<?php get_footer(); ?>
