<?php

/**

 * @package    WordPress

 * @subpackage Time

 * @since      1.0

 */

get_header(); ?>
<div class="outer-container page-topics">
	<!------------ Sub Menu Area--------------------------------------->
	<nav class="secondary">
		<div class="container">
		<?php //if(is_single()) { wp_nav_menu( array('menu' => 'Secondary Menu' ));} ?>
    		<?php //get_template_part('parts/nav-secondary', 'upper'); ?>
		</div>
	</nav>
	
	<!---- Getting Custom field values and related videos ------------------------>
	<?php 
		/************************  Fetching Speaker Basic Details ***********************/
			// Getting Post ID
			$post_id	=	 get_the_ID(); 
				 
			// Getting Feature image url
			$feat_image        = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );			
			$topic_banner_text = get_field( "topic_banner_text" );
			
			
	?>
	<!----------------- Feature Image --------------------------------->
	<section class="section sliderspesking">
		<article id="post-<?php the_ID(); ?>" <?php post_class(array('post', 'hentry')); ?>>
		<?php 
			if (!empty($feat_image)) { ?>
			<div class="row banner_image top_image_speaking"  style="background-image: url('<?php echo $feat_image; ?>');">
				<div class="col-md-12 banner_text text-center">
					<h1>Speaking Topic</h1>
					<?php echo $topic_banner_text; ?>
				</div>
			</div>
			<?php 
		} 
		?>
		</article>
	</section>
	<!---------------------------------------- Topic Decription -------------------------------------------->	
	<div class="row topic_desc">
		<div class="container">
			<div class="topic_title"><?php the_title(); ?></div>
			<div class="topic_description"><?php the_content(); ?></div>
		</div>
	</div>
	<!------------------------------------- Topic Related Speakers ----------------------------------------->
	<?php 
		$terms = wp_get_post_terms( $post_id, 'topic_categories', $args );
		$counter = 1;
	?>	
	<?php  if(!empty($terms)) { ?>
	<div class="row speaker_topics text-center">
		<div class="container">
			<div class="col-md-12">
				<h1>Related Speakers</h1>
				<div class="expertise_text">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tincidunt neque nec nibh vehicula, at rhoncus purus consectetur. Morbi sit amet sem ut libero consequat tincidunt. Maecenas non lorem accumsan, vehicula lectus vel
				</div>
			</div>
			<?php foreach($terms as $term) { 
						if ($counter > 3){
							break;
						}
						$args = array(
						  'name'        => $term->slug,
						  'post_type'   => 'speaker',
						  'post_status' => 'publish',
						  'numberposts' => 1
						);
						$single_speaker = get_posts($args);
						$speaker_id		=	$single_speaker[0]->ID;
						$speaker_image	=	get_field( "profile_image", $speaker_id );
			?>
			<div class="col-md-4">
				<?php if(!empty($speaker_image)) { ?><img class="img-responsive" src="<?php echo $speaker_image; ?>" /><?php } ?>
				<p class="topic_title">
					<?php 
						if (strlen($single_speaker[0]->post_title) > 70) {
							echo substr($single_speaker[0]->post_title,0,70)."...";									
						} else {
							echo $single_speaker[0]->post_title;							
						} 
					?>
				</p>
				<a class="topic_link" href="<?php echo get_post_permalink($speaker_id); ?>">Read More</a>
			</div>
			<?php 
				$counter++;
				} 
			?>
		</div>
    </div>
	<?php  } ?>
	<!-------------------------------------------- Showing comments on Topic ------------------------------------>
	<?php Time::openContent(); ?>



		<?php if (have_posts()): the_post(); ?>



			<section class="section">

				<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
					<?php comments_template(); ?>
<div class="clear"></div>

				</article>

			</section>



			<?php /*get_template_part('parts/author-bio');*/ ?>

			<?php get_template_part('parts/social-buttons'); ?>

			<?php get_template_part('parts/meta'); ?>

			



		<?php endif; ?>



	<?php Time::closeContent(); ?>
</div>



<?php get_footer(); ?>
