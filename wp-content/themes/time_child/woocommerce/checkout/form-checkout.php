<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>
<div class="row loginrow">
	<?php if(!(is_user_logged_in())) { ?> 			
	<div class="col-md-6">
	<div class="inner-container">
		<?php do_action( 'woocommerce_before_checkout_form', $checkout ); ?>
		</div>
	</div>
	<div class="col-md-6">
	<div class="inner-container">
		<h3>New customer? Create an account here…</h3> 	
		<p>During Checkout you can create an account to track your orders, make a Wish List and more</p>	
		<a href="javascript:;" class="btn btn-alert continue_button">Continue</a>
		</div>			
	</div>
	<?php } ?>
</div>
	<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">
		<div class="row checkout_row" >
			<div class="col-md-12">
				<h3 id="order_review_heading"><?php _e( 'Your order...', 'woocommerce' ); ?></h3>
				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php do_action( 'woocommerce_checkout_order_review' ); ?>				
				</div>
			</div>
		</div>
	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>	

	<?php endif; ?>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
	<?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Place Order" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>
</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
