<?php
/**
 * Customer processing order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#fdfdfd;border:1px solid #dcdcdc;border-radius:3px!important">
	<tbody>
	<tr>
		<td valign="top" align="center">											
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#ffffff;border-radius:3px 3px 0 0!important;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif">
				<tbody>
					<tr>
						<td align="center" style="padding:36px 48px;display:block">
						<img style="border:none;display:inline;font-size:14px;font-weight:bold;min-height:auto;line-height:100%;outline:none;text-decoration:none;text-transform:capitalize" alt="Switch &amp; Shift" src="https://ci3.googleusercontent.com/proxy/-JCQm4eWSSflkRRXt3zF9Iim0GdCxoeochcdh_9U1_Vkd2d3MgbsK5EB9TE9-njoL4DGImNdJ4NgBly_s6RssH_yakpBaMCfrEFa5vjDWYWyj9BxjnfS3U--ldhbXrmI2BoLhIhW-C8mawk=s0-d-e1-ft#http://dev2.signitysolutions.co.in/switchandshift/wp-content/uploads/2014/07/logo.jpg" class="CToWUd">
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
</table>
<?php do_action('woocommerce_email_header', $email_heading); ?>
<p><?php _e( "Your order has been received... you'll receive your Switch & Shift Swag soon!", 'woocommerce' ); ?></p>

<p><?php _e( "Here are the details of your order:", 'woocommerce' ); ?></p>
<br/>

<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>

<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( $order->is_download_permitted(), true, $order->has_status( 'processing' ) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:left;  <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>
<br/>
<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>
<br/>
<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>
