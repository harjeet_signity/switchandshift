 <?php

/**

 * @package    WordPress

 * @subpackage Time Child

 * @since      1.0

 */

function recent_posts_function() {
   query_posts(array('orderby' => 'date', 'order' => 'DESC' , 'showposts' => 1, 'offset' => 0, 'category' => -497));
   if (have_posts()) :
      while (have_posts()) : the_post();
         $return_string = '<a href="'.get_permalink().'">'.get_the_title().'</a>';
      endwhile;
   endif;
   wp_reset_query();
   return $return_string;
}

function register_shortcodes(){
   add_shortcode('recent-posts', 'recent_posts_function');
}

add_action( 'init', 'register_shortcodes');



function pippin_list_authors() {
 
	$authors = get_users(array(
			'orderby' => 'display_name',
			'count_totals' => false,
			'include' => array(19,190,52,41,9,27,73,79,2,34,78,139,158,126,6,77,26,21,189,132,180,46,20,24,117,17,87,67,55,39,58,76,82,8,48,38)
		)
	);
 
	$list = '';
 	$list_author = '';
	if($authors) :
 
		wp_enqueue_style('author-list', plugin_dir_url(__FILE__) . '/css/author-list.css');
 
		
 usort($authors, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);'));
			foreach($authors as $author) :

					$archive_url = get_author_posts_url($author->ID);
 
					$list .= get_avatar($author->user_email, 150);
					$list .= str_replace("class='avatar author_avatar", "class='alignleft size-thumbnail wp-image-1064", $class);
					$list .= '<a href="'. $archive_url . '" title="' . __('View all posts by ', 'pippin') . $author->display_name . '"><h2 class="link_title1">' . $author->display_name . '</h2></a>';
					$list .= '<a href="' . $author->user_email . '">' . $author->user_email . '</a>';
					$list .= '<p class="author-bio">' . get_user_meta($author->ID, 'description', true) . ' &nbsp;<a class="readmore" href="'. $archive_url . '" title="' . __('View all posts by ', 'pippin') . $author->display_name . '">' . __('View Author\'s Articles &raquo;', 'pippin') . '</a></p>';
					$list .= '<p class="author-archive"></p>';
					$list .= '<hr/>';
			endforeach;
	endif;
	return $list;
}
add_shortcode('authors', 'pippin_list_authors');
add_filter('get_avatar','change_avatar_css');

function change_avatar_css($class) {
$class = str_replace("class='avatar", "class='alignleft size-thumbnail wp-image-1064", $class) ;
return $class;
}





function new_excerpt_more($output) {
    return $output . '... ';
}
add_filter('get_the_excerpt', 'new_excerpt_more');

function contact_authors($atts) {
 $atts = shortcode_atts(
		array(
			'orderby' => 'display_name',
			'count_totals' => false,
			'include' => ''
		), $atts, 'c_authors' );
 
	$authors = get_users( $atts
	);
 
	$list1 = '';
	if($authors) :
 
		wp_enqueue_style('author-list', plugin_dir_url(__FILE__) . '/css/author-list.css');
 
		
 usort($authors, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);'));
			foreach($authors as $author) :
					$archive_url = get_author_posts_url($author->ID);
					$website = $author->user_url;
					$twitter = get_user_meta($author->ID, 'twitter', true);
					$facebook = get_user_meta($author->ID, 'facebook', true);
					$google = get_user_meta($author->ID, 'googleplus', true);
					$linkedin = get_user_meta($author->ID, 'linkedin', true);
					$flickr = get_user_meta($author->ID, 'flickr', true);
					$tumblr = get_user_meta($author->ID, 'tumblr', true);
					$vimeo =  get_user_meta($author->ID, 'vimeo', true);
					$youtube = get_user_meta($author->ID, 'youtube', true);
					
					$list1	.=	'<div class="row single_speaker speakerrow">
									<div class="col-md-4 speaker_left text-center">
										<div class="profile_image">';
					$list1	.=				get_avatar($author->user_email, $size = '250');
					$list1	.=			'</div>';
					$list1	.=			'<div class="author_email">';
					$list1	.=				'<a href="mailto:' . $author->user_email . '">' . $author->user_email . '</a>';
					$list1	.=			'</div>';
					$list1	.=			'<div class="follow_me">
											';
												if($author->user_url != ''){
													$list1 .= '<a class="icon-link" target="_blank" href="' . $website . '"></a>';
												}
												if($twitter !=''){
													$list1 .= '<a class="icon-twitter" target="_blank" href="' . $twitter . '"></a>';
												}
												if($google != ''){
													$list1 .= '<a class="icon-gplus" target="_blank" href="' . $google . '"></a>';
												}
												if($facebook != ''){
													$list1 .= '<a class="icon-facebook" target="_blank" href="' . $facebook . 	'"></a>';
												}
												if($linkedin != ''){
													$list1 .= '<a class="icon-linkedin" target="_blank" href="' . $linkedin . '"></a>';
												}
												if($flickr != ''){
													$list1 .= '<a class="icon-flickr" target="_blank" href="' . $flickr . '"></a>';
												}
												if($tumblr != ''){
													$list1 .= '<a class="icon-tumblr" target="_blank" href="' . $tumblr . '"></a>';
												}
												if($vimeo != ''){
													$list1 .= '<a class="icon-vimeo" target="_blank" href="' . $vimeo . '"></a>';
												}
												if($youtube != ''){
													$list1 .= '<a class="icon-youtube" target="_blank" href="' . $youtube . '"></a>';
												}
					$list1	.=				'
										</div>
									</div>
									<div class="col-md-8 speaker_right">';
					$list1 .=			 '<a href="'. $archive_url . '" title="' . __('View all posts by ', 'pippin') . $author->display_name . '">
											<h2 class="link_title1">' . $author->display_name . '
											</h2>
										  </a>
										  <div class="speaker_content">';
					$list1 .= 				  get_user_meta($author->ID, 'description', true);							
					$list1 .=			  '</div>';
					$list1 .= 			  '<a class="btn btn-primary" href="'. $archive_url . '" title="' . __('View all posts by ', 'pippin') . $author->display_name . '">' . __('View Author\'s Articles &raquo;', 'pippin') . '</a>';
					$list1 .=		'</div>';
					$list1 .=	'</div>';			
					
			endforeach;
	endif;
	return $list1;
}
add_shortcode('c_authors', 'contact_authors');


//sidebar widget
// Creating the widget 
class wpb_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'wpb_widget', 

// Widget name will appear in UI
__('About the Author', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'Displays author information', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
echo __( get_author_bio_box() );
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'wpb_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );


add_shortcode('list-users', 'list_users');

function sul_user_listing($atts, $content = null) {
  global $post;
  
  extract(shortcode_atts(array(
    "include" => ''
  ), $atts));
  

  // We're outputting a lot of HTML, and the easiest way 
  // to do it is with output buffering from PHP.
  ob_start();
  
    $my_users = new WP_User_Query( 
      array( 
        'include' => array('. $include .'), 
       
      ));

  // The authors object. 
  $authors = $my_users->get_results();

  // loop through each author
  foreach($authors as $author){
    $author_info = get_userdata($author->ID);
    ?>
    <li>
      <?php echo get_avatar( $author->ID, 90 ); ?> 
      <?php echo $author_info->linkedin; ?>
      <h2><a href="<?php echo get_author_posts_url($author->ID); ?>"><?php echo $author_info->display_name; ?></a> - <?php echo count_user_posts( $author->ID ); ?> posts</h2>
      <p><?php echo $author_info->description; ?></p>
      <?php $latest_post = new WP_Query( "author=$author->ID&post_count=1" ); 
      if (!empty($latest_post->post)){ ?>
      <p><strong>Latest Article:</strong>
      <a href="<?php echo get_permalink($latest_post->post->ID) ?>">
        <?php echo get_the_title($latest_post->post->ID) ;?>
      </a></p>
      <?php } //endif ?>
      <p><a href="<?php echo get_author_posts_url($author->ID); ?> ">Read <?php echo $author_info->display_name; ?> posts</a></p>
    </li>
    <?php
  }
?>
  </ul> <!-- .author-list -->

  
  <?php 
  // Output the content.
  $output = ob_get_contents();
  ob_end_clean();

  
  // Return only if we're inside a page. This won't list anything on a post or archive page. 
  if (is_page()) return  $output;

}

// Add the shortcode to WordPress. 
add_shortcode('userlisting', 'sul_user_listing');

// String limit words
function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}

add_action( 'pre_get_posts', 'alter_search_query' );

function wptuts_scripts_basic()
{
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_style( 'bootstrap.css', get_template_directory_uri() . '/css/bootstrap.css');
    wp_enqueue_style( 'flexslider.css', get_stylesheet_directory_uri() . '/css/flexslider.css');
    wp_enqueue_script( 'flexslider.js', get_stylesheet_directory_uri() . '/js/jquery.flexslider.js');
    wp_enqueue_script( 'main.js', get_stylesheet_directory_uri() . '/js/functions.js');
    
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );


function alter_search_query( $query ) {
  // not an admin page and is the main query
  if (!is_admin() && $query->is_main_query()){
    if(is_search() && ($query->query_vars['s'] == 'none')){

    	$author = get_query_var('author_name');
    	if($author != '') {
      	$query->set('author_name', $author);
      	// set search query 'none' to empty string ''
      	$query->set('s', '');
      	$query->set('post_type', 'post');
      }
    }
  }
}
function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}

add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) { 
	// Define the style_formats array
	$style_formats = array( 

		// Each array child is a format with it's own settings
		array( 

			'title'   => 'Blue Headline', 

			'block'   => 'span', 

			'classes' => 'blue-button',

			'wrapper' => true,

		),
		array( 

			'title'   => 'Blue Background', 

			'block'   => 'span', 

			'classes' => 'blue-background',

			'wrapper' => true,

		)

	); 

	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats ); 
	return $init_array;
}

// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 

function save_post_callback( $post_id ) {
	if ($_POST['post_type'] == 'speaker' && $_POST['original_post_status'] == 'auto-draft') {
		//wp_insert_term( $_POST['post_title'], 'topic_categories', $args = array() );
	}
	else if($_POST['post_type'] == 'speaker') {
		
		update_post_meta($_POST['post_ID'],'speaker_meta_data',$_POST['speaker_meta_data']);
	}else if($_POST['post_type'] == 'page') {		
		update_post_meta($_POST['post_ID'],'page_meta_data',$_POST['page_meta_data']);
	}
}
add_action( 'save_post', 'save_post_callback' );

function sponsorsCallback( $atts ) {
	global $wpdb;	
	// Fetch all sponsors	
	$table        = $wpdb->prefix."sponsors";
	$sponsors     = $wpdb->get_results("SELECT guid,link FROM ".$table);
	$return       = "";
	if(!empty($sponsors)) { 
		$link    = $sponsor->link != '' ? $sponsor->link : 'javascript:void(0)';
		$return .= '
			<div class="sponsors-content-container flexslider">
				<ul class="slides">';
					foreach ($sponsors as $sponsor) {
						$return .= '<li class="sponsor-row">
							<a href="'.$link.'">
								<img src="'.$sponsor->guid.'">
							</a>
						</li>';
					}
				$return .= '</ul>
			</div>';
	}
    return $return;
}
add_shortcode( 'sponsors', 'sponsorsCallback' );

add_action( 'add_meta_boxes', 'time_child_add_meta_box' );

function time_child_add_meta_box() {
	add_meta_box( 'briteevent', 'Brite Event Organizer', 'brite_event_organizer', 'speaker', 'normal' );
}

function brite_event_organizer($post) {
	$eventbrideurl    =    "https://www.eventbriteapi.com/v3/users/".ACCOUNT_ID."/organizers/?token=".PERSONAL_OAUTH_TOKEN;
	$ch = curl_init();
	$curlConfig = array(
		CURLOPT_URL            => $eventbrideurl,
		CURLOPT_POST           => false,
		CURLOPT_RETURNTRANSFER => true
		
	   
	);
	curl_setopt_array($ch, $curlConfig);
	$result   = curl_exec($ch);
	$result   = json_decode($result,true);
	
	$data = get_post_meta($post->ID, 'speaker_meta_data');
	
	?>
	<select name="speaker_meta_data[brite_event_organizer]">
		<option id="">Choose organizer</option>
		<?php 
		if ( !empty( $result['organizers'] ) ) {
			foreach ( $result['organizers'] as $value ) {
				?>
				<option value="<?php echo $value['id']; ?>" <?php echo $value['id'] == $data[0]['brite_event_organizer'] ? "selected" : ""; ?>><?php echo $value['name']; ?></option>
				<?php
			}
		}
		?>
	</select>
	<?php
}

// Hook to add custom fields after post editor
add_action( 'edit_form_after_editor', 'myprefix_edit_form_after_editor' );

/**
 * Callback function of hook to add custom field after post content
 * 
**/
function myprefix_edit_form_after_editor($post) {
	if ($post->post_type == 'speaker') {
		
		$data = get_post_meta($post->ID, 'speaker_meta_data');
		
		?>
		<div class="speaker-custom-fields-container">
			<div class="time-child-row">
				<label>About Speaker</label>
				<?php 
				$settings = array("textarea_name" => "speaker_meta_data[about_speaker]");
				wp_editor( $data[0]['about_speaker'], 'about-speaker', $settings ); ?>
			</div>
			<div class="time-child-row">
				<label>Speaker Reel Url: </label>
				<input type="text" name="speaker_meta_data[reel]" value="<?php echo $data[0]['reel'];  ?>" placeholder="Speaker Reel" />
			</div>
			<div class="time-child-row">
				<label>Banner Text:</label>
				<?php 
				$settings = array("textarea_name" => "speaker_meta_data[banner_text]");
				wp_editor( $data[0]['banner_text'], 'banner-text', $settings ); ?>
			</div>
			<div class="time-child-row">
				<label>Facebook Url</label>
				<input type="text" name="speaker_meta_data[fb]" value="<?php echo $data[0]['fb'];  ?>" placeholder="Facebook" />
			</div>
			<div class="time-child-row">
				<label>Twitter Url</label>
				<input type="text" name="speaker_meta_data[twitter]" value="<?php echo $data[0]['twitter'];  ?>" placeholder="Twitter" />
			</div>
			<div class="time-child-row">
				<label>Linkedin Url</label>
				<input type="text" name="speaker_meta_data[linkedin]" value="<?php echo $data[0]['linkedin'];  ?>" placeholder="Linkedin" />
			</div>	
		</div>
		<?php 
	} elseif($post->post_type == 'page') {
		$data = get_post_meta($post->ID, 'page_meta_data');				
		?>
		<div class="speaker-custom-fields-container">
			<div class="time-child-row">
				<label>Banner Text:</label>
				<?php 
				$settings = array("textarea_name" => "page_meta_data[banner_text]");
				wp_editor( $data[0]['banner_text'], 'banner-text', $settings ); ?>
			</div>
		</div>
		<?php 		
	}
}

// Hook to custom css file in admin
add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
	wp_register_style( 'admin_css', get_stylesheet_directory_uri() . '/css/admin-custom-style.css', false, '1.0.0' );
	wp_enqueue_style( 'admin_css', get_stylesheet_directory_uri() . '/css/admin-custom-style.css', false, '1.0.0' );
}

/**
 * Remove the slug from published post permalinks.
 */
function custom_remove_cpt_slug( $post_link, $post, $leavename ) {
    
    if ( 'speaker' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

   $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}
add_filter( 'post_type_link', 'custom_remove_cpt_slug', 10, 3 );
/**
 * Some hackery to have WordPress match postname to any of our public post types
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * Typically core only accounts for posts and pages where the slug is /post-name/
 */
function custom_parse_request_tricksy( $query ) {
    
    if ( array_key_exists('speaker', $query->query)) {
		unset($query->query['speaker']);
		unset($query->query['post_type']);
		unset($query->query_vars['speaker']);
		unset($query->query_vars['post_type']);
	}
    
    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;

    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'speaker', 'page' ) );
    }
}
add_action( 'pre_get_posts', 'custom_parse_request_tricksy' );

add_filter( 'wck_cptc_register_post_type_args', 'my_register_without_front', 10, 2 );
function my_register_without_front( $args, $post_type ) {
  if ( 'speaker' == $post_type )
	 $args['rewrite']['slug'] = false;
    $args['rewrite']['with_front'] = false;
    return $args;
}


// Custom widget area.
register_sidebar( array(
    'name' => __( 'Custom Cart Widget Area'),
    'id' => 'custom-widget-area',
    'description' => __( 'Widget Area to display cart', 'time_child' ),
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => "</li>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );

 if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Custom Cart Widget Area') ) :
 endif;
 
 /**
 * Changes the redirect URL for the Return To Shop button in the cart.
 *
 * @return string
 */
function wc_empty_cart_redirect_url() {
	return site_url().'/products';
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></a>
	<?php
	$fragments['a.cart-contents'] = ob_get_clean();
	return $fragments;
}

// Custom widget area to show social icons on header.
register_sidebar( array(
    'name' => __( 'Social Icons Widget Area'),
    'id' => 'social-icons-widget-area',
    'description' => __( 'Widget Area to display social icons on header', 'time_child' ),
    'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );


// Change cart page error messages woocommerce
add_filter('login_errors','login_error_message');

function login_error_message($error){ 
    //check if that's the error you are looking for
    if (is_int(strpos($error, 'A user could not be found with this email address'))) {
        //if invalid email address
        $error = "Oops! We couldn’t find that email address in our files. Please try again or create a new account."; 
    } else if (is_int(strpos($error, 'The password you entered for the username')) || is_int(strpos($error, 'The username or password you entered is incorrect'))) {	
		$error = "Oops! Your password doesn’t match our records… <a href='".wp_lostpassword_url()."'>need a hint?</a>";
	}
    return $error;
}

//Change the Billing Address checkout label
function wc_billing_field_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
		/// Comes Under Billing Address Fields
        case 'Town / City' :
            $translated_text = __( 'City', 'woocommerce' );
            break;
        case 'Address' :
            $translated_text = __( 'Primary Address', 'woocommerce' );
            break;
        case 'Expiry (MM/YY)' :
            $translated_text = __( 'Expiration Date (MM/YY)', 'woocommerce' );
            break;
        /// Comes under Authorize.net card deail
        case 'Card Code' :
            $translated_text = __( 'CVV', 'woocommerce' );
            break;
        case 'CVC' :
            $translated_text = __( 'CVV', 'woocommerce' );
            break;
    }
    return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_address_2']['label'] = 'Secondary Address';
     return $fields;
}

// Remove disqus comments on single Product page to show default reviews
remove_action('pre_comment_on_post', 'dsq_pre_comment_on_post');
add_action( 'the_post' , 'block_disqus');
function block_disqus() {
    if ( get_post_type() == 'product' )
        remove_filter('comments_template', 'dsq_comments_template');
}
?>
