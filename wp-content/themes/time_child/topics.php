<?php
/**
 * Template Name: Topics
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
get_header(); ?>
<div class="outer-container page-topics">
	<?php 
		get_template_part('parts/nav-secondary', 'lower'); 
		if (have_posts()): the_post(); ?>
			<?php
				// Getting Post ID
				$post_id	=	 get_the_ID(); 
					 
				// Getting Feature image url
				$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
				
				// Getting Banner Text
				$banner_text = get_field( "topic_banner_text", $post_id );
 			?>
			<section class="section sliderspesking">
				<article id="post-<?php the_ID(); ?>" <?php post_class(array('post', 'hentry')); ?>>
					<?php 
						if (!empty($feat_image)) { ?>
							<div class="row banner_image top_image_speaking"  style="background-image: url('<?php echo $feat_image; ?>');">
								<div class="col-md-12 banner_text text-center">
									<?php echo $banner_text; ?>
								</div>
							</div>
							<?php 
						} ?>
				</article>
			</section>
			<?php 
		endif; 
	?>
	<div class="container">
		<div class="row speakers_header">
			<p>Speaking Topics</strong></p>
		</div>
		<?php
			// Fetch total topics for pagination
			$posts_array    = array(
				'post_type'      => 'post',
				'order'          => 'DESC',
				'tax_query'      => array(
						array(
						'taxonomy'   => 'category',
						'field'      => 'slug',
						'terms'      => 'speaker'
						 )
					  )
			);
			$total_topics = query_posts( $posts_array );
			$total_topics = count( $total_topics );
			
			// Set Pagination Variables
			$paged          = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			$posts_per_page = get_option('posts_per_page');
			
			// Fetch topics based on page and offset
			$posts_array    = array(
				'posts_per_page' => $posts_per_page,
				'post_type'      => 'post',
				'order'          => 'DESC',
				'paged'          => $paged,
				'tax_query'      => array(
						array(
						'taxonomy'   => 'category',
						'field'      => 'slug',
						'terms'      => 'speaker'
						 )
					  )
			);
			$topics = query_posts( $posts_array );
			foreach($topics as $topic) {
				// Getting Post ID
				$post_id	  =	$topic->ID; 
				$summary      = get_field( "summary", $post_id );
					 
				// Getting Speaker Profile Image
				$topic_image  =  wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
				?>
				<div class="row single_speaker speakerrow">
					<div class="col-md-4 speaker_left text-center">
						<?php 
							$topic_image = $topic_image != '' ? $topic_image : get_stylesheet_directory_uri().'/img/image_not_available.jpg';
						?>
						<div class="profile_image" style="background-image: url('<?php echo $topic_image; ?>');">
							
						</div>
					</div>
					<div class="col-md-8 speaker_right">
						<a href="<?php echo get_permalink($post_id); ?>">
							<h2><?php echo $topic->post_title; ?></h2>
						</a>
						<div class="speaker_content">
							<?php echo $summary; ?>
						</div>
						<?php 
						
						// Fetch Topic Speakers
						$topic_speakers_ids = get_post_meta( $post_id,'topic_speakers' );
						if ( is_array( $topic_speakers_ids[0] ) ) {
							$args = array(	
								'post__in'  => $topic_speakers_ids[0],
								'post_type' => 'speaker'
							);
						
							$speakers    = query_posts($args);
							?>
							<div class="row speaker_videos">
								<h3>Our Featured Speakers</h3>
								<ul class="topic-authors">
									<?php 
									foreach ( $speakers as $speaker ) {
										?>
										<li>
											<a href="<?php echo get_permalink($speaker->ID); ?>"><?php echo $speaker->post_title; ?></a>
										</li>
										<?php
									}
									
									wp_reset_query();
									?>
								</ul>
							</div>
							<?php
						}
						?>
						<br/>
						<a href="<?php echo get_permalink($post_id); ?>" class="btn btn-primary">Click to View Detail</a>
					</div>
				</div>
				<?php
			}
			
			?>
			<div class="pagination">
				<?php 
					// Pagination
					$big = 999999999; 

					echo paginate_links( array(
						'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format'    => '?paged=%#%',
						'current'   => $paged,
						'total'     => ceil($total_topics/$posts_per_page),
						'prev_next' => True,
						'prev_text' => __('« Previous'),
						'next_text' => __('Next »'),
					) );
				?>
			</div>
	</div>
</div>

<?php get_footer(); ?>
