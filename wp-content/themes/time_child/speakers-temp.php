<?php

/**

 * @template name: Speakers Temp

 * @package        WordPress

 * @subpackage     Time

 * @since          1.0

 */



add_filter('time_pagination_display', create_function(

	'$show, $type',

	'return $type == "blog" ? false : $show;'

), 10, 2);



?>



<?php get_header(); ?>



<?php

	if (have_posts()) {

		the_post();

		$content = get_the_content();

	} else {

		$content = '';

	}

?>



<?php if ($content): ?>

	<?php

		if (!preg_match('#\[content.*\].*\[/content\]#isU', $content)) {

			$content = "[content]\n\n{$content}\n\n[/content]";

		}

	?>

	<div class="outer-container">

		<?php get_template_part('parts/nav-secondary', 'lower'); ?>

		<?php echo DroneFunc::wpProcessContent($content); ?>

					
					<img alt='' src='http://0.gravatar.com/avatar/8e25cd32d844c17e55463a4a1ace7b0e?s=150&amp;d=http%3A%2F%2Fsolutionsred.com%2Fswitch%2Fwp-content%2Fthemes%2Ftime_child%2Fimg%2Fswitch_user_icon.jpg%3Fs%3D150&amp;r=G' class='alignleft size-thumbnail wp-image-1064 avatar-150 photo' height='150' width='150' />
					<a href="http://solutionsred.com/switch/author/carleen-mackay/" title="View all posts by Carleen MacKay"><h2 class="link_title1">Carleen MacKay</h2></a><a href="carleenmackay@sbcglobal.net">carleenmackay@sbcglobal.net</a>
					<ul style="list-style: none;">
						<li style="display: inline-block;padding-left: 5px;padding-right: 5px;"><a class="bio-icon bio-icon-website" target="_blank" href="http://www.agelessinamerica.com">Website</a></li>
						<li style="  display: inline-block;padding-left: 5px;padding-right: 5px;"><a class="bio-icon bio-icon-linkedin" target="_blank" href="http://www.linkedin.com/pub/carleen-mackay/0/19a/222">Linked In</a></li>
					</ul>
					<p class="author-bio">1. Mid and Late Career Workforce Expert
					2. Emergent Workforce Expert; particular emphasis on alternative career strategies
					3. Author/Co-author 4 books (Return of the Boomers, Boom or Bust, Live Smart after 50, Plan “B” for Boomers and multiple products.
					4. National Keynoter
					5. Product &amp; Service Developer for Organizational Clients
					6. Coach Career and Talent Management Professionals in all Sectors of the Economy

					Carleen is particularly interested in helping to develop products and services for team members who are still active in building their businesses.

					Major clients/advisory boards include Career Partners International, the talent management company with the largest global footprint (over 200 offices in 26 countries); the San Diego Workforce Partnership, a non-profit that runs multiple One-Stops in San Diego County, The Intergenerational Center at Chaminade University in Honolulu, Hawaii; The Life Planning Network, a national network for professionals in the “over” 50 life space; The San Diego Mature Workforce Coalition. &nbsp;<a class="readmore" href="http://solutionsred.com/switch/author/carleen-mackay/" title="View all posts by Carleen MacKay">View Author's Articles &raquo;</a></p><p class="author-archive"></p><hr/>

	</div>

<?php endif; ?>




<?php get_footer(); ?>