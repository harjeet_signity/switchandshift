<?php

/**

 * @package    WordPress

 * @subpackage Time

 * @since      1.0

 */

?>



<?php get_header(); ?>


<div class="outer-container">
	<!--------------------------------------  Sub Menu Area--------------------------------------------------->
	<nav class="secondary">
		<div class="container">
			<?php if(is_single()) { wp_nav_menu( array('menu' => 'Secondary Menu' ));} ?>
    		<?php get_template_part('parts/nav-secondary', 'upper'); ?>
		</div>
	</nav>
	
	<!--------------------- Getting Custom field values ------------------------------------>
	<?php 
	
		// Getting Video Url
		$video_url = get_field( "video_url" );	
		$share_link		=	end(explode('/', $video_url));
	?>
	<div class="container">
    	<div class="video_detail video_detail_single">
			<div class="row text-center">
				<h1><?php echo the_title(); ?></h1>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $share_link; ?>" frameborder="0" allowfullscreen></iframe>
			</div>
			
		<!---------------------------------------- Video Decription -------------------------------------------->
			<div class="col-md-12">
				<div class="video_desc"><?php echo get_the_content(); ?></div>
		</div>
        </div>
	</div>
	
</div>
<?php get_footer(); ?>
