<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
?>


<?php get_header(); ?>

<div class="outer-container">
	
	<?php
		// Getting Current Term Information
		$queried_object = get_queried_object();
		
		// Getting term_id , taxonomy name to query posts.
		$term_id		=	$queried_object->term_id;
		$term_name		=	$queried_object->name;
		$term_taxonomy	=	$queried_object->taxonomy;
		
		// Fetching all videos related to Speaker
		$video_args = array(
		'post_type' => 'videos',
		'order'    => 'DESC',
		'tax_query' => array(
			array(
			'taxonomy' => $term_taxonomy,
			'field' => 'id',
			'terms' => $term_id
			 )
		  )
		);
		$speaker_related_videos = query_posts( $video_args );
		
		?>
		<div class="row speaker_videos_category text-center">
			<h2 class="speaker_name"><?php echo $term_name; ?></h2>
		<?php
		foreach($speaker_related_videos as $single_video) {
				$curr_vid_id 	= $single_video->ID;
				$video_url		= get_field( "video_url", $curr_vid_id );
				$share_link		=	end(explode('/', $video_url));
		?>
			
			<div class="col-md-4 single_video_category">
				<div class="single_video_area">					
					<iframe width="95%" src="https://www.youtube.com/embed/<?php echo $share_link; ?>" frameborder="0" allowfullscreen></iframe>
					<p class="post_excerpt"><?php echo $single_video->post_excerpt; ?></p>
					<a href="<?php echo $single_video->guid; ?>" class="video_full_link">Read More</a>
				</div>
			</div>
		<?php
			}
		?>
		
		</div>
</div>
<?php get_footer(); ?>
