<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
?>

		</div>		
		<div id="bottom">

			<?php get_sidebar('footer'); ?>

			<?php if (Time::to('footer/end_note/visible')): ?>
				<footer class="footer">

					<div class="container">

						<div class="section">
							<p class="small alpha"><?php echo nl2br(Time::to('footer/end_note/left')); ?></p>
							<p class="small beta"><?php echo nl2br(Time::to('footer/end_note/right')); ?></p>
						</div>

					</div>

				</footer>
			<?php endif; ?>

		</div>
		
		<?php wp_footer(); ?>
		<style>	
			/* Checkout page Address Style */
			#billing_email_field, #billing_country_field, #billing_address_2_field, #billing_state_field {
				float: left; padding-left: 10px; padding-right: 0px; width: 50%;
			}				
			#billing_company_field, #billing_phone_field, #billing_address_1_field, #billing_city_field, #billing_postcode_field, #account_password_field {
				float: left; padding-right: 10px; width: 50%;
			}
			 
			/* Hide author box for product page */
			.woocommerce #description #author-bio-box { display: none; }

		</style>
		<script>
			
			jQuery(document).ready(function(){
				/// Making woocommerce login form fields to full width by removing class
				jQuery( ".loginrow .login p" ).first().removeClass( "form-row-first" );
				jQuery( ".loginrow .login p" ).first().next().removeClass( "form-row-last" );

				//// Adding label for quantity in single product view
				jQuery('.single-product .quantity').prepend("<label>Quantity</label>");

				///// Setting maxlength for phone in checkout page 
				jQuery(".woocommerce-checkout #billing_phone").attr("maxlength","10");

				///// Hiding default  cart widget content and titel
				jQuery('.widget_shopping_cart_content').hide();
				jQuery('#woocommerce_widget_cart-2 .widget-title').remove();

				//// Hiding/Showing cart content on mouseover/mouseout
				jQuery("#woocommerce_widget_cart-2, .cart-new").mouseout(function(){
					jQuery('.widget_shopping_cart_content').stop().hide(500);
				}); 
				jQuery("#woocommerce_widget_cart-2, .cart-new").mouseover(function(){
					jQuery('.widget_shopping_cart_content').stop().show(500);
				}); 

				//// Preventing click on cart item widget display
				jQuery('.cart-new').click(function(e) {
					e.preventDefault();
				});

				///// Optin for email validations
				jQuery("#optinforms-form2").append("<span class='error_msg'></span>");
				jQuery("#optinforms-form2-button").click(function(){
					var email	= jQuery('#optinforms-form2-email-field').val();
	
					if(jQuery.trim(email) == ''){ //// if email is empty	
						jQuery('#optinforms-form2-email-field').addClass('error');
						jQuery(".error_msg").html("Email id is required");
						return false;

					} else {		
						var email_pattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
						if(email_pattern.test(email) == false) { //// if email pattern not found
							jQuery('#optinforms-form2-email-field').addClass('error');
							jQuery(".error_msg").html("Invalid Email id");
							return false;
						} else {
							jQuery('#optinforms-form2-email-field').removeClass('error');
							jQuery(".error_msg").html("");
							return true;
						}
					}
				});
				var message_success	 = jQuery('.yikes-easy-mc-success-message').text();
				var message_error	 = jQuery('.yikes-easy-mc-error-message').text();
				if(message_success.trim() != '' || message_error.trim() != '' ){
					jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, 1000);
				}
				// Displaying woocommerce login form
				jQuery('.woocommerce-checkout .login').show();
				/// Scrolling continue as guest button to user details
				jQuery(".continue_button").click(function() {
				  // jQuery(".bbp-topic-form").show();
				  jQuery("html, body").animate({ scrollTop: jQuery("#customer_details").offset().top },300);
				  return false;
				});
				
				/// Billing address field styles
				jQuery('#billing_company_field, #billing_email_field, #billing_phone_field, #billing_country_field, #billing_address_1_field, #billing_address_2_field, #billing_city_field, #billing_state_field, #billing_postcode_field').removeClass('form-row-first form-row-last form-row-wide');
				setTimeout(function(){						  
					jQuery('#billing_postcode_field').removeClass('form-row-last');
				}, 2000);
				
				jQuery('#billing_phone_field').next().remove();
									
			});
				
		</script>
	</body>
	
</html>
