<?php 
// Fetch all testimonials
$heading = true;
if(isset($testimonial_term_id) AND !empty($testimonial_term_id)){
	$testimonial_args = array(
		'post_type'      => 'testimonial',
		'order'          => 'DESC',
		'posts_per_page' => -1, // When set to -1 displays all post
		'tax_query'      => array(
				array(
					'taxonomy' => 'testimonial_categories',
					'field'    => 'id',
					'terms'    =>  $testimonial_term_id
				)
		)
	);
	
	$heading = false;
} else {
	$testimonial_args = array(
		'post_type'      => 'testimonial',
		'order'          => 'DESC',
		'posts_per_page' => -1 // When set to -1 displays all post
	);
}

$testimonials 	     = query_posts( $testimonial_args );
$total_testimonials  = count($testimonials); 
?>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<?php 
if(!empty($testimonials)) { 
	?>
	<div class="container">
		<?php 
			if ( $heading ) {
				?>
				<div class="row">
					<div class='col-md-offset-2 col-md-8 text-center'>
						<h1>Testimonials</h1>
					</div>
				</div>
				<?php
			}
		?>
		<div class='row testimnw'>
			<div class='col-md-offset-1 col-md-10'>
				<div class="carousel slide" data-ride="carousel" id="quote-carousel">
					<!-- Bottom Carousel Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
						<?php 
							for($i=1; $i < $total_testimonials; $i++) { ?>
								<li data-target="#quote-carousel" data-slide-to="<?php echo $i ; ?>"></li>
								<?php 
							} 
						?>
					</ol>
					<!-- Carousel Slides / Quotes -->
					<div class="carousel-inner">
						<?php 
						foreach($testimonials as $key => $single_testimonial) { 
							$feat_image = wp_get_attachment_url( get_post_thumbnail_id($single_testimonial->ID) );
							$feat_image = !empty($feat_image) ? $feat_image : home_url().'/wp-content/uploads/2015/12/demo_user.jpeg';
							?>
							<div class="item <?php if($key == 0) { echo 'active'; } ?>">
								<blockquote>
									<div class="row">
										<div class="col-sm-12 text-center">
												<img class="img-circle" src="<?php echo $feat_image; ?>">
											<div class="testi_title">
												<h3>
													<?php echo $single_testimonial->post_title; ?>
												</h3>
											</div>
											<div class="testi_excerpt">
												<?php 
													$testimonial_excerpt = strlen($single_testimonial->post_excerpt) > 150 ? substr($single_testimonial->post_excerpt,0,150)."..." : $single_testimonial->post_excerpt;
												?>	
												<p><?php echo $testimonial_excerpt; ?></p>													
											</div>
										</div>
									</div>
								</blockquote>
							</div>	
							<?php 
						}
						?>
					</div>
					<!-- Carousel Buttons Next/Prev -->
					<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
					<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
				</div>
			</div>  
		</div>
	</div>       
	<?php 
} 
?>   
