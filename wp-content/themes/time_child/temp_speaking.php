<?php
/**
 * Template Name: Speaking Template
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
get_header(); ?>
<div class="outer-container">
	<?php 
	get_template_part('parts/nav-secondary', 'lower'); 
	if (have_posts()): the_post(); ?>
		<?php
		// Getting Post ID
		$post_id	=	 get_the_ID(); 
			 
		// Getting Feature image url
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
		
		// Getting Banner Text
		$banner_text = get_field( "banner_text", $post_id );
		?>
		<section class="section sliderspesking">
			<article id="post-<?php the_ID(); ?>" <?php post_class(array('post', 'hentry')); ?>>
				<?php 
				if (!empty($feat_image)) { ?>
					<div class="row banner_image top_image_speaking"  style="background-image: url('<?php echo $feat_image; ?>');">
						<div class="col-md-12 banner_text text-center">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/speakers_icon.png" />
							<?php echo $banner_text; ?>
						</div>
					</div>
					<?php 
				} 
				?>
				<div class="top_image_speaking_slider_bottom">
					<?php the_content(); ?>
				</div>
			</article>
		</section>
		<?php 
	endif; 
	?>
	<!----------------- Print testimonials and sponsors ----------------------->
	
	<div class="testimonials-conatiner">
		<?php 
			get_template_part( "template", "testimonials" );
		?>
	</div>
	<div class="testimonials-conatiner">
		<div class="container sponsors-container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8 text-center">
					<h3>Past Engagements and Clients</h3>
				</div>
			</div>
			<?php get_template_part( "template", "sponsors" ); ?>
		</div>
	</div>
	<div class="container">
		<div class="row speakers_header">
			<p>Our<strong> Speakers</strong></p>
		</div>
		<?php
		$total_speaker      = wp_count_posts( 'speaker' );
		$paged              = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$posts_per_page 	= get_option('posts_per_page');
		
		$posts_array = array(
			'posts_per_page' => $posts_per_page,
			'post_type'      => 'speaker',
			'order'          => 'DESC',
			'paged'          => $paged
		);
		$speakers = query_posts( $posts_array );
		foreach( $speakers as $speaker ) {
			// Getting Post ID
			$post_id	=	$speaker->ID; 
				 
			// Getting Speaker Profile Image
			$speaker_profile_image = get_field( "profile_image", $post_id );
			
			$summary 			   = get_field( "summary", $post_id );
			
			// Fetch speaker meta
			$speaker_meta_data      = get_post_meta( $post_id ,'speaker_meta_data');
			
			?>
			<div class="row single_speaker speakerrow">
				<div class="col-md-4 speaker_left text-center">
					<?php 
						$speaker_profile_image = !empty($speaker_profile_image) ? $speaker_profile_image : get_stylesheet_directory_uri().'/img/demo_user.jpeg';
					?>
					<div class="profile_image" style="background-image: url('<?php echo $speaker_profile_image; ?>');">
					</div>
					<?php 
						if ( $speaker_meta_data[0]['fb'] != '' || $speaker_meta_data[0]['twitter'] != '' || $speaker_meta_data[0]['linkedin'] != '' ) {
							?>
							<div class="follow_me">
								<h3>Follow Me</h3>
								<?php 
									if($speaker_meta_data[0]['fb'] != ''){ ?>
										<a href="<?php 
											if (strpos($speaker_meta_data[0]['fb'],'http://') === false AND strpos($speaker_meta_data[0]['fb'],'https://') === false) {
												echo 'https://'.$speaker_meta_data[0]['fb'];
											} else {
												echo $speaker_meta_data[0]['fb'];
											}
										?>" target="_blank" class="icon-facebook">
										</a>
										<?php 
									} 
									
									if(!empty($speaker_meta_data[0]['twitter'])) { ?>
										<a href="<?php 
												if (strpos($speaker_meta_data[0]['twitter'],'http://') === false AND strpos($speaker_meta_data[0]['twitter'],'https://') === false) {
													echo 'https://'.$speaker_meta_data[0]['twitter'];
												}else {
													echo $speaker_meta_data[0]['twitter'];
												} 
										?>" target="_blank" class="icon-twitter">
										</a>
										<?php 
									}
									
									if(!empty($speaker_meta_data[0]['linkedin'])) { ?>							
										<a href="<?php 
											if (strpos($speaker_meta_data[0]['linkedin'],'http://') === false AND strpos($speaker_meta_data[0]['linkedin'],'https://') === false) {
												echo 'https://'.$speaker_meta_data[0]['linkedin'];
											} else {
												echo $speaker_meta_data[0]['linkedin'];
											} 
										?>" target="_blank" class="linkded_in">
										</a>
										<?php 
									} 
								?>
							</div>
							<?php 
						}
					?>
				</div>
				<div class="col-md-8 speaker_right">
					<a href="<?php echo get_permalink($post_id); ?>">
						<h2><?php echo $speaker->post_title; ?></h2>
					</a>
					<div class="speaker_content">
						<?php echo $summary; ?>
					</div>
					<div class="row speaker_videos">
						
						<?php 
							$videos_term_id = get_field( "related_video", $post_id );
							if(!empty($videos_term_id)) {
								/// Getting Full term and term_link 
								$full_term		= get_term_by('id', $videos_term_id, 'video_categories') ;
								$term_link		= get_term_link( $full_term );
								// Fetching all videos related to Speaker
								$video_args = array(
								'post_type' => 'videos',
								'order'    => 'DESC',
								'posts_per_page' => 3,
								'tax_query' => array(
									array(
									'taxonomy' => 'video_categories',
									'field' => 'id',
									'terms' => $videos_term_id
									 )
								  )
								);
								$speaker_related_videos = query_posts( $video_args ); 
								?>
									<h3>Videos</h3>
								<?php
									foreach($speaker_related_videos as $single_video) {
										$curr_vid_id 	= $single_video->ID;
										$video_url		= get_field( "video_url", $curr_vid_id );
										$share_link		=	end(explode('/', $video_url));
										?>

											<div class="col-md-4 single_video">
												<div class="video_area">
													<?php  $image_url = "http://img.youtube.com/vi/".$share_link."/0.jpg";?>
													<a href="<?php echo get_permalink($curr_vid_id); ?>">
														<img src="<?php echo $image_url; ?>" />
													</a>
												</div>
											</div>
										
										<?php
									}
									
									wp_reset_query();
								}
							?>
							
					
							<br/>
						</div>
						<?php 
							$topic_args = array(
												'post_type'      => 'post',
												'order'          => 'DESC',
												'posts_per_page' => 3,
												'author'         => $speaker->post_author,
												'tax_query'      => array(
													array(
													'taxonomy'   => 'category',
													'field'      => 'slug',
													'terms'      => 'speaker'
													 )
												  )
												);
													
							$speaker_topics = query_posts( $topic_args );
							
							if (!empty($speaker_topics)) {
								?>
								<div class="row speaker_videos">
									<h3>Expertise</h3>
									<ul class="related-topics">
										<?php 
											foreach ($speaker_topics as $key => $value) {	
												$title = strlen($value->post_title) > 80 ? substr($value->post_title,0,80).' ...' : $value->post_title;
												?>
												<li class="col-md-4">
													<a href="<?php echo get_permalink($value->ID); ?>"><?php echo $title; ?></a>
												</li>
												<?php
											}
										?>
									</ul>
								</div>
								<?php 
							}
						?>
						<br/>
						<a href="<?php echo get_permalink($post_id); ?>" class="btn btn-primary">Click to View Detail</a>
				</div>
			</div>
			<?php
		} ?>
		<div class="pagination">
			<?php 
			// Pagination
			$big = 999999999; // need an unlikely integer	
			echo paginate_links( array(
				'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'    => '?paged=%#%',
				'current'   => $paged,
				'total'     => ceil($total_speaker->publish/$posts_per_page),
				'prev_next' => True,
				'prev_text' => __('« Previous'),
				'next_text' => __('Next »'),
			) );
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
