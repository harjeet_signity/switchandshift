<?php
/**

 * @package    WordPress

 * @subpackage Time

 * @since      1.0

 */

get_header();
$category = get_the_category();

if ($category[0]->slug == 'speaker') {
	$post_id	=	 get_the_ID(); 
	set_query_var( 'post_id', $post_id );
	get_template_part('topic','detail');
} else {
?>
<div class="outer-container">
	
	<nav class="secondary">
		<div class="container">
		<?php //if(is_single()) { wp_nav_menu( array('menu' => 'Secondary Menu' ));} ?>
    		<?php //get_template_part('parts/nav-secondary', 'upper'); ?>
		</div>
	</nav>

	<?php Time::openContent(); ?>



		<?php if (have_posts()): the_post(); ?>



			<section class="section">

				<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
<?php
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail(700,450);
}
?>
					<?php get_template_part('parts/featured', get_post_format()); ?>
                    

					<?php the_content(); ?>

					<?php get_template_part('parts/paginate-links', 'page'); ?>
<div class="clear"></div>

				</article>

			</section>



			<?php /*get_template_part('parts/author-bio');*/ ?>

			<?php get_template_part('parts/social-buttons'); ?>

			<?php get_template_part('parts/meta'); ?>

			<?php comments_template(); ?>



		<?php endif; ?>



	<?php Time::closeContent(); ?>


</div>



<?php } 
get_footer(); ?>
