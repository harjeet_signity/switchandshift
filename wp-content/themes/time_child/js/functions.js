// Can also be used with $(document).ready()
jQuery(document).ready(function() {
 
jQuery('.sponsors-container .flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    	itemWidth: 220,
	controlNav: true,
	directionNav: true,
   	//itemMargin: 2,
    	//minItems: 2,
	//move : 1
  });

jQuery('.related_videos .flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    	itemWidth: 350,
	controlNav: true,
	move : 1,
	//directionNav: true,
   	//itemMargin: 2,
    	////minItems: 2
  });

jQuery('.speaker-products .flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    	itemWidth: 150,
	controlNav: true,
	directionNav: true,
   	itemMargin: 2,
    	minItems: 2
  });

jQuery(".wpcf7-range").change(function() {
	var value = jQuery(this).val();
	jQuery(".price-range-value").remove();
	jQuery(this).after( '<span class="price-range-value">'+value+'</span>');
});
});
