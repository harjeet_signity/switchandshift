<?php
/**
 * Template Name: Videos Template
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
get_header(); ?>

<div class="outer-container">
	<?php get_template_part('parts/nav-secondary', 'lower'); ?>
	<?php
		$total_videos 		= wp_count_posts('videos');
		$posts_per_page 	= get_option('posts_per_page');
		$paged 				= ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$videos_array = array(
			'posts_per_page' => $posts_per_page,
			'post_type' => 'videos',
			'order'    => 'DESC',
			'paged'          => $paged
		);
		$videos = query_posts( $videos_array );
		
	?>
	<!----------------------------------- Displaying related  Videos ------------------------------------------>
	<div class="container">
		<div class="video_detail">

			<center><h1>Videos</h1></center>
			<?php 
				foreach($videos as $key=>$video){
					$curr_post_id 	= $video->ID;
					$video_url		= get_field( "video_url", $curr_post_id );
					$share_link		= end(explode('/', $video_url));
					$post_category	= wp_get_object_terms( $curr_post_id, 'video_categories', array( 'fields' => 'names' ) );
					
					if($key==0) {
						echo "<div class='row'>";
					} elseif (($key%3 == 0) AND ($key!=$posts_per_page)) {
						echo "</div><div class='row'>";
					} 
					?>
					<div class="single_video col-md-4 text-center">
						<div class="video_area">							
							<iframe width="100%" src="https://www.youtube.com/embed/<?php echo $share_link; ?>" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="info_area">
							<div class="video_title">
								<h3><?php 
									if (strlen($video->post_title) > 52) {
										echo substr($video->post_title,0,52)."...";									
									} else {
										echo $video->post_title;
										
									}
								    ?>
								</h3>
							</div>
							<div class="video_excerpt">
								<?php 
									if (strlen($video->post_excerpt) > 68) {
										echo substr($video->post_excerpt,0,68)."...";									
									} else {
										echo $video->post_excerpt;
										
									}
								?>
							</div>
							<div class="video_speaker">
								<h4>Posted By : <?php echo $post_category['0']; ?></h4>
							</div>
							<div class="video_link">
								<a href="<?php echo get_permalink($curr_post_id); ?>">Read More</a>							
							</div>
							
						</div>
					</div>
				<?php
					if ($key == $posts_per_page-1) {
						echo "</div>";
					}
				}		
			?>
			<div class="row">
				<div class="pagination">
					<?php 
						// Pagination
						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format'    => '?paged=%#%',
							'current'   => $paged,
							'total'     => ceil($total_videos->publish/$posts_per_page),
							'prev_next' => True,
							'prev_text' => __('« Previous'),
							'next_text' => __('Next »'),
						) );
					?>
					e
				</div>
			</div>
		</div>
		
	</div>
	<?php get_template_part('parts/author-bio'); ?>
	<?php get_template_part('parts/social-buttons'); ?>
	<?php get_template_part('parts/meta'); ?>
	<?php comments_template(); ?>
</div>

<?php get_footer(); ?>

