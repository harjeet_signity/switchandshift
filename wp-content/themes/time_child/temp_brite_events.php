<?php
/**
 * Template Name: Brite Events
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
get_header(); ?>
<div class="outer-container brite-events-page">
	<?php 
	get_template_part('parts/nav-secondary', 'lower'); 
	if (have_posts()): the_post(); ?>
		<?php
		// Getting Post ID
		$post_id	=	 get_the_ID(); 
			 
		// Getting Feature image url
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
		
		// Getting Banner Text
		$page_meta_data      = get_post_meta( $post_id ,'page_meta_data');
		
		if (!empty($feat_image)) { ?>
			<div class="row banner_image top_image_speaking"  style="background-image: url('<?php echo $feat_image; ?>');">
				<div class="col-md-12 banner_textsingle text-center">
					<h2><?php echo the_title(); ?></h2>
					<span style="font-family: Avenir Book,serif;"><?php echo $page_meta_data[0]['banner_text']; ?></span>
				</div>
			</div>
			<?php 
		} 
		?>
		
		<div class="container">
			<div class="row speakers_header">
				<p>Our<strong> Events</strong></p>
			</div>
		<?php
			//$eventbrideurl    =  'https://www.eventbriteapi.com/v3/events/search/?organizer.id='.$organizer_id.'&token='.PERSONAL_OAUTH_TOKEN;
			
			$eventbrideurl = "https://www.eventbriteapi.com/v3/users/me/owned_events/?token=".PERSONAL_OAUTH_TOKEN;
			$ch = curl_init();
			$curlConfig = array(
				CURLOPT_URL            => $eventbrideurl,
				CURLOPT_POST           => false,
				CURLOPT_RETURNTRANSFER => true					   
			);
			curl_setopt_array($ch, $curlConfig);
			$result   = curl_exec($ch);
			$events   = json_decode($result,true);
			$total_events      = $events['pagination']['object_count'];
			$paged              = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			$posts_per_page 	= get_option('posts_per_page');
			$start_from = ($paged-1) * $posts_per_page;
			$end		= $start_from + $posts_per_page;
			$end = $end > $total_events ? $total_events : $end ;
			if ( isset( $events['events'] ) ) {
				
				//foreach ($events['events'] as $event) {
				for( $i = $start_from; $i < $end; $i++) {
					$time 		= strtotime($events['events'][$i]['start']['local']);
					$date_time 	= date("F j, Y", $time);
					$date 		= date('d',$time);
					$month 		= date('M',$time);
					$year 		= date('Y',$time);
					$venue_name = 'online';
					if( isset($events['events'][$i]['venue']) && isset( $events['events'][$i]['venue']['name'] )){
						$venue_name = $events['events'][$i]['venue']['name'];
					}
					
					$logo = is_array($events['events'][$i]['logo']) ? $events['events'][$i]['logo']['url'] : get_stylesheet_directory_uri().'/img/image_not_available.jpg'
					?>
					<div class="row single_speaker speakerrow">
						<div class="col-md-4 speaker_left text-center">
							<div class="profile_image" style="background-image: url('<?php echo $logo; ?>');">
								
							</div><br/>
							<div class="event-date date_section"><?php echo $date_time; ?></div>
						</div>
						<div class="col-md-8 speaker_right">
							<a href="<?php echo $events['events'][$i]['url']; ?>" target="_blank">
								<h2><?php echo $events['events'][$i]['name']['text']; ?></h2>
							</a>
							<span class="eb_event_list_location"><?php echo $venue_name; ?></span>
							<div class="speaker_content">
								<?php echo $events['events'][$i]['description']['text']; ?>
							</div>
							<br/>
							<a href="<?php echo $events['events'][$i]['url']; ?>" target="_blank" class="btn btn-primary">Click to View Detail</a>
						</div>						
					</div>
					<?php					
				}
				?>
				<div class="pagination">
					<?php 
					// Pagination
					$big = 999999999; // need an unlikely integer	
					echo paginate_links( array(
						'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format'    => '?paged=%#%',
						'current'   => $paged,
						'total'     => ceil($total_events/$posts_per_page),
						'prev_next' => True,
						'prev_text' => __('« Previous'),
						'next_text' => __('Next »'),
					) );
					?>
				</div>
				<?php
			}
		?>
		</div>
		<?php get_template_part('parts/paginate-links', 'page'); ?>					
		<?php get_template_part('parts/author-bio'); ?>
		<?php get_template_part('parts/social-buttons'); ?>
		<?php get_template_part('parts/meta'); ?>
		<?php comments_template(); ?>			
		<?php 
	endif; 
	?>
	
</div>
<?php get_footer(); ?>
