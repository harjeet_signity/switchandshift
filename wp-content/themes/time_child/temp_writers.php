<?php
/**
 * Template Name: Writers
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
get_header(); ?>

<div class="outer-container">

	<?php get_template_part('parts/nav-secondary', 'lower'); ?>

	<?php //Time::openContent(); ?>

		<?php if (have_posts()): the_post(); 
			// Getting Post ID
			$post_id	=	 get_the_ID(); 
				 
			// Getting Feature image url
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
			
			// Getting Banner Text
			$page_meta_data      = get_post_meta( $post_id ,'page_meta_data');
		?>
			<section class="">
				<article id="post-<?php the_ID(); ?>" <?php post_class(array('post', 'hentry')); ?>>
					<?php //get_template_part('parts/post-thumbnail'); ?>
					<?php //if (!Time::io('layout/page/hide_title', 'page/hide_title')) get_template_part('parts/title'); ?>
					<div class="container">
						<div class="main">
							<div class="row speakers_header">
								<p><strong> Writers</strong></p>
							</div>
							<?php the_content(); ?>
						</div>			
					</div>
					<?php get_template_part('parts/paginate-links', 'page'); ?>					
				</article>				
			</section>

			<?php get_template_part('parts/author-bio'); ?>
			<?php get_template_part('parts/social-buttons'); ?>
			<?php get_template_part('parts/meta'); ?>
			<?php comments_template(); ?>

		<?php endif; ?>

	<?php //Time::closeContent(); ?>

</div>
<?php get_footer(); ?>
