<?php
/**
 * Template Name: A World Gone Social
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */
get_header(); ?>
<div class="outer-container a_world_gone_social_template">
	<?php get_template_part('parts/nav-secondary', 'lower');  ?>
	
	<?php Time::openContent(); ?>

		<?php if (have_posts()): the_post(); 
		
			// Getting Post ID
			$post_id	=	 get_the_ID();
			// Getting ralated Testimonial category id(To query posts based on this id)		
			$testimonial_term_id = get_field( "select_testimonial" ); 
			?>
			<section class="section">
				<article id="post-<?php the_ID(); ?>" <?php post_class(array('post', 'hentry')); ?>>
					<?php get_template_part('parts/post-thumbnail'); ?>
					<?php if (!Time::io('layout/page/hide_title', 'page/hide_title')) get_template_part('parts/title'); ?>
					<?php the_content(); ?>
					<?php get_template_part('parts/paginate-links', 'page'); ?>
				</article>
			</section>

			<?php get_template_part('parts/author-bio'); ?>
			<?php get_template_part('parts/social-buttons'); ?>
			<?php get_template_part('parts/meta'); ?>
			<?php comments_template(); ?>

		<?php endif; ?>

	<?php Time::closeContent(); ?>
	<!----------------- Print testimonials and sponsors ----------------------->
	
	<div class="testimonials-conatiner">
		<?php 	
			set_query_var( 'testimonial_term_id', $testimonial_term_id);
			get_template_part( "template", "testimonials" );		 
		?>
	</div>										
</div>
<script>			
	jQuery(document).ready(function(){
		/// Repositioning Learn More link for A World Gone Social
		var button_html = jQuery('.full_web_link').html();
		jQuery('.full_web_link').remove();
		if(button_html.trim() != ''){
			jQuery('.testimonials-conatiner').append('<div style="text-align: center; padding: 10px;">'+button_html+'</div>');
		}	
	});
</script>
<?php get_footer(); ?>
