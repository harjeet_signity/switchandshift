<?php
/* 
Template Name: Archives
*/
get_header(); ?>

<div class="outer-container archive_container">


	<?php Time::openContent(); ?>


		<?php if (have_posts()): the_post(); ?>


			<section class="section">

				<ul class="archive_list">
				<?php
				$temp = $wp_query;
				$wp_query= null;
				$wp_query = new WP_Query();
				$wp_query->query('showposts=10'.'&paged='.$paged);
				?>
				<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<li>
				    <h2 class="archive_h2"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				    <p class="archive_credit">Written by: <?php the_author(); ?> | <?php the_time('F j, Y'); ?></p>
					<a href="<?php the_permalink() ?>" rel="bookmark">
				    <?php 
					$thumbs = the_post_thumbnail(700, 300);
					str_replace('class="attachment-700 wp-post-image"', "class='alignleft size-thumbnail wp-image-1064", $thumbs);
					echo $thumbs;   ?>
					</a>
				    <p><?php the_excerpt('READ MORE &raquo;'); ?></p>
					<div class="clear"> </div>
				</li>
				    <hr/>
				<?php endwhile; ?>
				</ul>

			</section>


		<?php endif; ?>

	<div class="navigation">
	 	<?php wp_pagenavi(); ?>
	</div>
	

	<?php Time::closeContent(); ?>


</div>


<?php get_footer(); ?>