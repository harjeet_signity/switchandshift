<?php
/* 
Template Name: The Human Side TV
*/
get_header(); ?>

<div class="outer-container archive_container">


	<?php Time::openContent(); ?>


		<?php if (have_posts()): the_post(); ?>


			<section class="section">
				
            <?php
 			$id = 4315;
 			$p = get_page($id);
 			echo apply_filters('the_content', $p->post_content);
 			?>

				<ul class="archive_list">
				<?php
				$temp = $wp_query;
				$wp_query= null;
				$wp_query = new WP_Query();
				$wp_query->query('showposts=10'.'&cat=1132'.'&paged='.$paged);
				?>
				<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<li>
					<a href="<?php the_permalink() ?>" rel="bookmark">
                     <?php if ( has_post_thumbnail() ) {
					the_post_thumbnail(700, 300);
					} else { ?>
  					<img src="<?php bloginfo('template_directory'); ?>/img/wtm_default.jpg" alt="<?php the_title(); ?>" />
					<?php } ?>
                    <?php 
     wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()))
 ?>
				    <?php 
					//$thumbs = the_post_thumbnail(700, 300);
					//str_replace('class="attachment-700 wp-post-image"', "class='alignleft size-thumbnail wp-image-1064", $thumbs);
					//echo $thumbs;   ?>
					</a>
				    <h2 class="archive_h2"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				    <p class="archive_credit">Written by: <?php the_author(); ?> | <?php the_time('F j, Y'); ?></p>
				    <p>
					<?php
					  $excerpt = get_the_excerpt();
					  echo string_limit_words($excerpt,20);
					  echo new_excerpt_more();
					?></p>
					<hr/>
					<div class="clear"> </div>
				</li>
				<?php endwhile; ?>
				</ul>

			</section>


		<?php endif; ?>

		<div class="navigation">
		 	<?php wp_pagenavi(); ?>
		</div>


	<?php Time::closeContent(); ?>


</div>


<?php get_footer(); ?>