<?php
/* 
Template Name: Work That Matters
*/
get_header(); ?>

<div class="outer-container archive_container work_matters_temp">


	<?php Time::openContent(); ?>


		<?php if (have_posts()): the_post(); ?>


			<section class="section">
            
            <?php
 			$id = 18594;
 			$p = get_page($id);
 			
 			echo apply_filters('the_content', $p->post_content);
 			?>
				<?php
				$temp = $wp_query;
				$wp_query= null;
				$wp_query = new WP_Query();
				$wp_query->query('showposts=10'.'&cat=1015'.'&paged='.$paged);
				?>
				<?php while ($wp_query->have_posts()) : $wp_query->the_post(); 
					// Getting Post ID
					$post_id	=	 get_the_ID(); 						 
					// Getting Feature image url
					$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
					?>
					<div class="row single_speaker speakerrow">
						<div class="col-md-5 speaker_left text-center">
							<div class="profile_image" style="background-image: url('<?php echo $feat_image; ?>');">
								
							</div><br/>
							<p class="written_by">Written by: <?php the_author(); ?></p>
							<p class="written_date date_section"><?php the_time('F j, Y'); ?></p>
						</div>
						<div class="col-md-7 speaker_right">
							<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
							<div class="speaker_content">
								<?php
								  $excerpt = get_the_excerpt();
								  //echo string_limit_words($excerpt,20);
								  echo new_excerpt_more(string_limit_words($excerpt,20));
								?>
							</div>
							<br/>
							<a href="<?php the_permalink(); ?>" rel="bookmark" class="btn btn-primary">Read More</a>
						</div>						
					</div>
				<?php endwhile; ?>
				
			</section>


		<?php endif; ?>

		<div class="navigation">
		 	<?php wp_pagenavi(); ?>
		</div>


	<?php Time::closeContent(); ?>


</div>


<?php get_footer(); ?>
