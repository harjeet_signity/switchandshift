<?php

/* 

Template Name: Contact Speaker

*/

//response generation function



  $response = "";



  //function to generate response

  function my_contact_form_generate_response($type, $message){



    global $response;



    if($type == "success") $response = "<div class='success'>{$message}</div>";

    else $response = "<div class='error'>{$message}</div>";



  }





//response messages

$not_human       = "Human verification incorrect.";

$missing_content = "Please supply all information.";

$email_invalid   = "Email Address Invalid.";

$message_unsent  = "Message was not sent. Try Again.";

$message_sent    = "Thanks! Your message has been sent.";

 

//user posted variables

$message_first_name = $_POST['message_first_name'];

$message_last_name = $_POST['message_last_name'];

$message_position = $_POST['message_position'];

$message_organization = $_POST['message_organization'];

$message_telephone = $_POST['message_telephone'];

$message_email = $_POST['message_email'];

$message_address = $_POST['message_address'];

$message_city = $_POST['message_city'];

$message_state = $_POST['message_state'];

$message_zip = $_POST['message_zip'];

$message_country = $_POST['message_country'];

$message_budget = $_POST['message_budget'];

$message_event_dates = $_POST['message_event_dates'];

$message_event_location = $_POST['message_event_location'];

$message_speaker = $_POST['message_speaker'];

$message_previous_speaker = $_POST['message_previous_speaker'];

$message_additional_comments = $_POST['message_additional_comments'];

$message_keynote = $_POST['message_keynote'];

$message_training = $_POST['message_training'];

$message_retreat = $_POST['message_retreat'];

$message_org_dev = $_POST['message_org_dev'];

$message_comment_other = $_POST['message_comment_other'];

$message_audience_employee = $_POST['message_audience_employee'];

$message_audience_executive = $_POST['message_audience_executive'];

$message_audience_customer = $_POST['message_audience_customer'];

$message_audience_association = $_POST['message_audience_association'];

$message_audience_other = $_POST['message_audience_other'];



$human = $_POST['message_human'];



$message = "First Name: $message_first_name

Last Name: $message_last_name

Position: $message_position

Organization: $message_organization

Telephone: $message_telephone

Email: $message_email

Address: $message_address

City: $message_city

State: $message_state

Zip: $message_zip

Country: $message_country

Budget: $message_budget

Event Date(s): $message_event_dates

Event Location: $message_event_location

Speaker: $message_speaker

Previous Speakers: $message_previous_speaker

Additional Comments: $message_additional_comments

Event Format: 

	$message_keynote

	$message_training

	$message_retreat

	$message_org_dev

	$message_comment_other

Audience:

	$message_audience_employee

	$message_audience_executive

	$message_audience_customer

	$message_audience_association

	$message_audience_other

"; 

//php mailer variables
$to = 'speakers@switchandshift.com';


$subject = "Someone sent a message from ".get_bloginfo('name');

$headers = 'From: '. $email . "rn" .

  'Reply-To: ' . $email . "rn";

  

if(!$human == 0){

    if($human != 2) my_contact_form_generate_response("error", $not_human); //not human!

    else {
    //validate email

      if(!filter_var($message_email, FILTER_VALIDATE_EMAIL))

        my_contact_form_generate_response("error", $email_invalid);

      else //email is valid

      {

        //validate presence of name and message

        if(empty($message_first_name) || empty($message_last_name) || empty($message)){

          my_contact_form_generate_response("error", $missing_content);

        }

        else //ready to go!

        {

         // $sent = wp_mail($to, $subject, strip_tags($message), $headers);

          if($sent) my_contact_form_generate_response("success", $message_sent); //message sent!

          else my_contact_form_generate_response("error", $message_unsent); //message wasn't sent

        }

      }

    }

  }

  else if ($_POST['submitted']) my_contact_form_generate_response("error", $missing_content);

get_header(); ?>

<div class="outer-container archive_container">

	<?php Time::openContent(); ?>

		<?php if (have_posts()): the_post(); ?>

			<section class="section">

            <?php

 			$id = 20024;

 			$p = get_page($id);

 			echo apply_filters('the_content', $p->post_content);

 			?>

<script>

function formReset(){

	document.getElementById("speakerForm").reset();	

	"<? $message_first_name = ''; ?>"

}

</script>

<hr/>

<h1 style="margin-top: 0; text-align: center;">Request a Booking Today! </h1>

					<div id="respond">

  <?php echo $response; ?>

  <form id="speakerForm" action="<?php the_permalink(); ?>" method="post">

    <p><label for="first_name">First Name<span>*</span>:</label> <input type="text" name="message_first_name" value="<?php echo esc_attr($_POST['message_first_name']); ?>" required></p>

    <p><label for="last_name">Last Name<span>*</span>:</label> <input type="text" name="message_last_name" value="<?php echo esc_attr($_POST['message_last_name']); ?>" required></p>

    <p><label for="position">Position/Title: </label><input type="text" name="message_position" value="<?php echo esc_attr($_POST['message_position']); ?>" ></p>

    <p><label for="organization">Organization<span>*</span>:</label> <input type="text" name="message_organization" value="<?php echo esc_attr($_POST['message_organization']); ?>" required></p>

    <p><label for="telephone">Telephone:</label> <input type="text" name="message_telephone" value="<?php echo esc_attr($_POST['message_telephone']); ?>" ></p>

    <p><label for="message_email">Email<span>*</span>:</label> <input type="email" name="message_email" value="<?php echo esc_attr($_POST['message_email']); ?>" required></p>

    <p><label for="address">Address:</label> <input type="text" name="message_address" value="<?php echo esc_attr($_POST['message_address']); ?>" ></p>

    <p><label for="city">City:</label> <input type="text" name="message_city" value="<?php echo esc_attr($_POST['message_city']); ?>" ></p>

    <p><label for="state">State:</label> <input type="text" name="message_state" value="<?php echo esc_attr($_POST['message_state']); ?>" ></p>

    <p><label for="zip">Zip:</label> <input type="text" name="message_zip" value="<?php echo esc_attr($_POST['message_zip']); ?>" ></p>

    <p><label for="country">Country:</label> <input type="text" name="message_country" value="<?php echo esc_attr($_POST['message_country']); ?>" ></p>

	

    	<p><label for="budget">Budget Range:</label> 

    	<select name="message_budget" size="3">

			<option value="$2,000 to $5,000" <? if ($message_budget == '$2,000 to $5,000') echo 'selected'; ?>>$2,000 to $5,000</option>

            <option value="$5,000 to $10,000" <? if ($message_budget == '$5,000 to $10,000') echo 'selected'; ?>>$5,000 to $10,000</option>

            <option value="$10,000+" <? if ($message_budget == '$10,000+') echo 'selected'; ?>>$10,000+</option>

      	</select>

        </p>


    <p><label for="event_dates">Event Date(s):</label> <input type="text" name="message_event_dates" value="<?php echo esc_attr($_POST['message_event_dates']); ?>" ></p>

    <p><label for="event_location">Event Location:</label> <input type="text" name="message_event_location" value="<?php echo esc_attr($_POST['message_event_location']); ?>" ></p>

    <p><label for="speaker">Speaker(s) of Interest:</label> <input type="text" name="message_speaker" value="<?php echo esc_attr($_POST['message_speaker']); ?>" ></p>

    <p><label for="previous_speaker">Previous Speakers (if any):</label> <input type="text" name="message_previous_speaker" value="<?php echo esc_attr($_POST['message_previous_speaker']); ?>" ></p>

	

    <p><label for="additional_comments">Additional Comments:</label>

		<textarea rows="7" placeholder="(Please provide us with as much information as you can about your event: themes, desired outcomes, etc.)" name="message_additional_comments" value="<?php echo esc_attr($_POST['message_additional_comments']); ?>" ></textarea></p>

	<p><label for="event_format" class="label_break">Please describe the event's format:</label><br>

    <input type="checkbox" name="message_keynote" value="Keynote" <? if ($message_keynote == 'Keynote') echo 'checked'; ?>>Keynote<br>

	<input type="checkbox" name="message_training" value="Training &amp; Workshops" <? if ($message_training == 'Training & Workshops') echo 'checked'; ?>>Training &amp; Workshops<br>

    <input type="checkbox" name="message_retreat" value="Executive Retreat" <? if ($message_retreat == 'Executive Retreat') echo 'checked'; ?>>Executive Retreat<br>

    <input type="checkbox" name="message_org_dev" value="Organizational Development" <? if ($message_org_dev == 'Organizational Development') echo 'checked'; ?>>Organizational Development<br>

	<input type="checkbox" name="message_other" value="Association Other" <? if ($message_other == 'Association Members') echo 'checked'; ?>>Other<br>

    <input type="text" placeholder="If Other, Please Describe." class="speaker_other" name="message_comment_other" value="<?php echo esc_attr($_POST['message_comment_other']); ?>">

	</p>



     <p><label for="audience" class="label_break">The audience will be:</label><br>

    <input type="checkbox" name="message_audience_employee" value="Employees" <? if ($message_audience_employee == 'Employees') echo 'checked'; ?>>Employees<br>

	<input type="checkbox" name="message_audience_executive" value="Company Executives" <? if ($message_audience_executive == 'Company Executives') echo 'checked'; ?>>Company Executives<br>

    <input type="checkbox" name="message_audience_customer" value="Customers" <? if ($message_audience_customer == 'Customers') echo 'checked'; ?>>Customers<br>

    <input type="checkbox" name="message_audience_association" value="Association Members" <? if ($message_audience_association == 'Association Members') echo 'checked'; ?> >Association Members<br>

	<input type="checkbox" name="message_audience_other" value="Association Members" <? if ($message_audience_other == 'Association Members') echo 'checked'; ?>>Other<br>

    <input type="text" placeholder="If Other, Please Describe." class="speaker_other" name="message_audience_other2" value="<?php echo esc_attr($_POST['message_audience_other2']); ?>">

    <? echo $message_audience_association;?>

    </p>

    <p><label for="message_human">Human Verification*: </label> <input type="text" style="width: 60px;" name="message_human"> + 3 = 5</p>

    <input type="hidden" name="submitted" value="1">

    <p><input type="submit" value="Submit">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="formReset()" value="Reset Form">&nbsp;&nbsp;&nbsp;&nbsp;<button type="reset" value="Reset">Reset</button>

  </form>

</div>

			</section>

		<?php endif; ?>

		<div class="navigation">

		 	<?php wp_pagenavi(); ?>

		</div>

	<?php Time::closeContent(); ?>

</div>

<?php get_footer(); ?>