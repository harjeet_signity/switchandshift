<?php
$eventbrideurl    =  'https://www.eventbriteapi.com/v3/events/search/?organizer.id='.$organizer_id.'&token='.PERSONAL_OAUTH_TOKEN;
$ch = curl_init();
$curlConfig = array(
    CURLOPT_URL            => $eventbrideurl,
    CURLOPT_POST           => false,
    CURLOPT_RETURNTRANSFER => true
    
   
);
curl_setopt_array($ch, $curlConfig);
$result   = curl_exec($ch);
$events   = json_decode($result,true);

if ( isset( $events['events'] ) ) {
	?>
	<div class="events-content-container">
		<div class="row brite-events-row">
			<center><h1>Events</h1></center>
		</div>
		<div class="eb_event_list">
			<?php
			foreach ($events['events'] as $event) {
				$time = strtotime($event['start']['local']);
				$date = date('d',$time);
				$month = date('M',$time);
				$year = date('Y',$time);
				$venue_name = 'online';
				if( isset($event['venue']) && isset( $event['venue']['name'] )){
					$venue_name = $event['venue']['name'];
				}
				
				$logo = is_array($event['logo']) ? $event['logo']['url'] : get_stylesheet_directory_uri().'/img/image_not_available.jpg'
				?>
				<div class="eb_event_list_item" id="evnt_div_<?php echo $event['id']?>">
					<div class="col-md-10">
						<div class="event-left-container">
							<div class="event-image">
								<img src="<?php echo $logo; ?>">
							</div>
						</div>
						<div class="event-content-container event-right-container">
							<a class="eb_event_list_title" href="<?php echo $event['url']; ?>" target="_blank"><?php echo $event['name']['text']; ?></a>
							<span class="eb_event_list_location"><?php echo $venue_name; ?></span>
						</div>
						<div class="row">
							<p></p><p><?php echo $event['description']['text']; ?></p><p></p>
						</div>
						</div><div class="event-date-container col-md-2"> <span class="eb_event_list_date"><?php echo $date.'<br/>'.$month.'<br/>'.$year; ?></span></div></div>
				<?php
			} ?>
		</div>
	</div>
	<?php
}
