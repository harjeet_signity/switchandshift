<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */

// -----------------------------------------------------------------------------

require_once get_template_directory().'/drone/drone.php'; // 5.5.0.2
require_once get_template_directory().'/inc/class-tgm-plugin-activation.php'; // 2.5.2

// -----------------------------------------------------------------------------

class Time extends \Drone\Theme
{

	// -------------------------------------------------------------------------

	const DEFAULT_SIDEBAR_WIDTH = 240;
	const ICON_FONT_PATH        = 'data/img/icons/icons.ttf';

	const LAYERSLIDER_VERSION          = '4.6.5';
	const LAYERSLIDER_REQUIRED_VERSION = '4.6.5';
	const WILD_GOOGLEMAP_VERSION       = '1.9.3';

	const ITEM_PAGE_URL = 'http://themeforest.net/item/time-responsive-wordpress-theme/5736361/?ref=kubasto';

	// -------------------------------------------------------------------------

	public static $headline_used    = false;
	public static $gallery_instance = 0;
	public static $sidebars_array;
	public static $post_formats_icons = array(
		'aside'   => 'doc-text',
		'audio'   => 'mic',
		'chat'    => 'chat',
		'gallery' => 'picture',
		'image'   => 'camera',
		'link'    => 'link',
		'quote'   => 'quote',
		'status'  => 'comment',
		'video'   => 'video'
	);

	// -------------------------------------------------------------------------

	/**
	 * Add meta options
	 *
	 * @since 1.0
	 *
	 * @param  object $group
	 * @param  bool   $default_visible
	 * @param  array  $default_items
	 * @return object
	 */
	protected function addMetaOptions($group, $default_visible, $default_items)
	{
		$visible = $group->addOption('boolean', 'visible', $default_visible, '', '', array('caption' => __('Visible', 'time')));
		return $group->addOption('group', 'items', $default_items, '', '', array('options' => array(
			'date_time'  => __('Date &amp time', 'time'),
			'date'       => __('Date', 'time'),
			'mod_date'   => __('Modification date', 'time'),
			'time_diff'  => __('Relative time', 'time'),
			'comments'   => __('Comments number', 'time'),
			'author'     => __('Author', 'time'),
			'permalink'  => __('Permalink', 'time'),
			'edit_link'  => __('Edit link', 'time')
		), 'indent' => true, 'multiple' => true, 'sortable' => true, 'owner' => $visible));
	}

	// -------------------------------------------------------------------------

	/**
	 * Add post meta options
	 *
	 * @since 1.0
	 *
	 * @param  object $group
	 * @param  bool   $default_visible
	 * @param  array  $default_items
	 * @return object
	 */
	protected function addPostMetaOptions($group, $default_visible, $default_items)
	{
		$visible = $group->addOption('boolean', 'visible', $default_visible, '', '', array('caption' => __('Visible', 'time')));
		return $group->addOption('group', 'items', $default_items, '', '', array('options' => array(
			'date_time'  => __('Date &amp time', 'time'),
			'date'       => __('Date', 'time'),
			'mod_date'   => __('Modification date', 'time'),
			'time_diff'  => __('Relative time', 'time'),
			'comments'   => __('Comments number', 'time'),
			'categories' => __('Categories', 'time'),
			'tags'       => __('Tags', 'time'),
			'author'     => __('Author', 'time'),
			'permalink'  => __('Permalink', 'time'),
			'edit_link'  => __('Edit link', 'time')
		), 'indent' => true, 'multiple' => true, 'sortable' => true, 'owner' => $visible));
	}

	// -------------------------------------------------------------------------

	/**
	 * Add social buttons options
	 *
	 * @since 1.0
	 *
	 * @param  object $group
	 * @param  bool   $default_visible
	 * @param  array  $default_items
	 * @return object
	 */
	protected function addSocialButtonsOptions($group, $default_visible, $default_items)
	{
		$visible = $group->addOption('boolean', 'visible', $default_visible, '', '', array('caption' => __('Visible', 'time')));
		return $group->addOption('group', 'items', $default_items, '', '', array('options' => array(
			'facebook'   => __('Facebook', 'time'),
			'twitter'    => __('Twitter', 'time'),
			'googleplus' => __('Google+', 'time'),
			'linkedin'   => __('LinkedIn', 'time'),
			'pinterest'  => __('Pinterest', 'time')
		), 'indent' => true, 'multiple' => true, 'sortable' => true, 'owner' => $visible));
	}

	// -------------------------------------------------------------------------

	/**
	 * Add layout options
	 *
	 * @since 1.0
	 *
	 * @param object $group
	 * @param array  $sidebar_options
	 */
	protected function addLayoutOptions($group, &$sidebar_options, &$nav_menus)
	{

		$background = $group->addGroup('background', __('Background', 'time'));
		$enabled = $background->addOption('boolean', 'enabled', false, '', '', array('caption' => __('Custom', 'time')));
		$background->addOption('background', 'background', Time::to_('general/background/background')->default, '', '', array('indent' => true, 'owner' => $enabled));

		$banner = $group->addGroup('banner', __('Banner', 'time'));
		$enabled = $banner->addOption('boolean', 'enabled', false, '', '', array('caption' => __('Custom', 'time')));
		$banner->addOption('banner', 'banner', Time::to_('banner/content')->default, '', '', array('editor' => true, 'indent' => true, 'owner' => $enabled));

		$sidebar = $group->addGroup('sidebar', __('Sidebar', 'time'));
		$enabled = $sidebar->addOption('boolean', 'enabled', false, '', '', array('caption' => __('Custom', 'time')));
		$sidebar->addOption('sidebar', 'sidebar', Time::to_('sidebar/layout')->default, '', '', array('options' => $sidebar_options, 'indent' => true, 'owner' => $enabled));

		$nav = $group->addGroup('nav_secondary', __('Secondary menu', 'time'));
		$nav->addOption('select', 'upper', 'inherit', __('Upper', 'time'), '', array('options' => array(
			'inherit' => __('Inherit', 'time'),
			'true'    => __('Show', 'time'),
			''        => __('Hide', 'time')
		)+$nav_menus, 'groups' => array(
			__('Custom menu', 'time') => array_keys($nav_menus)
		)));
		$nav->addOption('select', 'lower', 'inherit', __('Lower', 'time'), '', array('options' => array(
			'inherit' => __('Inherit', 'time'),
			'true'    => __('Show', 'time'),
			''        => __('Hide', 'time')
		)+$nav_menus, 'groups' => array(
			__('Custom menu', 'time') => array_keys($nav_menus)
		)));

		$group->addOption('select', 'headline', 'inherit', __('Headline', 'time'), '', array('options' => array(
			'inherit' => __('Inherit', 'time'),
			'true'    => __('Show', 'time'),
			''        => __('Hide', 'time')
		)));

		$page = $group->addGroup('page', __('Page', 'time'));
		$page->addOption('select', 'author_bio', 'inherit', __('Author details', 'time'), '', array('options' => array(
			'inherit' => __('Inherit', 'time'),
			'true'    => __('Show', 'time'),
			''        => __('Hide', 'time')
		)));
		$page->addOption('select', 'meta', 'inherit', __('Meta', 'time'), '', array('options' => array(
			'inherit' => __('Inherit', 'time'),
			'true'    => __('Show', 'time'),
			''        => __('Hide', 'time')
		)));
		$page->addOption('select', 'social_buttons', 'inherit', __('Social buttons', 'time'), '', array('options' => array(
			'inherit' => __('Inherit', 'time'),
			'true'    => __('Show', 'time'),
			''        => __('Hide', 'time')
		)));

	}

	// -------------------------------------------------------------------------

	/**
	 * Get attachment link URL
	 *
	 * $link - post, file, none
	 *
	 * @since 1.0
	 *
	 * @param  object $attachment
	 * @param  string $link
	 * @return string
	 */
	protected function getAttachmentLinkURL($attachment, $link = 'post')
	{
		if ($attachment->post_content && preg_match('#((https?://|mailto:).+)(\b|["\'])#i', $attachment->post_content, $matches)) {
			return $matches[1];
		} else if ($link == 'post') {
			return get_attachment_link($attachment->ID);
		} else if ($link == 'file') {
			list($src) = wp_get_attachment_image_src($attachment->ID, '');
			return $src;
		} else {
			return '';
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Cart menu item
	 *
	 * @since 2.0
	 *
	 * @return string
	 */
	protected function getCartMenuitem()
	{
		global $woocommerce;
		$a = \Drone\HTML::a()
			->href($woocommerce->cart->get_cart_url())
			->title(__('Cart', 'time'));
		$last_item = '';
		$before = '';
		foreach (Time::to('header/cart/content') as $item) {
			$before = $last_item && $last_item != 'icon' ? '&nbsp;&nbsp;' : '';
			$last_item = $item;
			switch ($item) {
				case 'icon':
					$a->add($this->shortcodeIcon(array('name' => Time::to('woocommerce/cart/icon'), 'size' => '1.2em', 'class' => 'icon-woocommerce-cart', 'style' => 'margin: 0 0 0 -0.4em;')));
					break;
				case 'phrase':
					$a->add($before, __('Cart', 'time'));
					break;
				case 'count':
					if ($woocommerce->cart->cart_contents_count > 0) {
						$a->add($before)->addNew('mark')->add($woocommerce->cart->cart_contents_count);
					}
					break;
				case 'total':
					if ($woocommerce->cart->cart_contents_count > 0) {
						$a->add($before)->addNew('mark')->add($woocommerce->cart->get_cart_total());
					}
					break;
			}
		}
		return $a->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Language menu item
	 *
	 * @since 1.0
	 *
	 * @param  array $lang
	 * @return string
	 */
	protected function getLanguageMenuitem($lang)
	{
		if (file_exists($this->template_dir.'/data/img/icons/langs/'.$lang['language_code'].'.png')) {
			$icon = Time::shortcodeIcon(array('name' => 'langs/'.$lang['language_code']));
		} else {
			$icon = '';
		}
		return \Drone\HTML::a()
			->href($lang['url'])
			->title($lang['native_name'])
			->add($icon, $lang['native_name'])
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Theme load
	 *
	 * @since 1.0
	 * @see \Drone\Theme::onLoad()
	 */
	protected function onLoad()
	{

		// Sidebars
		Time::$sidebars_array = apply_filters('time_sidebars', array(
			'alpha'   => __('Alpha', 'time'),
			'beta'    => __('Beta', 'time'),
			'gamma'   => __('Gamma', 'time'),
			'delta'   => __('Delta', 'time'),
			'epsilon' => __('Epsilon', 'time'),
			'zeta'    => __('Zeta', 'time'),
			'eta'     => __('Eta', 'time'),
			'theta'   => __('Theta', 'time'),
			'iota'    => __('Iota', 'time'),
			'kappa'   => __('Kappa', 'time'),
			'lambda'  => __('Lambda', 'time'),
			'mu'      => __('Mu', 'time'),
			'nu'      => __('Nu', 'time'),
			'xi'      => __('Xi', 'time'),
			'omicron' => __('Omicron', 'time'),
			'pi'      => __('Pi', 'time'),
			'rho'     => __('Rho', 'time'),
			'sigma'   => __('Sigma', 'time'),
			'tau'     => __('Tau', 'time'),
			'upsilon' => __('Upsilon', 'time'),
			'phi'     => __('Phi', 'time'),
			'chi'     => __('Chi', 'time'),
			'psi'     => __('Psi', 'time'),
			'omega'   => __('Omega', 'time')
		));

		// Post formats icons
		Time::$post_formats_icons = apply_filters('time_post_formats_icons', Time::$post_formats_icons);

	}

	// -------------------------------------------------------------------------

	/**
	 * Theme options compatybility
	 *
	 * @since 1.2
	 *
	 * @param array  $data
	 * @param string $version
	 */
	public function onThemeOptionsCompatybility(array &$data, $version)
	{

		// 1.2
		if (version_compare($version, '1.2') < 0) {
			foreach (array('standard', 'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video') as $format) {
				if (isset($data['format_posts'][$format]['content']) && $data['format_posts'][$format]['content'] == 'excerpt') {
					$data['format_posts'][$format]['content'] = 'excerpt_content';
				}
			}
		}

		// 2.1
		if (version_compare($version, '2.1') < 0) {
			if (isset($data['banner']['content']['404'])) {
				$data['banner']['content']['404,404'] = $data['banner']['content']['404'];
			}
			if (isset($data['nav']['secondary']['upper']) && in_array('404', $data['nav']['secondary']['upper'])) {
				$data['nav']['secondary']['upper'][] = '404,404';
			}
			if (isset($data['nav']['secondary']['lower']) && in_array('404', $data['nav']['secondary']['lower'])) {
				$data['nav']['secondary']['lower'][] = '404,404';
			}
			if (isset($data['nav']['headline']['visible']) && in_array('404', $data['nav']['headline']['visible'])) {
				$data['nav']['headline']['visible'][] = '404,404';
			}
		}

		// 2.2
		if (version_compare($version, '2.2') < 0) {
			if (isset($data['header']['search']) && is_bool($data['header']['search'])) {
				$data['header']['search'] = $data['header']['search'] ? array('desktop', 'mobile') : array();
			}
		}

		// 2.4
		if (version_compare($version, '2.4') < 0) {
			if (isset($data['site']['hover_icons']) && is_array($data['site']['hover_icons'])) {
				foreach ($data['site']['hover_icons'] as $type => $hover_icon) {
					$data['site']['hover_icons'][$type] = preg_replace('/^icon-/', '', $hover_icon);
				}
			}
			if (isset($data['woocommerce']['cart']['icon'])) {
				$data['woocommerce']['cart']['icon'] = preg_replace('/^icon-/', '', $data['woocommerce']['cart']['icon']);
			}
		}

		// 2.5
		if (version_compare($version, '2.5') < 0) {
			if (isset($data['banner']['content']) && is_array($data['banner']['content'])) {
				foreach (array_keys($data['banner']['content']) as $key) {
					if (preg_match('/^category\(([0-9]+)\)$/', $key, $m)) {
						$data['banner']['content']["time_category_or_term({$m[1]})"] = $data['banner']['content'][$key];
					}
				}
			}
			if (isset($data['sidebar']['layout']) && is_array($data['sidebar']['layout'])) {
				foreach (array_keys($data['sidebar']['layout']) as $key) {
					if (preg_match('/^category\(([0-9]+)\)$/', $key, $m)) {
						$data['sidebar']['layout']["time_category_or_term({$m[1]})"] = $data['sidebar']['layout'][$key];
					}
				}
			}
			$convert_cat = create_function('$s', 'return preg_replace(\'/^category\(([0-9]+)\)$/\', \'time_category_or_term(\\1)\', $s);');
			if (isset($data['nav']['secondary']['upper']) && is_array($data['nav']['secondary']['upper'])) {
				$data['nav']['secondary']['upper'] = array_map($convert_cat, $data['nav']['secondary']['upper']);
			}
			if (isset($data['nav']['secondary']['lower']) && is_array($data['nav']['secondary']['lower'])) {
				$data['nav']['secondary']['lower'] = array_map($convert_cat, $data['nav']['secondary']['lower']);
			}
			if (isset($data['nav']['headline']['visible']) && is_array($data['nav']['headline']['visible'])) {
				$data['nav']['headline']['visible'] = array_map($convert_cat, $data['nav']['headline']['visible']);
			}
		}

		// 3.3
		if (version_compare($version, '3.3-beta-13') < 0) {

			$conditional_tags_array = array(
				'front_page',
				'home,archive',
				'search',
				'singular(post)',
				'page',
				'singular(attachment)',
				'singular(gallery)',
				'singular(portfolio)',
				'404,404'
			);
			if (Time::isPluginActive('bbpress')) {
				$conditional_tags_array[] = 'bbpress';
			}
			if (Time::isPluginActive('woocommerce')) {
				$conditional_tags_array = array_merge($conditional_tags_array, array(
					'product',
					'shop,product_taxonomy,product',
					'cart',
					'checkout',
					'order_received_page',
					'account_page',
					'shop,product_taxonomy,product,cart,checkout,order_received_page,account_page'
				));
			}
			foreach (\Drone\Func::wpTermsList('category', array('hide_empty' => false)) as $id => $name) {
				$conditional_tags_array[] = "time_category_or_term({$id})";
			}

			$convert_tag = function($tag) {
				if (preg_match('/^singular\(([a-z]+)\)$/', $tag, $m)) {
					return 'post_type_'.$m[1];
				}
				if (preg_match('/^time_category_or_term\(([0-9]+)\)$/', $tag, $m)) {
					return 'term_category_'.$m[1];
				}
				if (preg_match('/^(product|cart|checkout|order_received_page|account_page)$/', $tag, $m)) {
					return 'woocommerce_'.$m[1];
				}
				switch ($tag) {
					case 'home,archive': return 'blog';
					case 'page':         return 'post_type_page';
					case '404,404':      return '404';
					case 'shop,product_taxonomy,product':                                                return 'woocommerce_shop';
					case 'shop,product_taxonomy,product,cart,checkout,order_received_page,account_page': return 'woocommerce';
					default: return $tag;
				}
			};

			if (isset($data['banner']['content']) && is_array($data['banner']['content'])) {
				$banner = array();
				foreach ($data['banner']['content'] as $tag => $value) {
					if (isset($value['type']) && $value['type'] != 'inherit') {
						if ($value['type'] == 'layerslider') {
							$value['type'] = 'slider';
						}
						if (isset($value['layerslider'])) {
							$value['slider'] = $value['layerslider'];
							unset($value['layerslider']);
						}
						$banner[$convert_tag($tag)] = $value;
					}
				}
				$data['banner']['content'] = $banner;
			}

			if (isset($data['sidebar']['layout']) && is_array($data['sidebar']['layout'])) {
				$sidebar = array();
				foreach ($data['sidebar']['layout'] as $tag => $value) {
					if (isset($value['sidebar']) && (!isset($value['enabled']) || $value['enabled'])) {
						$sidebar[$convert_tag($tag)] = $value['sidebar'];
					}
				}
				$data['sidebar']['layout'] = $sidebar;
			}

			foreach (array('upper', 'lower') as $pos) {
				if (isset($data['nav']['secondary'][$pos]) && is_array($data['nav']['secondary'][$pos])) {
					$nav = array('default' => count($data['nav']['secondary'][$pos]) >= count($conditional_tags_array));
					foreach ($conditional_tags_array as $tag) {
						if (in_array($tag, $data['nav']['secondary'][$pos]) != $nav['default']) {
							$nav[$convert_tag($tag)] = !$nav['default'];
						}
					}
					$data['nav']['secondary'][$pos] = $nav;
				}
			}

			if (isset($data['nav']['headline']['visible']) && is_array($data['nav']['headline']['visible'])) {
				$headline = array('default' => count($data['nav']['headline']['visible']) >= count($conditional_tags_array));
				foreach ($conditional_tags_array as $tag) {
					if (in_array($tag, $data['nav']['headline']['visible']) != $headline['default']) {
						$headline[$convert_tag($tag)] = !$headline['default'];
					}
				}
				$data['nav']['headline']['visible'] = $headline;
			}

		}

	}

	// -------------------------------------------------------------------------

	public function onPostOptionsCompatybility(array &$data, $version, $post_type)
	{

		// 3.3
		if (version_compare($version, '3.3') < 0) {

			if (in_array($post_type, array('post', 'page', 'gallery', 'portfolio', 'product'))) {
				if (isset($data['layout']['banner']['type'])) {
					$data['layout']['banner'] = array(
						'enabled' => $data['layout']['banner']['type'] != 'inherit',
						'banner'  => $data['layout']['banner']
					);
					switch ($data['layout']['banner']['banner']['type']) {
						case 'inherit':
							$data['layout']['banner']['banner']['type'] = '';
							break;
						case 'layerslider':
							$data['layout']['banner']['banner']['type'] = 'slider';
							break;
					}
				}
				if (isset($data['layout']['banner']['banner']['layerslider'])) {
					$data['layout']['banner']['banner']['slider'] = $data['layout']['banner']['banner']['layerslider'];
					unset($data['layout']['banner']['banner']['layerslider']);
				}
			}

		}

	}

	// -------------------------------------------------------------------------

	/**
	 * Theme setup
	 *
	 * @since 1.0
	 * @see \Drone\Theme::onSetupTheme()
	 */
	protected function onSetupTheme()
	{

		// Theme features
		$this->addThemeFeature('x-ua-compatible');
		$this->addThemeFeature('nav-menu-current-item');
		$this->addThemeFeature('force-img-caption-shortcode-filter');

		// Editor style
		add_editor_style('data/css/wp/editor.css');

		// Menus
		register_nav_menus(array(
			'primary-desktop' => __('Desktop primary menu', 'time'),
			'primary-mobile'  => __('Mobile primary menu', 'time'),
			'secondary-upper' => __('Upper secondary menu', 'time'),
			'secondary-lower' => __('Lower secondary menu', 'time')
		));

		// Title
		if (version_compare($this->wp_version, '4.1') < 0) { // @deprecated WordPress 4.1

			add_action('wp_head', function() {
				echo '<title>'.wp_title('-', false, 'right').'</title>';
			});

			add_filter('wp_title', function($title, $sep) {
				if (is_feed()) {
					return $title;
				}
				if ((is_home() || is_front_page()) && ($description = get_bloginfo('description', 'display'))) {
					return $sep ? get_bloginfo('name')." {$sep} {$description}" : get_bloginfo('name');
				} else {
					return $sep ? $title.get_bloginfo('name') : $title;
				}
			}, 10, 2);

		}

		// Sidebars
		foreach (Time::$sidebars_array as $id => $name) {
			register_sidebar(array(
				'name'          => $name,
				'id'            => $id,
				'before_widget' => '<section id="%1$s" class="section widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="title">',
				'after_title'   => '</h2>'
			));
		}

		for ($i = 0; $i < count(Time::getFooterLayoutClasses()); $i++) {
			register_sidebar(array(
				'name'          => sprintf(__('Footer column %d', 'time'), $i+1),
				'id'            => 'footer-'.$i,
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="title">',
				'after_title'   => '</h2>'
			));
		}

		// Images
		add_theme_support('post-thumbnails');
		if (Time::to('general/retina')) {
			$this->addThemeFeature('retina-image-size');
		}

		$max_width      = Time::to('general/max_width');
		$thumbnail_size = Time::to('post/thumbnail/size');

		add_image_size('post-thumbnail', $thumbnail_size['width'], $thumbnail_size['height'], true);
		add_image_size('post-thumbnail-mini', 48, 48, true);
		add_image_size('logo', 9999, 60, false);
		add_image_size('banner', $max_width, 9999, false);
		add_image_size('full-hd', 1920, 1080, false);
		add_image_size('ls-thumbnail', 100, 63, true);

		foreach (array(1 => 748, 2 => 748, 4 => 364) as $columns => $mobile_width) {
			$column_sizes[$columns] = max(round(($max_width-40-20*($columns-1)) / $columns), $mobile_width);
		}
		add_image_size('full-width',   $column_sizes[1], 9999, false);
		add_image_size('medium-width', $column_sizes[2], 9999, false);
		add_image_size('small-width',  $column_sizes[4], 9999, false);

		// Post formats
		add_theme_support('post-formats', array(
			'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
		));

		// Custom fonts
		$custom_fonts = array();
		foreach (Time::to_('font/custom')->options as $custom_font) {
			$custom_fonts[$custom_font->property('id')] = sprintf('[font id="%s"]%%s[/font]', \Drone\Func::stringID($custom_font->property('id')));
		}

		// Shortcodes
		$this->addThemeFeature('shortcode-page');
		$this->addThemeFeature('shortcode-no-format');
		$this->addThemeFeature('tinymce-shortcodes-menu', array(
			__('Horizontal line', 'time')   => '[hr]',
			__('Mark', 'time')              => array(
				__('Standard', 'time') => '[mark]%s[/mark]',
				__('Yellow', 'time')   => '[mark color="yellow"]%s[/mark]'
			),
			__('Dropcap', 'time')           => '[dc]%s[/dc]',
			__('Tooltip', 'time')           => array(
				__('Simple', 'time')   => '[tooltip title=""]%s[/tooltip]',
				__('Advanced', 'time') => '[tooltip title="" gravity="s" fade="false"]%s[/tooltip]',
			),
			__('Font', 'time')              => $custom_fonts,
			__('Icon', 'time')              => array(
				__('Simple', 'time')   => '[icon name="help"]',
				__('Advanced', 'time') => sprintf('[icon name="help" color="%s" size="1.2em"]', strtolower(Time::to('general/color')))
			),
			__('Button', 'time')            => array(
				__('Simple', 'time')   => sprintf('[button href="%s"]%%s[/button]', home_url('/')),
				__('Advanced', 'time') => sprintf('[button href="%s" target="_blank" size="big" left_icon="help" color="%s" caption="%s"]%%s[/button]', home_url('/'), strtolower(Time::to('general/color')), __('New caption', 'time'))
			),
			__('Quote', 'time')             => array(
				__('Simple', 'time')   => '[quote]%s[/quote]',
				__('Advanced', 'time') => '[quote author="" bar="true" align="left" width="300px"]%s[/quote]'
			),
			__('List', 'time')              => array(
				__('Simple', 'time')   => '[list icon="right-open"]%s[/list]',
				__('Advanced', 'time') => sprintf('[list icon="right-open" color="%s"]%%s[/list]', strtolower(Time::to('general/color')))
			),
			__('Message', 'time')           => array(
				__('Simple', 'time')   => '[message]%s[/message]',
				__('Advanced', 'time') => '[message color="blue" closable="true"]%s[/message]'
			),
			__('Rating', 'time')       => '[rating rate="5/5" author=""]%s[/rating]',
			__('Social buttons', 'time')    => array(
				__('Simple', 'time')   => '[social_buttons]',
				__('Advanced', 'time') => '[social_buttons style="big" media="facebook, twitter, googleplus"]'
			),
			__('Search form', 'time')       => '[search]',
			__('Contact form', 'time')      => '[contact]',
			__('Columns', 'time')           => array(
				__('Start columns', 'time') => '[columns]',
				__('1/2 column', 'time')    => '[column width="1/2"]%s[/column]',
				__('1/3 column', 'time')    => '[column width="1/3"]%s[/column]',
				__('1/4 column', 'time')    => '[column width="1/4"]%s[/column]',
				__('End columns', 'time')   => '[/columns]'
			),
			__('Tabs', 'time')              => array(
				__('Start tabs', 'time') => '[tabs]',
				__('Tab', 'time')        => sprintf('[tab title="%s"]%%s[/tab]', __('New tab', 'time')),
				__('End tabs', 'time')   => '[/tabs]'
			),
			__('Toggles', 'time')           => array(
				__('Start toggles', 'time') => '[toggles]',
				__('Toggle', 'time')        => sprintf('[toggle title="%s"]%%s[/toggle]', __('New toggle', 'time')),
				__('End toggles', 'time')   => '[/toggles]'
			),
			__('Posts', 'time')             => array(
				__('Simple', 'time')   => '[posts]',
				__('Advanced', 'time') => '[posts type="post" size="auto" orderby="date" order="desc" count="1" style="gallery" columns="auto" title="true" excerpt="false" taxonomy="tag"]'
			),
			__('Page', 'time')              => '[page id=""]',
			__('Portfolio', 'time')         => array(
				__('Simple', 'time')   => '[portfolio]',
				__('External', 'time') => '[portfolio id="auto"]',
				__('Advanced', 'time') => '[portfolio id="auto" size="auto" columns="inherit" filter="inherit" orderby="inherit" order="inherit" limit="inherit" pagination="inherit" titles="inherit" excerpts="inherit" taxonomies="inherit"]'
			),
			__('Media', 'time')             => array(
				__('Dekstop only', 'time') => '[media device="desktop"]%s[/media]',
				__('Mobile only', 'time')  => '[media device="mobile"]%s[/media]'
			),
			__('Custom galleries', 'time')  => array(
				__('Bricks', 'time')     => '[bricks]%s[/bricks]',
				__('Slider', 'time')     => '[slider]%s[/slider]',
				__('Scroller', 'time')   => '[scroller]%s[/scroller]',
				__('Super tabs', 'time') => '[super_tabs]%s[/super_tabs]'
			),
			__('Custom page', 'time')       => array(
				__('Content', 'time')       => '[content sidebars="inherit"]%s[/content]',
				__('Section', 'time')       => '[section]%s[/section]',
				__('Posts section', 'time') => '[section_posts]'
			)
		));

		// bbPress
		if (Time::isPluginActive('bbpress')) {
			$func = function($display) { return is_bbpress() ? false : $display; };
			add_filter('time_meta_display', $func, 20);
			add_filter('time_social_buttons_display', $func, 20);
		}

		// Breadcrumb trail
		add_theme_support('breadcrumb-trail');

		// Captcha
		if (Time::isPluginActive('captcha')) {
			if (has_action('comment_form_after_fields', 'cptch_comment_form_wp3')) {
				remove_action('comment_form_after_fields', 'cptch_comment_form_wp3', 1);
				remove_action('comment_form_logged_in_after', 'cptch_comment_form_wp3', 1);
				add_filter('comment_form_field_comment', array($this, 'filterCaptchaCommentFormFieldComment'));
			}
		}

		// LayerSlider
		if (Time::isPluginActive('layerslider')) {
			remove_action('wp_enqueue_scripts', 'layerslider_enqueue_content_res');
			remove_shortcode('layerslider');
		}

		// WooCommerce
		if (Time::isPluginActive('woocommerce')) {
			require $this->template_dir.'/inc/woocommerce.php';
			add_theme_support('woocommerce');
		}

		// WooCommerce Brands
		if (Time::isPluginActive('woocommerce-brands')) {
			remove_action('woocommerce_product_meta_end', array($GLOBALS['WC_Brands'], 'show_brand'));
		}

		// Illegal
		if (!is_admin() && Time::isIllegal()) {
			Time::to_('footer/end_note/right')->value = Time::to_('footer/end_note/right')->default;
		}

	}

	// -------------------------------------------------------------------------

	/**
	 * Initialization
	 *
	 * @since 2.0
	 * @see \Drone\Theme::onInit()
	 */
	public function onInit()
	{

		// Gallery
 		register_post_type('gallery', apply_filters('time_register_post_type_gallery_args', array(
			'label'       => __('Galleries', 'time'),
			'description' => __('Galleries', 'time'),
			'public'      => true,
			'menu_icon'   => 'dashicons-images-alt2',
			'supports'    => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions'),
			'rewrite'     => array('slug' => Time::to('gallery/slug')),
			'labels'      => array(
				'name'               => __('Galleries', 'time'),
				'singular_name'      => __('Gallery', 'time'),
				'add_new'            => _x('Add New', 'Gallery', 'time'),
				'all_items'          => __('All Galleries', 'time'),
				'add_new_item'       => __('Add New Gallery', 'time'),
				'edit_item'          => __('Edit Gallery', 'time'),
				'new_item'           => __('New Gallery', 'time'),
				'view_item'          => __('View Gallery', 'time'),
				'search_items'       => __('Search Galleries', 'time'),
				'not_found'          => __('No Galleries found', 'time'),
				'not_found_in_trash' => __('No Galleries found in Trash', 'time'),
				'menu_name'          => __('Galleries', 'time')
			)
		)));

 		// Portfolio
		register_post_type('portfolio', apply_filters('time_register_post_type_portfolio_args', array(
			'label'        => __('Portfolios', 'time'),
			'description'  => __('Portfolios', 'time'),
			'public'       => true,
			'menu_icon'    => 'dashicons-exerpt-view',
			'hierarchical' => true,
			'supports'     => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes'),
			//'taxonomies'   => array('portfolio-category', 'portfolio-tag'),
			'has_archive'  => true,
			'rewrite'      => array('slug' => Time::to('portfolio/slug')),
			'labels'       => array(
				'name'               => __('Portfolios', 'time'),
				'singular_name'      => __('Portfolio', 'time'),
				'add_new'            => _x('Add New', 'Portfolio', 'time'),
				'all_items'          => __('All Portfolios', 'time'),
				'add_new_item'       => __('Add New Portfolio', 'time'),
				'edit_item'          => __('Edit Portfolio', 'time'),
				'new_item'           => __('New Portfolio', 'time'),
				'view_item'          => __('View Portfolio', 'time'),
				'search_items'       => __('Search Portfolios', 'time'),
				'not_found'          => __('No Portfolios found', 'time'),
				'not_found_in_trash' => __('No Portfolios found in Trash', 'time'),
				'menu_name'          => __('Portfolios', 'time')
			)
		)));
		register_taxonomy('portfolio-category', array('portfolio'), apply_filters('time_register_taxonomy_portfolio_category_args', array(
			'label'        => __('Categories', 'time'),
			'hierarchical' => true,
			'rewrite'      => array('slug' => Time::to('portfolio/slug').'-category')
		)));
		register_taxonomy('portfolio-tag', array('portfolio'), apply_filters('time_register_taxonomy_portfolio_tag_args', array(
			'label'        => __('Tags', 'time'),
			'hierarchical' => false,
			'rewrite'      => array('slug' => Time::to('portfolio/slug').'-tag')
		)));

	}

	// -------------------------------------------------------------------------

	/**
	 * Widgets initialization
	 *
	 * @since 1.0
	 * @see \Drone\Theme::onWidgetsInit()
	 */
	public function onWidgetsInit()
	{

		// Widgets
		$this->addThemeFeature('widget-unwrapped-text');
		$this->addThemeFeature('widget-page');
		$this->addThemeFeature('widget-contact');
		$this->addThemeFeature('widget-posts-list');
		$this->addThemeFeature('widget-twitter');
		$this->addThemeFeature('widget-flickr');
		$this->addThemeFeature('widget-facebook-like-box');

		// LayerSlider
		if (Time::isPluginActive('layerslider')) {
			unregister_widget('LayerSlider_Widget');
		}

		// WooCommerce
		if (Time::isPluginActive('woocommerce')) {

			require $this->template_dir.'/inc/woocommerce-widgets.php';

			foreach (array(
				'WC_Widget_Cart',
				'WC_Widget_Layered_Nav_Filters',
				'WC_Widget_Layered_Nav',
				'WC_Widget_Price_Filter',
				'WC_Widget_Product_Categories',
				'WC_Widget_Product_Search',
				'WC_Widget_Product_Tag_Cloud',
				'WC_Widget_Products',
				'WC_Widget_Recent_Reviews',
				'WC_Widget_Recently_Viewed',
				'WC_Widget_Top_Rated_Products'
			) as $class) {
				if (class_exists($class)) {
					unregister_widget($class);
					register_widget('Time_'.$class);
				}
			}

		}

		// WooCommerce Brands
		if (Time::isPluginActive('woocommerce-brands')) {

			require $this->template_dir.'/inc/woocommerce-brands-widgets.php';

			foreach (array(
				'WC_Widget_Brand_Nav'
			) as $class) {
				if (class_exists($class)) {
					unregister_widget($class);
					register_widget('Time_'.$class);
				}
			}

		}

	}

	// -------------------------------------------------------------------------

	/**
	 * tgmpa_register action
	 *
	 * @internal action: tgmpa_register
	 * @since 1.0
	 */
	public function actionTGMPARegister()
	{
		$plugins = array(
			array(
			    'name'               => 'LayerSlider WP - Time Theme edition',
			    'slug'               => 'time-layerslider',
			    'source'             => $this->template_dir.'/plugins/time-layerslider.zip',
			    'required'           => false,
			    'version'            => Time::LAYERSLIDER_VERSION,
			    'force_activation'   => false,
			    'force_deactivation' => true
			),
			array(
			    'name'               => 'WiLD Googlemap',
			    'slug'               => 'wild-googlemap',
			    'source'             => $this->template_dir.'/plugins/wild-googlemap.zip',
			    'required'           => false,
			    'version'            => Time::WILD_GOOGLEMAP_VERSION,
			    'force_activation'   => false,
			    'force_deactivation' => true
			)
		);
		tgmpa($plugins);
	}

	// -------------------------------------------------------------------------

	/**
	 * Styles and scripts
	 *
	 * @internal action: wp_enqueue_scripts
	 * @since 1.0
	 */
	public function actionWPEnqueueScripts()
	{

		// Minimize sufix
		$min_sufix = !$this->debug_mode ? '.min' : '';

		// 3rd party styles
		wp_enqueue_style('time-3rd-party', $this->template_uri.'/data/css/3rd-party.min.css');

		// Main style
		wp_enqueue_style('time-style', $this->template_uri."/data/css/style{$min_sufix}.css");

		// Color scheme
		wp_enqueue_style('time-scheme', $this->template_uri.'/data/css/'.Time::to('general/scheme').$min_sufix.'.css');

		// Stylesheet
		wp_enqueue_style('time-stylesheet', get_stylesheet_uri());

		// Leading color
		require $this->template_dir.'/inc/color.php';
		$this->addDocumentStyle(sprintf(
			$color,
			Time::to('general/color'),
			implode(', ', array_map('hexdec', str_split(substr(Time::to('general/color'), 1), 2)))
		));

		// Comment reply
		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}

		// Responsive design
		if (Time::to('general/responsive')) {
			wp_enqueue_style('time-mobile', $this->template_uri."/data/css/mobile{$min_sufix}.css", array(), false, 'only screen and (max-width: 767px)');
		}

		// 3rd party scripts
		wp_enqueue_script('time-3rd-party', $this->template_uri.'/data/js/3rd-party.min.js', array('jquery'), false, true);

		// Main script
		wp_enqueue_script('time-script', $this->template_uri."/data/js/time{$min_sufix}.js", array('jquery'), false, true);

		// WooCommerce
		if (Time::isPluginActive('woocommerce')) {
			wp_deregister_script('wc-single-product');
			wp_register_script('wc-single-product', $this->template_uri."/data/js/woocommerce/single-product{$min_sufix}.js", array('jquery'), WOOCOMMERCE_VERSION, true);
		}

		// Configuration
		$this->addDocumentScript(sprintf(
<<<EOS
			timeConfig = {
				templatePath:       '%s',
				zoomHoverIcons:     %s,
				flexsliderOptions:  %s,
				layersliderOptions: %s
			};
EOS
			,
			$this->template_uri,
			json_encode(array_map(function($s) { return 'icon-'.$s; }, Time::to_('site/hover_icons')->toArray())),
			json_encode(\Drone\Func::arrayKeysToCamelCase(Time::to_('site/slider')->toArray())),
			Time::getLayerSliderOptions()
		));

		// Max. width style
		if (!Time::to_('general/max_width')->isDefault()) {
			$this->addDocumentStyle(sprintf(
<<<EOS
				.layout-boxed .outer-container, .container {
					max-width: %dpx;
				}
EOS
			, Time::to('general/max_width')));
		}

		// Colors styles
		foreach (Time::to_('color')->childs() as $name => $group) {
			if ($group->child('enabled')->value) {
				$color = $group->child($name);
				$this->addDocumentStyle(sprintf('%s { background-color: %s; }', $color->tag, $color->value));
			}
		}

		// Fonts styles
		foreach (\Drone\Options\Option\Font::getInstances() as $font) {
			if ($font->isVisible() && !is_null($font->tag)) {
				$this->addDocumentStyle($font->css($font->tag));
			}
		}

		// Flickr widget script
		if (is_active_widget(false, false, 'time-flickr')) {
			$this->addDocumentJQueryScript(
<<<EOS
				\$('#bottom .col-1-4 > .widget-flickr .flickr').addClass('fix-flickr-desktop');
EOS
			);
		}

		// List widgets script
		if (is_active_widget(false, false, 'pages') ||
			is_active_widget(false, false, 'archives') ||
			is_active_widget(false, false, 'categories') ||
			is_active_widget(false, false, 'recent-posts') ||
			is_active_widget(false, false, 'recent-comments') ||
			is_active_widget(false, false, 'bbp_forums_widget') ||
			is_active_widget(false, false, 'bbp_replies_widget') ||
			is_active_widget(false, false, 'bbp_topics_widget') ||
			is_active_widget(false, false, 'bbp_views_widget')) {
			$this->addDocumentJQueryScript(
<<<EOS
				\$('.widget_pages, .widget_archive, .widget_categories, .widget_recent_entries, .widget_recent_comments, .widget_display_forums, .widget_display_replies, .widget_display_topics, .widget_display_views').each(function() {
					$('ul', this).addClass('fancy alt');
					$('li', this).prepend(\$('<i />', {'class': 'icon-right-open'}));
					if (\$(this).closest('#top').length > 0) {
						$('i', this).addClass('color');
					}
				});
EOS
			);
		}

		// Tag cloud widget script
		if (is_active_widget(false, false, 'tag_cloud')) {
			$this->addDocumentJQueryScript(
<<<EOS
				\$('.widget_tag_cloud a').addClass('alt');
EOS
			);
		}

		// Custom menu widget script
		if (is_active_widget(false, false, 'nav_menu')) {
			$this->addDocumentJQueryScript(
<<<EOS
				\$('#top .widget_nav_menu div:has(> ul)').replaceWith(function() {
					return '<nav class="aside arrows">'+\$(this).html()+'</nav>';
				});
				\$('#bottom .widget_nav_menu').each(function() {
					$('ul', this).addClass('fancy alt');
					$('li', this).prepend(\$('<i />', {'class': 'icon-right-open'}));
				});
EOS
			);
		}

		// Meta widget script
		if (is_active_widget(false, false, 'meta')) {
			$this->addDocumentJQueryScript(
<<<EOS
				\$('#top .widget_meta > ul').wrap('<nav class="aside arrows" />');
EOS
			);
		}

		// bbPress
		if (Time::isPluginActive('bbpress') && is_active_widget(false, false, 'bbp_replies_widget')) {
			$this->addDocumentJQueryScript(
<<<EOS
				\$('.widget_display_replies li > div').addClass('small');
EOS
			);
		}

		// Disqus Comment System
		if (Time::isPluginActive('disqus-comment-system')) {
			$this->addDocumentJQueryScript(
<<<EOS
				\$('#disqus_thread').addClass('section');
EOS
			);
		}

		// WooCommerce
		if (Time::isPluginActive('woocommerce')) {
			$this->addDocumentStyle(sprintf(
<<<EOS
				.icon-woocommerce-cart {
					color: %s;
				}
				a:hover .icon-woocommerce-cart {
					color: %s;
				}
				.widget_price_filter .ui-slider .ui-slider-range,
				.widget_price_filter .ui-slider .ui-slider-handle {
					background-color: %s;
				}
EOS
				,
				Time::to('woocommerce/cart/color', '__default', 'inherit'),
				Time::to(array('woocommerce/cart/hover', 'general/color'), '__default'),
				Time::to('general/color')
			));
			if (Time::to('woocommerce/onsale/custom')) {
				$this->addDocumentStyle(sprintf(
<<<EOS
					.woocommerce .onsale,
					.woocommerce-page .onsale {
						background: %s;
						color: %s;
					}
EOS
					,
					Time::to('woocommerce/onsale/background'),
					Time::to('woocommerce/onsale/color')
				));
			}
			if (is_account_page()) {
				$this->addDocumentJQueryScript(
<<<EOS
					\$('.woocommerce .my_account_orders .order-actions .button').removeClass('button');
EOS
				);
			}
		}

		// WooCommerce Brands
		if (Time::isPluginActive('woocommerce-brands')) {
			wp_dequeue_style('brands-styles');
		}

	}

	// -------------------------------------------------------------------------

	/**
	 * pre_get_posts action
	 *
	 * @internal action: pre_get_posts
	 * @since 1.0
	 *
	 * @param object $query
	 */
	public function actionPreGetPosts($query)
	{
		if ($query->is_tax('portfolio-category') || $query->is_tax('portfolio-tag')) {
			$query->query_vars['posts_per_page'] = Time::to('portfolio/archive/count');
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * comment_form_before_fields action
	 *
	 * @internal action: comment_form_before_fields
	 * @since 1.0
	 */
	public function actionCommentFormBeforeFields()
	{
		echo '<div class="columns alt-mobile"><ul>';
	}

	// -------------------------------------------------------------------------

	/**
	 * comment_form_after_fields action
	 *
	 * @internal action: comment_form_after_fields
	 * @since 1.0
	 */
	public function actionCommentFormAfterFields()
	{
		echo '</ul></div>';
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_single_product_summary action
	 *
	 * @internal action: woocommerce_single_product_summary, 35
	 * @since 2.0
	 */
	public function actionWoocommerceSingleProductSummary()
	{

		if (!Time::isPluginActive('woocommerce-brands') || !Time::to('woocommerce/product/brands')) {
			return;
		}

		// Brand
		$brands = wp_get_post_terms(get_the_ID(), 'product_brand', array('fields' => 'ids'));

		if (count($brands) == 0) {
			return;
		}
		$brand = get_term($brands[0], 'product_brand');

		// Validation
		if (!$brand->description) {
			return;
		}

		// HTML
		$html = \Drone\HTML::make();
		$html->addNew('hr');

		// Thumbnail
		if ($thumbnail_id = get_woocommerce_term_meta($brand->term_id, 'thumbnail_id', true)) {
			$html->addNew('figure')
				->class('alignleft')
				->addNew('a')
					->attr(Time::getImageAttrs('a', array('border' => false, 'hover' => '')))
					->href(get_term_link($brand, 'product_brand'))
					->title($brand->name)
					->add(wp_get_attachment_image($thumbnail_id, 'logo'));
		}

		// Description
		$html->add(wpautop(wptexturize($brand->description)));

		echo $html->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * image_size_names_choose filter
	 *
	 * @internal filter: image_size_names_choose
	 * @since 1.0
	 *
	 * @param  array $sizes
	 * @return array
	 */
	public function filterImageSizeNamesChoose($sizes)
	{
		return array_merge($sizes, array(
			'full-width'   => __('1 column', 'time'),
			'medium-width' => __('2 or 3 columns', 'time'),
			'small-width'  => __('4+ columns', 'time')
		));
	}

	// -------------------------------------------------------------------------

	/**
	 * body_class filter
	 *
	 * @internal filter: body_class
	 * @since 1.0
	 *
	 * @param  array $classes
	 * @return array
	 */
	public function filterBodyClass($classes)
	{
		if (Time::isPluginActive('wpml')) {
			$classes[] = 'lang-'.ICL_LANGUAGE_CODE;
		}
		if (is_page_template('full-screen-gallery.php')) {
			Time::to_('general/layout')->value = 'open';
			$classes[] = 'full-screen';
		}
		$classes[] = 'layout-'.Time::to('general/layout');
		$classes[] = 'scheme-'.Time::to('general/scheme');
		return $classes;
	}

	// -------------------------------------------------------------------------

	/**
	 * wp_nav_menu_items and wp_list_pages filter
	 *
	 * @internal filter: wp_nav_menu_items
	 * @internal filter: wp_list_pages
	 * @since 1.0
	 *
	 * @param  string $items
	 * @param  array  $args
	 * @return string
	 */
	public function filterWPNavMenuItems($items, $args)
	{

		// Theme location
		if (isset($args->theme_location)) {
			$theme_location = $args->theme_location;
		} else if (isset($args['theme_location'])) {
			$theme_location = $args['theme_location'];
		} else {
			return $items;
		}

		// Icons
		$items = preg_replace_callback('#<li(.*)><a(.*)>(.*)</a>#iU', array($this, 'filterWPNavMenuItemsCallback'), $items);

		// Cart
		if ($theme_location == 'primary-desktop' && Time::to('header/cart/enabled')) {
			$items .= '<li class="cart">'.$this->getCartMenuitem().'</li>';
		}

		// Search
		if ($theme_location == 'primary-desktop' && Time::to_('header/search')->value('desktop')) {
			$items .= '<li>'.get_search_form(false).'</li>';
		}

		// Language menu
		if (($theme_location == 'primary-desktop' || $theme_location == 'primary-mobile') && Time::to('header/lang') == 'long') {
			$langs = icl_get_languages('skip_missing=0&orderby=code');
			$items .= '<li class="lang">';
			foreach ($langs as $lang) {
				if ($lang['active']) {
					$items .= $this->getLanguageMenuitem($lang);
					break;
				}
			}
			$items .= '<ul>';
			foreach ($langs as $lang) {
				if (!$lang['active']) {
					$items .= '<li>'.$this->getLanguageMenuitem($lang).'</li>';
				}
			}
			$items .= '</ul></li>';
		}

		// Result
		return $items;

	}

	// -------------------------------------------------------------------------

	/**
	 * wp_nav_menu_items and wp_list_pages filter helper function
	 *
	 * @since 1.0
	 *
	 * @param  array  $matches
	 * @return string
	 */
	protected function filterWPNavMenuItemsCallback($matches)
	{
		if (preg_match('/[ "](icon-([-_a-z0-9]+))[ "]/', $matches[1], $m)) {
			$icon = str_replace('_', '/', $m[2]);
			$matches[1] = str_replace($m[1], '', $matches[1]);
			return sprintf(
				'<li%s><a%s>%s%s</a>',
				$matches[1],
				$matches[2],
				$this->shortcodeIcon(array('name' => $icon, 'size' => '')),
				$matches[3]
			);
		} else {
			return $matches[0];
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * get_search_form filter
	 *
	 * @internal filter: get_search_form
	 * @since 1.0
	 *
	 * @return string
	 */
	public function filterGetSearchForm()
	{
		$search_form = \Drone\HTML::form()
			->method('get')
			->action(home_url('/'))
			->class('search')
			->role('search');
		$search_form->addNew('input')
			->type('text')
			->name('s')
			->value(get_search_query())
			->placeholder(__('Search site', 'time'));
		$search_form->addNew('button')
			->type('submit')
			->addNew('i')
				->class('icon-search');
		return $search_form->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * wp_get_attachment_image_attributes filter
	 *
	 * @internal filter: wp_get_attachment_image_attributes
	 * @since 1.0
	 *
	 * @param  array  $attr
	 * @param  object $attachment
	 * @return string
	 */
	public function filterWPGetAttachmentImageAttributes($attr, $attachment)
	{
		if (Time::to('general/retina')) {
			$size = str_replace('attachment-', '', $attr['class']);
	 		if ($image_2x = wp_get_attachment_image_src($attachment->ID, $size.'@2x')) {
				list($attr['data-2x']) = $image_2x;
			}
		}
		return $attr;
	}

	// -------------------------------------------------------------------------

	/**
	 * get_calendar filter
	 *
	 * @internal filter: get_calendar
	 * @since 1.0
	 *
	 * @param  string $calendar_output
	 * @return string
	 */
	public function filterGetCalendar($calendar_output)
	{
		if (gettype($calendar_output) !== 'string' || strpos($calendar_output, '<table') === false) {
			return $calendar_output;
		}
		return str_replace('<table', '<table class="fixed"', $calendar_output);
	}

	// -------------------------------------------------------------------------

	/**
	 * img_caption_shortcode filter
	 *
	 * @internal filter: img_caption_shortcode
	 * @since 1.0
	 *
	 * @param  string $depricated
	 * @param  array  $atts
	 * @param  string $content
	 * @return string
	 */
	public function filterImgCaptionShortcode($deprecated, $atts, $content = null)
	{

		// Attributes
		extract(shortcode_atts(array(
			'id'      => '',
			'align'   => 'alignnone',
			'width'   => '',
			'caption' => ''
		), $atts, 'caption'));

		// ID
		$int_id = (int)str_replace('attachment_', '', $id);

		// Class
		$class = preg_match('/class="(.*?)"/i', $content, $m) ? preg_replace('/\balign(none|left|right|center)\b/i', '', $m[1]) : '';

		// Content
		$content = preg_replace('/class="(.*?)"/i', '', trim($content));

		// Retina
		if (Time::to('general/retina') && $int_id && $width && preg_match('/\bsize-(thumbnail|medium|large|(full|medium|small)-width)\b/', $class, $matches)) {
			if ($image_2x = wp_get_attachment_image_src($int_id, $matches[1].'@2x')) {
				list($src_2x) = $image_2x;
				$content = str_replace('<img ', '<img data-2x="'.$src_2x.'" ', $content);
			}
		}

		// Images attributes
		$atts = array();
		if (preg_match_all('/\b(border|hover|fancybox)-([a-z]+)\b/i', $class, $matches, PREG_SET_ORDER) > 0) {
			foreach ($matches as $match) {
				$atts[$match[1]] = str_ireplace('none', '', $match[2]);
				$class = str_replace($match[0], '', $class);
			}
		}

		// Settings
		if (strpos($content, '<a ') === 0) {
			$content = str_replace('<a ', sprintf('<a data-fancybox-title="%s" %s ', $caption, \Drone\Func::arraySerialize(Time::getImageAttrs('a', $atts), 'html')), $content);
		} else {
			$content = sprintf('<div %s>%s</div>', \Drone\Func::arraySerialize(Time::getImageAttrs('div', $atts), 'html'), $content);
		}

		// Figure
		$figure = \Drone\HTML::figure()
			->id($id ? $id : null)
			->addClass($class, $align, $align == 'alignleft' || $align == 'alignright' ? 'fixed' : null)
			->style($width ? "width: {$width}px;" : null)
			->add($content);

		// Caption
		if ($caption) {
			$figure->addNew('figcaption')->add($caption);
		}

		return $figure->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * the_content filter
	 *
	 * @internal filter: the_content, 1
	 * @since 1.0
	 *
	 * @param  string $content
	 * @return string
	 */
	public function filterTheContent1($content)
	{
		return preg_replace(
			'|(<p([^>]*)>)?(( *(<a[^>]*>)?<img[^>]*class="[^"]*alignnone[^"]*"[^>]*>(</a>)? *){2,})(</p>)?|i',
			'<div class="figuregroup"\2>\3</div>',
			$content
		);
	}

	// -------------------------------------------------------------------------

	/**
	 * the_content filter
	 *
	 * @internal filter: the_content
	 *
	 * @param  string $content
	 * @return string
	 */
	public function filterTheContent($content)
	{
		return preg_replace('#(<p>)?(<(iframe|embed).*?></\3>)(</p>)?#i', '<div class="embed">\2</div>', $content);
	}

	// -------------------------------------------------------------------------

	/**
	 * post_gallery filter
	 *
	 * @internal filter: post_gallery
	 * @since 1.0
	 *
	 * @param  string $_
	 * @param  array  $atts
	 * @return string
	 */
	public function filterPostGallery($_, $atts)
	{

		// Post
		$post = get_post();

		// Attributes
		extract(shortcode_atts(array(
			'order'      => 'ASC',
			'orderby'    => 'menu_order ID',
			'id'         => $post ? $post->ID : 0,
			'columns'    => 3,
			'size'       => 'auto',
			'link'       => 'post',
			'include'    => '',
			'exclude'    => '',
			'full_width' => false,
			'captions'   => true,
			'border'     => 'inherit',
			'hover'      => 'inherit',
			'fancybox'   => 'inherit'
		), $atts, 'gallery'));

		// Attachments
		$params = array(
			'numberposts'    => -1,
			'post_parent'    => $id,
			'post_status'    => 'inherit',
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'orderby'        => $order == 'RAND' ? 'none' : $orderby,
			'order'          => $order
		);
		if (!empty($include)) {
			unset($params['post_parent']);
			$params['include'] = preg_replace('/[^0-9,]+/', '', $include);
		} else if (!empty($exclude)) {
			$params['exclude'] = preg_replace('/[^0-9,]+/', '', $exclude);
		}
		$attachments = get_posts($params);

		// Custom gallery HTML code
		if ($gallery = apply_filters('time_post_gallery_html', '', $atts, $attachments)) {
			return $gallery;
		}

		// JetPack
		if (defined('JETPACK__VERSION') && get_option('tiled_galleries')) {
			return '';
		}

		// Gallery post type
		if (is_singular('gallery')) {
			return $this->filterTimePostGalleryHTMLBricks('', $atts, $attachments);
		}

		// Size
		if ($size == 'auto') {
			$size = \Drone\Func::stringToBool($full_width) ? Time::getImageSize($columns) : 'thumbnail';
		}

		// Gallery
		$gallery = \Drone\HTML::div()
			->id('gallery-'.(++Time::$gallery_instance))
			->class('columns');

		// Gallery items
		$items = $gallery->addNew('ul');

		foreach ($attachments as $attachment) {

			// Figure
			$figure = $items->addNew('li')->class('col-1-'.$columns)->addNew('figure');
			if (\Drone\Func::stringToBool($full_width)) {
				$figure->class = 'full-width';
			} else {
				list(, $width) = wp_get_attachment_image_src($attachment->ID, $size);
				$figure->class = 'aligncenter';
				$figure->style = sprintf('width: %dpx;', $width);
			}

			// Hyperlink and image
			if ($url = $this->getAttachmentLinkURL($attachment, $link)) {
				$a = $figure->addNew('a')
					->rel($gallery->id)
					->attr(Time::getImageAttrs('a', compact('border', 'hover', 'fancybox')))
					->data('fancybox-title', $attachment->post_excerpt)
					->href($url)
					->add(wp_get_attachment_image($attachment->ID, $size));
			} else {
				$figure->addNew('div')
					->attr(Time::getImageAttrs('div', compact('border', 'hover', 'fancybox')))
					->add(wp_get_attachment_image($attachment->ID, $size));
			}

			// Caption
			if (\Drone\Func::stringToBool($captions) && trim($attachment->post_excerpt)) {
				$caption = $figure->addNew('figcaption')
					->add(wptexturize($attachment->post_excerpt));
			}

		}

		return $gallery->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * time_post_gallery_html filter for bricks
	 *
	 * @since 1.0
	 *
	 * @param  string $_
	 * @param  array  $atts
	 * @param  array  $attachments
	 * @return string
	 */
	public function filterTimePostGalleryHTMLBricks($_, $atts, $attachments)
	{

		// Attributes
		extract(shortcode_atts(array(
			'columns'  => 3,
			'size'     => 'auto',
			'link'     => 'post',
			'captions' => true,
			'border'   => 'inherit',
			'hover'    => 'inherit',
			'fancybox' => 'inherit'
		), $atts, 'bricks'));

		// Size
		if ($size == 'auto') {
			$size = Time::getImageSize($columns);
		}

		// Bricks
		$bricks = \Drone\HTML::div()
			->id('gallery-'.(++Time::$gallery_instance))
			->class('bricks')
			->data('bricks-columns', $columns);

		// Bricks items
		foreach ($attachments as $attachment) {

			// Figure
			$figure = $bricks->addNew('div')->addNew('figure')
				->class('full-width');

			// Hyperlink and image
			if ($url = $this->getAttachmentLinkURL($attachment, $link)) {
				$a = $figure->addNew('a')
					->rel($bricks->id)
					->attr(Time::getImageAttrs('a', compact('border', 'hover', 'fancybox')))
					->data('fancybox-title', $attachment->post_excerpt)
					->href($url)
					->add(wp_get_attachment_image($attachment->ID, $size));
			} else {
				$figure->addNew('div')
					->attr(Time::getImageAttrs('div', compact('border', 'hover', 'fancybox')))
					->add(wp_get_attachment_image($attachment->ID, $size));
			}

			// Caption
			if (\Drone\Func::stringToBool($captions) && trim($attachment->post_excerpt)) {
				$caption = $figure->addNew('figcaption')
					->add(wptexturize($attachment->post_excerpt));
			}

		}

		return $bricks->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * time_post_gallery_html filter for slider
	 *
	 * @since 1.0
	 *
	 * @param  string $_
	 * @param  array  $atts
	 * @param  array  $attachments
	 * @return string
	 */
	public function filterTimePostGalleryHTMLSlider($_, $atts, $attachments)
	{

		// Attributes
		extract(shortcode_atts(array(
			'size'     => 'full-width',
			'link'     => 'post',
			'captions' => true,
			'border'   => false,
			'hover'    => '',
			'fancybox' => 'inherit'
		), $atts, 'slider'));

		// Size
		if ($size == 'auto') {
			$size = 'full-width';
		}

		// Slider
		$slider = \Drone\HTML::div()
			->id('gallery-'.(++Time::$gallery_instance))
			->class('slider');

		// Slides
		$slides = $slider->addNew('ul')
			->class('slides');

		foreach ($attachments as $attachment) {

			// Figure
			$figure = $slides->addNew('li')->addNew('figure');

			// Hyperlink and image
			if ($attachment->post_content && preg_match('#<iframe.*?>\s*</iframe>#i', $attachment->post_content, $matches)) {
				$iframe = preg_replace_callback(
					'#src="(?P<url>(https?:)?//www.youtube.com/embed/[-_a-z0-9]+)\??(?P<get>.*?)"#i',
					function($m) {
						return sprintf('src="%s?wmode=opaque&amp;enablejsapi=1%s"', $m['url'], isset($m['get']) && $m['get'] ? '&amp;'.$m['get'] : '');
					},
					$attachment->post_content
				);
				$figure->tag('div')
					->class('embed')
					->add($iframe);
			} else if ($url = $this->getAttachmentLinkURL($attachment, $link)) {
				$figure->addNew('a')
					->rel($slider->id)
					->attr(Time::getImageAttrs('a', compact('border', 'hover', 'fancybox')))
					->data('fancybox-title', $attachment->post_excerpt)
					->href($url)
					->add(wp_get_attachment_image($attachment->ID, $size));
			} else {
				$figure->addNew('div')
					->attr(Time::getImageAttrs('div', compact('border', 'hover', 'fancybox')))
					->add(wp_get_attachment_image($attachment->ID, $size));
			}

			// Caption
			if (\Drone\Func::stringToBool($captions) && trim($attachment->post_excerpt)) {
				$caption = $figure->addNew('p')
					->class('caption')
					->add(wptexturize($attachment->post_excerpt));
			}

		}

		return $slider->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * time_post_gallery_html filter for scroller
	 *
	 * @since 1.0
	 *
	 * @param  string $_
	 * @param  array  $atts
	 * @param  array  $attachments
	 * @return string
	 */
	public function filterTimePostGalleryHTMLScroller($_, $atts, $attachments)
	{

		// Attributes
		extract(shortcode_atts(array(
			'size'      => 'thumbnail',
			'link'      => 'post',
			'buttons'   => false,
			'border'    => false,
			'hover'     => 'inherit',
			'fancybox'  => 'inherit'
		), $atts, 'scroller'));

		// Size
		if ($size == 'auto') {
			$size = 'thumbnail';
		} else if ($size == 'logo') {
			extract(array_diff_key(array(
				'border'   => false,
				'hover'    => 'grayscale',
				'fancybox' => false
			), $atts));
		}

		// Scroller
		$scroller = \Drone\HTML::div()
			->id('gallery-'.(++Time::$gallery_instance))
			->class('movable-container');

		if ($size == 'logo') {
			$scroller->addClass('content-size-logo');
		}

		if (\Drone\Func::stringToBool($buttons)) {
			$scroller->data('movable-container-force-touch-device', 'true');
		}

		// Scroller items
		foreach ($attachments as $attachment) {

			// Figure
			$scroller->add(' ');
			$figure = $scroller->addNew();

			// Hyperlink and image
			if ($url = $this->getAttachmentLinkURL($attachment, $link)) {
				$a = $figure->addNew('a')
					->rel($scroller->id)
					->attr(Time::getImageAttrs('a', compact('border', 'hover', 'fancybox')))
					->data('fancybox-title', $attachment->post_excerpt)
					->href($url)
					->add(wp_get_attachment_image($attachment->ID, $size));
			} else {
				$figure->addNew('div')
					->attr(Time::getImageAttrs('div', compact('border', 'hover', 'fancybox')))
					->add(wp_get_attachment_image($attachment->ID, $size));
			}

		}

		return $scroller->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * time_post_gallery_html filter for super tabs
	 *
	 * @since 1.0
	 *
	 * @param  string $_
	 * @param  array  $atts
	 * @param  array  $attachments
	 * @return string
	 */
	public function filterTimePostGalleryHTMLSuperTabs($_, $atts, $attachments)
	{

		// Attributes
		extract(shortcode_atts(array(
			'size'         => 'full-width',
			'link'         => 'post',
			'ordered'      => true,
			'descriptions' => true
		), $atts, 'slider'));

		// Size
		if ($size == 'auto') {
			$size = 'full-width';
		}

		// Super tabs
		$super_tabs = \Drone\HTML::div()
			->id('gallery-'.(++Time::$gallery_instance))
			->class('super-tabs')
			->data('super-tabs-ordered', \Drone\Func::boolToString(\Drone\Func::stringToBool($ordered)));

		// Super tabs tabs
		foreach ($attachments as $attachment) {

			// Figure
			$figure = $super_tabs->addNew('div')
				->title($attachment->post_title);

			// Hyperlink and image
			if ($url = $this->getAttachmentLinkURL($attachment, $link)) {
				$a = $figure->addNew('a')
					->rel($super_tabs->id)
					->href($url)
					->add(wp_get_attachment_image($attachment->ID, $size));
			} else {
				$figure->add(wp_get_attachment_image($attachment->ID, $size));
			}

			// Description
			if (\Drone\Func::stringToBool($descriptions)) {
				$figure->data('super-tabs-description', $attachment->post_excerpt);
			}

		}

		return $super_tabs->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * time_post_gallery_html filter for full screen gallery
	 *
	 * @since 1.0
	 *
	 * @param  string $_
	 * @param  array  $atts
	 * @param  array  $attachments
	 * @return string
	 */
	public function filterTimePostGalleryHTMLFullScreenGallery($_, $atts, $attachments)
	{

		// LayerSlider
		$layerslider = \Drone\HTML::div()->id('layerslider');

		// Slides
		foreach ($attachments as $attachment) {

			// Layer
			$layer = $layerslider->addNew('div')
				->class('ls-layer')
				->add(
					wp_get_attachment_image($attachment->ID, 'full-hd',      false, array('class' => 'ls-bg')),
					wp_get_attachment_image($attachment->ID, 'ls-thumbnail', false, array('class' => 'ls-tn'))
				);

		}

		return $layerslider->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * excerpt_more filter
	 *
	 * @internal filter: excerpt_more
	 * @since 1.2
	 *
	 * @param  string $more
	 * @return string
	 */
	public function filterExcerptMore($more)
	{
		return sprintf(' [&hellip;] <a href="%s" class="readmore">%s</a>', get_permalink(), Time::to('post/readmore').'<i class="icon-forward"></i>');
	}

	// -------------------------------------------------------------------------

	/**
	 * the_content_more_link filter
	 *
	 * @internal filter: the_content_more_link
	 * @since 1.0
	 *
	 * @param  string $s
	 * @return string
	 */
	public function filterTheContentMoreLink($s)
	{
		return str_replace('more-link', 'readmore', $s);
	}

	// -------------------------------------------------------------------------

	/**
	 * comment_form_defaults filter
	 *
	 * @internal filter: comment_form_defaults
	 * @since 1.0
	 *
	 * @param  array $defaults
	 * @return array
	 */
	public function filterCommentFormDefaults($defaults)
	{
		$commenter = wp_get_current_commenter();
		return array_merge($defaults, array(
			'fields'               => array(
				'author' => '<li class="col-1-3"><input class="full-width" type="text" name="author" placeholder="'.__('Name', 'time').'*" value="'.esc_attr($commenter['comment_author']).'" /></li>',
				'email'  => '<li class="col-1-3"><input class="full-width" type="text" name="email" placeholder="'.__('E-mail', 'time').' ('.__('not published', 'time').')*" value="'.esc_attr($commenter['comment_author_email']).'" /></li>',
				'url'    => '<li class="col-1-3"><input class="full-width" type="text" name="url" placeholder="'.__('Website', 'time').'" value="'.esc_attr($commenter['comment_author_url']).'" /></li>'
			),
			'comment_field'        => '<p><textarea class="full-width" name="comment" placeholder="'.__('Message', 'time').'"></textarea></p>',
			'must_log_in'          => str_replace('<p class="must-log-in">', '<p class="must-log-in small">', $defaults['must_log_in']),
			'logged_in_as'         => str_replace('<p class="logged-in-as">', '<p class="logged-in-as small">', $defaults['logged_in_as']),
			'comment_notes_before' => '',
			'comment_notes_after'  => '',
			'id_form'              => $defaults['id_form'],
			'id_submit'            => $defaults['id_submit'],
			'title_reply'          => __('Leave a comment', 'time'),
			'title_reply_to'       => __('Leave a reply to %s', 'time'),
			'cancel_reply_link'    => __('Cancel reply', 'time'),
			'label_submit'         => __('Send &rsaquo;', 'time'),
			'format'               => 'html5'
		));
	}

	// -------------------------------------------------------------------------

	/**
	 * wp_video_extensions filter
	 *
	 * @internal filter: wp_video_extensions
	 * @since 1.0
	 *
	 * @param  array $exts
	 * @return array
	 */
	public function filterWPVideoExtensions($exts)
	{
		$exts[] = 'ogg';
		return $exts;
	}

	// -------------------------------------------------------------------------

	/**
	 * wp_audio_shortcode_library and wp_video_shortcode_library filter
	 *
	 * @internal filter: wp_audio_shortcode_library
	 * @internal filter: wp_video_shortcode_library
	 * @since 1.0
	 *
	 * @param  string $library
	 * @return string
	 */
	public function filterWPAudioVideoShortcodeLibrary($library)
	{
		return $library == 'mediaelement' ? '' : $library;
	}

	// -------------------------------------------------------------------------

	/**
	 * wp_audio_shortcode and wp_video_shortcode filter
	 *
	 * @internal filter: wp_audio_shortcode
	 * @internal filter: wp_video_shortcode
	 * @since 1.0
	 *
	 * @param  string $html
	 * @return string
	 */
	public function filterWPAudioVideoShortcode($html)
	{
		$class = preg_match('/<(audio|video) /i', $html, $m) ? strtolower($m[1]) : '';
		return "<div class=\"embed {$class}\">".preg_replace('#^<div.*?>(.+?)</div>$#i', '\1', $html).'</div>';
	}

	// -------------------------------------------------------------------------

	/**
	 * get_previous_post_where and get_next_post_where filter
	 *
	 * @internal filter: get_previous_post_where
	 * @internal filter: get_next_post_where
	 * @since 1.0
	 *
	 * @param  string $query
	 * @return string
	 */
	public function filterGetAdjacentPostWhere($query)
	{
		if (is_singular('portfolio')) {
			global $wpdb, $post;
			$query .= $wpdb->prepare(' AND p.post_parent = %d', $post->post_parent);
		}
		return $query;
	}

	// -------------------------------------------------------------------------

	/**
	 * time_headline_content filter
	 *
	 * @internal filter: time_headline_content
	 * @since 2.0
	 *
	 * @param  string $content
	 * @return string
	 */
	public function filterBBPTimeHeadlineContent($content)
	{
		if (Time::isPluginActive('bbpress') && is_bbpress() && $content == 'navigation') {
			return Time::to('nav/headline/content') != 'navigation' ? 'breadcrumbs' : '';
		}
		return $content;
	}

	// -------------------------------------------------------------------------

	/**
	 * bbp_get_breadcrumb filter
	 *
	 * @internal filter: bbp_get_breadcrumb
	 * @since 1.1
	 *
	 * @param  string $trail
	 * @param  array  $crumbs
	 * @param  array  $r
	 * @return bool
	 */
	public function filterBBPGetBreadcrumb($trail, $crumbs, $r)
	{
		return !$r['before'] || !$r['after'] ? $trail : '';
	}

	// -------------------------------------------------------------------------

	/**
	 * breadcrumb_trail filter
	 *
	 * @internal filter: breadcrumb_trail
	 * @since 1.0
	 *
	 * @param  string $breadcrumb
	 * @param  array  $args
	 * @return string
	 */
	public function filterBreadcrumbTrail($breadcrumb, $args)
	{
		return preg_replace('#</?(nav|div|span).*?>#i', '', $breadcrumb);
	}

	// -------------------------------------------------------------------------

	/**
	 * comment_form_field_comment filter for Catpcha plugin
	 *
	 * @since 2.0
	 */
	public function filterCaptchaCommentFormFieldComment($comment_field)
	{
		$captcha = \Drone\Func::functionGetOutputBuffer('cptch_comment_form_wp3');
		$captcha = preg_replace('#<br( /)?>#', '', $captcha);
		return $comment_field.$captcha;
	}

	// -------------------------------------------------------------------------

	/**
	 * time_headline_content filter
	 *
	 * @internal filter: time_headline_content
	 * @since 2.0
	 *
	 * @param  string $content
	 * @return string
	 */
	public function filterWoocommerceTimeHeadlineContent($content)
	{
		if (Time::isPluginActive('woocommerce') && is_product() && $content == 'navigation') {
			return Time::to('nav/headline/content') != 'navigation' ? 'breadcrumbs' : '';
		}
		return $content;
	}

	// -------------------------------------------------------------------------

	/**
	 * time_author_bio_display, time_social_buttons_display, time_meta_display filter
	 *
	 * @internal filter: time_author_bio_display
	 * @internal filter: time_social_buttons_display
	 * @internal filter: time_meta_display
	 * @since 2.0
	 *
	 * @param  bool $show
	 * @return bool
	 */
	public function filterWoocommerceTimeDisplay($show)
	{
		if (Time::isPluginActive('woocommerce') && (is_cart() || is_checkout() || is_account_page() || is_order_received_page())) {
			return false;
		}
		return $show;
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_enqueue_styles filter
	 *
	 * @internal filter: woocommerce_enqueue_styles
	 * @since 2.3
	 *
	 * @return boolean
	 */
	public function filterWoocommerceEnqueueStyles()
	{
		return false;
	}

	// -------------------------------------------------------------------------

	/**
	 * loop_shop_columns filter
	 *
	 * @internal filter: loop_shop_columns
	 * @since 3.5
	 *
	 * @return int
	 */
	public function filterWoocommerceLoopShopColumns()
	{
		return Time::to('woocommerce/shop/columns');
	}

	// -------------------------------------------------------------------------

	/**
	 * loop_shop_per_page filter
	 *
	 * @internal filter: loop_shop_per_page
	 * @since 2.0
	 *
	 * @return int
	 */
	public function filterWoocommerceLoopShopPerPage()
	{
		return Time::to('woocommerce/shop/per_page');
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_output_related_products_args filter
	 *
	 * @internal filter: woocommerce_output_related_products_args
	 * @since 3.2.1
	 *
	 * @param  array $args
	 * @return array
	 */
	public function filterWoocommerceOutputRelatedProductsArgs($args)
	{
		$args['posts_per_page'] = Time::to('woocommerce/related_products/total');
		$args['columns']        = Time::to('woocommerce/related_products/columns');
		return $args;
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_cross_sells_total filter
	 *
	 * @internal filter: woocommerce_cross_sells_total
	 * @since 3.2.1
	 *
	 * @param  int $posts_per_page
	 * @return int
	 */
	public function filterWoocommercCrossSellsTotal($posts_per_page)
	{
		return Time::to('woocommerce/cross_sells/total');
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_cross_sells_columns filter
	 *
	 * @internal filter: woocommerce_cross_sells_columns
	 * @since 3.2.1
	 *
	 * @param  int $columns
	 * @return int
	 */
	public function filterWoocommercCrossSellsColumns($columns)
	{
		return Time::to('woocommerce/cross_sells/columns');
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_show_page_title filter
	 *
	 * @internal filter: woocommerce_show_page_title
	 * @since 2.0
	 *
	 * @return bool
	 */
	public function filterWoocommerceShowPageTitle()
	{
		return !Time::$headline_used;
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_placeholder_img_src filter
	 *
	 * @internal filter: woocommerce_placeholder_img_src
	 * @since 2.0
	 *
	 * @return string
	 */
	public function filterWoocommercePlaceholderImgSrc()
	{
		return $this->template_uri.'/data/img/woocommerce/placeholder.jpg';
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_placeholder_img filter
	 *
	 * @internal filter: woocommerce_placeholder_img
	 * @since 2.0
	 *
	 * @param  string $html
	 * @return string
	 */
	public function filterWoocommercePlaceholderImg($html)
	{
		return str_replace('<img ', '<img data-2x="'.$this->template_uri.'/data/img/woocommerce/placeholder@2x.jpg'.'" ', $html);
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_order_button_html filter
	 *
	 * @internal filter: woocommerce_order_button_html
	 * @since 2.3
	 *
	 * @param  string $html
	 * @return string
	 */
	public function filterWoocommerceOrderButtonHTML($html)
	{
		if (preg_match('/value="(.*?)"/', $html, $matches)) {
			return '<button type="submit" class="big" name="woocommerce_checkout_place_order" id="place_order"><span>'.$matches[1].'</span><i class="icon-right-bold color-grass"></i></button>';
		} else {
			return $html;
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * woocommerce_add_to_cart_fragments filter
	 *
	 * @internal filter: woocommerce_add_to_cart_fragments
	 * @since 3.0
	 *
	 * @param  array $fragments
	 * @return array
	 */
	public function filterWoocommerceAddToCartFragments($fragments)
	{
		$fragments['nav .cart a'] = Time::getCartMenuitem();
		return $fragments;
	}

	// -------------------------------------------------------------------------

	/**
	 * Hr shortcode
	 *
	 * @internal shortcode: hr
	 * @since 1.0
	 *
	 * @return string
	 */
	public function shortcodeHr()
	{
		return '<hr />';
	}

	// -------------------------------------------------------------------------

	/**
	 * Mark shortcode
	 *
	 * @internal shortcode: mark
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeMark($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'color' => ''
		), $atts, 'mark'));
		return \Drone\HTML::mark()
			->class($color)
			->add(\Drone\Func::wpShortcodeContent($content))
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Dropcap shortcode
	 *
	 * @internal shortcode: dc
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeDropcap($atts, $content = null)
	{
		$content = \Drone\Func::wpShortcodeContent($content);
		return \Drone\HTML::make()
			->add(
				\Drone\HTML::span()->class('dropcap')->add(mb_substr($content, 0, 1)),
				mb_substr($content, 1)
			)
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Tooltip shortcode
	 *
	 * @internal shortcode: tooltip
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeTooltip($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'tag'     => 'span',
			'title'   => '',
			'gravity' => 's',
			'fade'    => false
		), $atts, 'tooltip'));
		return \Drone\HTML::make($tag)
			->class('tipsy-tooltip')
			->title($title)
			->data('tipsy-tooltip-gravity', $gravity)
			->data('tipsy-tooltip-fade', \Drone\Func::boolToString(\Drone\Func::stringToBool($fade)))
			->add(\Drone\Func::wpShortcodeContent($content)) // preg_replace('/ +/', '&nbsp;', $content)
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Font shortcode
	 *
	 * @internal shortcode: font
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeFont($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'id'    => '',
			'tag'   => 'span',
			'class' => '',
			'style' => ''
		), $atts, 'font'));
		foreach (Time::to_('font/custom')->options as $custom_font) {
			if (\Drone\Func::stringID($custom_font->property('id')) == $id) {
				$style .= $custom_font->css();
			}
		}
		return \Drone\HTML::make($tag)
			->class($class)
			->style($style)
			->add(\Drone\Func::wpShortcodeContent($content))
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Icon shortcode
	 *
	 * @internal shortcode: icon
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @param  string $code
	 * @return string
	 */
	public function shortcodeIcon($atts, $content = null, $code = '')
	{
		extract(shortcode_atts(array(
			'name'  => 'help',
			'color' => '',
			'size'  => '1.2em',
			'class' => '',
			'style' => ''
		), $atts, 'icon'));
		if (strpos($name, '/') === false) {
			$icon = \Drone\HTML::i()->class('icon-'.preg_replace('/^icon-/', '', $name));
			if ($code == 'icon') {
				$icon->style = 'margin: 0 -0.4em;';
			}
			if ($color) {
				if (\Drone\Func::cssIsColor($color)) {
					$icon->style .= "color: {$color};";
				} else if ($color == 'color' || $color == 'leading') {
					$icon->addClass('color');
				} else {
					$icon->addClass('color-'.$color);
				}
			}
			if ($size) {
				if (preg_match('/^[0-9]+$/', $size)) {
					$size .= 'em';
				}
				$icon->style .= "font-size: {$size};";
			}
		} else {
			if (!file_exists("{$this->template_dir}/data/img/icons/{$name}.png")) {
				return;
			}
			if (($is = getimagesize("{$this->template_dir}/data/img/icons/{$name}.png")) !== false) {
				list($width, $height) = $is;
			} else {
				$width = $height = 16;
			}
			$icon = \Drone\HTML::img()
				->width($width)
				->height($height)
				->class('icon')
				->alt($name)
				->src("{$this->template_uri}/data/img/icons/{$name}.png");
			if ($code == 'icon') {
				$icon->style = 'margin: 0;';
			}
			if (Time::to('general/retina')) {
				$icon->data('2x', "{$this->template_uri}/data/img/icons/{$name}@2x.png");
			}
		}
		if ($class) {
			$icon->addClass($class);
		}
		if ($style) {
			$icon->style .= $style;
		}
		return $icon->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Button shortcode
	 *
	 * @internal shortcode: button
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeButton($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'size'       => 'normal', // normal, big, huge
			'icon'       => '',
			'left_icon'  => '',
			'right_icon' => '',
			'color'      => 'color',
			'href'       => '',
			'target'     => '_self',
			'caption'    => ''
		), $atts, 'button'));
		$button = \Drone\HTML::a()->class('button');
		if ($size) {
			$button->addClass($size);
		}
		if (!$left_icon && $icon) {
			$left_icon = $icon;
		}
		if ($left_icon) {
			$button->add(Time::shortcodeIcon(array('name' => $left_icon, 'color' => $color, 'size' => '')));
		}
		if ($content) {
			$button->addNew('span')->add($content);
		}
		if ($right_icon) {
			$button->add(Time::shortcodeIcon(array('name' => $right_icon, 'color' => $color, 'size' => '')));
		}
		if ($href) {
			$button->data('button-href', $href);
			if ($target) {
				$button->data('button-target', $target);
			}
		}
		if ($caption) {
			$button = \Drone\HTML::p()
				->class('horizontal-align')
				->add(
					$button, '<br />',
					\Drone\HTML::small()->add($caption)
				);
		}
		return $button->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Quote shortcode
	 *
	 * @internal shortcode: quote
	 * @internal shortcode: blockquote
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeQuote($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'author' => '',
			'bar'    => true,
			'align'  => 'none',
			'width'  => ''
		), $atts, 'quote'));
		$content = \Drone\Func::wpShortcodeContent($content);
		if ($author) {
			$content .= " <cite>~{$author}</cite>";
		}
		$blockquote = \Drone\HTML::blockquote()
			->class("align{$align}")
			->add($content);
		if (\Drone\Func::stringToBool($bar)) {
			$blockquote->addClass('bar');
		}
		if ($width) {
			if (preg_match('/^[0-9]+$/', $width)) {
				$width .= 'px';
			}
			$blockquote->style = "width: {$width};";
		}
		return $blockquote->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * List shortcode
	 *
	 * @internal shortcode: list
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeList($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'icon'  => 'right-open',
			'color' => 'color'
		), $atts, 'list'));
		$content = \Drone\Func::wpShortcodeContent($content);
		$content = str_replace('<ul>', '<ul class="fancy alt">', $content);
		$content = str_replace('<li>', '<li>'.Time::shortcodeIcon(array('name' => $icon, 'color' => $color, 'size' => '')), $content);
		return $content;
	}

	// -------------------------------------------------------------------------

	/**
	 * Message shortcode
	 *
	 * @internal shortcode: message
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeMessage($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'color'    => '',
			'closable' => false
		), $atts, 'message'));
		$message = \Drone\HTML::p()
			->class('message')
			->data('message-closable', \Drone\Func::boolToString(\Drone\Func::stringToBool($closable)))
			->add(\Drone\Func::wpShortcodeContent($content));
		if ($color) {
			$message->addClass($color);
		}
		return $message->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Rating shortcode
	 *
	 * @internal shortcode: rating
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeRating($atts, $content = null)
	{

		// Attributes
		extract(shortcode_atts(array(
			'tag'    => 'p',
			'rate'   => 5,
			'max'    => 0,
			'author' => ''
		), $atts, 'rating'));

		// Rate, max value
		if (strpos($rate, '/') !== false) {
			list($rate, $max) = explode('/', $rate);
		}
		$rate = max((float)str_replace(',', '.', $rate), 0);
		$max  = (int)$max;

		// Content
		$content = \Drone\Func::wpShortcodeContent($content);

		// Author
		if ($author) {
			$content .= " <cite>~{$author}</cite>";
		}

		// Rating
		$rating = \Drone\HTML::make($tag)->class('rating');
		$rate += 0.25;
		while ($rate >= 0.5 || $rating->count() < $max) {
			$star = $rating->addNew('i');
			if ($rate >= 1) {
				$rate -= 1.0;
				$star->class = 'icon-rating';
			} else if ($rate >= 0.5) {
				$rate -= 0.5;
				$star->class = 'icon-rating-half';
			} else {
				$star->class = 'icon-rating-empty';
			}
		}

		// Result
		if ($content) {
			$rating->add('<br />', $content);
		}

		return $rating->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * Social buttons shortcode
	 *
	 * @internal shortcode: social_buttons
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @return string
	 */
	public function shortcodeSocialButtons($atts)
	{

		// Validation
		if (!is_singular()) {
			return '';
		}

		// Attributes
		extract(shortcode_atts(array(
			'style' => 'big',
			'media' => 'facebook, twitter, googleplus'
		), $atts, 'social_buttons'));

		$media = array_map(function($s) { return strtolower(trim($s)); }, explode(',', $media));

		if ($style == 'big') {

			// Big
			$social_buttons = \Drone\HTML::div()->class('social-buttons');
			$ul = $social_buttons->addNew('ul');
			foreach ($media as $media) {
				switch ($media) {
					case 'facebook':
						$ul->addNew('li')->add(
							Time::getPostMetaFormat('<div class="fb-like" data-href="%link%" data-send="false" data-layout="box_count" data-show-faces="false"></div>')
						);
						break;
					case 'twitter':
						$ul->addNew('li')->add(
							Time::getPostMetaFormat('<a class="twitter-share-button" href="https://twitter.com/share" data-url="%link%" data-text="%title_esc%" data-count="vertical">Tweet</a>')
						);
						break;
					case 'googleplus':
						$ul->addNew('li')->add(
							Time::getPostMetaFormat('<div class="g-plusone" data-href="%link%" data-size="tall" data-annotation="bubble"></div>')
						);
						break;
					case 'linkedin':
						$ul->addNew('li')->add(
							Time::getPostMetaFormat('<script class="inshare" type="IN/Share" data-url="%link%" data-counter="top" data-showzero="true"></script>')
						);
						break;
					case 'pinterest':
						if (has_post_thumbnail()) {
							list($thumbnail_src) = wp_get_attachment_image_src(get_post_thumbnail_id());
						} else {
							$thumbnail_src = '';
						}
						$ul->addNew('li')->add(
							sprintf('<a data-pin-config="above" href="//pinterest.com/pin/create/button/?url=%s&amp;media=%s&amp;description=%s" data-pin-do="buttonPin"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>', urlencode(get_permalink()), urlencode($thumbnail_src), urlencode(get_the_title()))
						);
						break;
				}
			}

		} else {

			// Small
			$social_buttons = \Drone\HTML::ul()->class('meta social');
			foreach ($media as $media) {
				switch ($media) {
					case 'facebook':
						$social_buttons->addNew('li')->add(
							Time::getPostMetaFormat('<div class="fb-like" data-href="%link%" data-send="false" data-layout="button_count" data-show-faces="false"></div>')
						);
						break;
					case 'twitter':
						$social_buttons->addNew('li')->add(
							Time::getPostMetaFormat('<a class="twitter-share-button" href="https://twitter.com/share" data-url="%link%" data-text="%title_esc%" data-count="horizontal">Tweet</a>')
						);
						break;
					case 'googleplus':
						$social_buttons->addNew('li')->add(
							Time::getPostMetaFormat('<div class="g-plusone" data-href="%link%" data-size="medium" data-annotation="bubble"></div>')
						);
						break;
					case 'linkedin':
						$social_buttons->addNew('li')->add(
							Time::getPostMetaFormat('<script class="inshare" type="IN/Share" data-url="%link%" data-counter="right" data-showzero="true"></script>')
						);
						break;
					case 'pinterest':
						if (has_post_thumbnail()) {
							list($thumbnail_src) = wp_get_attachment_image_src(get_post_thumbnail_id());
						} else {
							$thumbnail_src = '';
						}
						$social_buttons->addNew('li')->add(
							sprintf('<a data-pin-config="beside" href="//pinterest.com/pin/create/button/?url=%s&amp;media=%s&amp;description=%s" data-pin-do="buttonPin"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>', urlencode(get_permalink()), urlencode($thumbnail_src), urlencode(get_the_title()))
						);
						break;
				}
			}

		}

		return $social_buttons->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * Search form shortcode
	 *
	 * @internal shortcode: search
	 * @since 1.0
	 *
	 * @return string
	 */
	public function shortcodeSearch()
	{
		return get_search_form(false);
	}

	// -------------------------------------------------------------------------

	/**
	 * Contact form shortcode
	 *
	 * @internal shortcode: contact
	 * @since 1.0
	 *
	 * @return string
	 */
	public function shortcodeContact()
	{
		return Time::getContactForm('shortcode');
	}

	// -------------------------------------------------------------------------

	/**
	 * Columns shortcode
	 *
	 * @internal shortcode: columns
	 * @internal shortcode: cols
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeColumns($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'separated' => false
		), $atts, 'columns'));
		$columns = \Drone\HTML::div()
			->class('columns')
			->add(\Drone\HTML::ul()->add(\Drone\Func::wpShortcodeContent($content)));
		if (\Drone\Func::stringToBool($separated)) {
			$columns->addClass('separated');
		}
		return $columns->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Column shortcode
	 *
	 * @internal shortcode: column
	 * @internal shortcode: col
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeColumn($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'width' => '1/2'
		), $atts, 'column'));
		list($span, $total) = explode('/', $width);
		if ($total > 20 || $span > $total) {
			return;
		}
		return \Drone\HTML::li()
			->class("col-{$span}-{$total}")
			->add(\Drone\Func::wpShortcodeContent($content))
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Tabs shortcode
	 *
	 * @internal shortcode: tabs
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeTabs($atts, $content = null)
	{
		return \Drone\HTML::div()
			->class('tabs')
			->add(\Drone\Func::wpShortcodeContent($content))
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Tab shortcode
	 *
	 * @internal shortcode: tab
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeTab($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'title'  => '',
			'active' => false
		), $atts, 'tab'));
		$html = \Drone\HTML::div()
			->title($title ? $title : '&nbsp;')
			->add(\Drone\Func::wpShortcodeContent($content));
		if (\Drone\Func::stringToBool($active)) {
			$html->addClass('active');
		}
		return $html->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Toggles shortcode
	 *
	 * @internal shortcode: toggles
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeToggles($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'singular' => false
		), $atts, 'toggles'));
		return \Drone\HTML::div()
			->class('toggles')
			->data('toggles-singular', \Drone\Func::boolToString(\Drone\Func::stringToBool($singular)))
			->add(\Drone\Func::wpShortcodeContent($content))
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Toggle shortcode
	 *
	 * @internal shortcode: toggle
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeToggle($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'title'  => '',
			'active' => false
		), $atts, 'toggle'));
		return \Drone\HTML::div()
			->title($title ? $title : '&nbsp;')
			->class(\Drone\Func::stringToBool($active) ? 'active' : null)
			->add(\Drone\Func::wpShortcodeContent($content))
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Post shortcode
	 *
	 * @internal shortcode: posts
	 * @internal shortcode: post
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @return string
	 */
	public function shortcodePosts($atts)
	{

		// Attributes
		extract(shortcode_atts(array(
			'id'             => '',
			'type'           => 'post',
			'category'       => 0,
			'size'           => 'auto',
			'orderby'        => 'date',
			'order'          => 'desc',
			'count'          => 1,
			'style'          => 'gallery',
			'columns'        => 'auto',
			'title'          => true,
			'excerpt'        => false,
			'excerpt_length' => 55,
			'taxonomy'       => 'tag'
		), $atts, 'posts'));

		// Validation
		if (!in_array($orderby, array('title', 'date', 'modified', 'comment_count', 'rand', 'menu_order'))) {
			$orderby = 'date';
		}
		if (!in_array($order, array('asc', 'desc'))) {
			$order = 'desc';
		}
		if (!in_array($taxonomy, array('', 'category', 'tag'))) {
			$taxonomy = 'tag';
		}

		// Category
		$category = preg_replace('/[^,0-9]/', '', $category);
		if ($type != 'post') {
			$category = 0;
		}

		// Count
		$count = max((int)$count, 1);

		// Columns
		$columns = $columns == 'auto' ? min($count, 4) : min(max((int)$columns, 1), 10);

		// Size
		if ($size == 'auto') {
			switch ($style) {
				case 'slider': $size = 'full-width'; break;
				default:       $size = Time::getImageSize($columns);
			}
		}

		// Taxonomy
		if ($taxonomy) {
			if ($type == 'post') {
				if ($taxonomy == 'tag') {
					$taxonomy = 'post_tag';
				}
			} else if ($type == 'portfolio') {
				$taxonomy = 'portfolio-'.$taxonomy;
			} else {
				$taxonomy = '';
			}
		}

		// Post
		if ($id) {
			if (is_null($post = get_post($id))) {
				return;
			}
			$posts = array($post);
		} else {
			$posts = get_posts(array(
				'numberposts' => $count,
				'category'    => $category,
				'post_status' => 'publish',
				'post_type'   => $type,
				'orderby'     => $order == 'RAND' ? 'none' : $orderby,
				'order'       => $order
			));
			if (count($posts) == 0) {
				return;
			}
		}

		// Slider
		if ($style == 'slider') {

			$html = \Drone\HTML::div()->class('slider');
			$ul = $html->addNew('ul')->class('slides');

			foreach ($posts as $post) {

				if (!has_post_thumbnail($post->ID)) {
					continue;
				}

				$figure = $ul->addNew('li')->addNew('figure');

				// Featured image
				$figure->addNew('a')
					->attr(Time::getImageAttrs('a'))
					->href(get_permalink($post->ID))
					->add(get_the_post_thumbnail($post->ID, $size));

				// Title
				if (\Drone\Func::stringToBool($title) || \Drone\Func::stringToBool($excerpt)) {
					$caption = $figure->addNew('p')->class('caption');
					if (\Drone\Func::stringToBool($title)) {
						$caption->addNew('strong')->add($post->post_title);
					}
					if (\Drone\Func::stringToBool($excerpt)) {
						if ($caption->count() > 0) {
							$caption->addNew('br');
						}
						$caption->add(wp_trim_words(!empty($post->post_excerpt) ? $post->post_excerpt : strip_shortcodes($post->post_content), $excerpt_length, '  [&hellip;]'));
					}
				}

			}

		}

		// Gallery
		else {

			$html = \Drone\HTML::make();
			if ($columns > 1 || $count > 1) {
				$ul = $html->tag('div')->class('columns')->addNew('ul');
			}

			foreach ($posts as $post) {

				$_html = \Drone\HTML::make();

				// Featured image
				if (has_post_thumbnail($post->ID)) {
					$_html->addNew('figure')
						->class('featured full-width')
					 	->addNew('a')
							->attr(Time::getImageAttrs('a'))
							->href(get_permalink($post->ID))
							->add(get_the_post_thumbnail($post->ID, $size));
				}

				// Title
				if (\Drone\Func::stringToBool($title)) {
					$_html->addNew('h3')->addNew('a')
						->href(get_permalink($post->ID))
						->title($post->post_title)
						->add($post->post_title);
				}

				// Excerpt
				if (\Drone\Func::stringToBool($excerpt)) {
					$_html->addNew('p')->add(wp_trim_words(!empty($post->post_excerpt) ? $post->post_excerpt : strip_shortcodes($post->post_content), $excerpt_length, '  [&hellip;]'));
				}

				// Taxonomy
				if ($taxonomy) {
					$_html->add(get_the_term_list($post->ID, $taxonomy, '<p class="small alt">', ', ', '</p>'));
				}

				// HTML
				if (isset($ul)) {
					$ul->addNew('li')->class('col-1-'.$columns)->add($_html);
				} else {
					$html->add($_html);
				}

			}

		}

		// Result
		return $html->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * Portfolio shortcode
	 *
	 * @internal shortcode: portfolio
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @return string
	 */
	public function shortcodePortfolio($atts)
	{

		// Attributes
		extract(shortcode_atts(array(
			'id'         => 'auto',
			'size'       => 'auto',
			'columns'    => 'inherit',
			'filter'     => 'inherit',
			'orderby'    => 'inherit',
			'order'      => 'inherit',
			'limit'      => 'inherit',
			'pagination' => 'inherit',
			'titles'     => 'inherit',
			'excerpts'   => 'inherit',
			'taxonomies' => 'inherit'
		), $atts, 'portfolio'));

		// Default and inherited options
		$default = Time::to_('portfolio/default');

		$columns    = $columns    == 'inherit' ? $default->value('columns') : $columns;
		$filter     = $filter     == 'inherit' ? $default->value('filter') : $filter;
		$orderby    = $orderby    == 'inherit' ? $default->value('orderby') : $orderby;
		$order      = $order      == 'inherit' ? $default->value('order') : $order;
		$limit      = $limit      == 'inherit' ? ($default->value('limit/enabled') ? $default->value('limit/limit') : -1) : (int)$limit;
		$pagination = $pagination == 'inherit' ? ($default->value('limit/enabled') ? $default->value('pagination') : false) : \Drone\Func::stringToBool($pagination);
		$titles     = $titles     == 'inherit' ? $default->value('title') : \Drone\Func::stringToBool($titles);
		$excerpts   = $excerpts   == 'inherit' ? $default->value('excerpt') : \Drone\Func::stringToBool($excerpts);
		$taxonomies = $taxonomies == 'inherit' ? ($default->value('taxonomy/visible') ? $default->value('taxonomy/taxonomy') : '') : $taxonomies;

		$columns_int = (int)trim($columns, ' +');

		if ($size == 'auto') {
			$size = Time::getImageSize($columns_int);
		}

		// Validation
		if (!in_array($filter, array('', 'category', 'tag'))) {
			$filter = 'category';
		}
		if (!in_array($orderby, array('title', 'date', 'modified', 'comment_count', 'rand', 'menu_order'))) {
			$orderby = 'date';
		}
		if (!in_array($order, array('asc', 'desc'))) {
			$order = 'desc';
		}
		if (!in_array($taxonomies, array('', 'category', 'tag'))) {
			$taxonomies = 'tag';
		}

		// Childs
		$query  = new WP_Query();
		$childs = $query->query(array(
			'post_type'      => 'portfolio',
			'post_parent'    => $id == 'auto' ? get_the_ID() : (int)$id,
			'post_status'    => 'publish',
			'orderby'        => $orderby,
			'order'          => $order,
			'posts_per_page' => $limit,
			'post__not_in'   => is_single() ? array(get_the_ID()) : array(),
			'paged'          => $pagination ? max(1, get_query_var('page')) : 1
		));
		if (count($childs) == 0) {
			return '';
		}

		// Portfolio
		$portfolio = \Drone\HTML::make();

		// Bricks
		$bricks = $portfolio->addNew('div')
			->class('bricks')
			->data('bricks-columns', $columns_int)
			->data('bricks-filter', \Drone\Func::boolToString($filter));

		// Childs
		foreach ($childs as $child) {

			// Item
			$item = $bricks->addNew('div');

			// Relation
			if ($filter) {
				$terms = \Drone\Func::wpPostTermsList($child->ID, 'portfolio-'.$filter);
				if (count($terms) > 0) {
					$item->rel = implode(' ', array_map(function($t) { return str_replace(' ', '_', $t); }, $terms));
				}
			}

			// Columns +
			if (ltrim($columns, '1234567890') == '+') {
				$ul = $item->addNew('div')->class('columns')->addNew('ul');
				$item_featured = $ul->addNew('li')->class('col-2-3');
				$item_desc     = $ul->addNew('li')->class('col-1-3');
			} else {
				$item_featured = $item_desc = $item;
			}

			// Featured image
			if (has_post_thumbnail($child->ID)) {
				$item_featured->addNew('figure')
					->class('featured full-width')
				 	->addNew('a')
						->attr(Time::getImageAttrs('a'))
						->href(get_permalink($child->ID))
						->add(get_the_post_thumbnail($child->ID, $size));
			}

			// Title
			if ($titles) {
				$item_desc->addNew($columns_int == 1 ? 'h2' : 'h3')->addNew('a')
					->href(get_permalink($child->ID))
					->title($child->post_title)
					->add($child->post_title);
			}

			// Excerpt
			if ($excerpts) {
				$item_desc->addNew('p')->add(wp_trim_words(!empty($child->post_excerpt) ? $child->post_excerpt : strip_shortcodes($child->post_content), 55, '  [&hellip;]'));
			}

			// Taxonomies
			if ($taxonomies) {
				$item_desc->add(get_the_term_list($child->ID, 'portfolio-'.$taxonomies, '<p class="small alt">', ', ', '</p>'));
			}

		}

		// Paginate links
		if ($pagination) {
			$args = array(
				'current'   => max(1, get_query_var('page')),
				'total'     => $query->max_num_pages,
				'prev_next' => Time::to('site/pagination') == 'numbers_navigation',
				'prev_text' => '&lsaquo;',
				'next_text' => '&rsaquo;',
				'mid_size'  => 1
			);
			if (is_front_page()) {
				$args['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
			} else {
				$args['base']   = get_option('permalink_structure') ? trailingslashit(get_permalink()).user_trailingslashit('%#%', 'single_paged') : esc_url_raw(add_query_arg('page', '%#%', get_permalink()));
				$args['format'] = '';
			}
			if ($pagination = paginate_links($args)) {
				$pagination = preg_replace_callback(
					'/class=[\'"](prev |next )?page-numbers( current)?[\'"]()/i',
					function($m) { return 'class="button'.str_replace('current', 'active', $m[2]).'"'; },
					$pagination
				);
				$portfolio->addNew('div')
					->class('pagination')
					->add($pagination);
			}
		}

		// Result
		return $portfolio->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * Media shortcode
	 *
	 * @internal shortcode: media
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeMedia($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'device' => 'desktop',
			'tag'    => 'div'
		), $atts, 'media'));
		if (!in_array($device, array('desktop', 'mobile'))) {
			$device = 'desktop';
		}
		return \Drone\HTML::make($tag)
			->class($device.'-only')
			->add(\Drone\Func::wpShortcodeContent($content))
			->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Custom gallery shortcode
	 *
	 * @internal shortcode: bricks
	 * @internal shortcode: slider
	 * @internal shortcode: scroller
	 * @internal shortcode: super_tabs
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @param  string $tag
	 * @return string
	 */
	public function shortcodeCustomGallery($atts, $content = null, $tag = '')
	{
		$filter = array($this, 'filterTimePostGalleryHTML'.\Drone\Func::stringPascalCase($tag));
		if (!is_callable($filter)) {
			return;
		}
		add_filter('time_post_gallery_html', $filter, 10, 3);
		if ($atts) {
			$content = preg_replace_callback(
				'/\[gallery(.*?)\]/',
				create_function('$m', 'return "[gallery ".\Drone\Func::arraySerialize(array_merge(shortcode_parse_atts($m[1]), '.var_export($atts, true).'), "html")."]";'),
				$content
			);
		}
		$content = \Drone\Func::wpShortcodeContent($content);
		remove_filter('time_post_gallery_html', $filter, 10);
		return $content;
	}

	// -------------------------------------------------------------------------

	/**
	 * Content shortcode
	 *
	 * @internal shortcode: content
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeContent($atts, $content = null)
	{

		// Validation
		if (!is_page_template('custom-page.php')) {
			return $content;
		}

		// Attributes
		extract(shortcode_atts(array(
			'sidebars'       => 'inherit',
			'author_bio'     => 'inherit',
			'meta'           => 'inherit',
			'social_buttons' => 'inherit',
			'comments'       => 'inherit'
		), $atts, 'content'));

		// Sidebars
		if ($sidebars == 'inherit') {
			$sidebars = false;
		} else if (preg_match_all('/(?<=^|[-,\|]) *(#|[a-z]+) *(?=[-,\|]|$)/i', strtolower($sidebars), $matches) && count(array_keys($matches[1], '#')) == 1) {
			$sidebars = array_slice($matches[1]+array('', '', ''), 0, 3);
		} else {
			$sidebars = false;
		}

		// Content
		if (!preg_match('/\[section.*?\]/i', $content)) {
			$content = "[section]{$content}[/section]";
		}
		$content = \Drone\Func::wpShortcodeContent($content);

		// Page elements
		foreach (compact('author_bio', 'meta', 'social_buttons', 'comments') as $element => $show) {
			if ($show != 'inherit') {
				$filter = function() use ($show) { return \Drone\Func::stringToBool($show); };
				add_filter("time_{$element}_display", $filter);
			}
			if ($element == 'comments') {
				$content .= \Drone\Func::functionGetOutputBuffer('comments_template');
			} else {
				$content .= \Drone\Func::functionGetOutputBuffer('get_template_part', 'parts/'.str_replace('_', '-', $element));
			}
			if ($show != 'inherit') {
				remove_filter("time_{$element}_display", $filter);
			}
		}

		// Result
		return Time::getContent($content, $sidebars);

	}

	// -------------------------------------------------------------------------

	/**
	 * Section shortcode
	 *
	 * @internal shortcode: section
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @param  string $content
	 * @return string
	 */
	public function shortcodeSection($atts, $content = null)
	{
		if (!is_page_template('custom-page.php')) {
			return $content;
		}
		extract(shortcode_atts(array(
			'background' => ''
		), $atts, 'section'));
		$section = \Drone\HTML::section()
			->class('section')
			->add(\Drone\Func::wpShortcodeContent($content));
		if ($background) {
			if (\Drone\Func::cssIsColor($background)) {
				$section->style = "background: {$background};";
			} else {
				$section->addClass('background-'.$background);
			}
		}
		return $section->html();
	}

	// -------------------------------------------------------------------------

	/**
	 * Posts section shortcode
	 *
	 * @internal shortcode: section_posts
	 * @see http://codex.wordpress.org/Class_Reference/WP_Query
	 * @since 1.0
	 *
	 * @param  string $atts
	 * @return string
	 */
	public function shortcodeSectionPosts($atts)
	{
		if (!is_page_template('custom-page.php')) {
			return;
		}
		query_posts(array_merge(array(
			'post_type' => 'post'
		), $atts ? $atts : array()));
		global $more;
		$more = 0;
		$blog = \Drone\Func::functionGetOutputBuffer('get_template_part', 'parts/posts');
		wp_reset_query();
		return $blog;
	}

	// -------------------------------------------------------------------------

	/**
	 * Comment template callback
	 *
	 * @since 1.0
	 *
	 * @param object $comment
	 * @param array  $args
	 * @param int    $depth
	 */
	public static function callbackComment($comment, $args, $depth)
	{
		$GLOBALS['comment'] = $comment;
		require get_template_directory().'/comment.php';
	}

	// -------------------------------------------------------------------------

	/**
	 * Comment end template callback
	 *
	 * @since 1.0
	 */
	public static function callbackCommentEnd()
	{
		echo '</ul></li>';
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_contact_form_field filter
	 *
	 * @internal filter: drone_contact_form_field
	 * @since 3.3.5
	 *
	 * @param  \Drone\HTML $field
	 * @param  string      $name
	 * @param  bool        $required
	 * @param  string      $label
	 * @param  string      $context
	 * @return \Drone\HTML
	 */
	public function filterContactFormField($field, $name, $required, $label, $context)
	{
		$field = \Drone\HTML::p();
		switch ($name) {
			case 'message':
				$field->addNew('textarea')
					->class(strpos($context, 'widget') === 0 ? 'full-width' : null)
					->name($name);
				break;
			case 'captcha':
				$field->add('%s');
				break;
			default:
				if ($required) {
					$label .= '*';
				}
				$field->addNew('input')
					->type('text')
					->name($name)
					->placeholder(strtolower($label));
		}
		return $field;
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_contact_form_output filter
	 *
	 * @internal filter: drone_contact_form_output
	 * @since 3.3.5
	 *
	 * @param  \Drone\HTML $output
	 * @return \Drone\HTML
	 */
	public function filterContactFormOutput($output)
	{
		$output->addNew('p')->add(
			\Drone\HTML::input()->type('submit')->value(__('Send', 'time').'&nbsp;&rsaquo;'),
			\Drone\HTML::i()->class('icon-arrows-ccw load'),
			\Drone\HTML::span()->class('msg small')
		);
		return $output;
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_posts_list_on_setup_options action
	 *
	 * @internal action: drone_widget_posts_list_on_setup_options
	 * @since 1.0
	 *
	 * @param object $options
	 * @param object $widget
	 */
	public function actionWidgetPostsListOnSetupOptions($options, $widget)
	{
		$options->addOption('select', 'orientation', 'scrollable', __('Orientation', 'time'), '', array('options' => array(
			'vertical'   => __('Vertical', 'time'),
			'scrollable' => __('Scrollable', 'time')
		)), 'count');
		$options->addOption('boolean', 'thumbnail', true, '', '', array('caption' => __('Show thumbnail', 'time')), 'author');
		$options->deleteChild('author');
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_posts_list_widget filter
	 *
	 * @internal filter: drone_widget_posts_list_widget
	 * @since 1.0
	 *
	 * @param object $html
	 * @param object $widget
	 */
	public function filterWidgetPostsListWidget($html, $widget)
	{
		$html->addClass('posts-list');
		if ($widget->wo('orientation') == 'scrollable') {
			$html->addClass('scroller');
		}
		return $html;
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_posts_list_post filter
	 *
	 * @internal filter: drone_widget_posts_list_post
	 * @since 1.0
	 *
	 * @param object $li
	 * @param object $widget
	 * @param object $post
	 */
	public function filterWidgetPostsListPost($li, $widget, $post)
	{
		$li = \Drone\HTML::li();
		if ($widget->wo('thumbnail') && has_post_thumbnail($post->ID)) {
			$li->addNew('figure')
				->class('alignleft fixed')
				->addNew('a')
					->attr(Time::getImageAttrs('a', array('border' => true, 'hover' => '', 'fanbcybox' => false)))
					->href(get_permalink($post->ID))
					->add(get_the_post_thumbnail($post->ID, 'post-thumbnail-mini'));
		}
		$li->addNew('h3')->addNew('a')
			->href(get_permalink($post->ID))
			->title($post->post_title)
			->add(wp_trim_words($post->post_title, $widget->wo('limit')));
		if ($widget->wo('comments')) {
			$GLOBALS['post'] = $post;
			$li->addNew('p')->class('small')->add(Time::getPostMeta('comments_number'));
			wp_reset_postdata();
		}
		return $li;
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_twitter_on_setup_options action
	 *
	 * @internal action: drone_widget_twitter_on_setup_options
	 * @since 1.0
	 *
	 * @param object $options
	 * @param object $widget
	 */
	public function actionWidgetTwitterOnSetupOptions($options, $widget)
	{
		$options->addOption('select', 'orientation', 'vertical', __('Orientation', 'time'), '', array('options' => array(
			'vertical'   => __('Vertical', 'time'),
			'horizontal' => __('Horizontal', 'time'),
			'scrollable' => __('Scrollable', 'time')
		)), 'count');
		$options->addOption('boolean', 'follow_me_button', true, '', '', array('caption' => __('Add "follow me" button', 'time')), 'oauth');
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_twitter_widget filter
	 *
	 * @internal filter: drone_widget_twitter_widget
	 * @since 1.0
	 *
	 * @param object $html
	 * @param object $widget
	 */
	public function filterWidgetTwitterWidget($html, $widget)
	{
		$html = \Drone\HTML::div()->class('twitter')->add($html);
		if ($widget->wo('orientation') == 'horizontal') {
			$ul = $html->child(0);
			$class = 'col-1-'.$ul->count();
			foreach ($ul->childs() as $li) {
				$li->addClass($class);
			}
			$ul->wrap('div');
			$html->child(0)->class = 'columns';
		} else if ($widget->wo('orientation') == 'scrollable') {
			$html->child(0)->addClass('scroller');
		}
		if ($widget->wo('follow_me_button')) {
			$html->addNew('p')->addNew('a')
				->class('button')
				->href('https://twitter.com/'.$widget->wo('username'))
				->add(__('follow me on twitter', 'time').' &rsaquo;');
		}
		return $html;
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_twitter_tweet filter
	 *
	 * @internal filter: drone_widget_twitter_tweet
	 * @since 1.0
	 *
	 * @param object $li
	 * @param object $widget
	 * @param object $tweet
	 */
	public function filterWidgetTwitterTweet($li, $widget, $tweet)
	{
		$li->insert('<i class="icon-twitter"></i>')->child(3)->addClass('alt');
		return $li;
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_flickr_widget filter
	 *
	 * @internal filter: drone_widget_flickr_widget
	 * @since 1.0
	 *
	 * @param object $html
	 * @param object $widget
	 */
	public function filterWidgetFlickrWidget($html, $widget)
	{
		return \Drone\HTML::div()->class('flickr')->add($html);
	}

	// -------------------------------------------------------------------------

	/**
	 * drone_widget_flickr_photo filter
	 *
	 * @internal filter: drone_widget_flickr_photo
	 * @since 1.0
	 *
	 * @param object $li
	 * @param object $widget
	 * @param object $photo
	 */
	public function filterWidgetFlickrPhoto($li, $widget, $photo)
	{
		if (Time::to_('site/image/settings')->value('fancybox') && $widget->wo('url') == 'image') {
			$li->child(0)->addClass('fb');
		}
		return $li;
	}

	// -------------------------------------------------------------------------

	/**
	 * Plugin active
	 *
	 * @since 3.0.1
	 *
	 * @param  string|array $name,...
	 * @return boolean
	 */
	public static function isPluginActive($name)
	{
		if (func_num_args() > 1) {
			$name = func_get_args();
		}
		if ($name === 'layerslider') {
			return defined('LS_TIME_EDITION') && defined('LS_PLUGIN_VERSION') && LS_TIME_EDITION && version_compare(LS_PLUGIN_VERSION, Time::LAYERSLIDER_REQUIRED_VERSION) >= 0;
		} else {
			return parent::isPluginActive($name);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Get main content layer
	 *
	 * @since 1.0
	 *
	 * @param  string     $content
	 * @param  array|bool $sidebars
	 * @return string
	 */
	public static function getContent($content, $sidebars = false)
	{

		// Layers
		$container = \Drone\HTML::div()->class('container');
		$main      = $container->addNew('div')->class('main')->add($content);

		// Layout
		if ($sidebars === false || !is_array($sidebars)) {
			$sidebars = Time::io('layout/sidebar/sidebar', 'sidebar/layout', '__hidden_ns');
		}
		$sidebars = apply_filters('time_layout', $sidebars);

		// Sidebars
		$pad  = array('left' => 0, 'right' => 0);
		$side = 'left';

		foreach ($sidebars as $sidebar) {

			if ($sidebar == '#') {

				$side = 'right';

			} else if ($sidebar) {

				$sidebar = apply_filters('time_sidebar', $sidebar, 'aside');

				if (is_null($width = Time::to_('sidebar/width/'.$sidebar))) {
					$width = new stdClass();
					$width->value = Time::DEFAULT_SIDEBAR_WIDTH;
					$widgets = \Drone\HTML::section()
						->class('section')
						->add(sprintf(__('The "%s" sidebar does not exist.', 'time'), $sidebar))
						->html();
				} else {
					$widgets = \Drone\Func::functionGetOutputBuffer('dynamic_sidebar', $sidebar);
				}
				$pad[$side] += $side == 'right' ? $width->value : Time::DEFAULT_SIDEBAR_WIDTH;

				$aside = \Drone\HTML::aside()
					->addClass('aside', $side == 'left' ? 'alpha' : 'beta')
					->add($widgets);
				if ($side == 'right') {
					$aside->style = "width: {$width->value}px;";
				}

				if ($side == 'left' && $sidebars[0] && $sidebars[1] == '#' && $sidebars[2]) { // left-content-right
					$container->insert($aside);
				} else if ($side == 'right' && $sidebars[0] == '#') { // content-right-right
					$container->insert($aside, 1);
				} else {
					$container->add($aside);
				}

			}

		}

		$main->addClass($pad['right'] ? 'alpha' : ($pad['left'] ? 'beta' : ''));
		$main->style = sprintf('padding: 0 %2$dpx 0 %1$dpx; margin: 0 -%2$dpx 0 -%1$dpx;', $pad['left'], $pad['right']);

		// Content width
		global $content_width;
		$content_width = apply_filters('time_content_width', Time::to('general/max_width') - array_sum($pad));

		// Content
		return \Drone\HTML::div()->class('content')->add($container)->html();

	}

	// -------------------------------------------------------------------------

	/**
	 * Open main content layer
	 *
	 * @since 1.0
	 */
	public static function openContent()
	{
		ob_start();
	}

	// -------------------------------------------------------------------------

	/**
	 * Close main content layer
	 *
	 * @since 1.0
	 *
	 * @param array|bool $layout
	 */
	public static function closeContent($layout = false)
	{
		echo Time::getContent(ob_get_clean(), $layout);
	}

	// -------------------------------------------------------------------------

	/**
	 * LayerSlider source code
	 *
	 * @since 1.0
	 *
	 * @param  int         $id
	 * @return string|bool
	 */
	public static function getLayerSlider($id)
	{
		return empty($id) ? false : layerslider_init(array('id' => $id));
	}

	// -------------------------------------------------------------------------

	/**
	 * LayerSlider JSON options
	 *
	 * @since 1.0
	 *
	 * @return string
	 */
	public static function getLayerSliderOptions()
	{

		// Full screen gallery
		if (is_page_template('full-screen-gallery.php')) {
			return sprintf("{skin: 'time-%s', autoStart: false, autoPlayVideos: false, navStartStop: false, navButtons: true, thumbnailNavigation: 'hover', tnHeight: 63}", Time::to('general/scheme'));
		}

		// Banner
		$banner = Time::io_('layout/banner/banner', 'banner/content', '__hidden_ns');
		if ($banner->property('type') != 'slider') {
			return '{}';
		}

		// Slider code
		if (($layerslider = Time::getLayerSlider($banner->property('slider'))) === false) {
			return '{}';
		}

		// Getting options
		if (preg_match('/lsjQuery\("#layerslider_[0-9]+"\)\.layerSlider\((\{.*?\})\);/is', $layerslider, $matches)) {
			$options = $matches[1];
		} else {
			return '{}';
		}

		// Post processing
		$options = preg_replace("/skin ?: ?'[-a-z0-9]+',/i", "skin : 'time-".Time::to('general/scheme')."',", $options);

		// Result
		return $options;

	}

	// -------------------------------------------------------------------------

	/**
	 * LayerSlider slider
	 *
	 * @since 1.0
	 *
	 * @param int $id
	 */
	public static function layerSliderSlider($id)
	{

		// Source
		if (($layerslider = Time::getLayerSlider($id)) === false) {
			return;
		}

		// Script tags
		$layerslider = preg_replace('#<script.*>.*</script>#isU', '', $layerslider);

		// ID
		$layerslider = preg_replace(
			'/<div +id="layerslider_[0-9]+" +class="(.*)" +style="width: ([0-9]+)px; height: ([0-9]+)px;.*">/iU',
			'<div id="layerslider" class="\1" style="width: \2px; height: \3px">',
			$layerslider
		);

		// Layers
		$layerslider = preg_replace_callback(
			'#<div +class="ls-layer" +style="(.*)">(.*)</div>(?=<div +class="ls-layer"|</div>$)#isU',
			'Time::layerSliderSliderCallback',
			$layerslider
		);

		// Result
		echo $layerslider;

	}

	// -------------------------------------------------------------------------

	/**
	 * LayerSlider slider helper function
	 *
	 * @since 1.0
	 *
	 * @param  array  $matches
	 * @return string
	 */
	protected static function layerSliderSliderCallback($matches)
	{

		$style   = $matches[1];
		$content = $matches[2];

		// Delay out
		$delayout = 0;
		if (preg_match_all('/style="[^"]*durationout ?: ?([0-9]+);[^"]*delayout ?: ?([0-9]+);[^"]*"/i', $content, $m, PREG_SET_ORDER) > 0) {
			foreach ($m as $_m) {
				$delayout = max($delayout, (int)$_m[1]+(int)$_m[2]);
			}
		}

		// Style
		$style = preg_replace('/(^| )(slide(out)?direction|((duration|easing)(in|out))|transition[23]d) ?: ?[,0-9a-z]+;/i', '', $style);
		$style = preg_replace('/delayout ?: ?[0-9]+;/', "delayout: {$delayout};", $style);

		// Background
		if (preg_match('#<img +src="(.+)" +class="ls-bg".*>#iU', $content, $m)) {
			if (($id = \Drone\Func::wpGetAttachmentID($m[1])) !== false) {
				$bg =
					wp_get_attachment_image($id, 'full-hd',      false, array('class' => 'ls-bg')).
					wp_get_attachment_image($id, 'ls-thumbnail', false, array('class' => 'ls-tn'));
				$content = preg_replace('#<img.*class="ls-bg".*>#iU', $bg, $content);
			}
		}

		// Button
		$content = preg_replace('#<div +class="(.*)" +style="(.*)">\s*<a(.*)class="(button.*)"(.*)>(.+)</a>\s*</div>#iU', '<button class="\1 \4" style="\2"\3\5>\6</button>', $content);

		// Result
		return sprintf('<div class="ls-layer" style="%s">%s</div>', trim($style), trim($content));

	}

	// -------------------------------------------------------------------------

	/**
	 * Footer layout classes
	 *
	 * @since 1.0
	 *
	 * @return array
	 */
	public static function getFooterLayoutClasses()
	{

		$layout = Time::to('footer/layout/layout');

		if ($layout == 'disabled') {
			return array();
		} else if ($layout == 'custom') {
			return array_map(
				function($s) { return 'col-'.str_replace('/', '-', $s); },
				explode('+', str_replace(' ', '', Time::to('footer/layout/custom')))
			);
		} else {
			return array_map(
				function($s) { return sprintf('col-%d-%d', $s{0}, $s{1}); },
				explode('_', $layout)
			);
		}

	}

	// -------------------------------------------------------------------------

	/**
	 * Get image size
	 *
	 * @since 1.0
	 *
	 * @param  int    $columns
	 * @return string
	 */
	public static function getImageSize($columns)
	{
		if ($columns >= 4) {
			return 'small-width';
		} else if ($columns >= 2) {
			return 'medium-width';
		} else {
			return 'full-width';
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Get image attributes
	 *
	 * @since 1.0
	 *
	 * @param  string       $tag
	 * @param  array        $atts
	 * @return array|string
	 */
	public static function getImageAttrs($tag, $atts = array())
	{

		// Image settings
		$settings = Time::to_('site/image/settings');

		// Attributes
		extract(array_merge($defaults = array(
			'border'   => $settings->value('border'),
			'hover'    => $settings->value('hover') ? 'zoom' : '',
			'fancybox' => $settings->value('fancybox')
		), $atts));

		// Border
		$border = $border === 'inherit' ? $defaults['border'] : \Drone\Func::stringToBool($border);

		// Hover
		if ($hover === 'inherit' || !in_array($hover, array('', 'zoom', 'image', 'grayscale'), true)) {
			$hover = $defaults['hover'];
		}

		// Fancybox
		$fancybox = $fancybox === 'inherit' ? $defaults['fancybox'] : \Drone\Func::stringToBool($fancybox);

		// Properties
		$attrs = array('class' => array());

		if ($border) {
			$attrs['class'][] = 'inset-border';
		}
		if ($tag == 'a') {
			if ($hover) {
				$attrs['class'][] = $hover.'-hover';
			}
			if ($fancybox) {
				$attrs['class'][] = 'fb';
			}
		}

		$attrs['class'] = implode(' ', $attrs['class']);

		// Output
		return $attrs;

	}

	// -------------------------------------------------------------------------

	/**
	 * Image attributes
	 *
	 * @since 1.0
	 *
	 * @param string $tag
	 * @param array  $atts
	 */
	public static function imageAttrs($tag, $atts = array())
	{
		echo \Drone\Func::arraySerialize(Time::getImageAttrs($tag, $atts), 'html');
	}

	// -------------------------------------------------------------------------

	/**
	 * Post format icon
	 *
	 * @since 1.0
	 *
	 * @return string
	 */
	public static function getPostIcon()
	{
		if (!Time::to('post/hide_icons') && ($post_format = get_post_format()) && isset(Time::$post_formats_icons[$post_format])) {
			return \Drone\HTML::i()->class('icon-'.Time::$post_formats_icons[$post_format])->html();
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Navigation menu
	 *
	 * @since 1.0
	 *
	 * @param string $theme_location
	 * @param int    $menu
	 * @param int    $depth
	 */
	public static function navMenu($theme_location, $menu = null, $depth = 0)
	{
		echo wp_nav_menu(array(
			'theme_location' => $theme_location,
			'menu'           => apply_filters('time_menu', $menu, $theme_location),
			'depth'          => $depth,
			'container'      => '',
			'menu_id'        => '',
			'menu_class'     => '',
			'echo'           => false,
			'fallback_cb'    => function() use ($theme_location, $depth) {
				return '<ul>'.wp_list_pages(array('theme_location' => $theme_location, 'title_li' => '', 'depth' => $depth, 'echo' => false)).'</ul>';
			}
		));
	}

	// -------------------------------------------------------------------------

	/**
	 * Get thumbnail caption for WooCommerce product image
	 *
	 * @since 2.0
	 *
	 * @param  int|object $thumbnail
	 * @return string
	 */
	public static function woocommerceGetThumbnailCaption($thumbnail)
	{
		if (!Time::to('woocommerce/product/captions')) {
			return '';
		}
		if (!is_object($thumbnail)) {
			$thumbnail = get_post($thumbnail);
		}
		switch (Time::to('woocommerce/product/captions')) {
			case 'title':
				return trim($thumbnail->post_title);
			case 'caption':
				return trim($thumbnail->post_excerpt);
			case 'caption_title':
				$caption = trim($thumbnail->post_excerpt) or $caption = trim($thumbnail->post_title);
				return $caption;
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Parse/fix WooCommerce widget list
	 *
	 * @since 2.0
	 *
	 * @param  string $s
	 * @return string
	 */
	public static function woocommerceWidgetParseList($s)
	{
		return preg_replace('#<ul class="([^"]*product_list_widget[^"]*)">#i', '<ul class="\1 posts-list">', $s);
	}

	// -------------------------------------------------------------------------

	/**
	 * Parse/fix WooCommerce widget navigation
	 *
	 * @since 2.0
	 *
	 * @param  string $s
	 * @return string
	 */
	public static function woocommerceWidgetparseNav($s)
	{
		$s = preg_replace('#<ul[^<>]*>.*</ul>#is', '<nav class="aside">\0</nav>', $s);
		$s = preg_replace('#(<a href="[^"]*">)([^<>]*)(</a>)\s*<(span|small) class="count">\(?([0-9]+)\)?</\4>#i', '\1\2 <small>(\5)</small>\3', $s);
		return $s;
	}

}

// -----------------------------------------------------------------------------

/**
 * is_category() with posts inside
 *
 * @since 2.5
 * @deprecated 3.3
 *
 * @param  mixed $id
 * @return bool
 */
function is_time_category_or_term($category)
{
	return is_singular() ? has_term($category, 'category') : is_category($category);
}

// -----------------------------------------------------------------------------

Time::getInstance();