<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */

$nav = Time::io('layout/nav_secondary/upper', 'nav/secondary/upper', '__default_ns');
if (!(\Drone\Func::wpAssignedMenu('secondary-upper') || is_numeric($nav)) || !apply_filters('time_nav_secondary_upper_display', (bool)$nav)) {
	return;
}

?>

<nav class="secondary">
	<div class="container">
		<?php Time::navMenu('secondary-upper', is_numeric($nav) ? $nav : null);

		 ?>
	</div>
</nav>
