<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */

$banner = Time::io_('layout/banner/banner', 'banner/content', '__hidden_ns');

?>

<?php if ($banner->property('type') == 'empty' && $banner->property('height')): // Empty ?>

	<div class="under-container banner-empty" style="position: relative; height: <?php echo $banner->property('height'); ?>px;">
		<?php get_template_part('parts/background', 'open'); ?>
	</div>

<?php elseif ($banner->property('type') == 'image' && $banner->property('image')): // Image ?>

	<div class="outer-container banner-image transparent">
		<?php get_template_part('parts/background', 'open'); ?>
		<div>
			<figure class="full-width">
				<?php echo wp_get_attachment_image($banner->property('image'), Time::to('general/layout') == 'boxed' ? 'banner' : 'full-hd'); ?>
			</figure>
		</div>
	</div>

<?php elseif ($banner->property('type') == 'thumbnail' && has_post_thumbnail()): // Featured image ?>

	<div class="outer-container banner-thumbnail transparent">
		<?php get_template_part('parts/background', 'open'); ?>
		<div>
			<figure class="full-width">
				<?php the_post_thumbnail(Time::to('general/layout') == 'boxed' ? 'banner' : 'full-hd'); ?>
			</figure>
		</div>
	</div>

<?php elseif ($banner->property('type') == 'slider'): // Slider ?>

	<div class="under-container banner-slider">
		<?php get_template_part('parts/background', 'open'); ?>
		<div class="container">
			<?php Time::layerSliderSlider($banner->property('slider')); ?>
		</div>
	</div>

<?php elseif ($banner->property('type') == 'custom'): // Custom ?>

	<div class="outer-container banner-custom transparent">
		<?php get_template_part('parts/background', 'open'); ?>
		<div class="container">
			<?php echo \Drone\Func::wpProcessContent($banner->property('custom')); ?>
		</div>
	</div>

<?php endif; ?>