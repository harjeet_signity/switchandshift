<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */

if (!is_null($background = Time::io_('layout/background/background', 'general/background/background', '__hidden_ns', '__hidden'))) {
	echo $background->background()->html();
}
