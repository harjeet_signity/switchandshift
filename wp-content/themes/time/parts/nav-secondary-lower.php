<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */

$nav = Time::io('layout/nav_secondary/lower', 'nav/secondary/lower', '__default_ns');
if (!(\Drone\Func::wpAssignedMenu('secondary-lower') || is_numeric($nav)) || !apply_filters('time_nav_secondary_lower_display', (bool)$nav)) {
	return;
}

?>

<nav class="secondary">
	<div class="container">
		<?php Time::navMenu('secondary-lower', is_numeric($nav) ? $nav : null); ?>
	</div>
</nav>