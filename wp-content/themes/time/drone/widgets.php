<?php










namespace Drone\Widgets;
use Drone\Func;
use Drone\HTML;
use Drone\Options;
use Drone\Theme;









const INC_FILENAME = 'widgets.php';











class Widget extends \WP_Widget
{

	

	





	const LABEL_SEPARATOR = '|';

	

	




	private $_options;

	




	private $_domain;

	




	private $_id;

	

	






	protected function onSetupOptions(\Drone\Options\Group\Widget $options) { }

	

	







	public function onOptionsCompatybility(array &$data, $version) { }

	

	







	protected function onWidget(array $args, \Drone\HTML &$html) { }

	

	







	protected function getOptions($data = null)
	{

		
		$options = new Options\Group\Widget(str_replace('[#]', '', $this->get_field_name('#')));
		$this->onSetupOptions($options);
		\Drone\do_action('widget_'.str_replace('-', '_', $this->_id).'_on_setup_options', $options, $this);

		
		if (is_int($data)) {
			$settings = $this->get_settings();
			if (isset($settings[$data])) {
				$data = $settings[$data];
			}
		}
		if (is_array($data)) {
			$options->fromArray($data, array($this, 'onOptionsCompatybility'));
		}

		return $options;

	}

	

	




















	public function __construct($label, $description = '', $class = '', $width = null)
	{

		
		$this->_domain = Theme::getInstance()->domain;
		$this->_id     = Func::stringID(
			str_replace(
				array(__CLASS__, Theme::getInstance()->class.'\Widgets\Widget', Theme::getInstance()->class.'Widget', Theme::getInstance()->class),
				'', get_class($this)
			)
		);

		
		parent::__construct(
			\Drone\apply_filters('widget_id_base', Theme::getInstance()->theme->id.'-'.$this->_id, $this->_id),
			Theme::getInstance()->theme->name.' '.self::LABEL_SEPARATOR.' '.$label,
			array('description' => $description, 'classname' => $class ? $class : 'widget-'.$this->_id),
			array('width' => $width)
		);

	}

	

	







	public function __get($name)
	{
		switch ($name) {
			case '_domain':
			case '_id':
				return $this->{$name};
		}
	}

	

	








	public function getTransientName($name)
	{
		return Theme::getInstance()->getTransientName("{$this->_id}_{$this->number}_{$name}");
	}

	

	








	public function wo_($name, $skip_if = null)
	{
		return $this->_options->findChild($name, $skip_if);
	}

	

	









	public function wo($name, $skip_if = null, $fallback = null)
	{
		$child = $this->wo_($name, $skip_if);
		return $child !== null && $child->isOption() ? $child->value : $fallback;
	}

	

	





	public function form($instance)
	{
		echo HTML::div()
			->class('drone-widget-options')
			->add($this->getOptions($instance)->html())
			->html();
	}

	

	





	public function update($new_instance, $old_instance)
	{
		$options = $this->getOptions($old_instance);
		$options->change($new_instance);
		return $options->toArray();
	}

	

	




	public function widget($args, $instance)
	{

		
		$this->_options = $this->getOptions($instance);

		
		$html = HTML::make();
		$this->onWidget((array)$args, $html);

		if (!$html->tag && $html->count() == 0) {
			return;
		}

		
		$html = \Drone\apply_filters('widget_'.str_replace('-', '_', $this->_id).'_widget', $html, $this, $args);

		
		$output = HTML::make()->add($args['before_widget']);
		if ($title = $this->wo('title')) {
			$output->add(
				$args['before_title'],
				\apply_filters('widget_title', $title, $instance, $this->id_base),
				$args['after_title']
			);
		}
		$output->add($html, $args['after_widget']);

		
		$output = \Drone\apply_filters('widget_'.str_replace('-', '_', $this->_id).'_output', $output, $this, $args);

		
		echo $output->html();

	}

}



namespace Drone\Widgets\Widget;
use Drone\Widgets\Widget;
use Drone\Func;
use Drone\HTML;
use Drone\Theme;








class UnwrappedText extends Widget
{

	

	





	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', '', __('Title', $this->_domain));
		$options->addOption('code', 'text', '', __('Text', $this->_domain), '', array('on_html' => function($option, &$html) {
			$html->css('height', '25em');
		}));
		$options->addOption('boolean', 'shortcodes', false, '', '', array('caption' => __('Allow shortcodes', $this->_domain)));
	}

	

	





	protected function onWidget(array $args, \Drone\HTML &$html)
	{
		if ($this->wo('shortcodes')) {
			$html->add(do_shortcode($this->wo_('text')->__()));
		} else {
			$html->add($this->wo_('text')->__());
		}
	}

	

	





	public function __construct()
	{
		parent::__construct(__('Unwrapped text', Theme::getInstance()->domain), __('For pure HTML code.', Theme::getInstance()->domain), '', 600);
	}

}








class Page extends Widget
{

	

	





	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', '', __('Title', $this->_domain));
		$options->addOption('post', 'id', 0, __('Page', $this->_domain), '', array('required' => false, 'options' => function() {
			return Func::wpPagesList();
		}));
	}

	

	





	public function onOptionsCompatybility(array &$data, $version)
	{
		if (version_compare($version, '5.2.6') < 0) {
			if (isset($data['page'])) {
				$data['id'] = $data['page'];
			}
		}
	}

	

	





	protected function onWidget(array $args, \Drone\HTML &$html)
	{
		if (function_exists('is_bbpress') && is_bbpress()) { 
			bbp_restore_all_filters('the_content');
		}
		$html->add($this->wo_('id')->getContent());
	}

	

	





	public function __construct()
	{
		parent::__construct(__('Page', Theme::getInstance()->domain), __('Displays content of a specified page.', Theme::getInstance()->domain));
	}

}








class Contact extends Widget
{

	

	





	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', '', __('Title', $this->_domain));
		$options->addOption('memo', 'description', '', __('Description', $this->_domain));
	}

	

	





	protected function onWidget(array $args, \Drone\HTML &$html)
	{
		if ($this->wo('description')) {
			$html->addNew('p')->add($this->wo_('description')->__());
		}
		$html->add(Theme::getContactForm('widget-'.$args['id']));
	}

	

	





	public function __construct()
	{
		parent::__construct(__('Contact form', Theme::getInstance()->domain), __('Displays contact form, which can be configured in Theme Options.', Theme::getInstance()->domain));
	}

}








class PostsList extends Widget
{

	

	





	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$_this = $this; 
		$options->addOption('text', 'title', '', __('Title', $this->_domain));
		$options->addOption('select', 'category', 0, __('Category', $this->_domain), '', array('options' => function() use ($_this) {
			return array(
				0         => '('.__('All', $_this->_domain).')',
				'current' => '('.__('Current', $_this->_domain).')'
			) + Func::wpTermsList('category', array('hide_empty' => false));
		}));
		$options->addOption('select', 'orderby', 'date', __('Sort by', $this->_domain), '', array('options' => array(
			'title'         => __('Title', $this->_domain),
			'date'          => __('Date', $this->_domain),
			'modified'      => __('Modified date', $this->_domain),
			'comment_count' => __('Comment count', $this->_domain),
			'rand'          => __('Random order', $this->_domain)
		)));
		$options->addOption('select', 'order', 'desc', __('Sort order', $this->_domain), '', array('options' => array(
			'asc'  => __('Ascending', $this->_domain),
			'desc' => __('Descending', $this->_domain)
		)));
		$options->addOption('number', 'count', 5, __('Posts count', $this->_domain), '', array('min' => 1, 'max' => 50));
		$options->addOption('number', 'limit', 10, __('Post title words limit', $this->_domain), '', array('min' => 1));
		$options->addOption('boolean', 'author', false, '', '', array('caption' => __('Show post author', $this->_domain)));
		$options->addOption('boolean', 'comments', false, '', '', array('caption' => __('Show comments count', $this->_domain)));
		$options->addOption('boolean', 'exclude_previous', false, '', '', array('caption' => __('Exclude already displayed posts', $this->_domain)));
	}

	

	





	protected function onWidget(array $args, \Drone\HTML &$html)
	{

		
		$exclude = is_single() ? array(get_the_ID()) : array();
		if ($this->wo('exclude_previous')) {
			$exclude = array_merge($exclude, Theme::getInstance()->posts_stack);
		}

		
		$posts = get_posts(array(
			'category'         => (string)$this->wo('category') == 'current' ? Func::wpGetCurrentTermID('category') : $this->wo('category'),
			'numberposts'      => $this->wo('count'),
			'orderby'          => $this->wo('orderby'),
			'order'            => strtoupper($this->wo('order')),
			'exclude'          => array_unique($exclude),
			'suppress_filters' => false
		));
		if (count($posts) == 0) {
			return;
		}

		
		$html = HTML::ul();
		foreach ($posts as $post) {
			$li = HTML::li(
				HTML::a()
					->href(\apply_filters('the_permalink', get_permalink($post->ID)))
					->title($post->post_title)
					->add(wp_trim_words($post->post_title, $this->wo('limit')))
			);
			if ($this->wo('author')) {
				$author = get_userdata($post->post_author);
				$li->add(
					'&nbsp;',
					sprintf(__('by %s', $this->_domain), HTML::a()->href(get_author_posts_url($post->post_author))->title($author->display_name)->add($author->display_name)->html())
				);
			}
			if ($this->wo('comments')) {
				$li->add(" ({$post->comment_count})");
			}
			$li = \Drone\apply_filters('widget_'.str_replace('-', '_', $this->_id).'_post', $li, $this, $post);
			$html->add($li);
		}

	}

	

	





	public function __construct()
	{
		parent::__construct(__('Posts list', Theme::getInstance()->domain), __('Displays list of posts by specific criteria (e.g.: newest posts, most commented, random posts, etc.).', Theme::getInstance()->domain));
	}

}








class Twitter extends Widget
{

	

	





	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', '',  __('Title', $this->_domain));
		$options->addOption('codeline', 'username', '', __('Username', $this->_domain), '', array(
			'on_sanitize' => function($option, $original_value, &$value) {
				if (preg_match('|^((https?://)?(www\.)?twitter\.com/(#!/)?)?(?P<username>.+?)/?$|i', $value, $matches)) {
					$value = $matches['username'];
				}
			}
		));
		$options->addOption('number', 'count', 5, __('Tweets count', $this->_domain), '', array('min' => 1, 'max' => 20));
		$options->addOption('interval', 'interval', array('quantity' => 30, 'unit' => 'm'), __('Update interval', $this->_domain), __('Tweets receiving interval.', $this->_domain), array('min' => '1m'));
		$options->addOption('boolean', 'include_retweets', true, '', '', array('caption' => __('Include retweets', $this->_domain)));
		$options->addOption('boolean', 'exclude_replies', false, '', '', array('caption' => __('Exclude replies', $this->_domain)));
		$oauth = $options->addGroup('oauth');
			$oauth->addOption('codeline', 'consumer_key', '', __('API key', $this->_domain));
			$oauth->addOption('codeline', 'consumer_secret', '', __('API secret', $this->_domain), '', array('password' => true));
			$oauth->addOption('codeline', 'access_token', '', __('Access token', $this->_domain));
			$oauth->addOption('codeline', 'access_token_secret', '', __('Access token secret', $this->_domain), '', array('password' => true));
	}

	

	





	protected function onWidget(array $args, \Drone\HTML &$html)
	{

		
		if (!$this->wo('username')) {
			return;
		}

		
		$options = array(
			'username'         => $this->wo('username'),
			'count'            => $this->wo('count'),
			'interval'         => $this->wo_('interval')->seconds(),
			'include_retweets' => $this->wo('include_retweets'),
			'exclude_replies'  => $this->wo('exclude_replies'),
			'oauth'            => $this->wo_('oauth')->toArray()
		);

		$tweets = Theme::getInstance()->getTransient(
			'twitter_'.crc32(serialize($options)),
			function(&$expiration, $outdated_value) use ($options) {
				$expiration = $options['interval'];
				$value = Func::twitterGetTweets(
					$options['oauth'],
					$options['username'],
					$options['include_retweets'],
					$options['exclude_replies'],
					$options['count']
				);
				if ($value === false) {
					return $outdated_value;
				}
				return $value;
			}
		);

		if ($tweets === false) {
			return;
		}

		
		$html = HTML::ul();
		foreach ($tweets as $tweet) {
			$li = HTML::li(
				$tweet['html'],
				HTML::br(),
				HTML::small(
					HTML::a()
						->href($tweet['url'])
						->add(sprintf(__('%s ago', $this->_domain), human_time_diff($tweet['date'])))
				)
			);
			$li = \Drone\apply_filters('widget_'.str_replace('-', '_', $this->_id).'_tweet', $li, $this, $tweet);
			$html->add($li);
		}

	}

	

	





	protected function getOptions($data = null)
	{
		$options = parent::getOptions($data);
		if ($data !== null && $options->isDefault()) {
			foreach ($this->get_settings() as $settings) {
				if (isset($settings['oauth']['consumer_key'])        && $settings['oauth']['consumer_key'] &&
					isset($settings['oauth']['consumer_secret'])     && $settings['oauth']['consumer_secret'] &&
					isset($settings['oauth']['access_token'])        && $settings['oauth']['access_token'] &&
					isset($settings['oauth']['access_token_secret']) && $settings['oauth']['access_token_secret']) {
					$options->child('oauth')->fromArray($settings['oauth']);
					break;
				}
			}
		}
		return $options;
	}

	

	





	public function __construct()
	{
		parent::__construct(__('Twitter', Theme::getInstance()->domain), __('Twitter stream.', Theme::getInstance()->domain));
	}

}








class Flickr extends Widget
{

	

	





	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', '',  __('Title', $this->_domain));
		$options->addOption('codeline', 'username', '', __('Username', $this->_domain), __('Screen name from Flickr account settings.', $this->_domain));
		$options->addOption('number', 'count', 4, __('Photos count', $this->_domain), '', array('min' => 1, 'max' => 50));
		$options->addOption('interval', 'interval', array('quantity' => 30, 'unit' => 'm'), __('Update interval', $this->_domain), __('Photos receiving interval.', $this->_domain), array('min' => '1m'));
		$options->addOption('select', 'url', 'image', 'Action after clickng on a photo', '', array('options' => array(
			'flickr' => __('Open Flickr page with the photo', $this->_domain),
			'image'  => __('Open bigger version of the photo', $this->_domain)
		)));
		$options->addOption('codeline', 'api_key', '', __('API Key', $this->_domain), __('Optional (use only if you want to use your key).', $this->_domain));
	}

	

	





	protected function onWidget(array $args, \Drone\HTML &$html)
	{

		
		if (!$this->wo('username')) {
			return;
		}

		
		$options = array(
			'username' => $this->wo('username'),
			'count'    => $this->wo('count'),
			'interval' => $this->wo_('interval')->seconds(),
			'api_key'  => $this->wo('api_key', '__empty', Func::FLICKR_API_KEY)
		);

		$photos = Theme::getInstance()->getTransient(
			'flickr_'.crc32(serialize($options)),
			function(&$expiration, $outdated_value) use ($options) {
				$expiration = $options['interval'];
				if (
					($userdata = Func::flickrGetUserdata($options['api_key'], $options['username'])) === false ||
					($value = Func::flickrGetPhotos($options['api_key'], $userdata['id'], $options['count'])) === false
				) {
					return $outdated_value;
				}
				return $value;
			}
		);

		if ($photos === false) {
			return;
		}

		
		$html = HTML::ul();
		foreach ($photos as $photo) {
			$li = HTML::li();
			$li->addNew('a')
				->href($this->wo('url') == 'flickr' ? $photo['url'] : sprintf($photo['src'], 'b'))
				->title($photo['title'])
				->rel($this->id)
				->addNew('img')
					->src(sprintf($photo['src'], 's'))
					->alt($photo['title'])
					->width(75)
					->height(75);
			$li = \Drone\apply_filters('widget_'.str_replace('-', '_', $this->_id).'_photo', $li, $this, $photo);
			$html->add($li);
		}

	}

	

	





	public function __construct()
	{
		parent::__construct(__('Flickr', Theme::getInstance()->domain), __('Flickr photo stream.', Theme::getInstance()->domain));
	}

}








class FacebookPage extends Widget
{

	

	





	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('codeline', 'href', '', __('Facebook Page URL', $this->_domain), sprintf(__('E.g. %s', $this->_domain), '<code>http://www.facebook.com/platform</code>'), array('on_sanitize' => function($option, $original_value, &$value) {
			$value = preg_replace('/\?[^\?]*$/', '', $value);
		}));
		$options->addOption('boolean', 'small_header', false, '', '', array('caption' => __('Use small header', $this->_domain)));
		$options->addOption('boolean', 'show_facepile', true, '', '', array('caption' => __('Show profile photos', $this->_domain)));
		$show_posts = $options->addOption('boolean', 'show_posts', false, '', '', array('caption' => __('Show posts from the Page\'s timeline', $this->_domain)));
		$options->addOption('number', 'height', 400, __('Height', $this->_domain), '', array('unit' => 'px', 'min' => 70, 'max' => 1000, 'owner' => $show_posts));
	}

	

	





	public function onOptionsCompatybility(array &$data, $version)
	{
		if (version_compare($version, '5.4.1') < 0) {
			if (isset($data['show_faces'])) {
				$data['show_facepile'] = $data['show_faces'];
			}
			if (isset($data['stream'])) {
				$data['show_posts'] = $data['stream'];
			}
		}
	}

	

	





	protected function onWidget(array $args, \Drone\HTML &$html)
	{
		$html = \Drone\HTML::div();
		$fb_page = $html->addNew('div')
			->class('fb-page')
			->data('href', $this->wo('href'))
			->data('small-header', \Drone\Func::boolToString($this->wo('small_header')))
			->data('show-facepile', \Drone\Func::boolToString($this->wo('show_facepile')))
			->data('show-posts', \Drone\Func::boolToString($this->wo('show_posts')));
		if ($this->wo_('height')->isVisible()) {
			$html->css('height', $this->wo('height'));
			$fb_page->data('height', $this->wo('height'));
		}
	}

	

	





	public function __construct()
	{
		parent::__construct(__('Facebook Page', Theme::getInstance()->domain), __('Configurable Facebook widget.', Theme::getInstance()->domain));
	}

}









class FacebookLikeBox extends FacebookPage {}