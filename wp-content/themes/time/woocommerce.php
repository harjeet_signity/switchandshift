<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.2.1
 */
?>

<?php get_header(); ?>

<div class="outer-container">

	<?php get_template_part('parts/nav-secondary', 'lower'); ?>

	<?php //Time::openContent(); ?>

		<section class="section">
			<?php 
			if ( is_singular( 'product' ) ) { 
				woocommerce_content();
			} 
			else {
				//For ANY product archive.
				//Product taxonomy, product search or /shop landing
				woocommerce_get_template( 'archive-product.php' );
			}

			//woocommerce_content();
			 ?>
		</section>

	<?php //Time::closeContent(); ?>

</div>

<?php get_footer(); ?>
