<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      2.0
 */

// -----------------------------------------------------------------------------

if (!defined('ABSPATH')) {
	exit;
}

// -----------------------------------------------------------------------------

function woocommerce_button_proceed_to_checkout()
{
	echo \Drone\HTML::a()
		->href(WC()->cart->get_checkout_url())
		->class('checkout-button button big')
		->add(
			\Drone\HTML::span()->add(__('Proceed to Checkout', 'woocommerce')),
			\Drone\HTML::i()->class('icon-right-bold color-grass')
		)->html();
}

// -----------------------------------------------------------------------------

function woocommerce_get_product_thumbnail($size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0)
{

	global $post, $product;

	// Figure
	$figure = \Drone\HTML::figure()
		->class('featured full-width');

	// Hyperlink
	$a = $figure->addNew('a')
		->attr(Time::getImageAttrs('a', array('hover' => Time::to('woocommerce/shop/image_hover'))))
		->href(get_permalink());

	// Image
	if (has_post_thumbnail()) {
		$a->add(get_the_post_thumbnail($post->ID, $size));
		if (Time::to('woocommerce/shop/image_hover') == 'image') {
			$attachment_ids = $product->get_gallery_attachment_ids();
			if (isset($attachment_ids[0])) {
				$a->add(wp_get_attachment_image($attachment_ids[0], $size));
			}
		}
	} elseif (woocommerce_placeholder_img_src()) {
		$a->add(woocommerce_placeholder_img($size));
	}

	return $figure->html();

}

// -----------------------------------------------------------------------------

function woocommerce_subcategory_thumbnail($category)
{

	// Figure
	$figure = \Drone\HTML::figure()
		->class('featured full-width');

	// Hyperlink
	$a = $figure->addNew('a')
		->attr(Time::getImageAttrs('a'))
		->href(get_term_link($category->slug, 'product_cat'));

	// Thumbnail
	$thumbnail_id   = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
	$thumbnail_size = apply_filters('single_product_small_thumbnail_size', 'shop_catalog');

	if ($thumbnail_id) {
		$a->add(wp_get_attachment_image($thumbnail_id, $thumbnail_size));
	} elseif (woocommerce_placeholder_img_src()) {
		$a->add(woocommerce_placeholder_img($thumbnail_size));
	}

	echo $figure->html();

}