<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */

// -----------------------------------------------------------------------------

namespace Time\Widgets\Widget;

// -----------------------------------------------------------------------------

if (!defined('ABSPATH')) {
	exit;
}

// -----------------------------------------------------------------------------

class SocialMedia extends \Drone\Widgets\Widget
{

	// -------------------------------------------------------------------------

	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', '', __('Title', 'time'));
		$options->addOption('memo', 'description', '', __('Description', 'time'));
		$options->addOption('collection', 'icons', array('icon' => '', 'title' => '', 'url' => 'http://'), __('Icons', 'time'), '', array('type' => 'social_media'));
		$options->addOption('select', 'gravity', 's', __('Tooltip position', 'time'), '', array('options' => array(
			'se' => __('Northwest', 'time'),
			's'  => __('North', 'time'),
			'sw' => __('Northeast', 'time'),
			'e'  => __('West', 'time'),
			'w'  => __('East', 'time'),
			'ne' => __('Southwest', 'time'),
			'n'  => __('South', 'time'),
			'nw' => __('Southeast', 'time')
		)));
		$options->addOption('boolean', 'native_colors', true, '', '', array('caption' => __('Native hover colors', 'time')));
		$options->addOption('boolean', 'new_window', false, '', '', array('caption' => __('Open links in new window', 'time')));
	}

	// -------------------------------------------------------------------------

	protected function onWidget(array $args, \Drone\HTML &$html)
	{
		if ($this->wo('description')) {
			$html->addNew('p')->add($this->wo_('description')->__());
		}
		$div = $html->addNew('div')->class('social-icons');
		$ul  = $div->addNew('ul')->class('alt');
		if ($this->wo('native_colors')) {
			$div->addClass('native-colors');
		}
		foreach ($this->wo('icons') as $icon) {
			if (!$icon['icon'] || !$icon['url']) {
				continue;
			}
			$li = $ul->addNew('li');
			$a = $li->addNew('a')->href($icon['url']);
			$a->addNew('i')->class('icon-'.$icon['icon']);
			if ($icon['title']) {
				$a->title = $icon['title'];
				$a->class = 'tipsy-tooltip';
				$a->data('tipsy-tooltip-gravity', $this->wo('gravity'));
			}
			if ($this->wo('new_window')) {
				$a->target = '_blank';
			}
		}
	}

	// -------------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct(__('Social media', 'time'), __('Social media icons.', 'time'));
	}

}

// -----------------------------------------------------------------------------

class SocialButtons extends \Drone\Widgets\Widget
{

	// -------------------------------------------------------------------------

	protected function onSetupOptions(\Drone\Options\Group\Widget $options)
	{
		$options->addOption('text', 'title', '', __('Title', 'time'));
		$options->addOption('memo', 'description', '', __('Description', 'time'));
		$options->addOption('collection', 'media', '', __('Media', 'time'), '', array('type' => 'select', 'trim_default' => false, 'options' => array(
			'facebook'   => __('Facebook', 'time'),
			'twitter'    => __('Twitter', 'time'),
			'googleplus' => __('Google+', 'time'),
			'linkedin'   => __('LinkedIn', 'time'),
			'pinterest'  => __('Pinterest', 'time')
		)));
	}

	// -------------------------------------------------------------------------

	protected function onWidget(array $args, \Drone\HTML &$html)
	{
		if (!is_singular()) {
			return;
		}
		if ($this->wo('description')) {
			$html->addNew('p')->add($this->wo_('description')->__());
		}
		$sb = \Time::getInstance()->shortcodeSocialButtons(array(
			'style' => 'big',
			'media' => implode(', ', $this->wo('media'))
		));
		$html->add($sb);
	}

	// -------------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct(__('Social buttons', 'time'), __('Social media buttons.', 'time'));
	}

}