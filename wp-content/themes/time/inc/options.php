<?php
/**
 * @package    WordPress
 * @subpackage Time
 * @since      1.0
 */

// -----------------------------------------------------------------------------

namespace Time\Options\Option;
use \Drone\HTML;

// -----------------------------------------------------------------------------

if (!defined('ABSPATH')) {
	exit;
}

// -----------------------------------------------------------------------------

class RetinaAttachment extends \Drone\Options\Option\Complex
{

	// -------------------------------------------------------------------------

	protected $image1x;
	protected $image2x;

	// -------------------------------------------------------------------------

	protected function _options()
	{
		return array(
			'image1x' => 'attachment',
			'image2x' => 'attachment'
		);
	}

	// -------------------------------------------------------------------------

	protected function _html()
	{
		$html = HTML::div()
			->class($this->getCSSClass(__CLASS__))
			->add($this->image1x->html());
		if (isset($this->image2x)) {
			$html->add(
				$this->image2x->html()
					->style('margin-top: 3px;')
					->add(' ', __('@2x', 'time'))
			);
		}
		return $html;
	}

	// -------------------------------------------------------------------------

	public function image()
	{
		if ($this->image1x->value === 0 || ($data1x = wp_get_attachment_image_src($this->image1x->value, '')) === false) {
			return;
		}
		$img = HTML::img()
			->src($data1x[0])
			->width($data1x[1])
			->height($data1x[2]);
		if (isset($this->image2x) && $this->image2x->value !== 0 && ($data2x = wp_get_attachment_image_src($this->image2x->value, '')) !== false) {
			$img->data('2x', $data2x[0]);
		}
		return $img;
	}

}

// -----------------------------------------------------------------------------

class Background extends \Drone\Options\Option\Background
{

	// -------------------------------------------------------------------------

	protected $image_ex;
	protected $stripes;

	// -------------------------------------------------------------------------

	protected function _options()
	{
		return parent::_options()+array(
			'image_ex' => 'retina_attachment',
			'stripes'  => 'boolean'
		);
	}

	// -------------------------------------------------------------------------

	protected function _html()
	{
		$html = parent::_html()
			->addClass($this->getCSSClass(__CLASS__));
		if (isset($this->image_ex)) {
			$html->delete(0);
			$html->insert($this->image_ex->html());
		}
		if (isset($this->stripes)) {
			$html->add('<br />', $this->stripes->html());
		}
		return $html;
	}

	// -------------------------------------------------------------------------

	public function __construct($name, $default, $properties = array())
	{
		$default['image'] = '';
		parent::__construct($name, $default, $properties);
		if (isset($this->stripes)) {
			$this->stripes->caption = __('Add stripes', 'time');
		}
	}

	// -------------------------------------------------------------------------

	public function css($selector = '')
	{
		if (isset($this->image_ex)) {
			$this->image->value = $this->image_ex->option('image1x')->uri();
		}
		$css = parent::css($selector);
		if (strpos($css, 'background-size:') === false && isset($this->image_ex) && ($size = $this->image_ex->option('image1x')->size()) !== false) {
			$css .= sprintf(' background-size: %dpx %dpx;', $size['width'], $size['height']);
		}
		return $css;
	}

	// -------------------------------------------------------------------------

	public function background()
	{
		$background = HTML::div()
			->style($this->css());
		if (isset($this->image_ex) && !is_null($image2x = $this->image_ex->option('image2x')) && $image2x->value > 0) {
			$background->data('bg-2x', $image2x->uri());
		}
		if (isset($this->stripes) && $this->stripes->value) {
			$background->class = 'stripes';
		}
		return $background;
	}

}

// -----------------------------------------------------------------------------

class Banner extends \Drone\Options\Option\Complex
{

	// -------------------------------------------------------------------------

	protected $type;
	protected $height;
	protected $image;
	protected $slider;
	protected $custom;

	public $editor  = false;

	// -------------------------------------------------------------------------

	protected function _options()
	{
		return array(
			'type'   => 'select',
			'height' => 'number',
			'image'  => 'attachment',
			'slider' => 'select',
			'custom' => $this->editor ? 'editor' : 'code'
		);
	}

	// -------------------------------------------------------------------------

	public function __construct($name, $default, $properties = array())
	{

		parent::__construct($name, $default, $properties);

		$type_options = array(
			''          => __('None', 'time'),
			'empty'     => __('Empty space', 'time'),
			'image'     => __('Image', 'time'),
			'thumbnail' => __('Featured image', 'time'),
			'slider'    => __('Slider', 'time'),
			'custom'    => __('Custom', 'time')
		);
		if (!isset($this->slider)) {
			unset($type_options['slider']);
		}
		$this->type->options = $type_options;

		$this->height->unit = 'px';
		$this->height->min = 0;
		$this->height->indent = true;
		$this->height->owner = $this->type;
		$this->height->owner_value = 'empty';

		$this->image->indent = true;
		$this->image->owner = $this->type;
		$this->image->owner_value = 'image';

		if (isset($this->slider)) {
			$this->slider->required = false;
			$this->slider->options = function($option) {
				$options = array();
				if (\Time::isPluginActive('layerslider')) {
					foreach (lsSliders(999) as $slider) {
						$options[$slider['id']] = $slider['name'];
					}
				}
				return apply_filters('time_sliders', $options);
			};
			$this->slider->indent = true;
			$this->slider->owner = $this->type;
			$this->slider->owner_value = 'slider';
		}

		$this->custom->indent = true;
		$this->custom->owner = $this->type;
		$this->custom->owner_value = 'custom';

	}

}

// -----------------------------------------------------------------------------

class CustomFont extends \Drone\Options\Option\Font
{

	// -------------------------------------------------------------------------

	protected $id;

	// -------------------------------------------------------------------------

	protected function _options()
	{
		return array('id' => 'text')+parent::_options();
	}

	// -------------------------------------------------------------------------

	protected function _html()
	{
		$html = parent::_html();
		$html->insert(array($this->id->html(), ' '));
		return $html;
	}

	// -------------------------------------------------------------------------

	public function __construct($name, $default, $properties = array())
	{
		parent::__construct($name, $default, $properties);
		$this->id->on_html = function($option, &$html) { $html->style('width: 140px;'); };
		$this->size->max = 1000;
	}

}

// -----------------------------------------------------------------------------

class Sidebar extends \Drone\Options\Option
{

	// -------------------------------------------------------------------------

	public $options = array();

	// -------------------------------------------------------------------------

	protected function _styles($instance_num)
	{
		static $outputted = false;
		if ($outputted) {
			return '';
		}
		$outputted = true;
		return
<<<'EOS'
			.time-option-sidebar {
				margin: -5px;
				overflow: hidden;
			}
			.time-option-sidebar > div {
				border: 1px solid #dfdfdf;
				background: #fbfbfb;
				cursor: move;
				float: left;
				margin: 5px;
				padding: 10px;
				width: 120px;
				height: 46px;
			}
			.time-option-sidebar > .time-option-sidebar-placeholder {
				border-style: dashed;
				background: none;
			}
			.time-option-sidebar > div > select {
				font-weight: normal;
				min-width: auto;
				width: 100%;
			}
EOS;
	}

	// -------------------------------------------------------------------------

	protected function _scripts($instance_num)
	{
		static $outputted = false;
		if ($outputted) {
			return '';
		}
		$outputted = true;
		return
<<<'EOS'
			jQuery(document).ready(function($) {
				var attach = function(e, options) {
					$('.time-option-sidebar:not(.drone-ready)', options).addClass('drone-ready').sortable({
						items:       '> div',
						placeholder: 'placeholder'
					});
				};
				$('body').on('drone_options_attach', attach); attach();
			});
EOS;
	}

	// -------------------------------------------------------------------------

	protected function _sanitize($value)
	{
		$value = (array)$value;
		$value = array_intersect($value, array_merge(array('#'), array_keys($this->options)));
		if (count($value) > 3) {
			$value = array_slice($value, -3);
		}
		if (count($value) < 3 || count(array_keys($value, '#')) != 1) {
			$value = $this->default;
		}
		return $value;
	}

	// -------------------------------------------------------------------------

	protected function _html()
	{
		$html = HTML::div()->class($this->getCSSClass(__CLASS__));
		foreach ($this->value as $value) {
			if ($value == '#') {
				$html->addNew('div')->add(
					__('Content', 'time'), HTML::makeInput('hidden', $this->input_name.'[]', '#')
				);
			} else {
				$html->addNew('div')->add(
					__('Sidebar', 'time'), '<br />', HTML::makeSelect($this->input_name.'[]', $value, $this->options)
				);
			}
		}
		return $html;
	}

}

// -----------------------------------------------------------------------------

class SocialMedia extends \Drone\Options\Option\Complex
{

	// -------------------------------------------------------------------------

	protected $icon;
	protected $title;
	protected $url;

	// -------------------------------------------------------------------------

	protected function _options()
	{
		return array(
			'icon'  => 'image_select',
			'title' => 'text',
			'url'   => 'codeline'
		);
	}

	// -------------------------------------------------------------------------

	protected function _html()
	{
		$html = HTML::div()->class($this->getCSSClass(__CLASS__));
		$html->add(
			$this->icon->html(),
			HTML::div()->style('margin: 0 0 4px 55px;')->add(
				$this->title->html()
			),
			$this->url->html()
		);
		return $html;
	}

	// -------------------------------------------------------------------------

	public function __construct($name, $default, $properties = array())
	{
		parent::__construct($name, $default, $properties);
		require get_template_directory().'/inc/social-media-option-options.php';
		$this->icon->required  = false;
		$this->icon->font_path = \Time::ICON_FONT_PATH;
		$this->icon->on_html   = function($option, &$html) { $html->style('float: left;'); };
	}

}