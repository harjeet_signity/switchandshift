<?php
/**
 * @package    WooCommerce/Templates
 * @subpackage Time
 * @since      2.0
 * @version    2.4.0
 */

// -----------------------------------------------------------------------------

if (!defined('ABSPATH')) {
	exit;
}

// -----------------------------------------------------------------------------

$tabs = apply_filters('woocommerce_product_tabs', array());

if (!empty($tabs)): ?>

	<div class="woocommerce-tabs tabs">
		<?php foreach ($tabs as $key => $tab): ?>
			<div id="<?php echo esc_attr($key); ?>" title="<?php echo apply_filters('woocommerce_product_'.$key.'_tab_title', $tab['title'], $key); ?>">
				<?php call_user_func($tab['callback'], $key, $tab); ?>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>