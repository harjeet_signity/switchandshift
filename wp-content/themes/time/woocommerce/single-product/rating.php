<?php
/**
 * @package    WooCommerce/Templates
 * @subpackage Time
 * @since      2.0
 * @version    2.3.2
 */

// -----------------------------------------------------------------------------

if (!defined('ABSPATH')) {
	exit;
}

// -----------------------------------------------------------------------------

global $product;

if (get_option('woocommerce_enable_review_rating') == 'no') {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

?>

<?php if ($rating_count > 0): ?>
	<div title="<?php printf(__('Rated %s out of 5', 'woocommerce'), $average); ?>" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		<?php echo Time::getInstance()->shortcodeRating(array('tag' => 'span', 'rate' => $average, 'max' => 5)); ?>
		<?php if (comments_open()): ?>
			<a href="#reviews" class="woocommerce-review-link" rel="nofollow">
				(<?php printf(_n('%s customer review', '%s customer reviews', $review_count, 'woocommerce'), '<span itemprop="reviewCount" class="count">'.$review_count.'</span>'); ?>)
			</a>
		<?php endif; ?>
	</div>
<?php endif; ?>