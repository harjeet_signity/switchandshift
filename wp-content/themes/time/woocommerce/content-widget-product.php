<?php
/**
 * @package    WooCommerce/Templates
 * @subpackage Time
 * @since      3.2.1
 */

// -----------------------------------------------------------------------------

if (!defined('ABSPATH')) {
	exit;
}

// -----------------------------------------------------------------------------

global $product;

?>

<li>
	<figure class="alignright fixed">
		<a href="<?php echo esc_url(get_permalink($product->id)); ?>" title="<?php echo esc_attr($product->get_title()); ?>" <?php Time::imageAttrs('a', array('border' => true, 'hover' => '', 'fanbcybox' => false)); ?>>
			<?php echo $product->get_image(); ?>
		</a>
	</figure>
	<h3>
		<a href="<?php echo esc_url(get_permalink($product->id)); ?>" title="<?php echo esc_attr($product->get_title()); ?>">
			<?php echo $product->get_title(); ?>
		</a>
	</h3>
	<?php if (!empty($show_rating) && ($average = $product->get_average_rating()) > 0): ?>
		<?php echo Time::getInstance()->shortcodeRating(array('tag' => 'div', 'rate' => $average, 'max' => 5)); ?>
	<?php endif; ?>
	<p><?php echo $product->get_price_html(); ?></p>
</li>