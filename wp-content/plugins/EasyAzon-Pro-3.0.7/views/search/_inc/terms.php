<p class="description" data-bind="visible: hasSearchTerms">
	<a href="#" data-bind="click: shortcodeSearch"><?php _e('Link to the search results page for '); ?>"<em data-bind="text: searchTerms"></em>"</a>
</p>